<?php

require_once('../../config.php');
require_once('lib.php');
require_once($CFG->dirroot.'/eduopen/lib.php');


if(isset($_REQUEST)){
	
	global $DB, $CFG;
	
	$mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : null;

	if($mode){
		switch($mode){
			
			case 'filter':
				
				$query = '';	
				$block_count = 0;
			
				echo '<script>$("#CourseArea > .loader").hide();</script>';
			
				$price = isset($_REQUEST['price']) ? $_REQUEST['price'] : null;	
			
                if($price){
					 
                    $price =  implode(" ",$price);
					if($price == 'Free'){			
						$query .= "AND ce.cost = 0 ";

					} else if($price == 'Paid') {
						$query .= "AND ce.cost > 0 ";
					} else {
						$query .= "AND ce.cost >= 0 ";

					}		 
                   
				}
				
				$acertificate = isset($_REQUEST['acertificate']) ? $_REQUEST['acertificate'] : null;
				if($acertificate){
					$acertificate = "'".implode("','", $acertificate)."'";
					$query .= "AND ce.certificate1 IN (".$acertificate.") ";
                }
				
				$vcertificate = isset($_REQUEST['vcertificate']) ? $_REQUEST['vcertificate'] : null;
				if($vcertificate){
					$vcertificate = "'".implode("','", $vcertificate)."'";
					$query .= "AND ce.certificate2 IN (".$vcertificate.") ";
                }
				
				/* $certificate = isset($_REQUEST['certificate']) ? $_REQUEST['certificate'] : null;
				if($certificate){
					$certificate = "'".implode("','", $certificate)."'";
					$query .= "AND ce.certificate IN (".$certificate.") ";
                } */
				
				$exam = isset($_REQUEST['exam']) ? $_REQUEST['exam'] : null;
			    if($exam){
					$exam = "'".implode("','", $exam)."'";
					$query .= "AND ce.certificate IN (".$exam.") ";
				}
				
				$category = isset($_REQUEST['category']) ? $_REQUEST['category'] : null; 
				if($category){		
					$category = implode(",", $category);
					$query .= "AND c.category IN (".$category.") ";
                }

				$language = isset($_REQUEST['language']) ? $_REQUEST['language'] : null;
				if($language){
					$language = "'".implode("','", $language)."'";
					$query .= "AND ce.language IN (".$language.") ";
				}
				
				$coursetype = isset($_REQUEST['coursetype']) ? $_REQUEST['coursetype'] : null;
				if($coursetype){
					$coursetype = "'".implode("','", $coursetype)."'";
					$query .= "AND ce.coursetype IN (".$coursetype.") ";
				}
				
				$courselevel = isset($_REQUEST['courselevel']) ? $_REQUEST['courselevel'] : null;
				if($courselevel){
					$courselevel = "'".implode("','", $courselevel)."'";
					$query .= "AND ce.courselevel IN (".$courselevel.") ";
				}
				
				$institution = isset($_REQUEST['institution']) ? $_REQUEST['institution'] : null;
				if($institution){
					$institution = "'".implode("','", $institution)."'";
					$query .= "AND ce.institution IN (".$institution.") ";

				}
			    
               	$specialization = isset($_REQUEST['specialization']) ? $_REQUEST['specialization'] : null;
				if($specialization){
					$specialization = implode("%' OR ce.specializations LIKE '%", $specialization);
					$query .= "AND ce.specializations LIKE '%$specialization%'";
				}
				$type = null;
				// TO DO
				$recognization = isset($_REQUEST['recognization']) ? $_REQUEST['recognization'] : null;
				if($recognization){
					$recognization = "'".implode("','", $recognization)."'";
                    $result = $DB->get_records_sql("SELECT name FROM {block_eduopen_master_special} where degree IN($recognization)");
                    foreach ($result as $key => $results) {
                        $degreename[] = $results->name;
                    }
                    $degreenamestring = implode("%' OR ce.specializations LIKE '% ", $degreename);
					$query .= "AND ce.specializations LIKE '%$degreenamestring%'";
					
				}
				
				$course_details_q = "SELECT c.id,c.category, ce.certificate,ce.certificate1,ce.certificate2,ce.cost, ce.formalcredit,
				ce.language,ce.coursetype,ce.courselevel,ce.institution,ce.specializations
				FROM {course} c 
				LEFT JOIN {course_extrasettings_general} ce ON c.id = ce.courseid  WHERE c.visible = 1 
				AND c.category != 0 $query  ORDER BY c.startdate";
				$course_details_rs = $DB->get_records_sql($course_details_q);
				
						if($course_details_rs){
							foreach($course_details_rs as $course_details){	
								$course_id = $course_details->id;
								$course_block = generate_course_block($course_id);
								if($course_block){
									$block_count++;
									echo $course_block;
									/* if( ( $block_count % 1 ) == 0 ){
										echo '</div>';
									}	
									if( ( $block_count % 1 ) == 0 ){//if two blocks are printed at a time then create a new row
										echo '<div class="row1">';
									} */
								}					
							}	
						}else{
							echo '<div class="row center">';
							echo '<div class="col-md-4 alert alert-danger">'.get_string('nocourse','theme_eduopen').'</div>';
							echo '</div>';
						}	
				break;
				
			case 'courseSearch':			
				$keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : null;	
					$data = array();
				if($keyword){
					
					$course_details_q = "SELECT id,fullname FROM {course} WHERE fullname Like '%$keyword%' AND visible = 1 AND category != 0";
					
					$course_details_rs = $DB->get_records_sql($course_details_q);
					if($course_details_rs){
						foreach($course_details_rs as $course_details){	
							$course_id = $course_details->id;
							$fullname = $course_details->fullname;
							$data[] = $fullname.'~'.$course_id;
							
						}	
						
						echo json_encode($data);
					}	
				}
				break;
			case 'watchlistadd':
			
				
					$course_id = isset($_REQUEST['courseid']) ? $_REQUEST['courseid'] : null;
				
					
					if($course_id){
						if(!in_watchlist($course_id)){	//prevent multiple entry for single course				
						
							$record = new stdClass();
							$record->user = $USER->id;
							$record->course = $course_id;
							
							$add_to_watch_list = $DB->insert_record('eduopen_watchlist',$record);
							if($add_to_watch_list){
								echo '<i class="fa fa-eye pull-right font15 mrg0T" data-placement="left" data-toggle="tooltip" data-original-title="'.get_string('watchlist_added','theme_eduopen').'"></i>';
							}
						}
					}
									
			
				break;
			case 'watchlistremove':			
				
					$course_id = isset($_REQUEST['courseid']) ? $_REQUEST['courseid'] : null;
					
					
					if($course_id){
					
						
						$remove_from_watch_list = $DB->delete_records('eduopen_watchlist', array('course'=>$course_id,'user'=>$USER->id));
						if($remove_from_watch_list){
							echo "success";
						}else{
							echo "failed";
						}
					}
									
			
				break;                                                                                                                        
            case 'background_image'://by naina on 05-mar-2015 code for background_image/banner  
		       $data=array();
		
				$bck = "SELECT id,name,banner FROM {block_eduopen_master_inst} ORDER BY RAND() LIMIT 1";
				
				$bck_rm = $DB->get_record_sql($bck);
				
				if($bck_rm){
					$inst= new stdClass();
					$inst->id=$bck_rm->id;
					$inst->ins_name = $bck_rm->name;
					$banner = banner_images($bck_rm);	
                    							
					if($banner){
						$banners=trim($banner,'"');
						$inst->bck_url = $banners;                                                                                                          
						echo json_encode($inst);
					}										
				}
		

		    break;  
            case 'spec_background_image'://by naina on 05-mar-2015 code for background_image/banner  
		       $data=array();
		
				$bck = "SELECT id,name,banner FROM {block_eduopen_master_inst} ORDER BY RAND() LIMIT 1";
				
				$bck_rm = $DB->get_record_sql($bck);
				
				if($bck_rm){
                 //$allspecial = $DB->get_records('course_extrasettings_general',array('institution'=>$bck_rm->name));
                   $allspecial = $DB->get_records_sql("SELECT specializations FROM {course_extrasettings_general} where institution = '$bck_rm->name'");
                 $specialcount = count($allspecial);
                 $spzli=Specialization;
                  /* foreach($allspecial as $speci){
                    $specialcont = "SELECT COUNT(id) AS userstotal FROM {block_eduopen_master_special} WHERE name ='$speci->specializations'";
                    $specialcount = $DB->get_record_sql($specialcont); */
                    
					$inst= new stdClass();
                    $inst->countid = $specialcount.' '.$spzli;
					$inst->id=$bck_rm->id;
					$inst->ins_name = $bck_rm->name;
					$banner = banner_images($bck_rm);	
                    							
					if($banner){
						$banners=trim($banner,'"');
						$inst->bck_url = $banners;                                                                                                          
						echo json_encode($inst);
					
                    }									
				}
			break;		
		    }
	    }
    }
