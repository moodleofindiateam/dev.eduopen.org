<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_eduopen', language 'en'
 *
 * @package   theme_eduopen
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


$string['deleted_msg'] = "Per eliminare il tuo profilo dal portale EduOpen, clicca sul bottone “Cancella il mio account”.
Tutti i tuoi dati personali (informazioni, immagini, file personali, iscrizioni ai corsi, attività, certificati e badge) verranno rimossi. I post nei forum non verranno cancellati con l’eliminazione dell’account e resteranno visibili nella piattaforma.
Potrai continuare a visualizzare i badge acquisiti sulla piattaforma Bestr qualora tu abbia creato uno specifico account sul portale.
Non potrai recuperare le informazioni relative all’account cancellato ma in ogni momento potrai creare un nuovo account su EduOpen e frequentare i nostri corsi.";

// Page before login Index for Demo.eduopen.org.
$string['loginhead_step0'] = "Attenzione";
$string['loginhead_step1'] = "Area <b>esclusivamente</b> dedicata alla <b>progettazione e sviluppo</b> dei corsi Eduopen";
$string['loginhead_step2'] = "Accesso per MANAGERS & INSTRUCTORS";
$string['loginhead_step4'] = "Per gli studenti:";
$string['loginhead_step5'] = "per iscriversi ai corsi visitare";
$string['loginhead_step5a'] = "https://learn.eduopen.org";
$string['loginhead_step6'] = "per informazioni sul progetto Eduopen visitare il portale ";
$string['loginhead_step6a'] = "www.eduopen.org";
