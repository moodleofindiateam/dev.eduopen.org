<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'theme_eduopen', language 'en'
 *
 * @package   theme_eduopen
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>eduopen</h2>
<p><img class=img-polaroid src="eduopen/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>About</h3>
<p>eduopen is a modified Moodle bootstrap theme which inherits styles and renderers from its parent theme.</p>
<h3>Parents</h3>
<p>This theme is based upon the Bootstrap theme, which was created for Moodle 2.5, with the help of:<br>
Stuart Lamour, Mark Aberdour, Paul Hibbitts, Mary Evans.</p>
<h3>Theme Credits</h3>
<p>Authors: Bas Brands, David Scotson, Mary Evans<br>
Contact: bas@sonsbeekmedia.nl<br>
Website: <a href="http://www.basbrands.nl">www.basbrands.nl</a>
</p>
<h3>Report a bug:</h3>
<p><a href="http://tracker.moodle.org">http://tracker.moodle.org</a></p>
<h3>More information</h3>
<p><a href="eduopen/README.txt">How to copy and customise this theme.</a></p>
</div></div>';

$string['configtitle'] = 'eduopen';
$string['eduopen'] = 'eduopen';
$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';

$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here if you want to add it to the header.<br>
If the height of your logo is more than 75px add the following CSS rule to the Custom CSS box below.<br>
a.logo {height: 100px;} or whatever height in pixels the logo is.';

$string['pluginname'] = 'eduopen';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['noaccount'] = 'Don\'t have an account yet?';
$string['createaccount'] = 'Create an account';
$string['signup'] = 'Register now';
$string['registered'] = 'Already Registered?';
$string['login'] = 'Login now.';
$string['inprogress'] = 'Single Course - Inprogress';
$string['past'] = 'Past Courses';
$string['watchlist'] = 'Watchlist';
$string['teaching'] = 'Teaching';
$string['notenrolled'] = 'It seems that you have not enrolled in any courses.';
$string['completed'] = 'Completed.';
$string['continuelearning'] = 'Continue Learning';
$string['continueteaching'] = 'Continue Teaching';
$string['view'] = 'View';
$string['courses'] = 'Courses';
$string['long'] = 'long';
$string['certificate'] = 'Certificate';
$string['specialization'] = 'Pathway';

$string['free'] = 'Free';
$string['paid'] = 'Paid';
// Course Browse Section
$string['browseby'] = 'Browse by:';
$string['filterby'] = 'Filter by:';
$string['browsebyattendence_fee'] = 'Attendance Fee';
$string['browsebyattendence_cert'] = 'Attendance Certificate';
$string['browsebyverified_cert'] = 'Verified Certificate';
$string['browsebyexam'] = 'Examination';
$string['browsebycategories'] = 'Categories';
$string['browsebylanguage'] = 'Language';
$string['browsebytype'] = 'Type';
$string['browsebylevel'] = 'Level';
$string['browsebyinst'] = 'Institutions';
$string['browsebypath'] = 'Pathway';
$string['browsebyrecog'] = 'Acknowledgement';


$string['yes'] = 'Yes';
$string['no'] = 'No';
/* $string['browsebycost'] = 'Browse by fee';
$string['browsebyexam'] = 'Browse by examination/credits';
$string['browsebycertificate'] = 'Browse by certificate/badge'; */
$string['browsebyinstitution'] = 'Browse by institution';
$string['websitenav'] = 'Website Navigation';
$string['nocourse'] = 'Sorry! no courses found with your search criteria.';
$string['nopathway'] = 'Sorry! no pathways found with your search criteria.';
$string['watchlist_added'] = 'Added to watchlist';
$string['watchlist_add'] = 'Add to watchlist';
$string['watchlist_remove'] = 'Remove from watchlist.';
$string['nocourse_watchlist'] = 'Your watchlist is empty.';
$string['nocourse_inprogress'] = 'You have no in-progress courses yet.';
$string['nocourse_past'] = 'You have not completed any course yet.';
$string['webnav'] = 'Website Navigation';
$string['editprofile'] = 'Edit Profile';
$string['pay_history'] = 'Payment History';
$string['notification'] = 'Notifications';
$string['close'] = 'Close';
$string['exam'] = 'Examination';
//haraprasad add lang file for eduopen folder
$string['meetourpartners'] = 'The Eduopen Institutions';
$string['partofthe'] = 'Part of the';
$string['aboutthecourse'] = 'About the Course';
$string['coursesyllabus'] = 'Course Activities';
$string['coursesyl'] = ' Click to view all activities';
$string['lessonname'] = 'Lectures';
$string['secsummary'] = 'Summary : ';
$string['recommendedbackground'] = 'Background and Requirements';
$string['courseformat'] = 'Course Format';
$string['faq'] = 'FAQ';
$string['sessions'] = 'Sessions';
$string['joinforfree'] = 'Join for free!';
$string['joinforpaid'] = 'Join for';
$string['joinforcurr'] = 'Currency';
$string['earnaverifiedcertificate'] = 'Earn a certificate';
$string['eligiblefor'] = 'Eligible for';
$string['courseataglance'] = 'Course at a Glance';
$string['english'] = 'English';
$string['instructors'] = 'Instructors';
$string['share'] = 'Share';
$string['relatedcourses'] = 'Related Courses';
$string['browsemorecourses'] = 'Browse more courses';

$string['WhatYoullLearn'] = 'Overview';
$string['certificate'] = 'Certificate';
$string['earnaspecializationcertificate'] = 'Earn a Pathway Certificate';
$string['toearn'] = 'To earn the Pathway Certificate, complete Signature Track for all 9 courses and the Capstone project.';
$string['whatissignaturetrack'] = 'What is Signature Track?';
$string['UpcomingSession'] = 'Upcoming Session';
$string['Duration'] = 'Duration';
$string['Estimated'] = 'Estimated';
$string['CourseDescription'] = 'Course Description';
//for notification.php
$string['msg-not'] ='Messages/Notifications';
$string['sino'] ='SI No.';
$string['coursename'] ='Course Name';
$string['msg'] ='Message';
$string['status'] ='Status';
$string['none'] ='None';
$string['accept'] ='Accepted';
$string['reject'] ='Rejected';

$string['English'] ='English';
$string['Italian'] ='Italian';
$string['Spanish'] ='Spanish';
$string['French'] ='French';
$string['German'] ='German';
$string['Others'] ='Others';
/*for home page
by Naina on 25/02/2015*/
$string['hmj']="Join a Qualified Academic Network";
$string['hmsearch']="Search your course...";
$string['hmjoin']="";
$string['hmcourse']="Learners";
$string['hmlearn']="";
$string['hmc']="Courses";
$string['hmp']=" Associated Institutions";
$string['hmhow']="Learn more about Eduopen";
$string['hmview']='View all courses';


$string['linkcolor'] = 'Link Color';
$string['linkcolordesc'] = 'Set the color of links in the theme, use html hex code.';

$string['linkhover'] = 'Link Hover Color';
$string['linkhoverdesc'] = 'Set the color of links (on hover) in the theme, use html hex code.';

$string['backcolor'] = 'Background Color';
$string['backcolordesc'] = 'Set the background color.';

$string['breadcrumbs'] = 'Breadcrumbs Color';
$string['breadcrumbsdesc'] = 'Set the breadcrumbs color.';
$string['maincolor'] = 'Main color';
$string['maincolordesc'] = 'Main color for blocks.';
$string['rendereroverlaycolour'] = 'Overlay colour';
$string['rendereroverlaycolourdesc'] = 'The colour of the overlay, when the "Tiles w/ overlay" renderer is selected.';

$string['rendereroverlayfontcolour'] = 'Overlay font colour';
$string['rendereroverlayfontcolourdesc'] = 'The colour of the font, when hovering over a coursebox with "Tiles w/ overlay" renderer enabled.';

$string['buttoncolour'] = 'Button colour';
$string['buttoncolourdesc'] = 'The colour of the main buttons used throughout the site.';

$string['buttonhovercolour'] = 'Button colour (When hovering)';
$string['buttonhovercolourdesc'] = 'The colour that the button changes to when hovering over the button.';
$string['coloursettings'] = 'Colors';
$string['coloursettingsheading'] = 'Modify the main colours used throughout the theme.';
$string['colourdesc'] = 'You can select the colours that you would like to use throughout the theme.';

$string['footer1header'] = 'Footer Section 1 Title';
$string['footer2header'] = 'Footer Section 2 Title';
$string['footer3header'] = 'Footer Section 3 Title';
$string['footer4header'] = 'Footer Section 4 Title';

$string['footer1desc'] = 'Add a title for footer section 1';
$string['footer2desc'] = 'Add a title for footer section 2';
$string['footer3desc'] = 'Add a title for footer section 3';
$string['footer4desc'] = 'Add a title for footer section 4';

$string['footer1content'] = 'Footer Section 1 Content';
$string['footer2content'] = 'Footer Section 2 Content';
$string['footer3content'] = 'Footer Section 3 Content';
$string['footer4content'] = 'Footer Section 4 Content';

$string['footer1contentdesc'] = 'Add content to footer section 1';
$string['footer2contentdesc'] = 'Add content to footer section 2';
$string['footer3contentdesc'] = 'Add content to footer section 3';
$string['footer4contentdesc'] = 'Add content to footer section 4';

$string['footersettings'] = 'Footer';
$string['footersettingsheading'] = 'Set the content that should appear in the footer.';
$string['footerdesc'] = 'Control the content that appears in the 4 footer sections of the page.';
$string['customcssjssettings'] = 'Custom CSS & JS';
$string['genericsettingsheading'] = 'Apply your own modifications';
$string['genericsettingsdescription'] = 'Here you can find various settings to add your own CSS and JavaScript code to the theme.';


$string['logobackcolor'] = ' Logo background color';
$string['logobackcolordesc'] = 'Set the logo background color.';

$string['homefontbackcolor'] = ' Homepage font background color';
$string['homefontbackcolordesc'] = 'Set the homepage font background color.';
$string['headersettings'] = 'Header';
$string['headersettingsheading'] = 'Customise the header of this theme.';
$string['headerdesc'] = 'Display alert boxes to warn users of actions, or provide notices to all users. Control the navbars that appear on the site.';
/*for general settiing*/
$string['generalsettings'] = 'General';
$string['generalsettingsheading'] = 'Customise the general related settings of this theme.';
$string['generaldesc'] = 'Display all the general related theme settings';

$string['toggleblock'] = 'Toggle block(Show/Hide)';
$string['toggleblockdesc'] = 'Show / Hide blocks';
$string['blockicons'] = 'Block Icons';
$string['blockiconsdesc'] = 'Set this to show block icons in the block header area.';
/* END*/
$string['sitetitle'] = 'Site title';
$string['sitetitledesc'] = 'Show / Hide site title';
$string['footerbgcolour'] = 'Footer background color';
$string['footerbgcolourdesc'] = 'Set the footer background color.';

$string['langua'] = 'Language';
$string['crstype'] = 'Type';
$string['crslevel'] = 'Level';
$string['teachingcourse'] = 'Teaching Courses';
$string['feature'] = 'Featured Courses';
$string['attendancef'] = 'Attendance';
$string['certificatecrs'] = 'Certificate';
$string['examcrs'] = 'Examination';
$string['instcrs'] = 'Instructors';
$string['featurespl'] = 'Featured Pathways';
$string['splz'] = 'Pathways';
$string['creditpoint'] = 'Credits';
$string['mastersplz'] = 'Master your knowledge';
$string['followone'] = 'follow one of our ';
$string['spzin'] = 'Pathway in ';
$string['spzview'] = 'View more Pathways';
$string['browsebyspecial'] = 'Browse by Pathways';
$string['coursest'] = 'Courses';
$string['contactad'] = 'Contact us';
$string['syllabus'] = 'Syllabus';
$string['enrollcs'] = 'Enroll to the course!';
$string['Applytoacquire'] = 'Apply to acquire formal credits';
$string['earnCertificate'] = 'Complete the course and earn a Certificate'; 
$string['examrls'] = 'Exam rules and dates';
$string['examsettings'] = 'Exam page';
$string['examdesc'] = 'Set the  text and image in exam page';
$string['examsettingsheading'] = 'Customise the exam page';
$string['headertext1'] = 'Exam page header text top1';
$string['headertext1desc'] = 'you can add header text top1 of exam page';
$string['headertext2'] = 'Exam page header text top2';
$string['headertext2desc'] = 'you can add header text top2 of exam page';
$string['headertext3'] = 'Exam page header text top3';
$string['headertext3desc'] = 'you can add header text top3 of exam page';
$string['headertext4'] = 'Exam page header text top4';
$string['headertext4desc'] = 'you can add header text top4 of exam page';
$string['text1'] = 'Exam page top text1 ';
$string['text1desc'] = 'you can add text1 of exam page';
$string['text2'] = 'Exam page top text2 ';
$string['text2desc'] = 'you can add text2 of exam page';
$string['text3'] = 'Exam page top text3 ';
$string['text3desc'] = 'you can add text3 of exam page';
$string['text4'] = 'Exam page top text4 ';
$string['text4desc'] = 'you can add text4 of exam page';
$string['image1'] = 'Image left';
$string['image1desc'] = 'you can add image left side of exam page';
$string['image2'] = 'Image right';
$string['image2desc'] = 'you can add image right side of exam page';
 $string['certificatesettings'] = 'Certificate page';
$string['certificatedesc'] = 'Set the  text and image in certificate page';
$string['certificatesettingsheading'] = 'Customise the certificate page';
$string['headertext5'] = 'Certificate page header text top1';
$string['headertext5desc'] = 'you can add header text top1 of certificate page';
$string['headertext6'] = 'Certificate page header text top2';
$string['headertext6desc'] = 'you can add header text top2 of certificate page';
$string['headertext7'] = 'Certificate page header text top3';
$string['headertext7desc'] = 'you can add header text top3 of certificate page';
$string['headertext8'] = 'Certificate page header text top4';
$string['headertext8desc'] = 'you can add header text top4 of certificate page';
$string['text5'] = 'Certificate page top text1 ';
$string['text5desc'] = 'you can add text1 of certificate page';
$string['text6'] = 'Certificate page top text2 ';
$string['text6desc'] = 'you can add text2 of certificate page';
$string['text7'] = 'Certificate page top text3 ';
$string['text7desc'] = 'you can add text3 of certificate page';
$string['text8'] = 'Certificate page top text4 ';
$string['text8desc'] = 'you can add text4 of certificate page';
$string['image3'] = 'Image left';
$string['image3desc'] = 'you can add image left side of certificate page';
$string['image4'] = 'Image right';
$string['image4desc'] = 'you can add image right side of certificate page';
 $string['certificatesettings'] = 'Certificate page';
$string['certificatedesc'] = 'Set the  text and image in certificate page';
$string['certificatesettingsheading'] = 'Customise the certificate page';
$string['headertext5'] = 'Certificate page header text top1';
$string['headertext5desc'] = 'you can add header text top1 of certificate page';
$string['headertext6'] = 'Certificate page header text top2';
$string['headertext6desc'] = 'you can add header text top2 of certificate page';
$string['headertext7'] = 'Certificate page header text top3';
$string['headertext7desc'] = 'you can add header text top3 of certificate page';
$string['headertext8'] = 'Certificate page header text top4';
$string['headertext8desc'] = 'you can add header text top4 of certificate page';
$string['text5'] = 'Certificate page top text1 ';
$string['text5desc'] = 'you can add text1 of certificate page';
$string['text6'] = 'Certificate page top text2 ';
$string['text6desc'] = 'you can add text2 of certificate page';
$string['text7'] = 'Certificate page top text3 ';
$string['text7desc'] = 'you can add text3 of certificate page';
$string['text8'] = 'Certificate page top text4 ';
$string['text8desc'] = 'you can add text4 of certificate page';
$string['image3'] = 'Image left';
$string['image3desc'] = 'you can add image left side of certificate page';
$string['image4'] = 'Image right';
$string['image4desc'] = 'you can add image right side of certificate page';
$string['aboutussettings'] = 'About us page';
$string['aboutusdesc'] = 'Set the  text and image in about us page';
$string['aboutussettingsheading'] = 'Customise the about us page';
$string['headertext9'] = 'About us page header text top1';
$string['headertext9desc'] = 'you can add header text top1 of  about us page';
$string['headertext10'] = 'About us page header text top2';
$string['headertext10desc'] = 'you can add header text top2 of  about us page';
$string['headertext11'] = 'About us page header text top3';
$string['headertext11desc'] = 'you can add header text top3 of  about us page';
$string['headertext12'] = 'About us page header text top4';
$string['headertext12desc'] = 'you can add header text top4 of  about us page';
$string['headertext13'] = 'About us page header text top5';
$string['headertext13desc'] = 'you can add header text top5 of  about us page';
$string['headertext14'] = 'About us page header text top6';
$string['headertext14desc'] = 'you can add header text top6 of  about us page';
$string['headertext15'] = 'About us page header text top7';
$string['headertext15desc'] = 'you can add header text top7 of  about us page';
$string['headertext16'] = 'About us page header text top8';
$string['headertext16desc'] = 'you can add header text top8 of  about us page';
$string['headertext17'] = 'About us page header text top9';
$string['headertext17desc'] = 'you can add header text top9 of  about us page';
$string['headertext18'] = 'About us page header text top10';
$string['headertext18desc'] = 'you can add header text top10 of  about us page';
$string['text9'] = 'About us page top text1 ';
$string['text9desc'] = 'you can add text1 of  about us page';
$string['text10'] = 'About us page top text2 ';
$string['text10desc'] = 'you can add text2 of  about us page';
$string['text11'] = 'About us page top text3 ';
$string['text11desc'] = 'you can add text3 of  about us page';
$string['text12'] = 'About us page top text4 ';
$string['text12desc'] = 'you can add text4 of  about us page';
$string['text13'] = 'About us page top text5 ';
$string['text13desc'] = 'you can add text5 of  about us page';
$string['text14'] = 'About us page top text6 ';
$string['text14desc'] = 'you can add text6 of  about us page';
$string['text15'] = 'About us page top text7';
$string['text15desc'] = 'you can add text7 of  about us page';
$string['text16'] = 'About us page top text8 ';
$string['text16desc'] = 'you can add text8 of  about us page';
$string['text17'] = 'About us page top text9 ';
$string['text17desc'] = 'you can add text9 of  about us page';
$string['text18'] = 'About us page top text 10';
$string['text18desc'] = 'you can add text10 of  about us page';
$string['image5'] = 'Image left';
$string['image5desc'] = 'you can add image first left side of  about us page';
$string['image6'] = 'Image right';
$string['image6desc'] = 'you can add image first right side of  about us page';
$string['image7'] = 'Image left';
$string['image7desc'] = 'you can add image second left side of  about us page';
$string['image8'] = 'Image right';
$string['image8desc'] = 'you can add image  second  right side of  about us page';
$string['image9'] = 'Image left';
$string['image9desc'] = 'you can add image third left side of  about us page';
$string['image10'] = 'Image right';
$string['image10desc'] = 'you can add image  third right side of  about us page';
$string['image11'] = 'Image left';
$string['image11desc'] = 'you can add image  fourth left side of  about us page';
$string['region-center-post'] = 'Center Top';

$string['ongoing_join'] = 'On going. Join us!';
$string['join_us'] = 'Join us!';
$string['continue'] = 'Continue!';

// added by shiuli on 15th June
$string['title1'] = 'Box1 Title';
$string['title2'] = 'Box2 Title';
$string['text1'] = 'Box1 Text';
$string['text2'] = 'Box2 Text';

$string['no_pathcourse'] = 'There are no courses to show under this pathway.';

// added on 23rd june
$string['cs_start_cs'] = 'Coming soon';
$string['cs_start_c'] = 'Current';
$string['cs_start_u'] = 'Upcoming';

$string['nodeg'] = 'No Official Acknowledgement';
$string['mdeg'] = 'Master';
$string['adeg'] = 'Course Degree';
$string['odeg'] = 'Credits';

// added on 30th June
$string['stars_on'] = 'Started On - ';
$string['effort'] = 'Effort';
$string['atncert'] = 'Attendance and Certificates';

$string['cstarts'] = 'Starts';
$string['cstarted'] = 'Started';

$string['fee-free'] = 'FREE!';

$string['verified'] = 'Verified';
$string['vernexam'] = 'Credits';
$string['categ'] = 'Category';

// ===========================haraprasad add new string for certificate page============================================

$string['OurApp'] = 'Our Approach';
$string['OurAppdesc'] = 'Thats why we designed our platform based on proven teaching methods verified by top researchers.  Here are 4 key ideas that were influential in shaping our vision:';
$string['Effectonline'] = 'Effectiveness of online learning';
$string['Effectonlinedesc'] = 'Online learning plays a significant role in a lifelong education. In fact, recent reports and experience found that"classes with online learning (whether taught completely online or blended)
on average produce stronger student learning outcomes than do classes with solely face-to-face instruction.';
$string['Masterylearning'] = 'Mastery learning';
$string['Masterylearningdesc'] = 'Based on an approach developed by top research this kind of educational approach helps learners fully understand a topic before moving onto a more advanced topic. We typically give immediate feedback on a concept a learner did not understand.';
$string['Peerassessments'] = 'Peer assessments';
$string['Peerassessmentsdesc'] = 'In many courses, the most meaningful assignments cannot be easily graded by a computer. Thats why we use assessments, where instructors can evaluate and provide feedback on students work';
// ===========================haraprasad add new string for exam page============================================

$string['earninstitutional'] = 'Earn Institutional Credits (CFU or ECTS)';
$string['earninstitutionaldesc'] = 'We provide open access to academic courses issued by Italian universities and their partners';
$string['howworks'] = 'How It Works';
$string['howworksdesc'] = 'Eduopen is an education platform that partners with Italian  universities and their partners,to offer open courses. Everyone can take courses for free or can apply (for a small fee) to obtain a certificate and a badge or formal academic credits (CFU).';
$string['discovercourseandsignup'] = 'Discover a course and sign up today';
$string['discovercourseandsignupdesc'] = 'Choose from the set of courses in any knowledge area, browse them by language and by type. You can even choose to take a Specialization, a set of course in a specific area with related topics and goals. Courses are open to anyone, and learning is free.';
$string['learnonyourownschedule'] = 'Learn on your own schedule';
$string['learnonyourownscheduledesc'] = 'Watch short video lectures, take interactive quizzes, complete online assessments, and connect with fellow learners and instructors. If you wish to apply for a certificate and a badge you should also take a final test. If you wish to take formal credits you should apply to the University issuing the course.';
// ===========================haraprasad add new string for about us page============================================

$string['ourmissions'] = 'Our Missions';
$string['ourmissionsdesc'] = 'We provide open access to academic courses issued by Italian universities and their partners.';
$string['achieveyourgoals'] = 'Achieve your goals';
$string['achieveyourgoalsdesc'] = 'Finish your course and receive formal recognition for your accomplishment with an optional Certificate and/or achieving formal academic credits.';
$string['eduopenapproach'] = 'The Eduopen approach';
$string['eduopenapproachdesc'] = 'Its simple. We want to help you learn better and faster. Thats why we designed our platform based on proven teaching methods verified by top researchers. Here are 4 key ideas that were influential in shaping our vision:';

// ===========================haraprasad add new string for footersetting page============================================
$string['edp'] = 'EDUOPEN';
$string['edpdesc'] = 'EDUOPEN is a network of selected Italian and European Universities and it provides universal access to the best education and offers courses for anyone to take, for free. ';
$string['sterring'] = 'STEERING';
$string['sterringdesc'] = 'The Steering Group is composed by the Universities of Modena and Reggio Emilia, Parma, Ferrara, Foggia, Genova, Salento, Bari, Politecnico di Bari, Piemonte Orientale.';
$string['partners'] = 'PARTNERS';
$string['partnersdesc'] = 'EDUOPEN is leaded by EDUNOVA (Centro Interuniversitario per lInnovazione Didattica) in collaboration with CINECA';
$string['open'] = 'OPEN';
$string['opendesc'] = 'EDUOPEN is open to other Universities in Italy, in Europe and WorldWide.Please contact us: info@eduopen.it
Tel: ++39 0522 522 521';
$string['footernotetop'] = 'THIS IS A TEST ONLY PLATFORM!';
$string['footernotemiddle'] = 'All the information are not true and are fantasy generated';
$string['footernotelast'] = '(c) 2015 Eduopen All rights reserved - Version 0.1b (Test)';


//added by Shiuli on 25th Sep for frontpage LEGENDs.
$string['legendverify'] = 'Verified';
$string['legendexam'] = 'Examination';
$string['legendmaster'] = 'Master Degree';
$string['legendadvance'] = 'Advanced Course Degree';

//added by Shiuli on 25th Sep for frontpage Top Headings.
$string['featured_courses'] = 'Featured Courses';
$string['featured_pathways'] = 'Featured Pathways';
$string['most_popular_courses'] = 'Most Popular Courses';

$string['legendofficial'] = 'Official Credits (CFU/ECTS)';

//added by Shiuli on 25th Sep for frontpage Top Headings.
$string['featured_courses'] = 'Featured Courses';
$string['featured_pathways'] = 'Featured Pathways';
$string['most_popular_courses'] = 'Most Popular Courses';

// added by Shiuli on 6th oct.
$string['browseallcourses'] = 'Browse all Courses';
$string['browseallpathways'] = 'Browse all Pathways';
$string['cost'] = 'Cost';

//added by Shiuli on 7th Oct.
$string['filterbycourses'] = 'Courses';
$string['filterbypathways'] = 'Pathways';
$string['select_inst'] = 'Select Institution';


$string['fcourse'] = 'Filtered Courses';
$string['fpathway'] = 'Filtered Pathways';

$string['cdesclocal'] = 'Description in Local:';
$string['cdesceng'] = 'Description in English:';


/* Added by Shiuli on 9th oct for Custom Menu*/
$string['mydashboard'] = 'My Dashboard';
$string['myadmin'] = 'My Admin';
$string['manageinstitution'] = 'Manage Institutions';
$string['addinstitution'] = 'Add Institutions';
$string['viewinstitution'] = 'View Institutions';
$string['managepathways'] = 'Manage Pathways';
$string['addfaqs'] = 'Add Faqs';
$string['managefaqs'] = 'Manage Faqs';
$string['viewfaqs'] = 'View Faqs';
$string['browselistofusers'] = 'Browse list of Users';
$string['managecoursescategory'] = 'Manage courses and catagories';
$string['myprofiles'] = 'My profiles';
$string['mypreference'] = 'My Preference';
$string['messages'] = 'Messages';
$string['myprivatefiles'] = 'My private files';
$string['mybadges'] = 'My badges';
$string['logout'] = 'Log Out';

$string['dltmyprofiles'] = 'Delete My Account';
//for login page
$string['eduopenmember'] = 'Eduopen Member';
$string['noteduopenmember'] = 'Not yet an Eduopen Member';
$string['registerasnewlearner'] = 'Register as NEW';
$string['universitymember'] = 'Affiliated University Members';
$string['universitymember_desc'] = '(select this option if your University is affiliated to IDEM-GARR or EduGain)';
$string['notuniversitymember'] = 'Eduopen Account';
$string['notuniversitymember_desc'] = '(select this option if you are not a University Member or Student or your University is NOT affiliated to IDEM-GARR or EduGain)';
$string['logindesc'] = 'Insert your username and password to <b>LOG IN</b>';
$string['eduopendeclaration'] = 'By signing up to create an account i accept Eduopen'."'".'s <a href="http://eduopen.org/privacy-policies-and-terms-of-use.html" target="_blank" class="decltext">Terms of Use</a> and <a href="http://eduopen.org/user-policies.html" target="_blank" class="decltext">User Policies</a>, including the <a href="http://eduopen.org/privacy-statement.html" target="_blank" class="decltext">Processing of your personal data (Privacy Policies)</a>';

/*for policy page setting*/
$string['policy_en_infonotice_privacy_setting'] = 'Information Notice on Privacy Setting(en)';
$string['policy_en_infonotice_settingsheading'] = 'Information Notice on Privacy';
$string['policy_en_infonoticedesc'] = 'Gives the setting to set the information notice on privacy policy';

$string['policy_it_infonotice_privacy_setting'] = 'Information Notice on Privacy Setting(it)';
$string['policy_it_infonotice_settingsheading'] = 'Information Notice on Privacy';
$string['policy_it_infonoticedesc'] = 'Gives the setting to set the information notice on privacy policy';

$string['policy_en_condition_elearn_setting'] = 'Condition For Using Elearning platform Setting(en)';
$string['policy_en_condition_settingsheading'] = 'Condition For Using Elearning platform';
$string['policy_en_conditiondesc'] = 'Gives the setting to set the Condition For Using Elearning platform policy';

$string['policy_it_condition_elearn_setting'] = 'Condition For Using Elearning platform Setting(it)';
$string['policy_it_condition_settingsheading'] = 'Condition For Using Elearning platform';
$string['policy_it_conditiondesc'] = 'Gives the setting to set the Condition For Using Elearning platform policy';


$string['policy_en_infonotice_header'] = 'Title';
$string['policy_it_infonotice_header'] = 'Title';
$string['policy_infonotice_desc'] = 'Add a title for information notice';
$string['policy_en_infonotice_content1title'] = 'Content1 Title';
$string['policy_it_infonotice_content1title'] = 'Content1 Title';
$string['policy_infonotice_content1title_desc'] = 'Add content title to the information notice on privacy';
$string['policy_en_infonotice_content1'] = 'Content1';
$string['policy_it_infonotice_content1'] = 'Content1';
$string['policy_infonotice_content1_desc'] = 'Add content to the information notice on privacy';
$string['policy_en_infonotice_content2title'] = 'Content2 Title';
$string['policy_it_infonotice_content2title'] = 'Content2 Title';
$string['policy_infonotice_content2title_desc'] = 'Add content title to the information notice on privacy';
$string['policy_en_infonotice_content2'] = 'Content2';
$string['policy_it_infonotice_content2'] = 'Content2';
$string['policy_infonotice_content2_desc'] = 'Add content to the information notice on privacy';

$string['policy_en_condition_header'] = 'Title';
$string['policy_it_condition_header'] = 'Title';
$string['policy_condition_desc'] = 'Add a title for condition of elearning';
$string['policy_en_condition_content1title'] = 'Content1 Title';
$string['policy_it_condition_content1title'] = 'Content1 Title';
$string['policy_condition_content1title_desc'] = 'Add content title to the condition of elearning';
$string['policy_en_condition_content1'] = 'Content1';
$string['policy_it_condition_content1'] = 'Content1';
$string['policy_condition_content1_desc'] = 'Add content to the condition of elearning';

$string['policy_en_condition_content2title'] = 'Content2 Title';
$string['policy_it_condition_content2title'] = 'Content2 Title';
$string['policy_condition_content2title_desc'] = 'Add content title to the condition of elearning';
$string['policy_en_condition_content2'] = 'Content2';
$string['policy_it_condition_content2'] = 'Content2';
$string['policy_condition_content2_desc'] = 'Add content to the condition of elearning';

// Added by Shiuli on 19th Nov.
$string['noneditteacher'] = 'co-Instructors / Tutors';

/*Added by Shiuli on 19th Nov. for dashboard page */
$string['dash-tab1'] = 'Courses I am following';
$string['dash-tab2'] = 'Certificates of achievements for courses';
$string['dash-tab3'] = 'Certificates of achievements for paths';
$string['dash-tab4'] = 'Average score';
$string['dash-tab6'] = 'Earned Badges';

/*added on 5th jan for dashboard changes*/

$string['mycertificates'] = 'My Certificates';
$string['mydiaries'] = 'My Diaries';
$string['myprivatefiles'] = 'My Private Files';
$string['myinprogresscourses'] = 'My Inprogress Courses';
$string['mycompletedcourses'] = 'My Completed Courses';
$string['myteachingcourses'] = 'My Teaching Courses';
$string['myallcertificates'] = 'My All Certificates';
$string['myalldiaries'] = 'My All Diaries';
$string['myallbadges'] = 'Earned Badges';

/* Added by shiuli on 13th feb */
$string['myanalytics'] = 'Myanalytics';
$string['myfav'] = 'My Favorite';
$string['sitelevel'] = 'Site Level';
$string['siteover'] = 'Site Overview';
$string['crslevel'] = 'Course Level';
$string['crsover'] = 'Course Overview';
$string['crslevelreport'] = 'Course Level Report';
$string['userlevel'] = 'User Level';
$string['userlevelreport'] = 'User Level Report';
$string['usercompletion'] = 'User Completion';

// Added by Shiuli on 26th feb for eduopen new UI.
$string['brwscat'] = 'Browse Catalog';
$string['catalogsearch'] = 'CATALOG';
$string['categories'] = 'CATEGORIES';
$string['catalogheading'] = 'Catalog';
$string['advancedfil'] = 'ADVANCED FILTER';
$string['path'] = 'Pathway: ';

$string['nocourses'] = 'There are no Courses..';
$string['nopathways'] = 'There are no Pathways..';
$string['norelatedcrs'] = 'No related course found';
$string['nocoursesunderinst'] = 'No courses found under this institution';
$string['nopathwaysunderinst'] = 'No pathways found under this institution';
$string['noteachingcrs'] = 'There are no teaching courses';

$string['instlist'] = 'Network of Institutions';
$string['instlist_it'] = 'Rete di Istituzioni';
$string['searchincatalog'] = 'Search in Catalog';
$string['norecfound'] = 'No Records found';
$string['categorydirectlink'] = 'ALL';

$string['topheader_phone'] = '++39 0522 522 521';
$string['topheader_mail'] = 'info@eduopen.org';
$string['topheader_contact'] = 'SUPPORT PORTAL';
$string['loginhead'] = "LOGIN";

$string['report_certificates_noreports'] = 'You do not have any certificates issued to you.';


// Final footer.
$string['foot_policies'] = 'Policies';
$string['foot_policies_content1'] = 'Terms of use and Privacy Policy';
$string['foot_policies_content2'] = 'User Infornation statement (EN)';
$string['foot_policies_content3'] = 'Informazioni per gli utenti (IT)';
$string['foot_policies_content4'] = 'Privacy Statement (EN)';
$string['foot_policies_content5'] = 'Trattamento dati personali (IT)';

$string['foot_partners'] = 'Partners';
$string['foot_partners_content1'] = 'LMS of India';
$string['foot_partners_content2'] = 'CINECA';
$string['foot_partners_content3'] = 'GARR';
$string['foot_partners_content4'] = 'Blackboard';

$string['foot_acknowledgements'] = 'Acknowledgements';
$string['foot_acknowledgements_content1'] = 'The Eduopen Project has been financially supported by MIUR, Italian Ministery for Education, University and Research.';
$string['foot_acknowledgements_content1_it'] = "Il Progetto Eduopen è stato sostenuto finanziariamente dal MIUR , Ministero italiano dell'Istruzione , dell'Università e della Ricerca";

$string['foot_contactus'] = 'Contact Us';
$string['foot_contactus_it'] = 'Contattaci';
$string['foot_contactus_content1'] = 'Viale Antonio Allegri 13, 42121 Reggio Emilia (Italy)';
$string['foot_contactus_content2'] = 'info@eduopen.org';
$string['foot_contactus_content2a'] = 'SUPPORT PORTAL';
$string['foot_contactus_content3'] = '++39 0522 522 521';
$string['foot_contactus_content4'] = 'www.eduopen.org';

$string['footer_content1'] = 'Creative Commons License';
$string['footer_content2'] = 'All contents are licenced under a ';
$string['footer_content2a'] = 'Creative Commons';
$string['footer_content3'] = 'Attribution-NonCommercial-ShareAlike 4.0 International License.';
$string['footer_content4'] = "Eduopen LMS has been developed by <a class='lmshref' href='http://lmsofindia.com'>LMS of India</a> & Centro Interateneo Edunova";
$string['footer_content5'] = 'Eduopen LMS (c) Ver. 1.0.2 - May 13th, 2016 - All rights are reserved';

$string['logintable_heading'] = 'EduOpen is affiliated to the Identity Federations IDEM-GARR and EduGain. If your University is affiliated to IDEM-GARR or EduGain you can use your istitution credentials (both Students and Members) to login. Otherwise you can create a FREE login on EduOpen.';
$string['comeback'] = 'Please come back later.';
$string['learningactivitystart'] = 'Learning activities will start on';
$string['youareenrolled'] = 'You are now enrolled in';

// New Ui for pathway details page
$string['pathenrol'] = 'Enrol to Pathway';
$string['path_continue'] = 'Continue';

$string['enrolme_course'] = 'Enrol Me';
$string['enrolme_now'] = 'Enrol Me Now';
$string['enrolled_continue'] = 'Continue';
$string['Starts'] = 'Starts';


$string['Pathway'] = 'Pathway';
$string['mypathwaycourses'] = 'My Courses under each Pathway';
$string['nocourse_underpath'] = 'You have no courses under Pathway';
$string['mypathwaycourses_inprogress'] = 'Inprogress Courses';
$string['mypathway_noinprogress'] = 'You have no Inprogress Courses';
$string['mypathwaycourses_completed'] = 'Completed Courses';
$string['mypathway_nocompleted'] = 'You have not comleted any course yet';
$string['mypathwaycourses_nocompleted'] = 'May you have not completed any course under this pathway';

$string['inpro'] = 'Single Course - Inprogress';
$string['no_pathcourse_seq'] = 'There are no Courses under this Pathway';

// Login Page strings via Script.
$string['LgoinEnterOrgNameEN'] = "Enter your organization's name";
$string['LgoinEnterOrgNameIT'] = "Inserisci il nome della tua Università";

$string['LgoinSelOrgListEN'] = "Select your organization from the list";
$string['LgoinSelOrgListIT'] = "Seleziona la tua organizzazione dalla lista";

$string['LgoinSuggestedSelecEN'] = "Use a suggested selection";
$string['LgoinSuggestedSelecIT'] = "Università suggerita";

$string['LoginshowListEN'] = "Or Click here to select your organization from the list";
$string['LoginshowListIT'] = "O clicca qui per selezionare da un elenco";

$string['LoginshowsearchEN'] = "Or Click here to enter your organization name";
$string['LoginshowsearchIT'] = "Oppure clicca qui per inserire il nome dell'organizzazione";

$string['loginpagebtnEN'] = "LOGIN";
$string['loginpagebtnIT'] = "ACCESSO";

//Dashboard-28th April.
$string['CCompleted'] = "Completed";

$string['news_letter'] = "News Letter";
$string['add_domain'] = "Add Domain";
$string['add_news_letter'] = "Add News Letter";
$string['view_domain'] = "View Domain";
$string['mail_lists'] = "Mail Lists";

$string['deleteaccount'] = 'Delete Account';
$string['deleted_msg'] = "If you want to delete your EduOpen account, click on “Delete my account”.
All your personal data (information, profile image, personal files, enrollments, activities, certificates and badges) will be erased. The posts in the forums will not be deleted and will remain available to other platform users.
You can explore your earned badges on Bestr platform if you created an account on that portal.
You can't restore information regarding your deleted account but at any time you could create a new EduOpen account and attend our courses.";
$string['mybadgesbestrhead'] = "My Eduopen Badges from BESTR";
$string['mybadgesotherbackpackhead']  = "My badges from other backpack";
$string['unauthorizeaccess'] = 'Unauthorize Access';

$string['selfpaced'] = 'Self Paced';

$string['instructors'] = 'Instructor(s)';
$string['tutors'] = 'Tutors/Co-Instructors';

// Page before login Index for Demo.eduopen.org.
$string['loginhead_step0'] = "Caution";
$string['loginhead_step1'] = "Area <b> only </b> dedicated to the <b> design and development </b> of Eduopen courses";
$string['loginhead_step2'] = "Access for MANAGERS & INSTRUCTORS";
$string['loginhead_step4'] = "For students:";
$string['loginhead_step5'] = "To register for classes visit";
$string['loginhead_step5a'] = "https://learn.eduopen.org";
$string['loginhead_step6'] = "For information about the project visit the Portal Eduopen";
$string['loginhead_step6a'] = "www.eduopen.org";

$string['cursession'] = "Current Session";

$string['archived_course'] = "Archived Course";
