$(document).ready(function () {//all document ready functions must go here 
    if (filtermode.value === 'browseallcourses') {

        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'coursefilter', offset: 0, showrecord: 21},
                function () {

                    var ht = $("#rightsideTab").height();
                    extraheight = ht + 2;
                    $(".sidebackcolor").height(extraheight);
                    $("#sidebar").height(extraheight);

                }); //Load all the course when this page is accessed

        // //for infite scroll chnages
        // $(document).ready(function(){                
        //     $(window).bind('scroll',fetchMore);
        // });
        // fetchMore = function (){
        //     if( $(window).scrollTop() + $(window).height() > $(document).height() - 300) {
        //         $(window).unbind('scroll',fetchMore);
        //         var alreadyshowedbox = $(".box-shadow").length;
        //         $('.loader').remove();   
        //         $.post('../theme/eduopen/ajax_load.php', {mode: 'coursefilter', offset: alreadyshowedbox, showrecord: 6},
        //             function (response) {
        //                 if(response){
        //                 $("#CourseArea").append('<div class="loader"><img class="imgloader" src='+loaderurl+'></div>'); //append loader
        //                     setTimeout(function(){
        //                         $(".loader").remove();
        //                         $("#CourseArea").append(response);
        //                         $(window).bind('scroll',fetchMore);
        //                     }, 1000);
        //                 }
        //                 var ht = $("#rightsideTab").height();
        //                 extraheight = ht + 2;
        //                 $(".sidebackcolor").height(extraheight);
        //                 $("#sidebar").height(extraheight);
        //             });
        //     }
        // }
        loadmore('coursefilter');
    } else {
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'pathwayfilter', offset: 0, showrecord: 21},
                function () {
                    var ht = $("#rightsideTab").height();
                    extraheight = ht + 2;
                    $(".sidebackcolor").height(extraheight);
                    $("#sidebar").height(extraheight);
                }); //Load all the pathway when this page is accessed
        //         //for infite scroll chnages
        //         $(document).ready(function(){                
        //             $(window).bind('scroll',fetchMore);
        //         });

        //     fetchMore = function (){
        //         if( $(window).scrollTop() + $(window).height() > $(document).height() - 300) {
        //             $(window).unbind('scroll',fetchMore);
        //             var alreadyshowedbox = $(".box-shadow-spz").length;
        //             $('.loader').remove();   
        //             $.post('../theme/eduopen/ajax_load.php', {mode: 'pathwayfilter', offset: alreadyshowedbox, showrecord: 6},
        //                 function (response) {
        //                 if(response){
        //                     $("#CourseArea").append('<div class="loader" style="float:left;left: 330px;"><img class="imgloader" src='+loaderurl+'></div>'); //append loader
        //                         setTimeout(function(){
        //                             $(".loader").remove();
        //                             $("#CourseArea").append(response);
        //                             $(window).bind('scroll',fetchMore);
        //                         }, 1000);

        //                 }
        //                 var ht = $("#rightsideTab").height();
        //                 extraheight = ht + 2;
        //                 $(".sidebackcolor").height(extraheight);
        //                 $("#sidebar").height(extraheight);
        //         });
        //     }
        // }
        // /*end of infinite scroll*/
        loadmore('pathwayfilter');
    }
    var price = [];
    var acertificate = []; // for attendance certificate
    var vcertificate = []; //for verified certificate
    var exam = []; //for exam certificate
    var language = []; //can have multiple values at one request
    var coursetype = [];
    var courselevel = [];
    var category = [];
    var specialization = [];
    var recognization = [];
    $('#PriceFilter input[type="checkbox"]').on('change', function () {
        Element = $(this).val();
        if ($(this).is(":checked")) {
            price.push(Element);
        } else {
            price.slice(1);
            price = jQuery.grep(price, function (value) {
                return value != Element;
            });
        }
    });
    $('#aCertificateFilter input[type="checkbox"]').on('change', function () {
        Element = $(this).val();
        if ($(this).is(":checked")) {
            acertificate.push(Element);
        } else {
            acertificate.slice(1);
            acertificate = jQuery.grep(acertificate, function (value) {
                return value != Element;
            });
        }

    });
    $('#vCertificateFilter input[type="checkbox"]').on('change', function () {
        Element = $(this).val();
        if ($(this).is(":checked")) {
            vcertificate.push(Element);
        } else {
            vcertificate.slice(1);
            vcertificate = jQuery.grep(vcertificate, function (value) {
                return value != Element;
            });
        }

    });
    $('#ExamFilter input[type="checkbox"]').on('change', function () {
        Element = $(this).val();
        if ($(this).is(":checked")) {
            exam.push(Element);
        } else {
            exam.slice(1);
            exam = jQuery.grep(exam, function (value) {
                return value != Element;
            });
        }

    });
    $('#LanguageFilter input[type="checkbox"]').on('change', function () {
        ElementValue = $(this).val();
        if ($(this).is(":checked")) {
            language.push(ElementValue);
        } else {
            language.slice(1);
            language = jQuery.grep(language, function (value) {
                return value != ElementValue;
            });
        }
    });
    $('#TypeFilter input[type="checkbox"]').on('change', function () {
        ElementValue = $(this).val();
        if ($(this).is(":checked")) {
            coursetype.push(ElementValue);
        } else {
            coursetype.slice(1);
            coursetype = jQuery.grep(coursetype, function (value) {
                return value != ElementValue;
            });
        }
    });
    $('#LevelFilter input[type="checkbox"]').on('change', function () {
        ElementValue = $(this).val();
        if ($(this).is(":checked")) {
            courselevel.push(ElementValue);
        } else {
            courselevel.slice(1);
            courselevel = jQuery.grep(courselevel, function (value) {
                return value != ElementValue;
            });
        }
    });
    $('#CategoryFilter input[type="checkbox"],#CategoryFilter input[type="radio"]').on('change', function () {
        Element = $(this).val();
        ElementType = $(this).attr('type');
        if (ElementType == 'checkbox') {
            if ($(this).is(":checked")) {
                category.push(Element);
            } else {
                category.slice(1);
                category = jQuery.grep(category, function (value) {
                    return value != Element;
                });
            }
        } else {
            category = [];
            category.push(Element);
        }
    });
    // $('#CategoryFilter input[type="radio"]').on('change', function () {

    //     Element = $(this).val();
    //     if ($(this).is(":checked")) {
    //         category = Element;
    //     } 
    // });

    $('#specializationFilter input[type="checkbox"]').on('change', function () {
        Element = $(this).val();
        if ($(this).is(":checked")) {
            specialization.push(Element);
        } else {
            specialization.slice(1);
            specialization = jQuery.grep(specialization, function (value) {
                return value != Element;
            });
        }
    });
    $('#recognizationFilter input[type="checkbox"]').on('change', function () {
        Element = $(this).val();
        if ($(this).is(":checked")) {
            recognization.push(Element);
        } else {
            recognization.slice(1);
            recognization = jQuery.grep(recognization, function (value) {
                return value != Element;
            });
        }
    });
    $('#browseallpathways button[class="catpath"]').on('click', function () {
        resetleftpanel();
        $('#BrowsepathwayFilter input[type="checkbox"]').prop('checked', false);
        $('#BrowsecourseFilter input[type="checkbox"]').prop('checked', false);
        var institutionid = $('#institutionFilter select[name="institutions"]').val();
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'pathwayfilter', institutionid: institutionid}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
            $("#sidebar").height(extraheight);
        });
    });
    $('#browseallcourses button[class="catcrs"]').on('click', function () {
        resetleftpanel();
        $('#BrowsepathwayFilter input[type="checkbox"]').prop('checked', false);
        $('#BrowsecourseFilter input[type="checkbox"]').prop('checked', false);
        var browseallcourses = $(this).val();
        var institutionid = $('#institutionFilter select[name="institutions"]').val();
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'coursefilter', institutionid: institutionid}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
        });
    });
    $('#institutionFilter select[name="institutions"]').on('change', function () {
        resetleftpanel();
        $('#BrowsepathwayFilter input[type="checkbox"]').prop('checked', false);
        $('#BrowsecourseFilter input[type="checkbox"]').prop('checked', false);
        var institutionid = $(this).val();
        var mode = $("input[type='radio'][name='browseall']:checked").val();
        if (typeof mode != 'undefined') {
            $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: mode, institutionid: institutionid}, function () {
                var ht = $("#rightsideTab").height();
                extraheight = ht + 2;
                $(".sidebackcolor").height(extraheight);
                $("#sidebar").height(extraheight);
            });
        } else {
            $("#CourseArea").html('<div class="alert alert-danger">Please select any of the above<strong> ( Browse all Courses/ Browse all Pathways )</strong></div>');
        }
    });
    //for filtering on tab switch
    $("ul.nav-tabs li").click(function () {
        if ($(this).attr('id') == 'coursefilter') {
            var mode = $(this).attr('id');
            mode = 'filter'; //this for as we have swictch case kay as "filter" for coursefilter
            settablevelfilter(mode);
            //loadmore(mode);
        } else {
            // reset event archived course.
            $("#Arch_Crs").removeClass('active');

            var mode = 'pathwayfilter';
            settablevelfilter(mode);
            //loadmore(mode);
        }
    });

    $('#FilterForm input[type="radio"],#FilterForm input[type="checkbox"]').on('change', function () {//Load every time filter options are clicked or changed
        resetrightpanel();
        resetadvancefilter();
        $("#coursemain_row").hide();
        $("#coursearea_row").show();
        $("#CourseArea > .loader").show();
        if ($("ul.nav-tabs li#coursefilter").attr('class') == 'active') {
            mode = 'filter';
        } else {
            mode = 'pathwayfilter'
        }

        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: mode, price: price,
            acertificate: acertificate, vcertificate: vcertificate, language: language, coursetype: coursetype, courselevel: courselevel,
            category: category, exam: exam, specialization: specialization, recognization: recognization}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
            $("#sidebar").height(extraheight);
        });
    });
    $('div#CategoryFilter li').click(function () {
        $(this).find('input[name="categories[]"]').prop('checked', true);
        catval = $(this).find('input[name="categories[]"]').val();
        var category = [];
        category.push(catval);
        resetrightpanel();
        resetadvancefilter();
        $("#coursemain_row").hide();
        $("#coursearea_row").show();
        $("#CourseArea > .loader").show();
        if ($("ul.nav-tabs li#coursefilter").attr('class') == 'active') {
            mode = 'filter';
        } else {
            mode = 'pathwayfilter'
        }
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: mode, price: price,
            acertificate: acertificate, vcertificate: vcertificate, language: language, coursetype: coursetype, courselevel: courselevel,
            category: category, exam: exam, specialization: specialization, recognization: recognization, offset: 0, showrecord: 21}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
            $("#sidebar").height(extraheight);
        });

        // reset event archived course.
        $("#Arch_Crs").removeClass('active');
    });

    // Added by Shiuli for archived course.
    $('div#archived li').click(function () {
        resetmainpanel();
        $("ul.nav-tabs li#coursefilter").addClass('active');
        resetleftpanel();
        resetadvancefilter();
        resetcategorypanel();

        $(this).find('input[name="archive[]"]').prop('checked', true);
        $("#Arch_Crs").addClass('active');
        $("#coursemain_row").hide();
        $("#coursearea_row").show();
        $("#CourseArea > .loader").show();
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'coursefilter',
            archived: 'Arc0', offset: 0, showrecord: 3}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
            $("#sidebar").height(extraheight);
        });
    });


    $('#FilterForm span#all').on('click', function () {
        resetcommonfilter();
        resetadvancefilter();
        if ($("ul.nav-tabs li#coursefilter").attr('class') == 'active') {
            mode = 'filter';
        } else {
            mode = 'pathwayfilter'
        }
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: mode}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
            $("#sidebar").height(extraheight);
        }); //Load all the course when this page is accessed
    });
    $('#BrowsepathwayfilterForm input[type="checkbox"]').on('change', function () {//Load every time filter options are clicked or changed
        resetrightpanel();
        $("#coursemain_row").hide();
        $("#coursearea_row").show();
        $("#CourseArea > .loader").show();
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'pathwayfilter', language: language,
            category: category, recognization: recognization}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            if (extraheight <= 540) {
                $("#nav-accordion").height(515);
                $(".sidebackcolor").height(extraheight);
                $("#sidebar").height(extraheight);
            } else {
                $("#nav-accordion").height('auto');
                $(".sidebackcolor").height(extraheight);
                $("#sidebar").height(extraheight);
            }
        });
    });
    $('#FilterForm span[id="browseallcoursesleftpanel"]').on('click', function () {
        resetrightpanel();
        $("#coursemain_row").hide();
        $("#coursearea_row").show();
        $("#CourseArea > .loader").show();
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'filter', price: price,
            acertificate: acertificate, vcertificate: vcertificate, language: language, coursetype: coursetype, courselevel: courselevel,
            category: category, exam: exam, specialization: specialization, recognization: recognization}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
            $("#sidebar").height(extraheight);
        });
    });
    $('#BrowsepathwayfilterForm span[id="browseallpathwaysleftpanel"]').on('click', function () {
        resetrightpanel();
        resetcommonfilter();
        category = [];
        $("#coursemain_row").hide();
        $("#coursearea_row").show();
        $("#CourseArea > .loader").show();
        $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: 'pathwayfilter', language: language,
            category: category, recognization: recognization}, function () {
            var ht = $("#rightsideTab").height();
            extraheight = ht + 2;
            $(".sidebackcolor").height(extraheight);
            $("#sidebar").height(extraheight);
        });
    });
});
//Add course to watch-list 

function AddToWatchList(courseID, loggedinStat) {
    if (loggedinStat == 1) {
        var elementID = "#course" + courseID;
        var html = "";
        $(elementID).html("");
        $(elementID).load('../theme/eduopen/ajax_load.php', {mode: 'watchlistadd', courseid: courseID});
    } else {
        window.location.replace(page.loginurl);
    }
}

//remove course watch-list 
function RemoveFromWatchlist(courseID) {

    var elementID = "#AllCoursesBlock" + courseID;
    $.post("../theme/eduopen/ajax_load.php", {mode: "watchlistremove", courseid: courseID}, function (data, status) {
        if (status == 'success' && data == 'success') {
            $(elementID).fadeOut("slow");
        }
    });
}

function resetrightpanel() {
    $('#browseallcourses input[type="radio"]').prop('checked', false);
    $('#browseallpathways input[type="radio"]').prop('checked', false);
    $('#institutionFilter select[name="institutions"]').val('');
}
function resetleftpanel() {
    $('#BrowsepathwayFilter input[type="checkbox"]').prop('checked', false);
    $('#BrowsecourseFilter input[type="checkbox"]').prop('checked', false);
}

// by Shiuli
function resetmainpanel() {
    $('#coursefilter').removeClass('active');
    $('#pathwayfilter').removeClass('active');
}
function resetcategorypanel() {
    $('#CategoryFilter input[type="checkbox"]').prop('checked', false);
    $('#CategoryFilter li').removeClass('catactive');
    $('#CategoryFilter li a').removeClass('active');
}

function resetadvancefilter() {
    $('#BrowsepathwayFilter input[type="checkbox"]').prop('checked', false);
}
function resetcommonfilter() {
    $('#FilterForm input[type="radio"]').prop('checked', false);
}
/*function to fetch more course box or pathway box while scrolling--firstimeinpageload*/
function loadmore(mode) {
    //for infite scroll chnages
    $(document).ready(function () {
        $(window).bind('scroll', fetchMore);
    });
    fetchMore = function () {
        if (($(window).scrollTop() + $(window).height()) > ($(document).height() - 400)) {

            var category = [];
            var categoryval = $('#CategoryFilter input[type="radio"]:checked').val();
            category.push(categoryval);

            /*for archived course*/
            var archived = [];
            var archivedval = $('#archived input[type="radio"]:checked').val();
            if (typeof archivedval !== 'undefined') {
                archived.push(archivedval);
            }
            $(window).unbind('scroll', fetchMore);
            if ($("ul.nav-tabs li#coursefilter").attr('class') == 'active') {
                if (archived.length === 0) {
                    mode = 'filter';
                } else {
                    mode = 'coursefilter';
                    archived = 'reached0';
                }
                //mode = archived.length ? 'filter' : 'coursefilter';
            } else {
                mode = 'pathwayfilter';
            }
            if (mode == 'pathwayfilter') {
                var alreadyshowedbox = $(".box-shadow-spz").length;
            } else {
                var alreadyshowedbox = $(".box-shadow").length;
            }
            $('.loader').remove();
            if (alreadyshowedbox) {
                $.post('../theme/eduopen/ajax_load.php', {mode: mode, category: category, archived: 'reached0', offset: alreadyshowedbox, showrecord: 6},
                        function (response) {
                            if (response) {
                                if (mode == 'pathwayfilter') {
                                    $("#CourseArea").append('<div class="col-md-12 CatalogBeforeLoader"><div class="loader" style="float:left;"><img class="imgloader" src=' + loaderurl + '></div></div>'); //append loader
                                } else {
                                    $("#CourseArea").append('<div class="col-md-12 CatalogBeforeLoader"><div class="loader" style="float:left;><img class="imgloader" src=' + loaderurl + '></div></div>'); //append loader
                                }
                                setTimeout(function () {
                                    $(".loader").remove();
                                    if (response != 'EndReached') {
                                        $("#CourseArea").append(response);
                                    } else {
                                        $("#CourseArea").append('');
                                    }
                                    $(window).bind('scroll', fetchMore);
                                }, 1000);

                            }
                            var ht = $("#rightsideTab").height();
                            extraheight = ht + 2;
                            $(".sidebackcolor").height(extraheight);
                            $("#sidebar").height(extraheight);
                        });
            }
        }
    }
    /*end of infinite scroll*/
}
function settablevelfilter(mode) {
    var category = [];
    var categoryval = $('#CategoryFilter input[type="radio"]:checked').val();
    category.push(categoryval);
    $("#CourseArea").load('../theme/eduopen/ajax_load.php', {mode: mode, category: category, offset: 0, showrecord: 21},
            function () {
                var ht = $("#rightsideTab").height();
                extraheight = ht + 2;
                $(".sidebackcolor").height(extraheight);
                $("#sidebar").height(extraheight);

            }); //Load all the course when this page is accessed
    //loadmore(mode);
}
//javascript for font awesome icon chnage in advance filer
$("#browseallcourse").click(function () {
    $('#browseallcourse i').toggleClass('fa-minus-square', 'fa-plus-square');
    var browseallpathclass = $('#browseallpathway').attr('class');
    var substring = 'active';
    if (browseallpathclass.indexOf(substring) > -1) {
        $('#browseallpathway i').toggleClass('fa-minus-square', 'fa-plus-square');
    }

});
$("#browseallpathway").click(function () {
    $('#browseallpathway i').toggleClass('fa-minus-square', 'fa-plus-square');
    var browseallcourseclass = $('#browseallcourse').attr('class');
    var substring = 'active';
    if (browseallcourseclass.indexOf(substring) > -1) {
        $('#browseallcourse i').toggleClass('fa-minus-square', 'fa-plus-square');
    }
});