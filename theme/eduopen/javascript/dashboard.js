$("#block-region-content").hide(); 

$(document).ready(function () {
	//Dynamic search box
	$("#CategorySearch").on("keyup click input", function () {		
		if (this.value.length > 0) {
			$(".SidebarHeading").hide();
		  $(".widget-collapsible").show().filter(function () {
			return $(this).find('.widget-head').text().toLowerCase().indexOf($("#CategorySearch").val().toLowerCase()) == -1;
		  }).hide();
		}
		else {
		  $(".widget-collapsible").show();
		  $(".SidebarHeading").show();
		}
	});		
	
});
