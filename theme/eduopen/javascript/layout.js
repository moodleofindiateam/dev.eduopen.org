$(document).ready(function(){
	
	//Adding Web site navigation menu form top navbar to side nav bar
	$(".dcjq-parent-li ul").removeClass("dropdown-menu");
	$(".dcjq-parent-li ul").addClass("sub");
	$(".dcjq-parent-li  a > b").hide();
	$("fieldset").removeClass("hidden");	
	
	//Course search box
	var searchData = {};			
	var drew = false;	
	
	$("#SearchBox .search").on("keyup", function(event){		
		
		var query = $(this).val();
		if(query.length > 3){
			var width = $('#SearchBox .search').width();			
		
			//TODO: When moved to server change the location
			$.post( page.url +'/theme/eduopen/ajax_load.php', { mode: "courseSearch", keyword: query}, function( data ) { 
				
				searchData = data;
				
				console.log(searchData);
				
				
				//First search
				if(drew == false){
					
					 //Create list for results
					$("#SearchBox .search").after('<ul id="SearchResult" class="searchul"></ul>');
					$("#SearchResult").width($('#SearchBox .search').outerWidth(false));//setting the ul width same as the selector's width
					
					//Prevent redrawing/binding of list
					drew = true;
					
					
					//Bind click event to list elements in results
					$("#SearchResult").on("click", "li", function(){
						$("#SearchBox .search").val($(this).text());											
						$("#SearchResult").empty();
						$("#SearchResult").hide();
						//open the course view page //TODO: When moved to server change the location
						window.location.href = page.url +'/eduopen/course_details.php?courseid='+$(this).attr('id');
											
					});
					
					//when the search boxes reduces size after focus out hide search results also
					$( ".search" ).focusout(function(){
						$( ".searchul" ).fadeOut(6000);
					});
					
					
				}else{//Clear old results  
				
					$("#SearchResult").empty();  
					$("#SearchResult").hide();
					
				}
				
				for(term in searchData){  
					var returnedData = searchData[term];	
					//extract user name and id from the json data
					var separator = returnedData.indexOf('~');
					var courseName= returnedData.substring(0, separator);
					var courseID = returnedData.substring(separator+1);
					console.log(courseName);
											
					$("#SearchResult").show();
					$("#SearchResult").append("<li id='"+ courseID +"' class='searchli'>" + courseName + "</li>");  //make the user id as li id.         					
				}						
				
			},"json");
		}else if(drew){//Handle backspace/delete so results don't remain with less than 3 characters
			$("#SearchResult").empty();
			$("#SearchResult").hide();		
			
		}
		 
	});
	/* $('body').tooltip({
		selector: '[data-toggle=tooltip]'
	}); */
	
	$(".fancybox").fancybox({
		openEffect	: 'fade',
    	closeEffect	: 'fade',
	});
	
/*    $.post( 'http://eduopen.it/theme/eduopen/ajax_load.php', { mode: "spec_background_image"},function( data ) {
	     recieved_data = data;
    var inst_countid = recieved_data.countid;
	 var inst_id = recieved_data.id;
	 var inst_name = recieved_data.ins_name;
	 var inst_url = recieved_data.bck_url;
	 console.log(inst_countid);
	
	  
	  
	  $('.inst-link a').attr('href','../../../eduopen/institution_details.php?institutionid='+inst_id);
	  //$('#spback').css("background-image",'url(' + inst_url + ')');
	 // $(".inst-link span").text(inst_name);
	  //$(".ctspecial span").text(inst_countid);
   },"json"); */	
	

});

//Adding Web site navigation menu form top navbar to side nav bar
$(window).resize(function() {
  // This will execute whenever the window is resized
  var height = $(window).height();
  var width = $(window).width();
  if(width <= 985){
	  $("#CustomMenu").hide();
	  $("#WebNav").show();
  }else{
	  $("#CustomMenu").show();
	  $("#WebNav").hide();
  }
});


 



