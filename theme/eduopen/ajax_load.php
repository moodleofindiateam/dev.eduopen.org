<?php

require_once('../../config.php');
require_once('lib.php');
require_once($CFG->dirroot . '/eduopen/lib.php');

if (isset($_REQUEST)) {
    global $DB, $CFG;
    $block_count = 0;
    $mode = isset($_REQUEST['mode']) ? $_REQUEST['mode'] : null;
    $offset = isset($_REQUEST['offset']) ? $_REQUEST['offset'] : null;
    $showrecord = isset($_REQUEST['showrecord']) ? $_REQUEST['showrecord'] : null;
    if ($mode) {
        switch ($mode) {

            case 'coursefilter' :
                try {
                    $query = '';
                    $query1 = '';
                    if ($offset == 0) {
                        echo html_writer::start_div('container-fluid  tcenter fcourse');
                        if (isset($_REQUEST['archived'])) {
                            echo html_writer::tag('h2', get_string('archived_course', 'theme_eduopen'), array('class' => 'cpfilter'));
                        } else {
                            echo html_writer::tag('h2', get_string('fcourse', 'theme_eduopen'), array('class' => 'cpfilter'));
                        }
                        echo html_writer::div('<hr>');
                        echo html_writer::end_div();
                    }
                    if (!empty($_REQUEST['institutionid'])) {
                        $query = "AND ce.institution IN ('" . $_REQUEST['institutionid'] . "') ";
                    }

                    // Added by Shiuli for archived course.
                    $archived = isset($_REQUEST['archived']) ? $_REQUEST['archived'] : null;
                    if ($archived) {
                        $archived = substr($archived, -1);
                        $query1 .= "AND ce.coursestatus = $archived";
                    }
                    if (isset($_REQUEST['archived'])) {
                        $coursedetailsq = 'SELECT c.id, c.category, ce.certificate, ce.certificate1, ce.certificate2, ce.cost,
                            ce.formalcredit, ce.language, ce.coursetype, ce.courselevel, ce.institution, ce.specializations
                            FROM {course} c JOIN {course_extrasettings_general} ce ON c.id = ce.courseid
                            WHERE c.visible = 1 AND ce.coursestatus=0 AND c.category != 0 ' . $query1 . ' ORDER BY c.startdate';
                        $coursecount1 = $DB->get_records_sql($coursedetailsq);
                        $coursedetailsq = 'SELECT c.id, c.category, ce.certificate,
                            ce.certificate1, ce.certificate2, ce.cost,
                            ce.formalcredit, ce.language, ce.coursetype, ce.courselevel,
                            ce.institution, ce.specializations, ce.coursestatus
                            FROM {course} c JOIN {course_extrasettings_general} ce
                            ON c.id = ce.courseid
                            WHERE c.visible = 1 AND c.category != 0 ' . $query1 . '
                            ORDER BY c.startdate LIMIT ' . $showrecord . ' OFFSET ' . $offset;
                        $param1 = 'archive';
                    } else {
                        $coursedetailsq = 'SELECT c.id, c.category, ce.certificate, ce.certificate1, ce.certificate2, ce.cost,
                            ce.formalcredit, ce.language, ce.coursetype, ce.courselevel, ce.institution, ce.specializations
                            FROM {course} c JOIN {course_extrasettings_general} ce ON c.id = ce.courseid
                            WHERE c.visible = 1 AND ce.coursestatus=1 AND c.category != 0 ' . $query . ' ORDER BY c.startdate';
                        $coursecount1 = $DB->get_records_sql($coursedetailsq);
                        $coursedetailsq = 'SELECT c.id, c.category, ce.certificate,
                            ce.certificate1, ce.certificate2, ce.cost,
                            ce.formalcredit, ce.language, ce.coursetype, ce.courselevel,
                            ce.institution, ce.specializations
                            FROM {course} c JOIN {course_extrasettings_general} ce
                            ON c.id = ce.courseid
                            WHERE c.visible = 1 AND ce.coursestatus=1 AND c.category != 0 ' . $query . '
                            ORDER BY c.startdate LIMIT ' . $showrecord . ' OFFSET ' . $offset;
                        $param1 = '';
                    }
                    $coursedetailsrs = $DB->get_records_sql($coursedetailsq);
                    if ($coursedetailsrs) {
                        echo '<div class="row-fluid" id="frontrowpad">';
                        foreach ($coursedetailsrs as $id => $coursedetails) {
                            $block_count++;
                            echo catalog_featuredcourse_block($id, $param1);
                            if (( $block_count % 3 ) == 0) {//if two blocks are printed at a time then create a new row
                                echo '</div>';
                                echo '<div class="row-fluid" id="frontrowpad">';
                            }
                        }
                    } else if ($coursecount1) {
                        if (count($coursecount1) == $offset) {
                            echo "EndReached";
                        }
                    } else {
                        echo '<div class="row-fluid center">';
                        echo '<div class="col-md-12 alert alert-danger">' . get_string('nocourse', 'theme_eduopen') . '</div>';
                        echo '</div>';
                    }
                } catch (Exception $ex) {
                    echo $ex->getMessage();
                }

                break;
            case 'pathwayfilter' :

                if ($offset == 0) {
                    echo html_writer::start_div('container-fluid tcenter fcourse');
                    echo html_writer::tag('h2', get_string('fpathway', 'theme_eduopen'), array('class' => 'cpfilter'));
                    echo html_writer::div('<hr>');
                    echo html_writer::end_div();
                }
                $query = '';
                $list = [];
                $result = null;
                if (!empty($_REQUEST['institutionid'])) {
                    //$pathwaydetails = $DB->get_records('course_extrasettings_general',array('institution' =>  $_REQUEST['institutionid']));
                    $tempinst = $_REQUEST['institutionid'];
                    $pathwaydetailsq = "SELECT * FROM {course_extrasettings_general} WHERE institution = '$tempinst'"
                            . " AND specializations != 'none' AND coursestatus=1";
                    $pathwaydetails = $DB->get_records_sql($pathwaydetailsq);

                    foreach ($pathwaydetails as $pathwaydetailsrs) {
                        $pathwayidexplode = explode(',', $pathwaydetailsrs->specializations);

                        foreach ($pathwayidexplode as $pathwayidexplodes) {
                            $pathwayvisibilitycheck = $DB->get_record('block_eduopen_master_special', array('id' => $pathwayidexplodes, 'status' => '1'));

                            if (!isset($list[$pathwayidexplodes]) AND $pathwayvisibilitycheck) {
                                $list[$pathwayidexplodes] = $pathwayidexplodes;
                            }
                        }
                    }

                    $result = $list;
                } else {
                    $category = isset($_REQUEST['category']) ? $_REQUEST['category'] : null;
                    if ($category) {
                        $category = implode(",", $category);
                        $query .= "AND es.category IN (" . $category . ") ";
                    }
                    $language = isset($_REQUEST['language']) ? $_REQUEST['language'] : null;
                    if ($language) {
                        $language = "'" . implode("','", $language) . "'";
                        $query .= "AND es.pathlanguage IN (" . $language . ") ";
                    }
                    $recognization = isset($_REQUEST['recognization']) ? $_REQUEST['recognization'] : null;
                    if ($recognization) {
                        $recognization = "'" . implode("','", $recognization) . "'";
                        $result = $DB->get_records_sql("SELECT id FROM {block_eduopen_master_special} where
                                                                degree IN($recognization)");
                        if ($result) {
                            foreach ($result as $key => $results) {
                                $degreename[] = $results->id;
                            }
                            $degreenamestring = implode(" OR es.id =", $degreename);
                            $query .= "AND (es.id = $degreenamestring)";
                        } else {
                            $dummyid = '99999999999';  //this is for if there is no respective degree pathways not present
                            $query .= "AND (es.id = $dummyid)";
                        }
                    }
                    $resultq = "SELECT * FROM {block_eduopen_master_special} es WHERE es.category != 0 AND status='1' $query";
                    $pathwaycount = $DB->get_records_sql($resultq);
                    // if(count($pathwaycount) <= $offset){
                    //  echo false;
                    // }

                    $resultq = "SELECT * FROM {block_eduopen_master_special} es WHERE es.category != 0 AND status='1' $query LIMIT " . $showrecord . " OFFSET " . $offset;
                    $result = $DB->get_records_sql($resultq);
                }
                if (!empty($result) and isset($result)) {
                    foreach ($result as $id => $pathwaydetails) {
                        $pathwayblock = catalog_featuredpathway_block($id);
                        if ($pathwayblock) {
                            echo $pathwayblock;
                        }
                    }
                } else if ($pathwaycount) {
                    if (count($pathwaycount) == $offset) {
                        echo "EndReached";
                    }
                } else {
                    echo '<div class="row-fluid center">';
                    echo '<div class="col-md-12 alert alert-danger">' . get_string('nopathway', 'theme_eduopen') . '</div>';
                    echo '</div>';
                }
                break;

            case 'filter':
                if ($offset == 0) {
                    echo html_writer::start_div('container-fluid tcenter fcourse');
                    echo html_writer::tag('h2', get_string('fcourse', 'theme_eduopen'), array('class' => 'cpfilter'));
                    echo html_writer::div('<hr>');
                    echo html_writer::end_div();
                }
                $query = '';
                $blockcount = 0;
                //echo '<script>$("#CourseArea > .loader").hide();</script>';
                $price = isset($_REQUEST['price']) ? $_REQUEST['price'] : null;
                if ($price) {
                    $price = implode(" ", $price);
                    if ($price == 'Free') {
                        $query .= "AND ce.cost = 0 ";
                    } else if ($price == 'Paid') {
                        $query .= "AND ce.cost > 0 ";
                    } else {
                        $query .= "AND ce.cost >= 0 ";
                    }
                }

                $acertificate = isset($_REQUEST['acertificate']) ? $_REQUEST['acertificate'] : null;
                if ($acertificate) {
                    $acertificate = "'" . implode("','", $acertificate) . "'";
                    $query .= "AND ce.certificate1 IN (" . $acertificate . ") ";
                }
                $vcertificate = isset($_REQUEST['vcertificate']) ? $_REQUEST['vcertificate'] : null;
                if ($vcertificate) {
                    $vcertificate = "'" . implode("','", $vcertificate) . "'";
                    $query .= "AND ce.certificate2 IN (" . $vcertificate . ") ";
                }
                /* $certificate = isset($_REQUEST['certificate']) ? $_REQUEST['certificate'] : null;
                  if($certificate){
                  $certificate = "'".implode("','", $certificate)."'";
                  $query .= "AND ce.certificate IN (".$certificate.") ";
                  } */
                $exam = isset($_REQUEST['exam']) ? $_REQUEST['exam'] : null;
                if ($exam) {
                    $exam = "'" . implode("','", $exam) . "'";
                    $query .= "AND ce.certificate IN (" . $exam . ") ";
                }
                $category = isset($_REQUEST['category']) ? $_REQUEST['category'] : null;
                if ($category) {
                    $category = implode(",", $category);
                    $query .= "AND c.category IN (" . $category . ") ";
                }
                $language = isset($_REQUEST['language']) ? $_REQUEST['language'] : null;
                if ($language) {
                    $language = "'" . implode("','", $language) . "'";
                    $query .= "AND ce.language IN (" . $language . ") ";
                }
                $coursetype = isset($_REQUEST['coursetype']) ? $_REQUEST['coursetype'] : null;
                if ($coursetype) {
                    $coursetype = "'" . implode("','", $coursetype) . "'";
                    $query .= "AND ce.coursetype IN (" . $coursetype . ") ";
                }
                $courselevel = isset($_REQUEST['courselevel']) ? $_REQUEST['courselevel'] : null;
                if ($courselevel) {
                    $courselevel = "'" . implode("','", $courselevel) . "'";
                    $query .= "AND ce.courselevel IN (" . $courselevel . ") ";
                }

                $specialization = isset($_REQUEST['specialization']) ? $_REQUEST['specialization'] : null;
                if ($specialization) {
                    //$specialization = implode("%' OR ce.specializations LIKE '%", $specialization);
                    $specialization = implode("%' OR ce.specializations LIKE '%", $specialization);
                    $query .= "AND ce.specializations @@ to_tsquery('$specialization')";
                }
                $recognization = isset($_REQUEST['recognization']) ? $_REQUEST['recognization'] : null;
                if ($recognization) {
                    $recognization = "'" . implode("','", $recognization) . "'";
                    $result = $DB->get_records_sql("SELECT id FROM {block_eduopen_master_special} where
                    degree IN($recognization)");
                    foreach ($result as $key => $results) {
                        $degreename[] = $results->id;
                    }
                    $degreenamestring = implode("%' OR ce.specializations LIKE '%", $degreename);
                    $query .= "AND (ce.specializations @@ to_tsquery('$degreenamestring'))";
                }

                $coursedetailsq1 = 'SELECT c.id, c.category, ce.certificate, ce.certificate1, ce.certificate2, ce.cost,
                    ce.formalcredit, ce.language, ce.coursetype, ce.courselevel, ce.institution, ce.specializations
                    FROM {course} c JOIN {course_extrasettings_general} ce ON c.id = ce.courseid
                    WHERE c.visible = 1 AND ce.coursestatus=1 AND c.category != 0 ' . $query . ' ORDER BY c.startdate';
                $coursecount = $DB->get_records_sql($coursedetailsq1);


                $coursedetailsq1 = "SELECT c.id, c.category, ce.certificate, ce.certificate1, ce.certificate2, ce.cost,
                ce.formalcredit, ce.language, ce.coursetype, ce.courselevel, ce.institution, ce.specializations
                FROM {course} c LEFT JOIN {course_extrasettings_general} ce ON c.id = ce.courseid
                WHERE c.visible = 1 AND ce.coursestatus=1 AND c.category != 0 $query ORDER BY c.startdate LIMIT " . $showrecord . " OFFSET " . $offset;

                $resultrs = $DB->get_records_sql($coursedetailsq1);
                if ($resultrs) {
                    foreach ($resultrs as $coursedetails) {
                        $courseid = $coursedetails->id;
                        //$courseblock = generate_course_block($courseid);
                        $courseblock = catalog_featuredcourse_block($courseid, '');
                        if ($courseblock) {
                            $blockcount++;
                            echo $courseblock;
                            /* if( ( $block_count % 1 ) == 0 ){
                              echo '</div>';
                              }
                              if( ( $block_count % 1 ) == 0 ){//if two blocks are printed at a time then create a new row
                              echo '<div class="row1">';
                              } */
                        }
                    }
                } else if ($coursecount) {
                    if (count($coursecount) == $offset) {
                        echo "EndReached";
                    }
                } else {
                    echo '<div class="row-fluid center">';
                    echo '<div class="col-md-12 alert alert-danger">' . get_string('nocourse', 'theme_eduopen') . '</div>';
                    echo '</div>';
                }
                break;
            case 'courseSearch':
                $keyword = isset($_REQUEST['keyword']) ? $_REQUEST['keyword'] : null;
                $data = array();
                if ($keyword) {
                    $coursedetailsq = "SELECT id,fullname FROM {course} WHERE fullname Like '%$keyword%'
                    AND visible = 1 AND category != 0";
                    $coursedetailsrs = $DB->get_records_sql($coursedetailsq);
                    if ($coursedetailsrs) {
                        foreach ($coursedetailsrs as $coursedetails) {
                            $courseid = $coursedetails->id;
                            $fullname = $coursedetails->fullname;
                            $data[] = $fullname . '~' . $courseid;
                        }
                        echo json_encode($data);
                    }
                }
                break;
            case 'watchlistadd':
                $courseid = isset($_REQUEST['courseid']) ? $_REQUEST['courseid'] : null;
                if ($courseid) {
                    if (!in_watchlist($courseid)) {//prevent multiple entry for single course
                        $record = new stdClass();
                        $record->userid = $USER->id;
                        $record->course = $courseid;
                        $addtowatchlist = $DB->insert_record('eduopen_watchlist', $record);
                        if ($addtowatchlist) {
                            echo '<i class="fa fa-desktop" data-placement="left" data-toggle="tooltip"
                            data-original-title="' . get_string('watchlist_added', 'theme_eduopen') . '"></i>' .
                            get_string('watchlist_added', 'theme_eduopen');
                        }
                    }
                }
                break;
            case 'watchlistremove':
                $courseid = isset($_REQUEST['courseid']) ? $_REQUEST['courseid'] : null;
                if ($courseid) {
                    $removefromwatchlist = $DB->delete_records('eduopen_watchlist', array('course' => $courseid, 'userid' => $USER->id));
                    if ($removefromwatchlist) {
                        echo "success";
                    } else {
                        echo "failed";
                    }
                }
                break;
            case 'background_image'://by naina on 05-mar-2015 code for background_image/banner
                $data = array();
                $bck = "SELECT id, name, banner FROM {block_eduopen_master_inst} ORDER BY RAND() LIMIT 1";
                $bckrm = $DB->get_record_sql($bck);
                if ($bckrm) {
                    $inst = new stdClass();
                    $inst->id = $bckrm->id;
                    $inst->ins_name = $bckrm->name;
                    $banner = banner_images($bckrm);
                    if ($banner) {
                        $banners = trim($banner, '"');
                        $inst->bck_url = $banners;
                        echo json_encode($inst);
                    }
                }
                break;
            case 'spec_background_image': //by naina on 05-mar-2015 code for background_image/banner
                $data = array();
                $bck = "SELECT id, name, banner FROM {block_eduopen_master_inst} ORDER BY RAND() LIMIT 1";
                $bckrm = $DB->get_record_sql($bck);
                if ($bckrm) {
                    //$allspecial = $DB->get_records('course_extrasettings_general',array('institution'=>$bck_rm->name));
                    $allspecial = $DB->get_records_sql("SELECT specializations FROM {course_extrasettings_general}
                        WHERE institution = '$bckrm->name' AND coursestatus=1");
                    $specialcount = count($allspecial);
                    $spzli = Specialization;
                    /* foreach($allspecial as $speci){
                      $specialcont = "SELECT COUNT(id) AS userstotal FROM {block_eduopen_master_special} WHERE
                      name ='$speci->specializations'";
                      $specialcount = $DB->get_record_sql($specialcont);
                     */
                    $inst = new stdClass();
                    $inst->countid = $specialcount . ' ' . $spzli;
                    $inst->id = $bckrm->id;
                    $inst->ins_name = $bckrm->name;
                    $banner = banner_images($bckrm);
                    if ($banner) {
                        $banners = trim($banner, '"');
                        $inst->bck_url = $banners;
                        echo json_encode($inst);
                    }
                }
                break;
            case 'categorycolor':
                $categoryid = $_REQUEST['catid'];
                $getcolor = $DB->get_record('categorycolor', array('catid' => $categoryid), 'colorcode,catid');
                $catname = $DB->get_record('course_categories', array('id' => $getcolor->catid), 'name');
                $result = array('catname' => $catname->name, 'colorcode' => $getcolor->colorcode);
                echo json_encode($result);
                break;
            case 'theme_eduopen_zoom':
                $value = optional_param('value', NULL, PARAM_TEXT);

                if (confirm_sesskey() && data_submitted()) {
                    if (set_user_preference($mode, $value)) {
                        $response = ['status' => true, 'message' => 'preferance has been set'];
                        echo json_encode($response);
                    }
                }
        }
    }
}