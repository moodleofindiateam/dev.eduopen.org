<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * eduopen theme.
 *
 * @package    theme
 * @subpackage eduopen
 * @copyright  &copy; 2014-onwards G J Barnard in respect to modifications of the Bootstrap theme.
 * @author     G J Barnard - gjbarnard at gmail dot com and {@link http://moodle.org/user/profile.php?id=442195}
 * @author     Based on code originally written by Bas Brands, David Scotson and many other contributors.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . "/course/renderer.php");
global $CFG, $DB, $COURSE;

// Topcoll Format.
class theme_eduopen_core_course_renderer extends core_course_renderer {

    /**
     * Renders html to display a name with the link to the course module on a course page
     *
     * If module is unavailable for user but still needs to be displayed
     * in the list, just the name is returned without a link
     *
     * Note, that for course modules that never have separate pages (i.e. labels)
     * this function return an empty string
     *
     * @param cm_info $mod
     * @param array $displayoptions
     * @return string
     */
    public function course_section_cm_name(cm_info $mod, $displayoptions = array()) {
        global $CFG, $DB, $COURSE, $USER, $PAGE;

        $output = '';
        if (!$mod->uservisible && empty($mod->availableinfo)) {
            // nothing to be displayed to the user
            return $output;
        }

        $sql1 = "SELECT DISTINCT cs.section, cs.name, cs.sequence, cm.instance, cm.module FROM {course_modules} cm
            JOIN {course_sections} cs ON cs.course=cm.course AND cs.id=cm.section
            WHERE cs.visible=1 AND cm.course=$COURSE->id AND cm.id=$mod->id ORDER BY cs.section";
        $modsection = $DB->get_record_sql($sql1);
        $url = $mod->url;
        if (!$url) {
            return $output;
        }

        //Accessibility: for files get description via icon, this is very ugly hack!
        $instancename = $mod->get_formatted_name();
        $altname = $mod->modfullname;

// Avoid unnecessary duplication: if e.g. a forum name already
// includes the word forum (or Forum, etc) then it is unhelpful
// to include that in the accessible description that is added.
        if (false !== strpos(core_text::strtolower($instancename), core_text::strtolower($altname))) {
            $altname = '';
        }
// File type after name, for alphabetic lists (screen reader).
        if ($altname) {
            $altname = get_accesshide(' ' . $altname);
        }

// For items which are hidden but available to current user
// ($mod->uservisible), we show those as dimmed only if the user has
// viewhiddenactivities, so that teachers see 'items which might not
// be available to some students' dimmed but students do not see 'item
// which is actually available to current student' dimmed.
        $linkclasses = '';
        $accesstext = '';
        $textclasses = '';
        if ($mod->uservisible) {
            $conditionalhidden = $this->is_cm_conditionally_hidden($mod);
            $accessiblebutdim = (!$mod->visible || $conditionalhidden) &&
                    has_capability('moodle/course:viewhiddenactivities', $mod->context);
            if ($accessiblebutdim) {
                $linkclasses .= ' dimmed fancybox fancybox.iframe';
                $textclasses .= ' dimmed_text fancybox fancybox.iframe';
                if ($conditionalhidden) {
                    $linkclasses .= ' conditionalhidden';
                    $textclasses .= ' conditionalhidden';
                }
// Show accessibility note only if user can access the module himself.
                $accesstext = get_accesshide(get_string('hiddenfromstudents') . ':' . $mod->modfullname);
            }
        } else {
//added by nihar to show facy box in course view page
            $linkclasses .= ' dimmed fancybox fancybox.iframe';
            $textclasses .= ' dimmed_text fancybox fancybox.iframe';
        }

// Get on-click attribute value if specified and decode the onclick - it
// has already been encoded for display (puke).
        $onclick = htmlspecialchars_decode($mod->onclick, ENT_QUOTES);

        $groupinglabel = $mod->get_grouping_label($textclasses);

        $activitylink = html_writer::empty_tag('img', array('src' => $mod->get_icon_url(),
                    'class' => 'iconlarge activityicon', 'alt' => ' ', 'role' => 'presentation')) . $accesstext .
                html_writer::tag('span', $instancename . $altname, array('class' => 'Instan instancename'));
        if ($mod->uservisible) {
            if (($COURSE->format == 'eduopen') || ($COURSE->format == 'topcoll')) {
                $Url = $url . '&section=' . $modsection->section;
            } else {
                $Url = $url;
            }
            $output .= html_writer::link($Url, $activitylink, array('class' => 'instMod-' . $mod->id . $linkclasses, 'onclick' => $onclick)) .
                    $groupinglabel;
        } else {
// We may be displaying this just in order to show information
// about visibility, without the actual link ($mod->uservisible)
            $output .= html_writer::tag('div', $activitylink, array('class' => $textclasses)) .
                    $groupinglabel;
        }
        return $output;
    }
}
