<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/layout.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/eduopen/js/easyResponsiveTabs.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/eduopen/js/eduopen.js'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot . '/eduopen/css/easy-responsive-tabs.css'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $DB, $CFG, $USER;
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("includes/head.php"); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(array('full-width')); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
             <header class="header container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->

                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php include('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header>
            <!--header end-->
        </div>
        <section id="container" class="hr-menu">
            <section id="main-content">
                <section class="wrapper">
                    <div id="page" class="container-fluid cmediaqry frontcont">
                        <div id="page-content" class="row-fluid">				

                            <div id="<?php echo $regionbsid ?>">
                                <div class="col-md-10 brdhide"><?php //echo $OUTPUT->navbar();      ?></div>
                                <div class="col-md-2"><?php echo $OUTPUT->page_heading_button(); ?></div>
                                <div class="row-fluid">
                                    <section id="region-main">
                                        <?php
                                        echo $OUTPUT->course_content_header();
                                        echo $OUTPUT->main_content();
                                        echo $OUTPUT->course_content_footer();
                                        ?>
                                    </section>
                                    <?php //echo $OUTPUT->blocks('side-pre', 'col-md-4 desktop-first-column');  ?>
                                </div>
                            </div>
                            <?php //echo $OUTPUT->blocks('side-post', 'col-md-3');  ?>
                        </div>
                    </div>
                </section>
            </section>
            <?php //include($CFG->dirroot."/eduopen/includes/footer.php");  ?>
            <!-- <footer id="page-footer">
    <div id="course-footer"><?php //echo $OUTPUT->course_footer();      ?></div>
    <p class="helplink"><?php //echo $OUTPUT->page_doc_link();      ?></p>
            <?php
            // echo $html->footnote;
            // echo $OUTPUT->login_info();
            // echo $OUTPUT->home_link();
            // echo $OUTPUT->standard_footer_html();
            ?>
</footer> -->
        </section>
        <?php include("includes/footersetting.php"); ?>
        <?php include("includes/foot.php"); ?>
        <?php echo $OUTPUT->standard_end_of_body_html() ?>
    </body>
</html>
<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>