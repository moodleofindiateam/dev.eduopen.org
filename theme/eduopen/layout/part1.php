<div class="row" id="rowmrglr1">
    <h2><center><?php print_string('featured_courses', 'theme_eduopen');?></center></h2>
    <hr>
</div>
<div class="row" id="rowmrglr2">
    <?php
    $coursedetails = "SELECT c.id, c.fullname, c.startdate, ce.courseimage, ce.language,
    ce.contextid, ce.institution, ce.featurecourse, ce.engtitle, ce.certificate2,
    ce.formalcredit FROM {course} c LEFT JOIN {course_extrasettings_general} ce
    ON c.id=ce.courseid WHERE c.category!=0 and c.visible=1 and ce.featurecourse=1 ORDER BY c.startdate ASC LIMIT 8";
    $rs = $DB->get_records_sql($coursedetails);
    $count = 0;
    if ($rs) {
        foreach ($rs as $details) {
            $courseid = $details->id;
            $coursename = $details->fullname;// Comes from Course edit settings.
            $coursetitle = $details->engtitle;// Comes from extra settings.
            $startdatetimestamp = $details->startdate; //course start date.
            $dateafteramonth = time() + (30 * 24 * 60 * 60); // date after 1 month.
            $feature = $details->featurecourse;
            $exam = $details->formalcredit;
            $institute = $details->institution;
            $csettingsinstitutionid = $DB->get_record('block_eduopen_master_inst',
            array('id' => $institute), 'name');
            if ($csettingsinstitutionid) {
                $instname = $csettingsinstitutionid->name;
            }
            $cstartdate = date("F d, Y", $startdatetimestamp);
            $startdate = gmdate("M dS", $startdatetimestamp);
            $courseimageid = $details->courseimage;
            $contextid = $details->contextid;
            $defaultimage = $CFG->wwwroot.'/theme/eduopen/pix/default_course.png';
            //$imageurl = get_uploaded_image($courseimageid, $contextid);
            $imageurl = csetting_images($details);
            
            $imageurl = $imageurl ? $imageurl : $defaultimage;
            $count++;
    ?>
    <div class="col-md-3 col-sm-6 col-xs-12 textcenter pad0A">
        <div class="box-shadow">
            <div class="course-img front_cimg<?php echo $details->certificate2;?>">
                <a class="eduopen-link" 
                href="<?php echo $CFG->wwwroot.'/eduopen/course_details.php?courseid='.$courseid ?>">
                    <img src="<?php echo $imageurl ?>" width="100%" alt='<?php echo $coursename ?>'>
                </a>
                <?php if ($details->certificate2 == '1') { ?>
                <div class="banner"><?php echo "VERIFIED";?></div>
                <?php } else { ?>
                <div class="empty_ban"></div>
                <?php } ?>
            </div>
            <div class="course-details">
                <a class="eduopen-link" 
                href="<?php echo $CFG->wwwroot.'/eduopen/course_details.php?courseid='.$courseid ?>">
                    <p><?php echo strip_tags($coursetitle); ?></p>
                    <?php if ($details->language != 'en') {?>
                    <h5 id="tsfont"><?php echo strip_tags($coursename); ?></h5>
                    <?php } ?>
                </a>
            </div>
            <div id="f_inst" class="ban<?php echo $details->certificate2; ?>" class="padl5">
                <a class="eduopen-link"
                href="<?php echo $CFG->wwwroot.'/eduopen/institution_details.php?institutionid='.$institute ?>">
                    <p class="instclr"><?php echo strip_tags($instname) ?></p>
                </a>
            </div>
            
            <div class="col-md-12 course-date-btn start_time padl5">
                <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                    <div class="col-md-12 pad0A">
                        <?php
                        $textc = get_string('cs_start_c', 'theme_eduopen');//current
                        $textu = get_string('cs_start_u', 'theme_eduopen');//upcoming
                        $textcs = get_string('cs_start_cs', 'theme_eduopen');//cooming soon
                        if ($startdatetimestamp < time()) {
                                $alldates = $textc;
                                $class = 'cu';
                            } else if ($startdatetimestamp > time() &&
                            $startdatetimestamp < $dateafteramonth) {
                                $alldates = $textcs;
                                $class = 'cs';
                            } else if ($startdatetimestamp > $dateafteramonth) {
                                $alldates = $textu;
                                $class = 'upcom';
                            }
                        ?>
                        <div class="availibility time <?php echo $class;?>">
                            <?php echo $alldates; ?>
                        </div>
                        <div class="date cstarts padl5">
                            <?php
                            $textdate = get_string('cs_start_u', 'theme_eduopen');
                            //echo get_string('stars_on', 'theme_eduopen').'  '.$cstartdate;
                            echo $cstartdate;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-2 col-sm-2 col-xs-2  fp_exam1 padl5">
                    <?php if ($exam == 1) { ?>
                    <img src="<?php echo $CFG->wwwroot.'/theme/eduopen/pix/exam.jpg'; ?>">
                    <?php } ?>
                </div>
            </div>
            <!-- <?php //if ($feature == 1) { ?>
            <div class="course-details">
                <p>
                    <span class="label label-info"> 
                        <?php //print_string('feature', 'theme_eduopen'); ?>
                    </span>
                </p>
            </div>
            <?php //} ?> -->
        </div>
    </div>
    <?php
    if (($count % 4) == 0) {
        echo "</div>";
    }
    if (($count % 4) == 0) {
        echo "<div class='row ' id ='rowmrglr2'>";
    }
    }
    }
?>
</div><!--end of rowmrglr2-->
<div class="row" id="rowmrglr3">
    <a class="btn btn-info" href="<?php echo $CFG->wwwroot.'/eduopen/catalog.php?mode=browseallcourses' ?> ">
        <?php print_string('hmview', 'theme_eduopen') ?>
    </a>
</div>