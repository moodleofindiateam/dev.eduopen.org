<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
require_once(dirname(dirname(dirname(dirname(__FILE__)))) . '/blocks/crsnavbuttons/footer.php');

$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $PAGE, $DB, $CFG, $USER;
$PAGE->requires->jquery();
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("includes/head.php"); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(array('full-width')); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
            <header class="header container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->

                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php include('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header>
            <!--header end-->
        </div>
        <section id="container" class="hr-menu"> 
            <section id="main-content">
                <section class="wrapper">
                    <div id="page" class="container-fluid cmediaqry frontcont">
                        <div id="page-content" class="row-fluid">
                            <?php
                            $crscontext = context_course::instance($COURSE->id);
                            $student = get_student($COURSE->id, $USER->id, $crscontext->id);
                            if (strpos($PAGE->bodyid, 'eduplayer')) {
                                $padding = 'pad0A';
                            } else {
                                $padding = '';
                            }
                            if (!$student) {
                                ?>
                                <div id="<?php echo $regionbsid ?>" class="col-md-12 <?php echo $padding ?>">
                                    <div class="col-md-9">
                                        <?php
                                        if (($COURSE->format == 'eduopen') || ($COURSE->format == 'topcoll')) {
                                            echo $OUTPUT->custom_navbar();
                                        } else {
                                            echo $OUTPUT->navbar();
                                        }
                                        ?>
                                    </div>
                                    <?php
                                    if (strpos($PAGE->bodyid, 'eduplayer')) {
                                        $toggebut = '';
                                    } else {
                                        $toggebut = 'toggleblockdiv';
                                    }
                                    ?>
                                    <div class="col-md-3 <?php echo $toggebut ?>"><?php echo $OUTPUT->page_heading_button(); ?></div>
                                    <div class="row-fluid">
                                        <?php if ((strpos($PAGE->bodyid, 'eduplayer')) || (strpos($PAGE->bodyid, 'edustream')) || (strpos($PAGE->bodyid, 'enrol'))) { ?>
                                            <section id="region-main" class="col-md-12 pad0A">
                                                <?php
                                                echo $OUTPUT->course_content_header();
                                                echo $OUTPUT->main_content();
                                                echo $OUTPUT->course_content_footer();
                                                ?>
                                            </section>
                                        <?php } else { ?>
                                            <?php
                                            if (get_user_preferences('theme_eduopen_zoom') == 'zoomin') {
                                                $wdth = 'width100';
                                            } else {
                                                $wdth = '';
                                            }
                                            ?>
                                            <section id = "region-main" class = "col-md-9 <?php echo $wdth ?>">   
                                                <?php
                                                echo $OUTPUT->course_content_header();
                                                ?>
                                                <section id="region-content">
                                                    <?php echo $OUTPUT->blocks('center-post'); ?>
                                                </section>
                                                <?php
                                                echo $OUTPUT->main_content();
                                                echo $OUTPUT->course_content_footer();
                                                ?>
                                            </section>
                                            <?php
                                            if ((strpos($PAGE->bodyid, 'eduplayer')) || (strpos($PAGE->bodyid, 'edustream')) || (strpos($PAGE->bodyid, 'enrol'))) {
                                                
                                            } else {
                                                if (get_user_preferences('theme_eduopen_zoom') == 'zoomin') {
                                                    echo $OUTPUT->blocks('side-pre', 'col-md-3 hide desktop-first-column');
                                                } else {
                                                    echo $OUTPUT->blocks('side-pre', 'col-md-3 desktop-first-column');
                                                }
                                            }
                                            ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <!-- added by Shiuli  -->
                                <?php
                            } else if ($student) {
                                if ($PAGE->cm->modname == 'eduplayer') {
                                    $PreCLS = 'PrevBt';
                                    $NextCLS = 'NextBt';
                                } else {
                                    $PreCLS = '';
                                    $NextCLS = '';
                                }
                                ?>
                                <?php if ($PAGE->cm->id) { ?>
                                    <div id="<?php echo $regionbsid ?>" class="col-md-12 <?php echo $padding; ?>">
                                        <!-- added by Shiuli.-->
                                        <div class="col-md-12 col-sm-12 col-xs-12 " id="navbt">
                                            <?php
                                            if ($PAGE->cm->modname == 'eduplayer') {
                                                $colPrev = 'col-md-4 col-sm-4 col-xs-4';
                                                $colMid = 'col-md-4 col-sm-4 col-xs-4';
                                                $colNext = 'col-md-4 col-sm-4 col-xs-4';
                                            } else {
                                                $colPrev = 'col-md-2 col-sm-2 col-xs-2';
                                                $colMid = 'col-md-8 col-sm-2 col-xs-2';
                                                $colNext = 'col-md-2 col-sm-2 col-xs-2';
                                            }
                                            ?>
                                            <div class="<?php echo $colPrev . ' ' . $PreCLS; ?>" id="prevbtn">
                                                <?php
                                                if ($PAGE->cm->modname == 'eduplayer') {
                                                    echo draw_crsprevbuttons($COURSE->id);
                                                }
                                                ?>
                                            </div>
                                            <div class="<?php echo $colMid; ?>" id="crsbackbtn">
                                                <p><?php echo custom_section_name($COURSE->id); ?></p>
                                                <p class="back"><?php echo draw_crsbackbuttons($COURSE->id); ?></p>
                                            </div>
                                            <div class="<?php echo $colNext . ' ' . $NextCLS; ?>" id="nextbtn">
                                                <?php
                                                if ($PAGE->cm->modname == 'eduplayer') {
                                                    echo draw_crsnextbuttons($COURSE->id);
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="row-fluid">
                                            <?php
                                            if ($PAGE->pagetype == 'mod-quiz-attempt') {
                                                echo $OUTPUT->blocks('side-pre', 'col-md-4');
                                                ?>
                                                <section id="region-main" class="col-md-8 pad0A">
                                                <?php } else { ?>
                                                    <section id="region-main" class="col-md-12 pad0A">
                                                        <?php
                                                    }
                                                    echo $OUTPUT->course_content_header();
                                                    echo $OUTPUT->main_content();
                                                    echo $OUTPUT->course_content_footer();
                                                    ?>
                                                </section>
                                        </div>
                                    </div>
                                <?php } ?>

                                <?php
                            } else {
                                echo $OUTPUT->main_content();
                            }
                            ?>
                            <!-- end shiuli  -->
                        </div>
                    </div>
                </section>
            </section>
        </section>
        <?php include("includes/footersetting.php"); ?>
        <?php echo $OUTPUT->standard_end_of_body_html() ?>
        <?php include("includes/foot.php"); ?>
    </body>
</html>
<script>
    /*$("#PreArrow").click(function () {
     document.getElementById("PreviousBtn").submit();
     });
     $("#NextArrow").click(function () {
     document.getElementById("NextBtn").submit();
     });*/
</script>
<script type="text/javascript">
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>

