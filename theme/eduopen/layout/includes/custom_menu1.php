<?php
global $PAGE;
if (empty($CFG->authloginviaemail)) {
    $strusername = get_string('username');
} else {
    $strusername = get_string('usernameemail');
}
?>
<script>
    var page = {baseurl: "<?php echo $CFG->wwwroot ?>"};
</script>
<li class="toplistnone">
    <div class="top-icon-bar dropdown" id="topmenudiv">
        <a class="top-menu-link" href="" title="" class="user-ico clearfix" data-toggle="dropdown" >
            <?php
            if (isloggedin()) {//If the user logged in display separate menu MAIN IF
                global $USER, $DB;
                $visibleRole = visible_roles();
                $sessionkey = sesskey();
                $userid = $USER->id;
                $user = $DB->get_record('user', array('id' => $userid));
                $userFname = $user->firstname;
                $userLname = $user->lastname;
                /* $username = strip_tags($username);

                  if (strlen($username) > 8) {

                  // truncate string
                  $usernameCut = substr($username, 0, 8);
                  $username= $usernameCut.'...';
                  }
                 */
                //to get the pathway block instance id &pass it in path way url--added by nihar
                $pathwayinstance = $DB->get_field_sql("SELECT id FROM {block_instances} 
                    WHERE blockname = 'specialization' AND pagetypepattern = 'admin-index'");
                $pathwayblockcontext = CONTEXT_BLOCK::instance($pathwayinstance);
                //to get the institution block instance id &pass it in institution block url--added by nihar
                $institutioninstance = $DB->get_field_sql("SELECT id FROM {block_instances} 
                    WHERE blockname = 'institution' AND pagetypepattern = 'admin-index'");
                $instblockcontext = CONTEXT_BLOCK::instance($institutioninstance);
                $username = ucfirst($userFname) . " " . $userLname;
                ?>
                <span class="tooltip-button pad5l" data-placement="right" data-original-title="<?php echo $user->firstname; ?>"><?php echo $username; ?></span> <?php echo $OUTPUT->user_picture($user, array('size' => 35)); ?>
                <i class="fa fa-sort-desc pad5l"></i>                   
            </a>                                
            <?php
            if (is_siteadmin()) {
                ?>                                                          
                <ul id="main_menu" class="dropdown-menu AdminUserView">
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/my' ?>" title="">
                            <i class="fa fa-tachometer pad10R"></i>
                            <?php print_string('mydashboard', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/admin' ?>" title="">
                            <i class="fa fa-adn pad10R"></i>
                            <?php print_string('myadmin', 'theme_eduopen'); ?>
                        </a>
                    </li>

                    <!-- My analytics Portion -->
                    <li class="dropdown-submenu sub_menu_register1 float-left" id="sub_menu_register">                              
                        <a class="afterCont" href="javascript:;" title=""><i class="fa fa-bar-chart pad10R"></i>
                            <?php echo get_string('myanalytics', 'theme_eduopen'); ?>
                        </a>
                        <ul class="dropdown-menu sbmemuhide" id="sbmemu">
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/site/favourites.php' ?>" title="">
                                    <?php print_string('myfav', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li class="dropdown-submenu float-left subsubmenu1" id="sub_memus_register">
                                <a class="arrowTog" href="javascript:;" title="">
                                    <?php print_string('sitelevel', 'theme_eduopen'); ?>
                                </a>
                                <ul class="dropdown-menu sitelev sbmemushide" id="sbmemus">
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/site/site_overview.php' ?>" title="">
                                            <?php print_string('siteover', 'theme_eduopen'); ?>
                                        </a>
                                    </li>                             
                                </ul>
                            </li>
                            <li class="dropdown-submenu float-left subsubmenu2" id="sub_memus_register">
                                <a class="arrowTog" href="javascript:;" title="">
                                    <?php print_string('crslevel', 'theme_eduopen'); ?>
                                </a>
                                <ul class="dropdown-menu crslev sbmemushide" id="sbmemus">
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/course/course_overview.php' ?>" title="">
                                            <?php print_string('crsover', 'theme_eduopen'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/course/course_level_report.php' ?>" title="">
                                            <?php print_string('crslevelreport', 'theme_eduopen'); ?>
                                        </a>
                                    </li>                               
                                </ul>
                            </li>
                            <li class="dropdown-submenu float-left subsubmenu3" id="sub_memus_register">
                                <a class="arrowTog" href="javascript:;" title="">
                                    <?php print_string('userlevel', 'theme_eduopen'); ?>
                                </a>
                                <ul class="dropdown-menu userlev sbmemushide" id="sbmemus">
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/user/user_search.php'?>" title="">
                                            <?php print_string('userlevelreport', 'theme_eduopen'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/user/user_completions.php' ?>" title="">
                                            <?php print_string('usercompletion', 'theme_eduopen'); ?>
                                        </a>
                                    </li>                               
                                </ul>
                            </li>                            
                        </ul>
                    </li>
                    <!-- My analytics Portion end -->

                    <li class="dropdown-submenu sub_menu_register2 float-left" id="sub_menu_register">                              
                        <a class="afterCont" href="javascript:;" title=""><i class="fa fa-university pad10R"></i>
                            <?php echo get_string('manageinstitution', 'theme_eduopen'); ?>
                        </a>
                        <ul class="dropdown-menu sbmemuhide" id="sbmemu">
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/blocks/institution/add_institution.php?context=' . $instblockcontext->id ?>" title="">
                                    <?php print_string('addinstitution', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/blocks/institution/view_institution.php?context=' . $instblockcontext->id ?>" title="">
                                    <?php print_string('viewinstitution', 'theme_eduopen'); ?>
                                </a>
                            </li>                               
                        </ul>
                    </li>
                    <li class="dropdown-submenu sub_menu_register3 float-left" id="sub_menu_register">                              
                        <a class="afterCont" href="javascript:;" title=""> <i class="fa fa-connectdevelop pad10R"></i>
                            <?php echo get_string('managepathways', 'theme_eduopen'); ?>
                        </a>
                        <ul class="dropdown-menu sbmemuhide" id="sbmemu">
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/blocks/specialization/add_pathway.php?context=' . $pathwayblockcontext->id ?>" title="">
                                    <?php echo get_string('add', 'block_specialization'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/blocks/specialization/view_pathway.php?context=' . $pathwayblockcontext->id ?>" title="">
                                    <?php echo get_string('view', 'block_specialization'); ?>
                                </a>
                            </li>                               
                        </ul>
                    </li>
                    <li class="dropdown-submenu sub_menu_register4 float-left" id="sub_menu_register">                              
                        <a class="afterCont" href="javascript:;" title=""><i class="fa fa-star-o pad10R"></i>
                            <?php print_string('managefaqs', 'theme_eduopen'); ?>
                        </a>
                        <ul class="dropdown-menu sbmemuhide" id="sbmemu">
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/blocks/eduopen_faq/create_faq.php' ?>" title="">
                                    <?php print_string('addfaqs', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/blocks/eduopen_faq/view_faq.php' ?>" title="">
                                    <?php print_string('viewfaqs', 'theme_eduopen'); ?>
                                </a>
                            </li>                               
                        </ul>
                    </li>
                    <li class="dropdown-submenu sub_menu_register4 float-left" id="sub_menu_register">                              
                        <a class="afterCont" href="javascript:;" title=""><i class="fa fa-book pad10R"></i>
                            <?php print_string('news_letter', 'theme_eduopen'); ?>
                        </a>
                        <ul class="dropdown-menu sbmemuhide" id="sbmemu">
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/newsletter/add_domain.php' ?>" title="">
                                    <?php print_string('add_domain', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/newsletter/add_news_letter.php' ?>" title="">
                                    <?php print_string('add_news_letter', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/newsletter/view_domain.php' ?>" title="">
                                    <?php print_string('view_domain', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/newsletter/maillist.php' ?>" title="">
                                    <?php print_string('mail_lists', 'theme_eduopen'); ?>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/admin/user.php'; ?>" title="">
                            <i class="fa fa-users pad10R"></i>
                            <?php print_string('browselistofusers', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/course/management.php'; ?>" title="">
                            <i class="fa fa-cogs pad10R"></i>
                            <?php print_string('managecoursescategory', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/profile.php?id=' . $userid ?>" title="">
                            <i class="fa fa-user pad10R"></i>
                            <?php print_string('myprofiles', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/preferences.php' ?>" title="">
                            <i class="fa fa-cog pad10R"></i>
                            <?php print_string('mypreference', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/message/index.php' ?>" title="">
                            <i class="fa fa-envelope pad10R"></i>
                            <?php print_string('messages', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/files.php' ?>" title="">
                            <i class="fa fa-file pad10R"></i>
                            <?php print_string('myprivatefiles', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/login/logout.php?sesskey=' . $sessionkey ?>" title="">
                            <i class="fa fa-sign-out pad10R"></i>
                            <?php print_string('logout', 'theme_eduopen'); ?>
                        </a>
                    </li>                       
                </ul>
            <?php } else if ($visibleRole) {
                ?>
                <ul id="main_menu" class="dropdown-menu TeacherUserView">
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/my' ?>" title="">
                            <i class="fa fa-tachometer pad10R"></i>
                            <?php print_string('mydashboard', 'theme_eduopen'); ?>
                        </a>
                    </li>

                    <!-- My analytics Portion -->
                    <li class="dropdown-submenu sub_menu_register1 float-left" id="sub_menu_register">                              
                        <a class="afterCont" href="javascript:;" title=""><i class="fa fa-bar-chart pad10R"></i>
                            <?php echo get_string('myanalytics', 'theme_eduopen'); ?>
                        </a>
                        <ul class="dropdown-menu sbmemuhide" id="sbmemu">
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/site/favourites.php' ?>" title="">
                                    <?php print_string('myfav', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li class="dropdown-submenu float-left subsubmenu2" id="sub_memus_register">
                                <a class="arrowTog" href="javascript:;" title="">
                                    <?php print_string('crslevel', 'theme_eduopen'); ?>
                                </a>
                                <ul class="dropdown-menu crslev sbmemushide" id="sbmemus">
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/course/course_level_report.php' ?>" title="">
                                            <?php print_string('crslevelreport', 'theme_eduopen'); ?>
                                        </a>
                                    </li>                               
                                </ul>
                            </li>
                            <li class="dropdown-submenu float-left subsubmenu3" id="sub_memus_register">
                                <a class="arrowTog" href="javascript:;" title="">
                                    <?php print_string('userlevel', 'theme_eduopen'); ?>
                                </a>
                                <ul class="dropdown-menu userlev sbmemushide" id="sbmemus">
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/user/user_search.php' ?>" title="">
                                            <?php print_string('userlevelreport', 'theme_eduopen'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/user/user_completions.php' ?>" title="">
                                            <?php print_string('usercompletion', 'theme_eduopen'); ?>
                                        </a>
                                    </li>                               
                                </ul>
                            </li>                           
                        </ul>
                    </li>
                    <!-- My analytics Portion end -->
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/profile.php?id=' . $userid ?>" title="">
                            <i class="fa fa-user pad10R"></i>
                            <?php print_string('myprofiles', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/preferences.php' ?>" title="">
                            <i class="fa fa-cog pad10R"></i>
                            <?php print_string('mypreference', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/message/index.php' ?>" title="">
                            <i class="fa fa-envelope pad10R"></i>
                            <?php print_string('messages', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/files.php' ?>" title="">
                            <i class="fa fa-file pad10R"></i>
                            <?php print_string('myprivatefiles', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/login/logout.php?sesskey=' . $sessionkey ?>" title="">
                            <i class="fa fa-sign-out pad10R"></i>
                            <?php print_string('logout', 'theme_eduopen'); ?>
                        </a>
                    </li>                       
                </ul>
            <?php } else {
                ?>
                <ul id="main_menu" class="dropdown-menu StudentUserView">
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/my' ?>" title="">
                            <i class="fa fa-tachometer pad10R"></i>
                            <?php print_string('mydashboard', 'theme_eduopen'); ?>
                        </a>
                    </li>

                    <!-- My analytics Portion -->
                    <li class="dropdown-submenu sub_menu_register1 float-left" id="sub_menu_register">                              
                        <a class="afterCont" href="javascript:;" title=""><i class="fa fa-bar-chart pad10R"></i>
                            <?php echo get_string('myanalytics', 'theme_eduopen'); ?>
                        </a>
                        <ul class="dropdown-menu sbmemuhide" id="sbmemu">
                            <li>
                                <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/site/favourites.php' ?>" title="">
                                    <?php print_string('myfav', 'theme_eduopen'); ?>
                                </a>
                            </li>
                            <li class="dropdown-submenu float-left subsubmenu3" id="sub_memus_register">
                                <a class="arrowTog" href="javascript:;" title="">
                                    <?php print_string('userlevel', 'theme_eduopen'); ?>
                                </a>
                                <ul class="dropdown-menu userlev sbmemushide" id="sbmemus">
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/user/user_info.php?id='.$userid ?>" title="">
                                            <?php print_string('userlevelreport', 'theme_eduopen'); ?>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php echo $CFG->wwwroot . '/local/myanalytics/user/user_completions.php' ?>" title="">
                                            <?php print_string('usercompletion', 'theme_eduopen'); ?>
                                        </a>
                                    </li>                               
                                </ul>
                            </li>                           
                        </ul>
                    </li>
                    <!-- My analytics Portion end -->

                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/profile.php?id=' . $userid ?>" title="">
                            <i class="fa fa-user pad10R"></i>
                            <?php print_string('myprofiles', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/preferences.php' ?>" title="">
                            <i class="fa fa-cog pad10R"></i>
                            <?php print_string('mypreference', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/message/index.php' ?>" title="">
                            <i class="fa fa-envelope pad10R"></i>
                            <?php print_string('messages', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/user/files.php' ?>" title="">
                            <i class="fa fa-file pad10R"></i>
                            <?php print_string('myprivatefiles', 'theme_eduopen'); ?>
                        </a>
                    </li>
                    <li>
                        <a class="" href="<?php echo $CFG->wwwroot . '/login/logout.php?sesskey=' . $sessionkey ?>" title="">
                            <i class="fa fa-sign-out pad10R"></i>
                            <?php print_string('logout', 'theme_eduopen'); ?>
                        </a>
                    </li>                       
                </ul>           
                <?php
            }
        } else if ($PAGE->pagelayout != 'login') {
            ?>                                                  
            <a href ="<?php echo $CFG->wwwroot . '/login/index.php' ?>" title="">
                <button type="button" id="logbutton">Log in</button>
            </a>
        </div>
    </li>

    <?php
} else {
    ?>
    <a href="<?php echo $CFG->wwwroot ?>">
        <button type="button" id="homebtn">Home</button>
    </a>
    <?php
}
?>

<script>
    $(window).load(function () {
        var width1 = $(window).width();
        if (width1 < 767) {
            $("#sub_menu_register .afterCont").append("<i class='Common fa fa-plus pull-right'></i>");
            $("#sub_memus_register .arrowTog").append("<i class='SubCommon fa fa-plus pull-right'></i>");
        }
        $(".sub_menu_register1, .sub_menu_register2, .sub_menu_register3, .sub_menu_register4").click(function () {
            var width2 = $(window).width();
            if (width2 < 767) {
                $(this).children('#sbmemu').toggleClass("sbmemushow");
                $(this).find('.Common').toggleClass('fa-minus', 'fa-plus');
            }
        });
        $(".subsubmenu1, .subsubmenu2, .subsubmenu3").click(function () {
            var width3 = $(window).width();
            if (width3 < 767) {
                $(this).find('.SubCommon').toggleClass('fa-minus', 'fa-plus');
                $("#sub_menu_register").find('.Common').toggleClass('fa-minus', 'fa-plus');
                $(this).children('#sbmemus').toggleClass("sbmemusshow");
                $(this).parent('#sbmemu').removeClass("sbmemuhide");
                $(this).parent('#sbmemu').toggleClass("sbmemushow");
                $(this).children('#sbmemus').removeClass("sbmemushide");
            }
        });
    });

    $(window).resize(function () {
        $(".sub_menu_register1, .sub_menu_register2, .sub_menu_register3, .sub_menu_register4").click(function () {
            var width4 = $(window).width();
            if (width4 < 767) {
                $(this).children('#sbmemu').toggleClass("sbmemushow");
                $(this).find('.Common').toggleClass('fa-minus', 'fa-plus');
            }
        });
        $(".subsubmenu1, .subsubmenu2, .subsubmenu3").click(function () {
            var width5 = $(window).width();
            if (width5 < 767) {
                $(this).find('.SubCommon').toggleClass('fa-minus', 'fa-plus');
                $("#sub_menu_register").find('.Common').toggleClass('fa-minus', 'fa-plus');
                $(this).children('#sbmemus').toggleClass("sbmemusshow");
                $(this).parent('#sbmemu').removeClass("sbmemuhide");
                $(this).parent('#sbmemu').toggleClass("sbmemushow");
                $(this).children('#sbmemus').removeClass("sbmemushide");
            }
        });
    });
</script>
