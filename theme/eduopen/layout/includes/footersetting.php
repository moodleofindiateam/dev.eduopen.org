<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 *
 * @package    theme
 * @subpackage bcu
 * @copyright  2014 Birmingham City University <michael.grant@bcu.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
$curlang = current_language();
?>

<footer id="page-footer" class="settingfooter container-fluid">
    <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
    <div class="container-fluid" id="page-foot">
        <div class="row-fluid col-md-12 col-sm-12 col-xs-12" id="pagefoot">

            <div class="size4of16 col-md-3 col-sm-3 col-xs-12 pad0A" id="footer_Policies">
                <h3 title="Policies"><?php echo get_string('foot_policies', 'theme_eduopen'); ?></h3>
                <p align = "left">
                    <a href="http://www.eduopen.org/privacy-policies-and-terms-of-use.html">
                        <?php echo get_string('foot_policies_content1', 'theme_eduopen'); ?>
                    </a>
                </p>
                <p align = "left">
                    <a href="<?php echo $CFG->wwwroot . '/eduopen/information-en.php' ?>">
                        <?php echo get_string('foot_policies_content2', 'theme_eduopen'); ?>
                    </a>
                </p>
                <p align = "left">
                    <a href="<?php echo $CFG->wwwroot . '/eduopen/information-it.php' ?>">
                        <?php echo get_string('foot_policies_content3', 'theme_eduopen'); ?>
                    </a>
                </p>
                <p align = "left">
                    <a href="<?php echo $CFG->wwwroot . '/eduopen/privacy-en.php' ?>">
                        <?php echo get_string('foot_policies_content4', 'theme_eduopen'); ?>
                    </a>
                </p>
                <p align = "left">
                    <a href="<?php echo $CFG->wwwroot . '/eduopen/privacy-it.php' ?>">
                        <?php echo get_string('foot_policies_content5', 'theme_eduopen'); ?>
                    </a>
                </p>
            </div>

            <div class="size4of16 col-md-2 col-sm-2 col-xs-12 pad0A" id="footer-Partners">
                <h3 title="Partners"><?php echo get_string('foot_partners', 'theme_eduopen'); ?></h3>
                <p align = "left">
                    <a href="http://lmsofindia.com">
                        <?php echo get_string('foot_partners_content1', 'theme_eduopen'); ?>
                    </a>
                </p>
                <p align = "left">
                    <a href="http://cineca.it">
                        <?php echo get_string('foot_partners_content2', 'theme_eduopen'); ?>
                    </a>
                </p>
                <p align = "left">
                    <a href="http://www.garr.it">
                        <?php echo get_string('foot_partners_content3', 'theme_eduopen'); ?>
                    </a>
                </p>
                <p align = "left">
                    <a href="http://uki.blackboard.com/">
                        <?php echo get_string('foot_partners_content4', 'theme_eduopen'); ?>
                    </a>
                </p>
            </div>

            <div class="size4of16 col-md-3 col-sm-3 col-xs-12 pad0A" id="footer_Acknowledgements">
                <h3 title="Acknowledgements"><?php echo get_string('foot_acknowledgements', 'theme_eduopen'); ?></h3>
                <p align = "left">
                    <?php
                    if ($curlang == 'en') {
                        echo get_string('foot_acknowledgements_content1', 'theme_eduopen');
                    } else if ($curlang == 'it') {
                        echo get_string('foot_acknowledgements_content1_it', 'theme_eduopen');
                    }
                    
                    ?>
                </p>
                <p align = "left">
                    <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/miur_logo.png' ?>">
                </p>
            </div>
            <div class="col-md-1 col-sm-1 col-xs-12 pad0A" id=""></div>

            <div class="size4of16 col-md-3 col-sm-3 col-xs-12 pad0A" id="footer_Contact_us">
                <h3 title="Contact Us">
                    <?php
                    if ($curlang == 'en') {
                        echo get_string('foot_contactus', 'theme_eduopen');
                    } else if ($curlang == 'it') {
                        echo get_string('foot_contactus_it', 'theme_eduopen');
                    }
                    ?>
                </h3>
                <div align = "left" class="col-md-12 col-sm-12 col-xs-12 pad0A">
                    <div class="col-md-1 col-sm-2 col-xs-2 pad0A">
                        <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/place.svg' ?>">
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-10 pad0A">
                        <p><?php echo get_string('foot_contactus_content1', 'theme_eduopen'); ?></p>
                    </div>
                </div>
                <div align = "left" class="col-md-12 col-sm-12 col-xs-12 pad0A">
                    <div class="col-md-1 col-sm-2 col-xs-2 pad0A">
                        <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/post_office.svg' ?>">
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-10 pad0A">
                        <p><?php echo get_string('foot_contactus_content2', 'theme_eduopen'); ?></p>
                    </div>
                </div>
                <div align = "left" class="col-md-12 col-sm-12 col-xs-12 pad0A">
                    <div class="col-md-1 col-sm-2 col-xs-2 pad0A">
                        <img class="img-responsive" src="<?php echo $CFG->wwwroot ?>/theme/eduopen/pix/support.png">
                    </div>
                    <div class="col-md-11 col-sm-2 col-xs-2 pad0A ">
                        <p>
                            <a href="http://support.eduopen.org">
                                <?php echo get_string('foot_contactus_content2a', 'theme_eduopen'); ?>
                            </a>
                        </p>
                    </div>
                </div>
                <div align = "left" class="col-md-12 col-sm-12 col-xs-12 pad0A">
                    <div class="col-md-1 col-sm-2 col-xs-2 pad0A">
                        <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/phone.svg' ?>">
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-10 pad0A">
                        <p><?php echo get_string('foot_contactus_content3', 'theme_eduopen'); ?></p>
                    </div>
                </div>
                <div align = "left" class="col-md-12 col-sm-12 col-xs-12 pad0A">
                    <div class="col-md-1 col-sm-2 col-xs-2 pad0A">
                        <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/web.svg' ?>">
                    </div>
                    <div class="col-md-11 col-sm-10 col-xs-10 pad0A">
                        <p><a href="http://eduopen.org"><?php echo get_string('foot_contactus_content4', 'theme_eduopen'); ?></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="info container2 clearfix" id="last_footer">
        <div class="footer-inner page ptm pbl container-fluid">
            <div class="text_to_html">
                <p><br></p>
                <hr class="horigontall">
                <p>
                    <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/">
                        <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/last_footer.png' ?>">
                    </a>
                </p>
                <p class="gray">
                    <?php echo get_string('footer_content1', 'theme_eduopen'); ?>
                </p>
                <p>
                    <span class="gray"><?php echo get_string('footer_content2', 'theme_eduopen'); ?></span>
                    <?php echo get_string('footer_content2a', 'theme_eduopen'); ?>
                </p>
                <p>
                    <?php echo get_string('footer_content3', 'theme_eduopen'); ?>
                </p>
                <br>
                <p>
                    <?php echo get_string('footer_content4', 'theme_eduopen'); ?>
                </p>
                <p>
                    <?php echo get_string('footer_content5', 'theme_eduopen'); ?>
                </p>
            </div>
        </div>
        <div class="pull-right">
            <?php
            echo $OUTPUT->standard_footer_html();
            ?>
        </div>
    </div>
</footer>