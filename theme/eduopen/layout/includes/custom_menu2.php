<li class="listnone">
    <div class="top-icon-bar dropdown" id="topmenudiv">
		<a class="top-menu-link" href="" title="" class="user-ico clearfix" data-toggle="dropdown" >
			<?php
			
			if(isloggedin()){//If the user logged in display separate menu MAIN IF
				$sessionkey = sesskey();
				$userid = $USER->id;	
				$user = $DB->get_record('user', array('id' => $userid));	
				$userFname= $user->firstname;
				$userLname =$user->lastname;
				/* $username = strip_tags($username);

				if (strlen($username) > 8) {

					// truncate string
					$usernameCut = substr($username, 0, 8);							
					$username= $usernameCut.'...'; 
				}
						 */		
			//to get the pathway block instance id &pass it in path way url--added by nihar
			$pathwayinstance = $DB->get_field_sql("SELECT id FROM {block_instances} 
				WHERE blockname = 'specialization' AND pagetypepattern = 'admin-index'");
			$pathwayblockcontext = CONTEXT_BLOCK::instance($pathwayinstance);
			//to get the institution block instance id &pass it in institution block url--added by nihar
			$institutioninstance = $DB->get_field_sql("SELECT id FROM {block_instances} 
				WHERE blockname = 'institution' AND pagetypepattern = 'admin-index'");
			$instblockcontext = CONTEXT_BLOCK::instance($institutioninstance);

			$username = ucfirst($userFname)." ".$userLname;
				
			?>
				
			<span class="tooltip-button pad5l" data-placement="right" data-original-title="<?php echo $user->firstname; ?>"><?php echo $username; ?></span> <?php echo $OUTPUT->user_picture($user, array('size' => 35)); ?>
			<i class="fa fa-sort-desc pad5l"></i>					
		</a>								
		<?php	
	    if (is_siteadmin()) {
			
		?>                                                          
        <ul id="main_menu" class="dropdown-menu">
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/my' ?>" title="">
					<i class="fa fa-tachometer pad10R"></i>
					<?php print_string('mydashboard', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/admin' ?>" title="">
					<i class="fa fa-adn pad10R"></i>
					<?php print_string('myadmin', 'theme_eduopen'); ?>
				</a>
			</li>
			<li class="dropdown-submenu float-left" id="sub_menu_register">                              
			    <a href="javascript:;" title=""><i class="fa fa-university pad10R"></i>
			    	<?php echo get_string('manageinstitution', 'theme_eduopen'); ?>
			    </a>
			    <ul class="dropdown-menu" id="sbmemu">
					<li>
						<a href="<?php echo $CFG->wwwroot.'/blocks/institution/add_institution.php?context='.$instblockcontext->id ?>" title="">
							<?php print_string('addinstitution', 'theme_eduopen'); ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $CFG->wwwroot.'/blocks/institution/view_institution.php?context='.$instblockcontext->id  ?>" title="">
							<?php print_string('viewinstitution', 'theme_eduopen'); ?>
						</a>
					</li>								
				</ul>
			</li>
            <li class="dropdown-submenu float-left" id="sub_menu_register">                              
				<a href="javascript:;" title=""> <i class="fa fa-connectdevelop pad10R"></i>
					<?php echo get_string('managepathways', 'theme_eduopen'); ?>
				</a>
				<ul class="dropdown-menu" id="sbmemu">
					<li>
						<a href="<?php echo $CFG->wwwroot.'/blocks/specialization/add_pathway.php?context='.$pathwayblockcontext->id ?>" title="">
							<?php echo get_string('add', 'block_specialization'); ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $CFG->wwwroot.'/blocks/specialization/view_pathway.php?context='.$pathwayblockcontext->id ?>" title="">
							<?php echo get_string('view', 'block_specialization'); ?>
						</a>
					</li>								
				</ul>
			</li>
            <li class="dropdown-submenu float-left" id="sub_menu_register">                              
				<a href="javascript:;" title=""><i class="fa fa-star-o pad10R"></i>
					<?php print_string('managefaqs', 'theme_eduopen'); ?>
				</a>
				<ul class="dropdown-menu" id="sbmemu">
					<li>
						<a href="<?php echo $CFG->wwwroot.'/blocks/eduopen_faq/create_faq.php' ?>" title="">
							<?php print_string('addfaqs', 'theme_eduopen'); ?>
						</a>
					</li>
					<li>
						<a href="<?php echo $CFG->wwwroot.'/blocks/eduopen_faq/view_faq.php' ?>" title="">
							<?php print_string('viewfaqs', 'theme_eduopen'); ?>
						</a>
					</li>								
				</ul>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/admin/user.php'; ?>" title="">
					<i class="fa fa-users pad10R"></i>
					<?php print_string('browselistofusers', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/course/management.php'; ?>" title="">
					<i class="fa fa-cogs pad10R"></i>
					<?php print_string('managecoursescategory', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/user/profile.php?id='.$userid ?>" title="">
					<i class="fa fa-user pad10R"></i>
					<?php print_string('myprofiles', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/message/index.php' ?>" title="">
					<i class="fa fa-envelope pad10R"></i>
					<?php print_string('messages', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/user/files.php' ?>" title="">
					<i class="fa fa-file pad10R"></i>
					<?php print_string('myprivatefiles', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/badges/mybadges.php' ?>" title="">
					<i class="fa fa-trophy pad10R"></i>
					<?php print_string('mybadges', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/login/logout.php?sesskey='.$sessionkey ?>" title="">
					<i class="fa fa-sign-out pad10R"></i>
					<?php print_string('logout', 'theme_eduopen'); ?>
				</a>
			</li>						
		</ul>
		<?php } else {  
		?>
		<ul id="main_menu" class="dropdown-menu">
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/my' ?>" title="">
					<i class="fa fa-tachometer pad10R"></i>
					<?php print_string('mydashboard', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/user/profile.php?id='.$userid ?>" title="">
					<i class="fa fa-user pad10R"></i>
					<?php print_string('myprofiles', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/message/index.php' ?>" title="">
					<i class="fa fa-envelope pad10R"></i>
					<?php print_string('messages', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/user/files.php' ?>" title="">
					<i class="fa fa-file pad10R"></i>
					<?php print_string('myprivatefiles', 'theme_eduopen'); ?>
				</a>
			</li>
			<li>
				<a class="" href="<?php echo $CFG->wwwroot.'/badges/mybadges.php' ?>" title="">
					<i class="fa fa-trophy pad10R"></i>
					<?php print_string('mybadges', 'theme_eduopen'); ?>
				</a>
			</li>
			 <li>
				<a class="" href="<?php echo $CFG->wwwroot.'/login/logout.php?sesskey='.$sessionkey ?>" title="">
					<i class="fa fa-sign-out pad10R"></i>
					<?php print_string('logout', 'theme_eduopen'); ?>
				</a>
			</li>						
		</ul> 			
		<?php }
		} else { 
		?>							 						
		<a href="javascript:;" title="">
                    <button type="button" id="logbutton">Log in</button>
		</a>
	    

	</div>
</li>

<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>
<link rel="stylesheet" href="<?php echo $CFG->wwwroot.'/login/style.css'?>"/>
<link rel="stylesheet" href="<?php echo $CFG->wwwroot.'/login/idpselect.css'?>"/>
<script src="<?php echo $CFG->wwwroot.'/login/idpselect_config.js'?>" type="text/javascript" language="javascript"></script>

 <script>
  $(function() {
    $("#tabs").tabs();
    $("#logbutton").click(function() {
	if ((/^1px/).test($("#logbutton").css("border-bottom"))) {
		$("#logbutton").css("border-bottom", "none");
	} else {
		$("#logbutton").css("border-bottom", "1px solid black");
	}
        $("#login").toggle(500);
    });
    //$("#local").click(function() { alert("Not implemented."); });
    if (typeof $("#idpSelectPreferredIdPTile").css("height") === "undefined") {
	$("div.filler").css("display", "none");
    } else {
	$("#login").css("height", "auto");
    }
  });
  </script>
  <div id="login">
	<div class="filler"></div>
	<div class="edsblock">
		<p class="smalltit">Log in with local account</p>
		<form action="<?php echo $CFG->wwwroot.'/login/index.php' ?>" method="post">
		<p class="edslabel">Username<br/>
		<input type="text" id="username" name="username" class="white"><br/>
		Password<br/>
		<input type="password" id="password" name="password" class="white"><br/>
		<input id="check" type="checkbox" class="white" checked="checked"/> Remember me</p>
		<input type="submit" id="local" value="Log in with local account" class="gobutton">
		</form>
	</div>
	<div class="edsblock inst">
		<p class="smalltit">Log in via an institution account</p>
		<div id="idpSelect"></div>
        <script src="<?php echo $CFG->wwwroot.'/login/idpselect.js'?>" type="text/javascript" language="javascript"></script>
	</div>
	<div class="col-md-12 login-register">
        <form action="/login/signup.php" method="get" id="signup">
           <button type="submit" value="Create new account" class="btn btn-danger newlearner1">Register as NEW Open Learner!</button>
        </form>
    </div>
</div>
<?php   }  ?>
