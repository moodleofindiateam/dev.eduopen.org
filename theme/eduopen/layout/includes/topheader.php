<div class="ct-topBar container-fluid header" id="">
    <div class="" id="">
        <div class="top-nav clearfix">
            <ul class="ct-panel user list-inline text-uppercase pull-left">
                <li id="email">
                    <a href="javascript:;" class="ct-js-signup">
                        <i class="fa fa-envelope"></i>
                        <span><?php echo get_string('topheader_mail', 'theme_eduopen'); ?></span>
                    </a>
                </li>
                <li id="contact">
                    <a href="http://support.eduopen.org" class="ct-js-signup" target="_blank">
                        <!--<i class="fa fa-phone-square"></i>-->
                        <span>
                            <?php echo get_string('topheader_contact', 'theme_eduopen'); ?>
                        </span>
                    </a>
                </li>        
            </ul>
            <ul class="nav pull-right top-menu rightmenu">
                <!-- user login dropdown start-->
                <li class="lmenudrop">
                    <?php echo $OUTPUT->custom_lang_menu(); ?>
                </li>
                <?php require("custom_menu1.php"); ?>
                <?php echo $OUTPUT->page_heading_menu(); ?>
            </ul>
        </div>
        <div class="ct-widget--group pull-right">
        </div>
        <div class="clearfix" id=""></div>
    </div>
</div>