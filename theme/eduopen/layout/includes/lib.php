<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */


/**
 * Parses CSS before it is cached.
 *
 * This function can make alterations and replace patterns within the CSS.
 *
 * @param string $css The CSS
 * @param theme_config $theme The theme config object.
 * @return string The parsed CSS The parsed CSS.
 */
function theme_eduopen_process_css($css, $theme) {

    // Set the background image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_eduopen_set_logo($css, $logo);

    // Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_eduopen_set_customcss($css, $customcss);
    if (!empty($theme->settings->backcolor)) {
        $backcolor = $theme->settings->backcolor;
    } else {
        $backcolor = null;
    }
    $css = theme_eduopen_set_backcolor($css, $backcolor);
     // Set the link hover color.
    if (!empty($theme->settings->linkhover)) {
        $linkhover = $theme->settings->linkhover;
    } else {
        $linkhover = null;
    }
    $css = theme_eduopen_set_linkhover($css, $linkhover);
      // Set the main color.
    if (!empty($theme->settings->maincolor)) {
        $maincolor = $theme->settings->maincolor;
    } else {
        $maincolor = null;
    }
    $css = theme_eduopen_set_maincolor($css, $maincolor);
    // Set the link color.
    if (!empty($theme->settings->linkcolor)) {
        $linkcolor = $theme->settings->linkcolor;
    } else {
        $linkcolor = null;
    }
    $css = theme_eduopen_set_linkcolor($css, $linkcolor);
   // Set the link color.
    if (!empty($theme->settings->breadcrumbs)) {
        $breadcrumbs = $theme->settings->breadcrumbs;
    } else {
        $breadcrumbs = null;
    }
    $css = theme_eduopen_set_breadcrumbs($css, $breadcrumbs);
    // Set the logo bg color.
    if (!empty($theme->settings->logobackcolor)) {
        $logobackcolor = $theme->settings->logobackcolor;
    } else {
        $logobackcolor = null;
    }
    $css = theme_eduopen_set_logobackcolor($css, $logobackcolor);
     // Set the homepage font bg color.
    if (!empty($theme->settings->homefontbackcolor)) {
        $homefontbackcolor = $theme->settings->homefontbackcolor;
    } else {
        $homefontbackcolor = null;
    }
    $css = theme_eduopen_set_homefontbackcolor($css, $homefontbackcolor);
    // Set the buttoncolour
    if (!empty($theme->settings->buttoncolour)) {
        $buttoncolour = $theme->settings->buttoncolour;
    } else {
        $buttoncolour = null;
    }
    $css = theme_eduopen_set_buttoncolour($css, $buttoncolour);
    // Set buttonhovercolour
    if (!empty($theme->settings->buttonhovercolour)) {
        $buttonhovercolour = $theme->settings->buttonhovercolour;
    } else {
        $buttonhovercolour = null;
    }
    $css = theme_eduopen_set_buttonhovercolour($css, $buttonhovercolour);
    // Set footer colour
    if (!empty($theme->settings->footerbgcolour)) {
        $footerbgcolour = $theme->settings->footerbgcolour;
    } else {
        $footerbgcolour = null;
    }
    $css = theme_eduopen_set_footerbgcolour($css, $footerbgcolour);
    return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_eduopen_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_eduopen_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'logo') {
        $theme = theme_config::load('eduopen');
        // By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_eduopen_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}
function theme_eduopen_set_backcolor($css, $backcolor) {
    $tag = '[[setting:backcolor]]';
    $replacement = $backcolor;
    if (is_null($replacement)) {
        $replacement = '#F1EEE7';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_eduopen_set_linkhover($css, $linkhover) {
    $tag = '[[setting:linkhover]]';
    $replacement = $linkhover;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_eduopen_set_maincolor($css, $maincolor) {
    $tag = '[[setting:maincolor]]';
    $replacement = $maincolor;
    if (is_null($replacement)) {
        $replacement = '#001e3c';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_eduopen_set_linkcolor($css, $linkcolor) {
    $tag = '[[setting:linkcolor]]';
    $replacement = $linkcolor;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_eduopen_set_breadcrumbs($css, $breadcrumbs) {
    $tag = '[[setting:breadcrumbs]]';
    $replacement = $breadcrumbs;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_logobackcolor($css, $logobackcolor) {
    $tag = '[[setting:logobackcolor]]';
    $replacement = $logobackcolor;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_eduopen_set_homefontbackcolor($css, $homefontbackcolor) {
    $tag = '[[setting:homefontbackcolor]]';
    $replacement = $homefontbackcolor;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_eduopen_set_buttoncolour($css, $buttoncolour) {
    $tag = '[[setting:buttoncolour]]';
    $replacement = $buttoncolour;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_buttonhovercolour($css, $buttonhovercolour) {
    $tag = '[[setting:buttonhovercolour]]';
    $replacement = $buttonhovercolour;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
function theme_eduopen_set_footerbgcolour($css, $footerbgcolour) {
    $tag = '[[setting:footerbgcolour]]';
    $replacement = $footerbgcolour;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}
/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add eduopen specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_eduopen_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-center">'.format_text($page->theme->settings->footnote).'</div>';
    }

    return $return;
}

/**
 * All theme functions should start with theme_eduopen_
 * @deprecated since 2.5.1
 */
function eduopen_process_css() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_eduopen_
 * @deprecated since 2.5.1
 */
function eduopen_set_logo() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

/**
 * All theme functions should start with theme_eduopen_
 * @deprecated since 2.5.1
 */
function eduopen_set_customcss() {
    throw new coding_exception('Please call theme_'.__FUNCTION__.' instead of '.__FUNCTION__);
}

//Following function returns id of the user(s) assigned as a role of teacher into moodle course by Pratim.
function get_teacher($course_id){
	global $DB;
	$teacher_id = array();
	$return = null;
	$context_rs = $DB->get_record('context',array('instanceid'=>$course_id,'contextlevel'=> '50'));
	
	if($context_rs){		
		$context_id = $context_rs->id;		
		$role_assignment_rs = $DB->get_records('role_assignments',array('contextid'=>$context_id,'roleid'=>'3'));
		if($role_assignment_rs){
			foreach($role_assignment_rs as $role_assignment){
				$teacher_id[] = $role_assignment->userid;
			}
			$return = $teacher_id;
		}			
	}	
	return $return;	
}


//Following function returns all the module ids and names of a course by Pratim.

function mod_info($course_id){
	
	global $DB;
	$i=1;
	$return = null;
	$course_module_rs = $DB->get_records('course_modules',array('course'=>$course_id,'visible'=>'1'));
	if($course_module_rs){
		foreach($course_module_rs as $course_modules){
			$module = $course_modules->module;
			$module_type = $DB->get_field('modules', 'name', array('id'=>$module), MUST_EXIST);
			
			if($module_type){
				
				$return['mod '.$i] = new stdClass();
					$return['mod '.$i]->mod_id = $course_modules->id;
					$return['mod '.$i]->mod_name = $module_type;
					
			}
			$i++;
		}
		
	}
	return $return;
}

//Following function checks that if a module has attended or not by Pratim.
function has_attended_module($mod_id){
	global $DB,$USER;
	$return = false;
	$has_viewed = $DB->record_exists('course_modules_completion',array('coursemoduleid'=>$mod_id,'userid'=>$USER->id,'viewed'=>'1'));
	if($has_viewed){
		$return = true;
	}
	return $return;
}


//Following function returns overall progress in percentage of a student by Pratim.

function progress($course_id){
	
	global $DB,$USER;
	$count = 0;
	$progress = 0;
	$return = null;
	$modules = mod_info($course_id);	
	if($modules){
		
		foreach($modules as $mod_info){
			
			$mod_id = $mod_info->mod_id;
			$mod_name = $mod_info->mod_name;
			if($mod_name != 'label'){
				$count++;
			}
			$has_completed = has_attended_module($mod_id);	
			if($has_completed){
				$progress++;
			}
			
		}
		$progress_rate = round(($progress/$count)*100);
		$return = $progress_rate;		
	}	
	
	return $return;
}

//Following function gets the user profile picture original author Mihir, updated by Pratim.
function get_user_picture($size, $user){
	global $PAGE, $OUTPUT;
	$user_name = ucfirst($user->firstname)." ".ucfirst($user->lastname);
	$html = '';
	if(isloggedin() && !isguestuser()) {
		$html.= $OUTPUT->user_picture($user, array('size' => $size));
	}
	else {
		if($size > 35) {
			$html.= "<img alt='Picture of ".$user_name."' title='Picture of ".$user_name."' class='userpicture' role='presentation' src='".$OUTPUT->pix_url('g/f1')."' />";
		}
		else {
			$html.= "<img alt='Picture of ".$user_name."' title='Picture of ".$user_name."' class='userpicture' role='presentation' src='".$OUTPUT->pix_url('g/f2')."' />";
		}
	}
	return $html;
}
//This function takes course details as array and returns formatted course block, if the last parameter is provided as null it returns the block for teacher as in teaching courses block by Pratim.
function generate_course_block_dashboard($course_id,$course_name,$course_summary,$student){
	global $DB,$CFG,$USER,$COURSE;

	
	$progress = 0;
	$output = '';
	$block_count = 0;
	for($i=0; $i<sizeof($course_id); $i++){//Print the course block for each course
		$block_count++;
		$c_settings_rs = $DB->get_record('course_extrasettings_general',array('courseid'=>$course_id[$i]));
		if($c_settings_rs){
			$course_image_id = $c_settings_rs->courseimage;
			$context_id = $c_settings_rs->contextid;		
			$course_image_url = get_uploaded_image($course_image_id,$context_id);
					
		}
		$default_course_image = $CFG->wwwroot.'/theme/eduopen/pix/default_course.png';
		$course_image_url = isset($course_image_url) ? $course_image_url : $default_course_image;
		
		if( ( $block_count % 3 ) == 0 ){//if two blocks are printed at a time then create a new row
			$output .=  '<div class="row">';
		}	
		$output .= '<div id="CourseBlock" class="col-lg-4 col-md-3 col-sm-12 col-xs-12">';
			//modal start
			$output .= '<section class="panel">';
				$output .= '<header class="panel-heading">';
					$output .= '<div class="row">';
						$output .= '<div class="col-lg-11 col-md-12 col-sm-12 pad0R">';
							$output .= trim($course_name[$i]); 
						$output .= '</div>';
						$output .= '<div class="col-lg-1 col-md-12 col-sm-12 pad0L">';
							$output .= '<span class="tools pull-right">';
								$output .= '<a href="javascript:;" class="fa fa-chevron-down"></a>';											
							$output .= '</span>';
						$output .= '</div>';
					$output .= '</div>';
				$output .= '</header>';
				$output .='<div class="panel-body">';
					$output .= '<div class="row">';
						$output .= '<div class="col-md-4">';
							$output .= '<img src="'.$course_image_url.'" class="img-responsive img-rounded" height="90px" alt='.$course_name[$i].'"/>';
						$output .= '</div>';
						
						$output .= '<div class="col-md-8"><!--course summary row-->';
						
						//Course summary raw
						$course_summary_unprocessed = $course_summary[$i];
					
						//process the course summary 
						$course_summary_processed = trim(strip_tags($course_summary_unprocessed));
						
						
							if (strlen($course_summary_processed) > 70) {//if the course summary is more than 50 characters show a read-more link.

								// truncate string
								$short_course_summary = substr($course_summary_processed, 0, 70);		
															
								$output .=  $short_course_summary.'...<a href="#Summary'.$course_id[$i].'" class="eduopen-link fancybox">Read more</a>'; 
								$output .= '<div id="Summary'.$course_id[$i].'" style="width:400px;display: none;">';
									$output .= '<h3>'.trim($course_name[$i]).'</h3>';
									$output .= '<p>';
										$output .= $course_summary_unprocessed;
									$output .= '</p>';
								$output .= '</div>';
							}else{//else just print the course summary								
								$output .= '<p>'.$course_summary_processed.'</p>';
							}						
					
						$output .= '</div>';
						
					$output .= '</div>';
					
					$teacher = get_teacher($course_id[$i]);
					
					
					
					if($teacher){
						$output .= '<div class="row"><!--Course instructor row-->';
							$output .= '<div class="col-md-12">';
							$output .= '<h5>'.get_string('with','theme_eduopen').'</h5>'; 							
									
							
							for($j=0;$j<sizeof($teacher);$j++){
								$teacher_id = $teacher[$j];
						
								$teacher_name_rs = $DB->get_record('user',array('id'=>$teacher_id),'firstname,lastname');
								if($teacher_name_rs){
									$teacher_name = ucfirst($teacher_name_rs->firstname)." ".ucfirst($teacher_name_rs->lastname);
								}
								
								$teacher_rs = $DB->get_record('user',array('id'=>$teacher_id));
								if($teacher_rs){
									
									$user_picture = get_user_picture(40, $teacher_rs); 
									$output .= '<div class="row mrg5T mrg5B">';
										$output .= '<div class="col-md-12">';
											$output .= '<div class="col-lg-2 col-md-4 col-sm-12 pad0A mrg0A"><!--user picture-->';
												$output .= $user_picture; 
											$output .= '</div>';
											$output .= '<div class="col-lg-10 col-md-8 col-sm-12 pad0L"><!--Teacher name and institute-->';
												$output .= '<div class="row">';
													$output .= '<div class="col-md-12"><!--Teacher name-->';
														$output .= '<span class="usertext">'.$teacher_name.'</span>';
													$output .= '</div>';
												$output .= '</div>';	
												$output .= '<div class="row">';
													$output .= '<div class="col-md-12"><!--Institute-->';
														$output .= '<a class="eduopen-link mrg10pL" target="_blank" href="javascript:;">xyz</a>';
													$output .= '</div>';
												$output .= '</div>';
													
											$output .= '</div>';
										$output .= '</div>';
									$output .= '</div>';
								
								}																					
							}
																
							$output .= '</div>';
						$output .= '</div>'; 
						
					}
					
										
					if($student){
						$progress = progress($course_id[$i]); 						
						
						if($progress !=100){
							$output .= '<div class="row pad10A"><!--Progress bar row-->';
								$output .= '<div class="progress progress-xs">';
									$output .='<div class="progress-bar progress-bar-success eduopen-theme" role="progressbar" aria-valuenow="'.$progress.'" aria-valuemin="0" aria-valuemax="100" style="width:'.$progress.'%">';
										$output .= '<span class="sr-only">'.$progress.'%Complete (success)</span>';
									$output .= '</div>';
								$output .= '</div>';
							$output .= '</div>';
							$output .= '<div class="row">';
								$output .= '<div class="col-md-12">';
									$output .= '<span class="course-percent">'.$progress.'% &nbsp;</span><span>'. get_string('completed','theme_eduopen').'</span>';
								$output .= '</div>';
							
						}						
					
					}
						$output .= '<div class="row">';
							$output .= '<div class="col-md-11">';
								$output .= '<a href="'.$CFG->wwwroot.'/course/view.php?id='.$course_id[$i].'" class="eduopen-link pull-right">';
								if($progress == 100){
									$output .= 	get_string('view','theme_eduopen');								
								}else{
									$output .= ($student ? get_string('continuelearning','theme_eduopen') : get_string('continueteaching','theme_eduopen'));
								}								
								$output .= ' >></a>';
							$output .= '</div>';
						$output .= '</div>';
						
					$output .= '</div>';
			$output .= '</section>';								
		$output .= '</div>';
		if( ( $block_count % 3 ) == 0 ){//if two blocks are printed at a time then create a new row
			$output .=  '</div>';
		}
	}
	
	return $output;
	
}

//this function checks that user has completed every module in the course or not by Pratim.
function has_completed_course($course_id){
	global $DB,$USER;
	$return = false;
	$mod_count = 0;
	$completed_mod_count = 0;
	$modules = mod_info($course_id);
	if($modules){
		
		foreach($modules as $mod_info){
			
			$mod_id = $mod_info->mod_id;
			$mod_name = $mod_info->mod_name;
			if($mod_name != 'label'){
				$mod_count++;
			}				
			$has_completed = $DB->record_exists('course_modules_completion',array('coursemoduleid'=>$mod_id,'userid'=>$USER->id,'completionstate'=>'1'));
			if($has_completed){
				$completed_mod_count++;
			}		
		}
		if($mod_count == $completed_mod_count){
			
			$return = true;
		}		
	}	
	return $return;
}


//This function returns formatted course details block which is used display short description about the course.

function generate_course_block($course_id){
	global $DB,$CFG;
	
	$output = null;
	$course_details_rs = $DB->get_records('course',array('id'=>$course_id,'visible'=>'1'));
	if($course_details_rs){
		$output = '';
		foreach($course_details_rs as $course_details){
			
			
			$course_id = $course_details->id;
			$course_name = $course_details->fullname;
			$start_date = $course_details->startdate;
			$specialization = true; //to be updated when specialization block is finished
			if($start_date != 0){
				$start_date = date('M jS, Y',$start_date);
			}
            $course_name = strip_tags($course_name);//Code updated by haraprasad on march312015

						if (strlen($course_name) > 15) {

							// truncate string
							$course_nameCut = substr($course_name, 0, 15);							
							$course_name= $course_nameCut.'..'; 
						}
			$instructor = get_teacher($course_id);
			
			$c_settings_rs = $DB->get_record('course_extrasettings_general',array('courseid'=>$course_id));

			if($c_settings_rs){
				$course_image_id = $c_settings_rs->courseimage;
				$institute = $c_settings_rs->institution;
				$length = $c_settings_rs->length;				
				$cost = $c_settings_rs->cost;
				if(!$cost || $cost=='' || $cost==0){
					$cost = get_string('free','theme_eduopen');
				}else{
					$cost = '<i class="fa fa-eur"></i>&nbsp'.$cost;
				}
				$certificate = $c_settings_rs->certificate;
				$context_id = $c_settings_rs->contextid;
				$default_course_image = $CFG->wwwroot.'/theme/eduopen/pix/default_course.png';
				$course_image_url = get_uploaded_image($course_image_id,$context_id);
				$course_image_url = $course_image_url ? $course_image_url : $default_course_image;

				$c_settings_instid = $DB->get_records('block_eduopen_master_inst',array('name'=>$c_settings_rs->institution));

				foreach($c_settings_instid as $instid){ //haraprasad add this line 
                					
				$output .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pad20L pad20R">';//begin block
					$output .= '<div id="AllCoursesBlock'.$course_id.'" class="well well-sm AllCoursesBlock row">';//Main row
						
						
							$output .= '<div class="col-lg-4 col-md-12">';//First column display course image
								
								$output .= '<a href="'.$CFG->wwwroot.'/eduopen/course_details.php?courseid='.$course_id.'"><img src="'.$course_image_url.'" class="img-responsive img-rounded" width="100%" height="100px" alt="'.$course_name.'"/></a>';
							
							$output .= '</div>';//end of first column
							
							$output .= '<div class="col-lg-4 col-md-12">';//second column display institute name, course name, instructor name 
									$output .= '<div class="row">';
										$output .= '<div class="col-md-12">';
											$output .= '<a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/institution_details.php?institutionid='.$instid->id.'">'.$institute.'</a>';
										$output .= '</div>';
									$output .= '</div>';
									
									$output .= '<div class="row mrg10T">';
										$output .= '<div class="col-md-12">';
											$output .= '<a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/course_details.php?courseid='.$course_id.'">'.$course_name.'</a>';
										$output .= '</div>';
									$output .= '</div>';
									
									$output .= '<div class="row mrg10T">';
										$output .= '<div class="col-md-12">';
										if($instructor){
											$output .= '<span>'.get_string('with','theme_eduopen').'&nbsp</span>';
											$instructor_name = array();
											for($i=0;$i<sizeof($instructor);$i++){//get instructor names by their id
												$instructor_id = $instructor[$i];
										
												$instructor_name_rs = $DB->get_record('user',array('id'=>$instructor_id),'firstname,lastname');
												if($instructor_name_rs){
													$instructor_name[] = ucfirst($instructor_name_rs->firstname)." ".ucfirst($instructor_name_rs->lastname);
												}
											}
											
											for($j=0;$j<4;$j++){	//Logic to print instructor names as a, b, c & d
			
												/* if(($j+1) == count($instructor_name) && count($instructor_name)!=1){
													$output .= ' & <a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
												}else if($j != 0){
													$output .= ', <a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
												}else{ */
											   if($j== 3){
												$output .= "<a class='eduopen-link' href=''>...</a>";
												} 
												 else if($j != 0){
												$output .= ', <a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
												}else{
												$output .= '<a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
												
												}
											}
										}								
										$output .= '</div>';
									$output .= '</div>';
							
							$output .= '</div>';//end of second column
							
							$output .= '<div class="col-lg-4 col-md-12">';//third column display time,price etc
								$output .= '<div class="row">';
									$output .= '<div class="col-md-12">';
										$output .= '<p class="text-bold">'.$start_date.'<br/>';
										$output .= $length.' '.get_string('long','theme_eduopen').'</p>';								
									$output .= '</div>';
									
									$output .= '<div class="col-md-12">';
										if($certificate==1){//If there is a certificate associated with thi course
											$output .= '<span class="label label-success">'.get_string('certificate','theme_eduopen').'</span>';
										}
									$output .= '</div>';
									
									$output .= '<div class="col-md-12 mrg10T">';
										//if this course is under specialization the show the link
										if($specialization){ //haraprasad add this line specializations
                                         $c_settings_special = $DB->get_records('block_eduopen_master_special',array('name'=>$c_settings_rs->specializations));
                                          foreach($c_settings_special as $specialid){
											$output .= '<a href="'.$CFG->wwwroot.'/eduopen/specialization_details.php?specialid='.$specialid->id.'" class="btn btn-info btn-xs">'.$specialid->name.'</a>'; //link to be updated after completion of specialization block.
										
									$output .= '</div>';
									
									$output .= '<div class="col-md-12 mrg10T">';
										
										$output .= '<span class="label label-info">'.$cost.'</span>';
										
										if(in_watchlist($course_id)){
											$output .= '<a href="javascript:; " class="tooltips eduopen-link" >';
												$output .= '<i class="fa fa-eye pull-right font15 mrg0T" data-placement="left" data-toggle="tooltip" data-original-title="'.get_string('watchlist_added','theme_eduopen').'"></i>';
											$output .= '</a>';
										}else{
											$logged_in = 0;
											if(isloggedin()){//User can only add course to watchlist if they are logged in
												$logged_in = 1;
											}
											$output .= '<a id="course'.$course_id.'" href="javascript:; " class="eduopen-link" onclick="AddToWatchList('.$course_id.','.$logged_in.'); ">';
												$output .= '<i class="fa fa-eye-slash pull-right font15 mrg0T" data-placement="left" data-toggle="tooltip" data-original-title="'.get_string('watchlist_add','theme_eduopen').'"></i>';
											$output .= '</a>';
										}
									$output .= '</div>';
									
								$output .= '</div>';
							
							$output .= '</div>';//end of third column
							
						
					
					$output .= '</div>';//end main row
				$output .= '</div>';//end block
				
			}	
		  }}}
		}	
	}
	return $output;
}
//The functions get_uploaded_image(), make_draftfile_url_for_image(), make_file_url_for_image() are used for generating a stored file url using files id and context id

function get_uploaded_image($file_id,$context_id){//function to retrieve the uploaded file
              
	$filearea = 'draft';
	$filepath = '/';
	$fs = get_file_storage();
	$picsfile = null;
	if(!empty($file_id)){
		$files = $fs->get_area_files($context_id, 'user', $filearea , $file_id, 'id', false);
		foreach ($files as $file) {
			$picsfile = make_draftfile_url_for_image($context_id,$file->get_itemid(), $file->get_filepath(), $file->get_filename());
			$itemid = $file->get_itemid();
			$filename = $file->get_filename();
																	  
		}
					
	}
	return $picsfile;
}

function make_draftfile_url_for_image($contextid, $draftid, $pathname, $filename, $forcedownload = false) { //function to make file url

	global $CFG, $USER;

	$urlbase = "$CFG->httpswwwroot/draftfile.php";

	return make_file_url_for_image($urlbase, "/$contextid/user/draft/$draftid".$pathname.$filename, $forcedownload);

}

function make_file_url_for_image($urlbase, $path, $forcedownload = false) {//function to generate stored file url

	$params = array();

	if ($forcedownload) {

		$params['forcedownload'] = 1;

	}

	$url = new moodle_url($urlbase, $params);

	$url->set_slashargument($path);

	return $url;

}

//function to check weather the course is in watchlist or not
function in_watchlist($course){
	global $DB,$USER;
	$return = false;
	
	$in_watchlist = $DB->record_exists('eduopen_watchlist',array('user'=>$USER->id,'course'=>$course));
	if($in_watchlist){
		$return = true;
	}
	
	return $return;
	
}


//renders course block from watchlist table
function generate_course_block_watchlist(){
	global $DB,$CFG,$USER;
	
	$output = null;
	$userid = $USER->id;
	$course_details_q = 'SELECT c.id,c.fullname,c.startdate,cs.courseimage,cs.institution,cs.length,cs.
						certificate,cs.contextid,cs.cost,cs.specializations FROM {course} c  LEFT JOIN  {eduopen_watchlist} w ON
						c.id = w.course LEFT JOIN {course_extrasettings_general} cs ON c.id = cs.courseid WHERE 
						c.visible = 1 AND c.category != 0 AND w.user = '. $userid;
	
	
						
	$course_details_rs = $DB->get_records_sql($course_details_q);
	if($course_details_rs){
		$output = '';
		$block_count = 0;
		foreach($course_details_rs as $course_details){
			$block_count++; 
			
			
			$course_id = $course_details->id;
			$course_name = $course_details->fullname;
			$start_date = $course_details->startdate;
			$specialization = true; //to be updated when specialization block is finished
			if($start_date != 0){
				$start_date = date('M jS, Y',$start_date);
			}  
            $course_name = strip_tags($course_name);//Code updated by haraprasad on march312015

				if (strlen($course_name) > 15) {

					// truncate string
					$course_nameCut = substr($course_name, 0, 15);							
					$course_name= $course_nameCut.'..'; 
				}
			$instructor = get_teacher($course_id);
			
			$course_image_id = $course_details->courseimage;
			$institute = $course_details->institution;
			$length = $course_details->length;
			
			
			$cost = $course_details->cost;
			if(!$cost || $cost=='' || $cost==0){
				$cost = get_string('free','theme_eduopen');
			}else{
				$cost = '<i class="fa fa-eur"></i>&nbsp'.$cost;
			}
			$certificate = $course_details->certificate;
			$context_id = $course_details->contextid;
			$default_course_image = $CFG->wwwroot.'/theme/eduopen/pix/default_course.png';
			$course_image_url = get_uploaded_image($course_image_id,$context_id);
			$course_image_url = $course_image_url ? $course_image_url : $default_course_image;

			$c_settings_instid = $DB->get_records('block_eduopen_master_inst',array('name'=>$institute));
			foreach($c_settings_instid as $instid){	//@todo:use join with main query and remove this loop.
				if( ( $block_count % 2 ) != 0 ){//if two blocks are printed at a time then create a new row
					$output .=  '<div class="row">';
				}	
				$output .= '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 pad20L pad20R">';//begin block
						$output .= '<div id="AllCoursesBlock'.$course_id.'" class="well well-sm AllCoursesBlock row">';//Main row
							
							
								$output .= '<div class="col-lg-4 col-md-12">';//First column display course image
									
									$output .= '<a href="'.$CFG->wwwroot.'/eduopen/course_details.php?courseid='.$course_id.'"><img src="'.$course_image_url.'" class="img-responsive img-rounded mrg10T mrg-10L" width="100%" height="100px" alt='.$course_name.'"/></a>';
								
								$output .= '</div>';//end of first column
								
								$output .= '<div class="col-lg-4 col-md-12 ">';//second column display institute name, course name, instructor name 
										$output .= '<div class="row mrg10T">';
											$output .= '<div class="col-md-12">';
												$output .= '<a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/institution_details.php?institutionid='.$instid->id.'">'.$institute.'</a>';
											$output .= '</div>';
										$output .= '</div>';
										
										$output .= '<div class="row mrg10T">';
											$output .= '<div class="col-md-12">';
												$output .= '<a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/course_details.php?courseid='.$course_id.'">'.$course_name.'</a>';
											$output .= '</div>';
										$output .= '</div>';
										
										$output .= '<div class="row mrg10T">';
											$output .= '<div class="col-md-12">';
											if($instructor){
												$output .= '<span>'.get_string('with','theme_eduopen').'&nbsp</span>';
												$instructor_name = array();
												for($i=0;$i<sizeof($instructor);$i++){//get instructor names by their id
													$instructor_id = $instructor[$i];
											
													$instructor_name_rs = $DB->get_record('user',array('id'=>$instructor_id),'firstname,lastname');
													if($instructor_name_rs){
														$instructor_name[] = ucfirst($instructor_name_rs->firstname)." ".ucfirst($instructor_name_rs->lastname);
													}
												}
												
											    for($j=0;$j<4;$j++){	//Logic to print instructor names as a, b, c & d
			
													/* if(($j+1) == count($instructor_name) && count($instructor_name)!=1){
														$output .= ' & <a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
													}else if($j != 0){
														$output .= ', <a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
													}else{ */
												   if($j== 3){
													$output .= "<a class='eduopen-link' href=''>...</a>";
													} 
													 else if($j != 0){
													$output .= ', <a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
													}else{
													$output .= '<a class="eduopen-link" href="'.$CFG->wwwroot.'/eduopen/instructor.php?id='.$instructor[$j].'" target="_blank" >'.$instructor_name[$j].'</a>';
												
												    }
											    }
											}								
											$output .= '</div>';
										$output .= '</div>';
								
								$output .= '</div>';//end of second column
								
								$output .= '<div class="col-lg-4 col-md-12">';//third column display time,price etc
								
									$output .= '<div class="row">';
										$output .= '<div class="col-md-12">';
											$output .= '<p class="text-bold">'.$start_date.'<br/>';
											$output .= $length.' '.get_string('long','theme_eduopen').'</p>';								
										$output .= '</div>';
										
										$output .= '<div class="col-md-12">';
											if($certificate==1){//If there is a certificate associated with thi course
												$output .= '<span class="label label-success">'.get_string('certificate','theme_eduopen').'</span>';
											}
										$output .= '</div>';
										
										$output .= '<div class="col-md-12 mrg10T">';
											//if this course is under specialization the show the link
				if($specialization){
					$c_settings_special = $DB->get_records('block_eduopen_master_special',array('name'=>$course_details->specializations));
					foreach($c_settings_special as $specialid){//@todo:use join with main query and remove this loop.
						$output .= '<a href="'.$CFG->wwwroot.'/eduopen/specialization_details.php?specialid='.$specialid->id.'" class="btn btn-info btn-xs">'.$course_details->specializations.'</a>'; //link to be updated after completion of specialization block.
					
						$output .= '</div>';
				
						$output .= '<div class="col-md-12 mrg10T">';
					
						$output .= '<span class="label label-info">'.$cost.'</span>';
						if(in_watchlist($course_id)){
							$output .= '<a href="javascript:; " class="eduopen-link" onclick="RemoveFromWatchlist('.$course_id.')" >';
								$output .= '<i class="fa fa-trash-o pull-right font15" data-placement="top" data-toggle="tooltip" data-original-title="'.get_string('watchlist_remove','theme_eduopen').'"></i>';
							$output .= '</a>';
						}
					
						$output .= '</div>';
				
						$output .= '</div>';
		
						$output .= '</div>';//end of third column
		
						$output .= '</div>';//end main row
						$output .= '</div>';//end block
				
		
		
					}	
				}
			}
			if( ( $block_count % 2 ) == 0 ){//if two blocks are printed at a time then create a new row
				$output .=  '</div>';
			}
		}
	}
	return $output;
}


