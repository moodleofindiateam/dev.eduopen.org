<?php /**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */

global $CFG, $PAGE;
$PAGE->requires->jquery();
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/jquery-ui-1.11.2/jquery-ui.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/bootstrap.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/jquery.dcjqaccordion.2.7.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/jquery.scrollTo.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/jQuery-slimScroll-1.3.0/jquery.slimscroll.js'),true);

$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/jquery.nicescroll.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/sweetalert/lib/sweet-alert.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/fancybox/source/jquery.fancybox.pack.js?v=2.1.5'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/scripts.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/jssor.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/jquery/jssor.slider.js'),true);
?>