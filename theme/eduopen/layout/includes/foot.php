<?php /**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
?>
<script type="text/javascript">
	$(document).ready(function() {
		
		$( ".mod-indent-outer " ).addClass( "csborderbottom" );       
		$('ul#CustomMenu li:nth-child(3)').addClass('listnone'); 
		$('ul#CustomMenu li:nth-child(1)').addClass('listnone'); 
		$('ul#CustomMenu li:nth-child(5)').addClass('listnone');
		$("ul#CustomMenu li>ul").attr("id", "main_menuid");
		//$( "ul li.modtype_eduvideo div.activityinstance a" ).addClass( "fancybox fancybox.iframe" );
            
	});
$("#id_forcesubscribe option[value='1'], #id_forcesubscribe option[value='2']").attr('disabled', 'disabled');
</script>


<!--<script type="text/javascript">
$(window).load(function() {
	var wdth = $(".fixedtophead .header").width();
	var fwd = (wdth-270);
	var divCss = document.getElementById("MainMenuEle");
	divCss.style.width =  fwd + 'px';
});
</script>
<script type="text/javascript">
var windowsize = $(window).width();
$(window).resize(function() {
	var windowsize = $(window).width();
	var wdth = $(".fixedtophead .header").width();
	var fwd = (wdth-240);
	var divCss = document.getElementById("MainMenuEle");
	if (windowsize > 767) {
		divCss.style.width =  fwd + 'px';
	}
});
</script>-->

<script>
$("#logbutton").click(function () {
       $("#page-login-index").css("background-color", "#000000");
 });
</script>
<script>
$(".hr-toggle").click(function(){
        $("#MainMenuEle").toggleClass("topmenushow");
    });
</script>
<!-- script to add hide/show block button-->
<?php
if(get_config('theme_eduopen', 'toggleblock')){
?>
<script>
    var prefcheck = "<?php echo get_user_preferences('theme_eduopen_zoom'); ?>";
    if(prefcheck == 'zoomin'){
    $("div.toggleblockdiv").prepend('<div class="toggleblock"><i class="fa fa-outdent"></i>&nbspShow Blocks</div>');
    } else {
    $("div.toggleblockdiv").prepend('<div class="toggleblock"><i class="fa fa-indent"></i>&nbspHide Blocks</div>'); 
    }
    $(".toggleblock").click(function(){
        if($('#region-main').hasClass('width100')){
            $.post("<?php echo $CFG->wwwroot . '/theme/eduopen/ajax_load.php' ?>",{sesskey:"<?php echo $USER->sesskey; ?>", mode:'theme_eduopen_zoom',value:'nozoom'},function(json){
                //$("div.toggleblock i").removeClass("fa-outdent").addClass("fa-indent");
                $(".toggleblock").html('<i class="fa fa-indent"></i>&nbspHide Blocks');
                $("#region-main").removeClass("width100");
                $(".block-region").removeClass("hide",500);
                return false;
            });
        } else {
            $.post("<?php echo $CFG->wwwroot . '/theme/eduopen/ajax_load.php' ?>", {sesskey: "<?php echo $USER->sesskey; ?>", mode: 'theme_eduopen_zoom', value: 'zoomin'}, function (json) {
                //$("div.toggleblock i").removeClass("fa-indent").addClass("fa-outdent");
                $(".toggleblock").html('<i class="fa fa-outdent"></i>&nbspShow Blocks');
                $("#region-main").addClass("width100", 1000);
                $(".block-region").addClass("hide");
                return false;
            });
            }
        });
</script>
<?php } ?>

<!-- script to block header icon-->
<?php
if(get_config('theme_eduopen', 'blockicons')){
?>
<script>
$("#page").addClass('showblockicons');
</script>
<?php } ?>
