<?php /**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */

?>


<!-- Latest compiled and minified JavaScript -->
<link rel="stylesheet" href="<?php echo $CFG->wwwroot.'/theme/eduopen/layout/includes/head/jquery-ui.css'; ?>">
<link rel="stylesheet" href="<?php echo $CFG->wwwroot.'/theme/eduopen/fonts/font-awesome.min.css'; ?>">	


<link href="<?php echo $CFG->wwwroot.'/theme/eduopen/jquery/sweetalert/lib/sweet-alert.css'; ?>" rel="stylesheet">
<link href="<?php echo $CFG->wwwroot.'/theme/eduopen/style/bucket-ico-fonts.css'; ?>" rel="stylesheet">
<link href="<?php echo $CFG->wwwroot.'/theme/eduopen/jquery/fancybox/source/jquery.fancybox.css?v=2.1.5'; ?>" rel="stylesheet">
<?php
include_once($CFG->dirroot.'/eduopen/lib.php');
?>
