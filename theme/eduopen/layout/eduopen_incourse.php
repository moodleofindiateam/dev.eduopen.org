<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $DB, $CFG, $USER;
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("includes/head.php"); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(array('full-width')); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html() ?>
        <section id="container" class="hr-menu">
            <!--header end-->
            <section id="main-content">
                <section class="wrapper">
                    <div id="page" class="container-fluid cmediaqry">
                        <div id="page-content" class="row-fluid">
                            <div id="<?php echo $regionbsid ?>" class="span12">
                                <div class="row-fluid">
                                    <section id="region-main" class="span12">
                                        <?php
                                        echo $OUTPUT->course_content_header();
                                        echo $OUTPUT->main_content();
                                        echo $OUTPUT->course_content_footer();
                                        ?>
                                    </section>
                                    <?php //echo $OUTPUT->blocks('side-pre', 'span3 desktop-first-column');  ?>
                                </div>
                            </div>
                            <?php //echo $OUTPUT->blocks('side-post', 'span3');  ?>
                        </div>
                    </div>
                </section>
            </section>
        </section>
        <?php include("includes/footersetting.php"); ?>
        <?php echo $OUTPUT->standard_end_of_body_html() ?>

        <?php include("includes/foot.php"); ?>

        <script type="text/javascript" src="<?php echo $CFG->wwwroot . '/theme/eduopen/javascript/layout.js'; ?>" ></script>
    </body>
</html>
<script>
    $(document).on('click', '.fancybox', function (e) {
        e.preventDefault();
        $.fancybox({
            href: $(this).attr('href'),
            type: 'iframe',
            autoSize: true,
            autoDimensions: true,
            padding: 0,
        });
    });
</script>

<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>