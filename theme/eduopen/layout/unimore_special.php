<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's eduopen theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_eduopen
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot.'/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/javascript/jquery.ddslick.min.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/javascript/language.js'),true);
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include("/includes/head.php"); ?>
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

	<?php echo $OUTPUT->standard_top_of_body_html() ?>
	<section id="container">
		<header class="header clearfix">
			<!--logo start-->
			<div class="brand">

				<a class="logo" href="<?php echo $CFG->wwwroot;?>">
				<?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
					?>
				</a>
				<div class="sidebar-toggle-box">
					<div class="tooltips fa fa-bars " data-placement="bottom" data-toggle="tooltip"  data-original-title="Show/Hide side bar"></div>
				</div>
			</div>
			<!--logo end-->

			<div class="nav notify-row" >
				<!--  notification start -->			
						
				<!--  notification end -->
			</div>
			<div id="top_menu" class="horizontal-menu">

				<ul id="CustomMenu" class="nav navbar-nav">
					<?php include('/includes/main_navigation.php'); ?>
                    <ul class="sub">
								<li class="page-scroll">
									<a href="#Overview">Overview</a>
								</li>
								<li class="page-scroll">
									<a href="#Certificate">Certificate</a>
								</li>
								<li class="page-scroll">
									<a href="#Courses">Courses</a>
								</li>
								<li class="page-scroll">
									<a href="#Instructors">Instructors</a>
								</li>
								<li class="page-scroll">
									<a href="#FAQs">FAQs</a>
								</li>
								<li class="page-scroll">
								 <a href="#Courses">
							     <button type="button" class="btn btn-primary btnspecial">Start Specialization</button> </a>
								</li>
							</ul>
                   
				</ul>
				
			</div>
			
			
			
			<div class="top-nav clearfix">				
				<!--search & user info start-->
				<ul class="nav pull-right top-menu">
					<li id="SearchBox">
						<input type="text" class="form-control search" placeholder=" Search">
					</li>
					<!-- user login dropdown start-->
					<li>
						<?php echo $OUTPUT->user_menu(); ?><!--User menu-->
					</li>
					<li>
						<?php echo $OUTPUT->page_heading_menu(); ?>
					</li>
					<!-- user login dropdown end -->				
					
				</ul>
				<!--search & user info end-->
			</div>
		</header>
		<!--header end-->
		<aside>
			<div id="sidebar" class="nav-collapse">
				<!-- sidebar menu start-->
				<div class="leftside-navigation">
					<ul class="sidebar-menu" id="nav-accordion">
						<li id="WebNav" class="sub-menu" style="display:none; ">
							<a href="javascript:;">
								<i class="fa fa-laptop"></i>
								<span>Website Navigation</span>
							</a>
							
						</li>
						<!--Left side bar items goes here-->    
					
					</ul>			
					 
					
				</div>
				<!-- sidebar menu end-->
			</div>
		</aside>
		
		<section id="main-content">
			<section class="wrapper">

				<div id="page">
					<div id="page-content" class="row-fluid">
						<div id="<?php echo $regionbsid ?>" class="span9">
							<div class="row-fluid">
								<section id="region-main" class="span8 pull-right">
									<?php
									echo $OUTPUT->course_content_header();
									echo $OUTPUT->main_content();
									echo $OUTPUT->course_content_footer();
									?>
								</section>
								<?php echo $OUTPUT->blocks('side-pre', 'span4 desktop-first-column'); ?>
							</div>
						</div>
						<?php echo $OUTPUT->blocks('side-post', 'span3'); ?>
					</div>
				</div>
			</section>
			
		
		</section>	
			
	</section>
	
		

		<?php echo $OUTPUT->standard_end_of_body_html() ?>

	<?php include("/includes/foot.php"); ?>
</body>
</html>
