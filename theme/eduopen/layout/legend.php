<div class="row" id="rowmrglr">
	<ul class="legendul">
		<li class="legendli">
			<img class="leg" src="<?php echo $CFG->wwwroot.'/theme/eduopen/pix/v.png'?>">
			<?php echo get_string('legendverify', 'theme_eduopen');?>
		</li>
		<li class="legendli">
			<img class="leg2" src="<?php echo $CFG->wwwroot.'/theme/eduopen/pix/exam.jpg'?>">
			<?php echo get_string('legendexam', 'theme_eduopen');?>
		</li>
		<li class="legendli">
			<img class="leg" src="<?php echo $CFG->wwwroot.'/theme/eduopen/pix/md.png'?>">
			<?php echo get_string('legendmaster', 'theme_eduopen');?>
		</li>
		<li class="legendli">
			<img class="leg" src="<?php echo $CFG->wwwroot.'/theme/eduopen/pix/ad.png'?>">
			<?php echo get_string('legendadvance', 'theme_eduopen');?>
		</li>
                <li class="legendli">
			<img class="leg" src="<?php echo $CFG->wwwroot.'/theme/eduopen/pix/od.png'?>">
			<?php echo get_string('legendofficial', 'theme_eduopen');?>
		</li>
	</ul>
</div>