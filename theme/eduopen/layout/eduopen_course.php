<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $PAGE, $DB, $CFG, $USER, $COURSE;
$PAGE->requires->jquery();
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("includes/head.php"); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(array('full-width')); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
            <header class="header container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->

                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php include('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header>
            <!--header end-->
        </div>
        <section id="container" class="hr-menu"> 
            <section id="main-content">
                <section class="wrapper">
                    <div id="page" class="container-fluid cmediaqry frontcont">
                        <div id="page-content" class="row-fluid">
                            <div id="<?php echo $regionbsid ?>" class="col-md-12">
                                <div class="col-md-9"><?php echo $OUTPUT->navbar(); ?></div>
                                <div class="col-md-3 toggleblockdiv"><?php echo $OUTPUT->page_heading_button(); ?></div>
                                <div class="row-fluid">
                                    <?php
                                    if (get_user_preferences('theme_eduopen_zoom') == 'zoomin') {
                                        $wdth = 'width100';
                                    } else {
                                        $wdth = '';
                                    }
                                    if ($COURSE->format == 'topcoll') {
                                        // Course Title & summary  by Shiuli.
                                        $courseName = $DB->get_record('course', array('id' => $COURSE->id), 'fullname, summary');
                                        echo '<div class="col-md-12 col-sm-12 col-xs-12">';
                                        echo '<p class="CName">' . $courseName->fullname . '</p>';

                                        // Instructor Name from extra settings.
                                        $currentLanguage = current_language();
                                        $details = $DB->get_record('course_extrasettings_general', array('courseid' => $COURSE->id));
                                        if (isset($details) && !empty($details)) {
                                            $institute = $details->institution;
                                            $csettingsinstitutionid = $DB->get_record('block_eduopen_master_inst', array('id' => $institute));
                                            if ($csettingsinstitutionid) {
                                                $instituteid = $csettingsinstitutionid->id;
                                                if ($currentLanguage == 'it') {
                                                    $instname = $csettingsinstitutionid->itname;
                                                } else {
                                                    $instname = $csettingsinstitutionid->name;
                                                }
                                            }
                                            echo '<a class="eduopen-link" href="' . $CFG->wwwroot .
                                            '/eduopen/institution_details.php?institutionid=' .
                                            $csettingsinstitutionid->id . '">'
                                            . '<p class="instNameinCrs">' . $instname . '</p>';
                                            echo '</a>';
                                        }

                                        /* course description--for course format */
                                        $about = $DB->get_record('course_extrasettings_general', array('courseid' => $COURSE->id), 'whatsinside');
                                        if (isset($about) && !empty($about)) {
                                            // More chevron .
                                            echo '<div class="col-md-12 col-sm-12 col-xs-12 more">'
                                            . '<i class="fa fa-chevron-down Morechev"></i>&nbsp;'
                                            . '<span id="MoreLessCont">More</span>'
                                            . '</div>';
                                            echo '<div class="InstDiv">';
                                            // ABout the course.

                                            echo '<div class="col-md-12 col-sm-12 col-xs-12 aboutcrs">'
                                            . '<p class="AboutCoursehead">'
                                            . get_string('aboutcrs', 'format_topcoll') . '</p>';
                                            echo '<p class="AboutCourse">' . $about->whatsinside . '</p>';
                                            echo '</div>';
                                            echo '</div>';
                                        }
                                        /* End of course description */
                                        echo '</div>';

//                                        // More chevron .
//                                        echo '<div class="col-md-12 col-sm-12 col-xs-12 more">'
//                                        . '<i class="fa fa-chevron-down Morechev"></i>&nbsp;<span id="MoreLessCont">More</span>'
//                                        . '</div>';
//                                        echo '<div class="InstDiv">';
//
//                                        // ABout the course.
//                                        $about = $DB->get_record('course_extrasettings_general', array('courseid' => $COURSE->id), 'whatsinside');
//                                        if ($about) {
//                                            echo '<div class="col-md-12 col-sm-12 col-xs-12 aboutcrs"><p class="AboutCoursehead">'
//                                            . get_string('aboutcrs', 'format_topcoll') . '</p>';
//                                            echo '<p class="AboutCourse">' . $about->whatsinside . '</p>';
//                                            echo '</div>';
//                                        }
//
//                                        // Course Description.
//                                        if ($courseName->summary) {
//                                            echo '<div class="col-md-12 col-sm-12 col-xs-12 aboutcrs"><p class="AboutCoursehead">'
//                                            . get_string('crssummary', 'format_topcoll') . '</p>';
//                                            echo '<p class="CourseDesc">' . strip_tags($courseName->summary) . '</p>';
//                                            echo '</div>';
//                                        }
//                                        echo '</div>';
//
//                                        //Instructor name.
//                                        $Crscontext = context_course::instance($COURSE->id);
//                                        $enrolledUsers = get_enrolled_users($Crscontext);
//                                        echo '<div class="col-md-12 col-sm-12 col-xs-12 InstructorName">';
//                                        echo '<span class="instructName">'
//                                        . get_string('instructors', 'theme_eduopen') . ':&nbsp;</span>';
//                                        echo '<span class="teacherul">';
//                                        foreach ($enrolledUsers as $enrolledUser) {
//                                            $role = $DB->get_record('role', array('shortname' => 'editingteacher'));
//                                            $instructor = user_has_role_assignment($enrolledUser->id, $role->id, $Crscontext->id);
//                                            if ($instructor) {
//                                                echo $OUTPUT->user_picture($enrolledUser, array('courseid' => $COURSE->id)) . '&nbsp; &nbsp;';
////                                                echo '<a href="' . $CFG->wwwroot . '/user/view.php?id=' .
////                                                $enrolledUser->id . '&course=' . $COURSE->id . '">' .
////                                                $enrolledUser->firstname . '&nbsp;' . $enrolledUser->lastname . '</a>' . '&nbsp; &nbsp;';
//                                                echo '<a href="' . $CFG->wwwroot . '/eduopen/instructor.php?id=' .
//                                                $enrolledUser->id . '">' .
//                                                $enrolledUser->firstname . '&nbsp;' . $enrolledUser->lastname . '</a>' . '&nbsp; &nbsp;';
//                                            }
//                                        }
//                                        echo '</span></div>';
//                                        //Tutor/Co-instructor name.
//                                        foreach ($enrolledUsers as $enrolledUser) {
//                                            $tutorrole = $DB->get_record('role', array('shortname' => 'teacher'));
//                                            $tutor = user_has_role_assignment($enrolledUser->id, $tutorrole->id, $Crscontext->id);
//                                            if ($tutor) {
//                                                $tutorPresent[] = $enrolledUser->id;
//                                            }
//                                        }
//                                        if (!empty($tutorPresent)) {
//                                            echo '<div class="col-md-12 col-sm-12 col-xs-12 InstructorName">';
//                                            echo '<span class="instructName">' . get_string('tutors', 'theme_eduopen') . ':&nbsp;</span>';
//                                            echo '<span class="teacherul">';
//                                            foreach ($enrolledUsers as $enrolledUser) {
//                                                $tutorrole = $DB->get_record('role', array('shortname' => 'teacher'));
//                                                $tutor = user_has_role_assignment($enrolledUser->id, $tutorrole->id, $Crscontext->id);
//                                                if ($tutor) {
//                                                    echo $OUTPUT->user_picture($enrolledUser, array('courseid' => $COURSE->id)) . '&nbsp; &nbsp;';
//                                                    echo $enrolledUser->firstname . '&nbsp;' . $enrolledUser->lastname . '&nbsp; &nbsp;';
//                                                }
//                                            }
//                                            echo '</span></div>';
//                                        }
                                    }
                                    ?>
                                    <section id = "region-main" class = "col-md-9 <?php echo $wdth ?>">   
                                        <?php
                                        echo $OUTPUT->course_content_header();
                                        ?>
                                        <section id="region-content">
                                            <?php echo $OUTPUT->blocks('center-post'); ?>
                                        </section>
                                        <?php
                                        echo $OUTPUT->main_content();
                                        echo $OUTPUT->course_content_footer();
                                        ?>
                                    </section>
                                    <?php
                                    if (get_user_preferences('theme_eduopen_zoom') == 'zoomin') {
                                        echo $OUTPUT->blocks('side-post', 'col-md-3 hide');
                                    } else {
                                        echo $OUTPUT->blocks('side-post', 'col-md-3');
                                    }
                                    ?>
                                </div>
                            </div>

                            <?php //echo $OUTPUT->blocks('side-post', 'col-md-3');         ?>
                        </div>
                    </div>
                </section>
            </section>
        </section>
        <?php include("includes/footersetting.php"); ?>
        <?php echo $OUTPUT->standard_end_of_body_html() ?>
        <?php include("includes/foot.php"); ?>
    </body>
</html>
<script type="text/javascript">
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
    var additionaloffset = 30;
    $('.contentwithoutlink  li a').click(function () {
        var target = $(this).attr("href");
        $('html, body').animate({
            scrollTop: $(target).position().top + additionaloffset},
                1000
                );
    });
</script>
<?php
$sectionLayots = $DB->get_field('course_format_options', 'value', array('courseid' => $COURSE->id, 'format' => 'topcoll',
    'name' => 'activitylayoutoption'));
if (isset($sectionLayots) && !empty($sectionLayots)) {
    $includelayout = 'yes';
} else {
    $includelayout = 'no';
}
$prefcheck = get_user_preferences('theme_eduopen_zoom');
if ($prefcheck == 'zoomin') {
    if (current_language() == 'en') {
        if ($sectionLayots == 0) {
            ?>
            <style>
                .card .nav-tabs li.active:first-child a[role="tab"] {
                    padding-top: 19% !important;
                    padding-bottom: 19% !important;
                }
                .card .nav-tabs li.active:nth-child(2) a[role="tab"] {
                    padding-top: 15% !important;
                    padding-bottom: 15% !important;
                }
                .card .nav-tabs li.active:nth-child(5) a[role="tab"] {
                    padding-top: 12% !important;
                    padding-bottom: 12% !important;
                }
                .card .nav-tabs li.active:nth-child(6) a[role="tab"] {
                    padding-top: 13% !important;
                    padding-bottom: 13% !important;
                }
            </style>
        <?php } else {
            ?>
            <style>
                ul.nav-tabs li[role='presentation']:first-child {
                    width: 25%;
                }
                .card .nav-tabs li.active a[role="tab"] {
                    padding-top: 12% !important;
                    padding-bottom: 12% !important;
                }
            </style>
            <?php
        }
    } else {
        if ($sectionLayots == 0) {
            ?>
            <style>
                .card .nav-tabs li.active:first-child a[role="tab"] {
                    padding-top: 19% !important;
                    padding-bottom: 19% !important;
                }
                .card .nav-tabs li.active:nth-child(2) a[role="tab"] {
                    padding-top: 15% !important;
                    padding-bottom: 15% !important;
                }
                .card .nav-tabs li.active:nth-child(5) a[role="tab"] {
                    padding-top: 7% !important;
                    padding-bottom: 7% !important;
                }
                .card .nav-tabs li.active:nth-child(6) a[role="tab"] {
                    padding-top: 17% !important;
                    padding-bottom: 17% !important;
                }
            </style>
        <?php } else {
            ?>
            <style>
                ul.nav-tabs li[role='presentation']:first-child {
                    width: 25%;
                }
                .card .nav-tabs li.active a[role="tab"] {
                    padding-top: 12% !important;
                    padding-bottom: 12% !important;
                }
            </style>
            <?php
        }
    }
} else {
    if (current_language() == 'en') {
        if ($sectionLayots == 0) {
            ?>
            <style>
                .card .nav-tabs li.active:first-child a[role="tab"] {
                    padding-top: 20% !important;
                    padding-bottom: 20% !important;
                }
                .card .nav-tabs li.active:nth-child(2) a[role="tab"] {
                    padding-top: 15% !important;
                    padding-bottom: 15% !important;
                }
                .card .nav-tabs li.active:nth-child(5) a[role="tab"] {
                    padding-top: 15% !important;
                    padding-bottom: 15% !important;
                }
                .card .nav-tabs li.active:nth-child(6) a[role="tab"] {
                    padding-top: 12% !important;
                    padding-bottom: 12% !important;
                }
            </style>
        <?php } else {
            ?>
            <style>
                ul.nav-tabs li[role='presentation']:first-child {
                    width: 24%;
                }
                ul.nav-tabs li[role='presentation']:nth-child(4) {
                    width: 26%;
                }
                .card .nav-tabs li.active:first-child a[role="tab"] {
                    padding-top: 15% !important;
                    padding-bottom: 15% !important;
                }
                .card .nav-tabs li.active a[role="tab"] {
                    padding-top: 12% !important;
                    padding-bottom: 12% !important;
                }
            </style>
            <?php
        }
    } else {
        if ($sectionLayots == 0) {
            ?>
            <style>
                .card .nav-tabs li.active:first-child a[role="tab"] {
                    padding-top: 20% !important;
                    padding-bottom: 20% !important;
                }
                .card .nav-tabs li.active:nth-child(2) a[role="tab"] {
                    padding-top: 16% !important;
                    padding-bottom: 16% !important;
                }
                .card .nav-tabs li.active:nth-child(5) a[role="tab"] {
                    padding-top: 15% !important;
                    padding-bottom: 15% !important;
                }
                .card .nav-tabs li.active:nth-child(6) a[role="tab"] {
                    padding-top: 17% !important;
                    padding-bottom: 17% !important;
                }
            </style>
        <?php } else {
            ?>
            <style>
                ul.nav-tabs li[role='presentation']:first-child {
                    width: 25%;
                }
                .card .nav-tabs li.active a[role="tab"] {
                    padding-top: 12% !important;
                    padding-bottom: 12% !important;
                }
            </style>
            <?php
        }
    }
}
