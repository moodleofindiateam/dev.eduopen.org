<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A one column layout for the eduopen theme.
 *
* @package   theme_eduopen
 * @copyright 2015 Moodle Of India - wwww.moodleofidia.com
 * @author    Pratim Sarangi 
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

echo $OUTPUT->doctype() 
require_once($CFG->dirroot.'/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/javascript/language.js'));
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include('includes/head.php'); ?>
</head>

<body <?php echo $OUTPUT->body_attributes(array('login-body')); ?>>

<?php echo $OUTPUT->standard_top_of_body_html();?>


<div id="page" class="container">


    <div id="page-content" class="row-fluid">
        <section id="region-main" class="col-md-12">
            <?php
          
            echo $OUTPUT->main_content();
     
            ?>
			<div class="row center">
				<div class="registration col-md-8 col-sm-12 col-xs-12 ">
						<?php print_string('registered','theme_eduopen'); ?>
					<a class="eduopen-link" href="<?php echo $CFG->wwwroot.'/login/index.php'; ?>">
						<?php print_string('login','theme_eduopen'); ?>
					</a>
				</div>
			</div>
	
			
        </section>
    </div> 

    <?php echo $OUTPUT->standard_end_of_body_html() ?>

</div>
<script>var Data = "<h2><?php print_string('signup','theme_eduopen'); ?></h2>"; </script>
<?php include("/includes/foot.php"); ?>
<script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/signup.js'; ?>" > </script>
<?php 
 if(isloggedin()){//The user is logged in and not allowed to create an account ?>
	<script>
		var My = "<?php echo $CFG->wwwroot.'/my'; ?>";
		$(document).ready(function(){
			swal({
			  title: "Logged in!",
			  text: "Please log out and try again.",
			  type: "warning",
			  showCancelButton: true,
			  confirmButtonColor: "#DD6B55",
			  confirmButtonText: "Okay!",
			  showCancelButton: false,
			  closeOnConfirm: false,		 
			},
			function(isConfirm){
			  if (isConfirm) {			
				window.location.replace(My);
			  } 
			});
			
		});		
	</script>
<?php } ?>
</body>
</html>
