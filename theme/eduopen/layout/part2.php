<div class="container-fluid frontcont tcenter fcourse" id=""> 
    <h2><?php print_string('featured_pathways', 'theme_eduopen');?></h2>
    <hr>
</div>
<div class="row-fluid" id="frontrowpad"> 
    <?php
    $blockcount = 0;
    $allspecial = $DB->get_records_sql("SELECT * FROM {block_eduopen_master_special} WHERE status='1' ORDER BY RANDOM() LIMIT 4");
    foreach ($allspecial as $specialdetails) {
        $specialimg = specialization_images($specialdetails);
        $defaultcourseimage = $CFG->wwwroot.'/theme/eduopen/pix/default_pathway.png';
        $specialimg = $specialimg ? $specialimg : $defaultcourseimage;
        $blockcount++;
    ?>
    <div class="col-md-3 col-sm-6 col-xs-12 special_imgs frontpagepath pad0A">
        <div class="cleanimg box-shadow-spz pad0A">
            <div class="path-img<?php echo $specialdetails->degree; ?>">
                <a href="<?php echo $CFG->wwwroot.'/eduopen/pathway_details.php?specialid='.$specialdetails->id ?> ">
                    <img src="<?php echo $specialimg ?>" class="img-responsive" width="100%"/>
                    <?php if ($specialdetails->degree == 1) { ?>
                    <div class="mbanner">
                        <?php echo $degree = get_string('mdeg', 'theme_eduopen');?>
                    </div>
                    <?php } else if ($specialdetails->degree == 2) { ?>
                    <div class="abanner">
                        <?php echo $degree = get_string('adeg', 'theme_eduopen');?>
                    </div>
                    <?php } else if ($specialdetails->degree == 3) { ?>
                    <div class="obanner">
                        <?php echo $degree = get_string('odeg', 'theme_eduopen');?>
                    </div>
                    <?php } else { ?>
                    <div class="empty_ban"></div>
                    <?php } ?>
                </a>
            </div>
            <!-- Rests of the divs -->
            <?php
            if ($specialdetails->degree == 0) {
                $classid = 0;
            } else {
                $classid = 1;
            }
            ?>
            <div class="restpathdivs<?php echo $classid; ?>">
            <div id="path_name" class="special_nms<?php echo $specialdetails->degree; ?>">
                <a href="<?php echo $CFG->wwwroot.'/eduopen/pathway_details.php?specialid='.$specialdetails->id ?> ">
                    <p><?php echo $specialdetails->name;?></p>
                </a>
            </div>
            <?php
            $gen = $DB->get_records_sql("SELECT DISTINCT institution FROM {course_extrasettings_general}
            WHERE specializations LIKE '%$specialdetails->id%' LIMIT 3");
            $gencount = count($gen);
            $instcount = 1;
            ?>
            <div class="inst_right">
            <?php
            foreach ($gen as $value1) {
                $ins = $DB->get_record_sql("SELECT * FROM {block_eduopen_master_inst} WHERE id=$value1->institution");
                if ($instcount == $gencount) {
                ?>
               <div class="institutnm">
                    <span><a href="<?php echo $CFG->wwwroot.'/eduopen/institution_details.php?institutionid='.$ins->id ?> ">
                        <?php echo $ins->name; ?>
                    </a></span>
                </div>
                <?php } else { ?>
                <div class="rests">
                    <span><a href="<?php echo $CFG->wwwroot.'/eduopen/institution_details.php?institutionid='.$ins->id ?> ">
                        <span><?php echo $ins->name . ', ' ; ?>
                    </a></span>
                </div>
                <?php
                }
                $instcount++;
            }
            ?>
         </div>
        </div>
        </div>
    </div>
    <?php
    if (($blockcount % 4) == 0) {
        // echo '</div>' ;
    }
    if (($blockcount % 4) == 0) {
        //echo '<div class ="row dsrow" id="rowmrgspl2">' ;
    }
    }
    ?>
</div>
<div class="container-fluid frontcont pathwaybtn tcenter viewmorebtn" id="">
    <a class="btn" href="<?php echo $CFG->wwwroot.'/eduopen/pathway.php?mode=browseallpathways' ?> ">
        <?php echo ucwords(get_string('spzview', 'theme_eduopen')); ?>
    </a>
</div>