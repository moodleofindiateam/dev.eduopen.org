<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');

$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $PAGE, $DB, $CFG, $USER, $COURSE;
$PAGE->requires->jquery();
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("includes/head.php"); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(array('full-width')); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
            <header class="header container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->

                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php include('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header>
            <!--header end-->
        </div>
        <section id="container" class="hr-menu"> 
            <section id="main-content">
                <section class="wrapper">
                    <div id="page" class="container-fluid cmediaqry frontcont">
                        <div id="page-content" class="row-fluid">
                            <?php
                            //$crscontext = context_course::instance($COURSE->id);
                           //$student = get_student($COURSE->id, $USER->id, $crscontext->id);
                           $notstudent =  visible_roles();
                            ?>
                            <div id="<?php echo $regionbsid ?>" class="col-md-12 pad0A">
                                <?php
                                if ((is_siteadmin()) || $notstudent) {
                                    ?>
                                    <div class="col-md-10"><?php echo $OUTPUT->navbar(); ?></div>
                                    <div class="col-md-2"><?php echo $OUTPUT->page_heading_button(); ?></div>
                                <?php } ?>
                                <div class="row-fluid">
                                    <section id="region-main" class="col-md-12">
                                        <section id="region-main" class="col-md-12 pull-right">
                                            <?php
                                            echo $OUTPUT->course_content_header();
                                            echo $OUTPUT->main_content();
                                            echo $OUTPUT->course_content_footer();
                                            ?>
                                        </section>
                                    </section>
                                </div>
                            </div>
                            <!-- end shiuli  -->
                        </div>
                    </div>
                </section>
            </section>
        </section>
        <?php include("includes/footersetting.php"); ?>
        <?php echo $OUTPUT->standard_end_of_body_html() ?>
        <?php include("includes/foot.php"); ?>
    </body>
</html>

<script type="text/javascript">
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>