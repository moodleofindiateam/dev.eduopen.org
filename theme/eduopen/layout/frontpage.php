<?php
/**
 * Moodle's
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '../../../../my/lib.php');
require_once(dirname(__FILE__) . '/../../../eduopen/lib.php');

//require_once('/../../eduopen/lib.php');
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jssorslider.js'), true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));


if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $PAGE, $DB, $CFG;
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php require_once("includes/head.php"); ?>
    </head>
    <body <?php echo $OUTPUT->body_attributes(array('full-width frontpagebody')); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
            <header class="header container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php
                            //echo format_string($SITE->shortname, true,
                            //array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->
                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php require('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header><!--header end-->
        </div>
        <section id="container" class="hr-menu">
            <section id="main-content">
                <section class="wrapper" id="main_sec1">
                    <div id="homepage">
                        <div id="page" class="container-fluid cmediaqry">
                            <div class="row frontsliderimg">
                                <div id="slider1_container" class="container-fluid jsocont">
                                    <!-- Loading Screen -->
                                    <div u="slides" class="jslide">
                                        <?php
                                        $bck = "SELECT id, name, banner FROM {block_eduopen_master_inst} ORDER BY RANDOM()";
                                        $institution = $DB->get_records_sql($bck);
                                        foreach ($institution as $institutions) {
                                            $institutionsobj = new stdClass();
                                            $institutionsobj = $institutions;
                                        }

                                        $banner = banner_images($institutionsobj);
                                        ?>
                                        <div id="bck" class="jumbotron">
                                            <img u="image" id="InstBan" src="<?php echo $banner; ?>" class="banimgg img-responsive" />
                                            <div class="message row-fluid" id="MessageDiv">
                                                <div class="col-md-12 col-sm-12 col-xs-12 mdquery" id="Mheight">
                                                    <h4 class="txtbgcolor tcenter">
                                                        <?php print_string('hmj', 'theme_eduopen') ?>
                                                    </h4>
                                                    <div class="tcenter toppad">
                                                        <a href="<?php echo $CFG->wwwroot . '/eduopen/catalog.php?mode=browseallcourses' ?>">
                                                            <button class="catbtn"><?php print_string('brwscat', 'theme_eduopen'); ?></button>
                                                        </a>
                                                    </div>
                                                    <div class="col-md-12 col-sm-12 col-xs-12 pad0A univpad">
                                                        <div class="col-md-8 col-sm-8 col-xs-8 pad0A">
                                                            <div class="allcount pull-right">
                                                                <span class="frontwhite">
                                                                    <?php
                                                                    $users = "SELECT COUNT(id) AS totalusers FROM {user}
                                                                    WHERE confirmed=1 AND deleted=0 
                                                                      AND username!='guest'AND username!='admin'";
                                                                    $user = $DB->get_record_sql($users);
                                                                    ?>
                                                                    <?php echo $user->totalusers ?>&nbsp;
                                                                    <?php print_string('hmcourse', 'theme_eduopen') ?>
                                                                </span>
                                                                &nbsp;<span>
                                                                    <img src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/frnt_circle.png' ?>" class="dotimg img-circle">
                                                                </span>&nbsp;
                                                                <span class="frontwhite">
                                                                    <?php print_string('hmlearn', 'theme_eduopen') ?>
                                                                    <?php
                                                                    //Updated by shiuli on 25th sep.
                                                                    $ccourse = "SELECT COUNT(id) AS
                                                                totalcourses FROM {course} WHERE id!=1 and visible=1";
                                                                    $cc = $DB->get_record_sql($ccourse);
                                                                    ?>
                                                                    <?php echo $cc->totalcourses ?>&nbsp;
                                                                    <?php print_string('hmc', 'theme_eduopen') ?>
                                                                </span>
                                                                &nbsp;<span>
                                                                    <img src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/frnt_circle.png' ?>" class="dotimg img-circle">
                                                                </span>&nbsp;
                                                                <span class="frontwhite">
                                                                    <?php print_string('hmjoin', 'theme_eduopen') ?>
                                                                    <?php
                                                                    $pathways = "SELECT COUNT(id) AS totalpathway FROM {block_eduopen_master_special}
                                                                    WHERE status='1'";
                                                                    $user = $DB->get_record_sql($pathways);
                                                                    ?>
                                                                    <?php echo $user->totalpathway; ?>&nbsp;
                                                                    <?php print_string('splz', 'theme_eduopen'); ?>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-4 col-sm-4 col-xs-4 pad0A">
                                                            <span class="instnamef pull-right">
                                                                <a href="<?php echo $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $institutionsobj->id; ?>">
                                                                    <?php echo $institutionsobj->name; ?> 
                                                                </a>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!--stats-->
                                        </div>
                                        <?php //} ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--Added by shiuli-->

                        <div class="container-fluid frontcont" id='MdWidth'>
                            <div class="container-fluid frontcont tcenter fcourse" id="">
                                <h2 class=""><?php print_string('featured_courses', 'theme_eduopen'); ?></h2>
                                <hr>
                            </div>
                            <?php
                            $coursedetails = "SELECT c.id, c.fullname, c.startdate, ce.courseimage, ce.language,
                            ce.contextid, ce.institution, ce.specializations, ce.featurecourse, ce.engtitle, ce.certificate2,
                            ce.formalcredit FROM {course} c LEFT JOIN {course_extrasettings_general} ce
                            ON c.id=ce.courseid WHERE c.category!=0 
                            AND c.visible=1 AND ce.featurecourse=1 AND ce.coursestatus=1
                            ORDER BY RANDOM() LIMIT 12";
                            $rs = $DB->get_records_sql($coursedetails);
                            $count = 0;
                            if ($rs) {
                                foreach ($rs as $details) {
                                    $cidArray[] = $details->id;
                                }
                                echo '<div class="row-fluid" id ="frontrowpad">';
                                echo featuredcourse_block($cidArray);
                                if (($count % 4) == 0) {
                                    echo '</div>';
                                }
                                if (($count % 4) == 0) {
                                    echo '<div class="row-fluid" id ="frontrowpad">';
                                }
                            }
                            ?>

                            <div class="container-fluid tcenter viewmorebtn" id="">
                                <a class="btn" href="<?php echo $CFG->wwwroot . '/eduopen/catalog.php?mode=browseallcourses' ?> ">
                                    <?php
                                    echo ucwords(get_string('hmview', 'theme_eduopen'));
                                    ?>
                                </a>
                            </div>
                        </div>
                        <div class="container-fluid front_pathway">
                            <div class="container-fluid tcenter fcourse" id=""> 
                                <h2><?php print_string('featured_pathways', 'theme_eduopen'); ?></h2>
                                <hr>
                            </div>
                            <div class="row-fluid" id="frontrowpad"> 
                                <?php //require_once("part2.php"); ?>
                                <?php
                                $blockcount = 0;
                                $allspecial = $DB->get_records_sql("SELECT * FROM {block_eduopen_master_special} WHERE status='1' ORDER BY RANDOM() LIMIT 4");
                                foreach ($allspecial as $specialdetails) {
                                    $pathidArray[] = $specialdetails->id;
                                    $specialimg = specialization_images($specialdetails);
                                    $defaultcourseimage = $CFG->wwwroot . '/theme/eduopen/pix/default_pathway.png';
                                    $specialimg = $specialimg ? $specialimg : $defaultcourseimage;
                                    $blockcount++;
                                }
                                echo featured_pathway_block($pathidArray);
                                ?>
                            </div>
                            <div class="container-fluid pathwaybtn tcenter viewmorebtn" id="">
                                <a class="btn" href="<?php echo $CFG->wwwroot . '/eduopen/catalog.php?mode=browseallpathways' ?> ">
                                    <?php echo ucwords(get_string('spzview', 'theme_eduopen')); ?>
                                </a>
                            </div>
                        </div>
                        <div class="container-fluid ">
                            <div class="container-fluid  tcenter fcourse">
                                <h2><?php print_string('most_popular_courses', 'theme_eduopen'); ?></h2>
                                <hr>
                            </div>
                            <div class="row-fluid" id="">
                                <?php
                                $coursedetails3 = "SELECT cxt.instanceid as CourseID, c.fullname as CourseName,
                                c.startdate, c.fullname, ce.courseimage, ce.language, ce.contextid, ce.institution, ce.specializations, 
                                ce.featurecourse, ce.engtitle, ce.certificate2, ce.formalcredit, ce.courseimage 
                                FROM {role_assignments} ra
                                JOIN {context} cxt on ra.contextid = cxt.id
                                JOIN {course} c on cxt.instanceid = c.id
                                JOIN {course_extrasettings_general} ce ON ce.courseid=c.id
                                JOIN {user} u on ra.userid = u.id
                                WHERE cxt.contextlevel = '50' AND roleid = '5' AND c.category!=0 AND c.visible=1
                                AND ce.coursestatus = 1
                                GROUP BY cxt.instanceid, c.fullname, c.startdate, c.fullname, ce.courseimage, ce.language,
                                ce.contextid, ce.institution, ce.specializations,
                                ce.featurecourse, ce.engtitle, ce.certificate2, ce.formalcredit, ce.courseimage 
                                ORDER BY COUNT (userid) DESC LIMIT 4";
                                $rs3 = $DB->get_records_sql($coursedetails3);
                                $count3 = 0;
                                if ($rs3) {
                                    foreach ($rs3 as $details3) {
                                        $cidArray3[] = $details3->courseid;
                                    }
                                    echo '<div class="row-fluid" id ="frontrowpad">';
                                    echo featuredcourse_block($cidArray3);
                                    if (($count3 % 4) == 0) {
                                        echo '</div>';
                                    }
                                    if (($count3 % 4) == 0) {
                                        echo '<div class="row-fluid" id ="frontrowpad">';
                                    }
                                }
                                ?>
                            </div>
                            <div class="container-fluid  tcenter viewmorebtn" id="">
                                <a class="btn" href="<?php echo $CFG->wwwroot . '/eduopen/catalog.php?mode=browseallcourses' ?> ">
                                    <?php echo ucwords(get_string('hmview', 'theme_eduopen')); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="hide"><?php echo $OUTPUT->main_content(); ?></div>
                </section>
            </section>
        </section>
        <?php include("includes/footersetting.php"); ?>
        <?php echo $OUTPUT->standard_end_of_body_html() ?>
        <?php include("includes/foot.php"); ?>
    </body>
</html>
<?php /* require("includes/footersetting.php");
  echo $OUTPUT->standard_end_of_body_html();
  require("includes/foot.php");
 */ ?>
<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>


<!-- Script for responsive banner image -->
<script>
    $(window).load(function () {
        var wdth = $(window).width();
        $("#InstBan").width(wdth);
        $("#MessageDiv").width(wdth);
        var hei = $("#InstBan").height();
        $("#Mheight").height(hei);
    });

    $(window).resize(function () {
        var wdth = $(window).width();
        $("#InstBan").width(wdth);
        $("#MessageDiv").width(wdth);
        var hei = $("#InstBan").height();
        $("#Mheight").height(hei);
    });
</script>

