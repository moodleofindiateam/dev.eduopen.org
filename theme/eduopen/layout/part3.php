<div class="container-fluid frontcont tcenter fcourse">
    <h2><?php print_string('most_popular_courses', 'theme_eduopen'); ?></h2>
    <hr>
</div>
<div class="row-fluid" id="frontrowpad">
    <?php
    $coursedetails = "SELECT cxt.instanceid as CourseID, c.fullname as CourseName,
    c.startdate, c.fullname, ce.courseimage, ce.language, ce.contextid, ce.institution, ce.specializations, 
    ce.featurecourse, ce.engtitle, ce.certificate2, ce.formalcredit, ce.courseimage 
    FROM {role_assignments} ra
    JOIN {context} cxt on ra.contextid = cxt.id
    JOIN {course} c on cxt.instanceid = c.id
    JOIN {course_extrasettings_general} ce ON ce.courseid=c.id
    JOIN {user} u on ra.userid = u.id
    WHERE cxt.contextlevel = '50' AND roleid = '5' AND c.category!=0 and c.visible=1
    GROUP BY cxt.instanceid, c.fullname, c.startdate, c.fullname, ce.courseimage, ce.language,
    ce.contextid, ce.institution, ce.specializations, ce.featurecourse, ce.engtitle, ce.certificate2, ce.formalcredit, ce.courseimage 
    ORDER BY COUNT (userid) DESC LIMIT 4";

    /* $coursedetails = "SELECT c.id, c.fullname, c.startdate, ce.courseimage, ce.language,
      ce.contextid, ce.institution, ce.featurecourse, ce.engtitle, ce.certificate2,
      ce.formalcredit FROM {course} c LEFT JOIN {course_extrasettings_general} ce
      ON c.id=ce.courseid WHERE c.category!=0 and c.visible=1 "; */
    $rs = $DB->get_records_sql($coursedetails);

    $count = 0;
    if ($rs) {
        foreach ($rs as $details) {
            $courseid = $details->courseid;
            $coursename = $details->fullname; // Comes from Course edit settings.
            $coursetitle = $details->engtitle; // Comes from extra settings.
            $startdatetimestamp = $details->startdate; //course start date.
            $dateafteramonth = time() + (30 * 24 * 60 * 60); // date after 1 month.
            $feature = $details->featurecourse;
            $exam = $details->formalcredit;
            $institute = $details->institution;
            $pathway = $details->specializations;
            ;
            $csettingsinstitutionid = $DB->get_record('block_eduopen_master_inst', array('id' => $institute), 'name');
            if ($csettingsinstitutionid) {
                $instname = $csettingsinstitutionid->name;
            }
            //$cstartdate = date("F d, Y", $startdatetimestamp);
            $cstartdate = date("dS F, Y", $startdatetimestamp);

            $startdate = gmdate("M dS", $startdatetimestamp);
            $courseimageid = $details->courseimage;
            $contextid = $details->contextid;
            $defaultimage = $CFG->wwwroot . '/theme/eduopen/pix/default_course.png';
            //$imageurl = get_uploaded_image($courseimageid, $contextid);
            $imageurl = csetting_images($details);

            $imageurl = $imageurl ? $imageurl : $defaultimage;
            $count++;
            /* $context = get_context_instance(CONTEXT_COURSE, $courseid);
              $students = get_role_users(5 , $context); */
            ?>
            <div class="col-md-3 col-sm-6 col-xs-12 textcenter pad0A">
                <div class="box-shadow">
                    <div class="course-img front_cimg<?php echo $details->certificate2; ?>">
                        <a class="eduopen-link" 
                           href="<?php echo $CFG->wwwroot . '/eduopen/course_details.php?courseid=' . $courseid ?>">
                            <img src="<?php echo $imageurl ?>" width="100%" alt='<?php echo $coursename ?>'>
                        </a>
                        <?php if ($details->certificate2 == '1') { ?>
                            <div class="banner"><?php echo "VERIFIED"; ?></div>
                        <?php } else { ?>
                            <div class="empty_ban"></div>
                        <?php } ?>
                    </div>
                    <!--rests of the divs after an image div.-->
                    <div class="restcontent cont<?php echo $details->certificate2 ?>">
                        <div id="f_inst" class="ban<?php echo $details->certificate2; ?>" class="padl5">
                            <a class="eduopen-link"
                               href="<?php echo $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $institute ?>">
                                <p class="instclr"><?php echo strip_tags($instname) ?></p>
                            </a>
                        </div>
                        <div class="course-details">
                            <a class="eduopen-link" 
                               href="<?php echo $CFG->wwwroot . '/eduopen/course_details.php?courseid=' . $courseid ?>">
                                <p><?php echo strip_tags($coursename); ?></p>
                            </a>
                        </div>
                        <div class="crs-pathname">
                            <?php
                            if ($pathway != 'none') {
                                $specializationid = explode(',', $pathway);
                                if ($specializationid) {
                                    ?>  
                                    <div class="frontpathhead">
                                        <b><?php echo get_string('splz', 'theme_eduopen') . ': '; ?></b>
                                        <span class="pathlist">
                                            <?php
                                            for ($i = 0; $i < count($specializationid); $i++) {
                                                $specialdata = $DB->get_record('block_eduopen_master_special', array('id' => $specializationid[$i]), 'name');
                                                if ($specializationid[$i] != 0) {
                                                    $deli = $i < count($specializationid) - 1 ? ', ' : ''; // comma between two pathways except last one
                                                    echo '<a class="" href="' . $CFG->wwwroot . '/eduopen/pathway_details.php?specialid=' . $specializationid[$i] . '">
                                             ' . strip_tags($specialdata->name) . $deli . '</a>';
                                                }
                                            }
                                            ?>
                                        </span>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>

                        <div class="col-md-12 course-date-btn start_time padl5">
                            <div class="col-md-12 col-sm-10 col-xs-10 pad0A">
                                <div class="col-md-12 pad0A">
                                    <div class="date cstarts padl5">
                                        <?php
                                        $textdate = get_string('cs_start_u', 'theme_eduopen');
                                        echo '<i class="fa fa-calendar"></i>&nbsp;';
                                        echo $cstartdate;
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            if (($count % 4) == 0) {
                echo "</div>";
            }
            if (($count % 4) == 0) {
                echo "<div class='row-fluid' id ='frontrowpad'>";
            }
        }
    }
    ?>
</div><!--end of rowmrglr2-->
<div class="container-fluid frontcont tcenter viewmorebtn" id="">
    <a class="btn" href="<?php echo $CFG->wwwroot . '/eduopen/catalog.php?mode=browseallcourses' ?> ">
        <?php echo ucwords(get_string('hmview', 'theme_eduopen')); ?>
    </a>
</div>