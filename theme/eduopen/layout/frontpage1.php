<?php


/**
 * frontpage file for site.
 * @package   theme_eduopen
 * @copyright 2015 Moodle Of India <httpe://www.moodleofindia.com>
 * @author Naina R <nainar@elearn10.com> 
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 

// Get the HTML for the settings bits.

//eduopen frontpage code by Naina on 05/mar/2015

	require_once(dirname(__FILE__) . '/../../../config.php');
	require_once($CFG->dirroot.'/theme/eduopen/lib.php');
	
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);

 global $DB;
echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<header class="header fixed-top">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="fa fa-bars"></span>
				</button>
			<!--logo start-->
			<div class="brand">

				<a class="logo" href="<?php echo $CFG->wwwroot;?>"><?php echo
					format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
					?>
				</a>
				
			</div>
			<!--logo end-->
			
			<div class="horizontal-menu navbar-collapse collapse ">

				<ul class="nav navbar-nav">
					<?php include('/includes/main_navigation.php'); ?>
				</ul>
				
			</div>
			
			<div class="top-nav hr-top-nav">
				<ul class="nav pull-right top-menu">
					<li id="SearchBox">
						<input type="text" class="form-control search" placeholder=" Search">
					</li>
					<!-- user login dropdown start-->
					<li>
						<?php echo $OUTPUT->user_menu(); ?><!--User menu-->
					</li>
					<li>
						<?php echo $OUTPUT->page_heading_menu(); ?>
					</li>
					<!-- user login dropdown end -->
					
					<li>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#custom-menu-navbar-collapse">
							<div class="fa fa-bars"></div>
						</button>
					</li>
				</ul>
				<!--search & user info end-->
			</div>
		</div>
		</header>
		<!--header end-->
<div id="homepage" >
	<div id="page" class="container-fluid">
		
		<div class="row">

			<div id="bck" class="jumbotron">
				
				<div class="message">
							
					<h1 class="msg"><?php print_string('hmj','theme_eduopen')?>
						<br/>
						<?php print_string('hmjo','theme_eduopen')?>
					</h1>
							
					

						<form>
							<label class="input-hide" for="search-input">
								<?php print_string('hmsearch','theme_eduopen')?>
							</label>
							<div class="search-input-container">
								<input id="search-input" class="search-input" autocomplete="off" data-js="search" placeholder="What would you like to learn about?"></input>
								<button class="search-button"></button>

							</div>
						</form>
				
					<div class="stats">
						<div>
							<h3>
							<?php print_string('hmjoin','theme_eduopen')?>
							<?php $users="SELECT COUNT(id) AS totalusers FROM {user} WHERE confirmed=1 AND deleted=0 AND username!='guest' AND username!='admin'";
								$user= $DB->get_record_sql($users); ?>
								
							<span><?php echo $user->totalusers ?>  </span>
								<?php print_string('hmcourse','theme_eduopen')?>
							</h3>
						</div>
						<div>
							<h3>
								<?php print_string('hmlearn','theme_eduopen')?>
								<?php $ccourse= "SELECT COUNT(id) AS totalcourses FROM {course}"; 
									$cc= $DB->get_record_sql($ccourse);
									//var_dump($cc);
								?>
								
								<span ><?php echo $cc->totalcourses ?> </span>
								
								 <?php print_string('hmc','theme_eduopen')?>
								 <?php $inst="SELECT COUNT(id) AS totalinstitution FROM {block_eduopen_master_inst}";
								 $ins= $DB->get_record_sql($inst);
								 ?>
								<span><?php echo $ins->totalinstitution ?> &nbsp;</span><?php print_string('hmp','theme_eduopen')?> 
							</h3>
						</div>
						
						<div>
							<h3>
							   <a  class="text-link" href="" style="text-decoration:none;"><?php print_string('hmhow','theme_eduopen')?> </a>
							</h3>
						</div>
						
						
							
					</div><!--stats-->
					
				</div><!-- /.msg -->
				
				<div class="inst-link"> 
					<a href=""></a>
				</div>
			</div>
		</div>
		
	</div>
</div>

<div class="row marg15T">
<?php
	
	$c_details="SELECT c.id,c.fullname,c.startdate,ce.courseimage,ce.contextid FROM {course} c LEFT JOIN {course_extrasettings_general} ce ON c.id=ce.courseid WHERE c.category!=0 and c.visible=1 LIMIT 12";
	
	$rs=$DB->get_records_sql($c_details);
	$count = 0;
	
	if($rs){
		
		foreach($rs as $details){
			$course_id = $details->id;
			$course_name = $details->fullname;
			$startdate_timestamp = $details->startdate;
			
			$startdate = gmdate("M dS", $startdate_timestamp);
			$c_imgid = $details->courseimage;
			$context_id = $details->contextid;
			$default_img = $CFG->wwwroot.'/theme/eduopen/pix/default_course.png';
			
			$img_url = get_uploaded_image($c_imgid,$context_id);
			
			$img_url = $img_url ? $img_url : $default_img; ?>
			
			

	
	
			 
			 <?php 
				 $count++;
				 if(($count % 3) == 0){
				 echo "<div class='row'>"; } ?>
					 <div class="col-md-4 col-sm-6 col-xs-6 textcenter">
				 <div class="course-img">
					<a class="eduopen-link" href="javascript:;">
							 <img src="<?php echo $img_url ?>" width="100%" height="250px" alt='<?php echo $course_name ?>'></img>
					 </a>
			 </div>
				 <div class="course-details">
					<a  class="eduopen-link" href=".$CFG->wwwwroot/">
						 <h3> <?php echo $course_name ?> </h3>
					 </a>
					 <a class="eduopen-link" href="javascript:;">
						UMD
					 </a>
					 <span> • </span>
					 <span><?php echo $startdate ?> </span>
				 </div>
			 </div>	
			 
				<?php 
				 if(($count % 3) == 0){
					 echo "</div>";
				 }
			 
			
		
		
		}
	}
	?>	
		
	
	</div>
	<div class="row marg15T">
		<div class="well">
			<a class="eduopen-link" href="javascript:;"><?php print_string('hmview','theme_eduopen') ?></a>
		</div>	
	</div>

    <div id="page-content" class="row-fluid">
        <section id="region-main" class="span12">
            <?php
                 echo $OUTPUT->main_content();            
            ?>
        </section>
    </div>

    <footer id="page-footer">
        <div id="course-footer"><?php echo $OUTPUT->course_footer(); ?></div>
        <p class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></p>
        <?php
        echo $html->footnote;
        echo $OUTPUT->login_info();
        echo $OUTPUT->home_link();
        echo $OUTPUT->standard_footer_html();
        ?>
    </footer>

    <?php echo $OUTPUT->standard_end_of_body_html() ?>
	
	<?php include("/includes/foot.php"); ?>
	<script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/homepage.js'; ?>" > </script>

</div>
</body>
</html>
