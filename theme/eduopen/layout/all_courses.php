<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/layout.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/all_courses.js'));

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $DB, $CFG, $USER;
//$update = $DB->execute("UPDATE mdl_eduplayer SET autostart='true' WHERE autostart='false'");
$mode = required_param('mode', PARAM_TEXT);
echo $OUTPUT->doctype()
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include_once("includes/head.php"); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(array('full-width catalogpagebody')); ?>>

        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
            <header class="header container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php
                            //echo format_string($SITE->shortname, true,
                            //array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->
                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php require('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header><!--header end-->
        </div>
        <section id="container">
            <section id="main-content" class="catpage">
                <section class="wrapper">
                    <div class="row catalogmd">
                        <div class="cseacrh col-md-12 col-sm-12 col-xs-12 ">
                            <div class="container-fluid maxwd">
                                <div class="col-md-3 col-sm-3 col-xs-5" id="crsbackcolor">
                                    <p id="filteredcname"><?php print_string('catalogheading', 'theme_eduopen'); ?></p>
                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-7 searchdiv">
                                    <?php
                                    $themename = $PAGE->theme->name;
                                    $allcourses = $DB->get_records_sql("SELECT id,fullname  FROM {course} where visible =1 AND id !=1");
                                    $PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/' . $themename . '/javascript/eduopencustom.js'));
                                    ?>
                                    <form id="datasearch"
                                          action=""
                                          >
                                        <div class="search-input-container">
                                            <input list="coursesearch" name="coursesearch" class="search-input" id ="coursename" value=""  placeholder = "Search in the Catalog">
                                            <datalist id="coursesearch">

                                                <?php foreach ($allcourses as $course) { ?>
                                                    <option value="<?php echo $course->fullname ?>" data-xyz ="<?php echo $course->id ?>">
                                                    <?php } ?>

                                            </datalist>
                                            <button class="search-button" name="search_btn" onclick="get_data()" title="Click to Search"  type="button"><i class="fa fa-search"></i></button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cust container-fluid">
                        <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                            <div class="col-md-3 col-sm-3 col-xs-5 sidebackcolor">
                                <div id="sidebar" class="nav-collapse">
                                    <!-- sidebar menu start-->
                                    <div class="leftside-navigation">
                                        <ul class="sidebar-menu" id="nav-accordion">
                                            <!--Left side bar items goes here--> 
                                            <div id="FilterForm">
                                                <p class="browse_courses"><?php print_string('categories', 'theme_eduopen') ?></p>

                                                <div id="CategoryFilter" class="as">
                                                    <!--<ul class="a" style="display: block !important;">-->
                                                    <li id="allfilters">
                                                        <a class="full-width" href="javascript:;"><span id="all"><?php print_string('categorydirectlink', 'theme_eduopen') ?></span></a>
                                                    </li>    
                                                    <?php
                                                    //Filter by course categories section.
                                                    $category_rs = $DB->get_records('course_categories', array('visible' => '1')); //only show the visible categories.
                                                    if ($category_rs) {
                                                        foreach ($category_rs as $category) {
                                                            $category_id = $category->id;
                                                            $category_name = $category->name;
                                                            $course_count = $category->coursecount;
                                                            echo '<li class="' . $category->name . '"><a  class="full-width " href="javascript:;"><input name="categories[]" value="' . $category_id . '" type="radio"/>' . $category_name . '</a></li>';
                                                        }
                                                    }
                                                    ?>
                                                    <!--</ul>-->
                                                </div>
                                                <!--  <a href="javascript:;">                             
                                                     <span class="full-width advancedfill"><?php print_string('advancedfil', 'theme_eduopen') ?></span>
                                                 </a> -->
                                                <br>
                                                <a href="javascript:;">                             
                                                    <span class="full-width advancedfill allinst"><?php echo strtoupper(get_string('browsebyinst', 'theme_eduopen')) ?></span>
                                                </a>
                                                <!--    <li id="BrowsecourseFilter" class="sub-menu ">
                                                       <a href="javascript:;" id="browseallcourse">                             
                                                           <span class="full-width " id ="browseallcoursesleftpanel"><i class="fa fa-plus-square"></i><?php print_string('filterbycourses', 'theme_eduopen') ?></span>
                                                       </a>
   
                                                       <ul class="sub">
                                                           <li id="TypeFilter" class="sub-menu">
                                                               <a href="javascript:;">                             
                                                                   <span class="full-width "><?php print_string('browsebytype', 'theme_eduopen') ?></span>
                                                               </a>
                                                               <ul class="sub">
                                                                   <li><a class="full-width " href="javascript:;"><input value="Online" name="coursetype" type="checkbox"/><?php print_string('online', 'local_course_extrasettings') ?></a></li>
                                                                   <li><a class="full-width " href="javascript:;"><input value="Classroom" name="coursetype" type="checkbox"/><?php print_string('classroom', 'local_course_extrasettings') ?></a></li>
                                                                   <li><a class="full-width " href="javascript:;"><input value="Blended" name="coursetype" type="checkbox"/><?php print_string('blended', 'local_course_extrasettings') ?></a></li>
                                                               </ul>
                                                           </li>
                                                           <li id="CategoryFilter" class="sub-menu">
                                                               <a href="javascript:;">                             
                                                                   <span class="full-width "><?php print_string('browsebycategories', 'theme_eduopen') ?></span>
                                                               </a>
                                                               <ul class="sub">
                                                <?php
                                                //Filter by course categories section.
                                                $category_rs = $DB->get_records('course_categories', array('visible' => '1')); //only show the visible categories.
                                                if ($category_rs) {
                                                    foreach ($category_rs as $category) {
                                                        $category_id = $category->id;
                                                        $category_name = $category->name;
                                                        $course_count = $category->coursecount;
                                                        echo '<li><a  class="full-width " href="javascript:;"><input name="categories[]" value="' . $category_id . '" type="checkbox"/>' . $category_name . ' (' . $course_count . ')' . '</a></li>';
                                                    }
                                                }
                                                ?>
                                                               </ul>
                                                           </li>
                                                           <li id="LanguageFilter" class="sub-menu">
                                                               <a href="javascript:;">                             
                                                                   <span class="full-width ">
                                                <?php print_string('browsebylanguage', 'theme_eduopen') ?>
                                                                   </span>
                                                               </a>
                                                               <ul class="sub">
                                                <?php
                                                $lang = get_string_manager()->get_list_of_translations();
                                                foreach ($lang as $key => $value) {
                                                    ?>
                                                                                                   <li><a class="full-width " href="javascript:;"><input value="<?php echo $key; ?>" name="language"
                                                                                                                                                         type="checkbox"/><?php echo $value; ?></a>
                                                                                                   </li>
                                                <?php } ?>
                                                               </ul>
                                                           </li>
   
                                                           <li id="LevelFilter" class="sub-menu">
                                                               <a href="javascript:;">                             
                                                                   <span class="full-width "><?php print_string('browsebylevel', 'theme_eduopen') ?></span>
                                                               </a>
                                                               <ul class="sub">
                                                                   <li><a class="full-width " href="javascript:;"><input value="Beginner" name="courselevel" type="checkbox"/><?php print_string('clevel1', 'local_course_extrasettings') ?></a></li>
                                                                   <li><a class="full-width " href="javascript:;"><input value="Intermediate" name="courselevel" type="checkbox"/><?php print_string('clevel2', 'local_course_extrasettings') ?></a></li>
                                                                   <li><a class="full-width " href="javascript:;"><input value="Advanced" name="courselevel" type="checkbox"/><?php print_string('clevel3', 'local_course_extrasettings') ?></a></li>
                                                               </ul>
                                                           </li>
   
                                                       </ul>
                                                   </li> --><!--there codes are  advance filter code.removed for temporary-->
                                                <div>
                                                    <br>
                                                    <?php
                                                    // $csinstituton = $DB->get_records('block_eduopen_master_inst');
                                                    $csinstituton = $DB->get_records_sql("SELECT * FROM {block_eduopen_master_inst} ORDER BY refferencename ASC");
                                                    if ($csinstituton) {
                                                        foreach ($csinstituton as $institutonname) {
                                                            ?>
                                                            <li>
                                                                <a  target="_blank" class="full-width " href="<?php echo $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $institutonname->id; ?>"><?php echo $institutonname->name; ?>
                                                                </a>
                                                            </li>
                                                            <?php
                                                        }
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <!-- <div id="BrowsepathwayfilterForm">
                                                <li id="BrowsepathwayFilter" class="sub-menu">
                                                    <a href="javascript:;" id="browseallpathway">                             
                                                        <span class="full-width " id ="browseallpathwaysleftpanel"><i class="fa fa-plus-square"></i><?php print_string('filterbypathways', 'theme_eduopen') ?></span>
                                                    </a>
                                                    <ul class="sub">
                                            <?php
                                            //Filter by course categories section
                                            $csspecial = $DB->get_records('block_eduopen_master_special', array('status' => '1')); //only show the visible categories.
                                            if ($csspecial) {
                                                foreach ($csspecial as $specialcourse) {
                                                    $specialid = $specialcourse->id;
                                                    $specialname = $specialcourse->name;
                                                    //echo '<li id="specializationFilter"><a  class="full-width " href="javascript:;"><input name="specialization[]" value="'.$specialid.'" type="checkbox"/>'.$specialname.'</a></li>';
                                                }
                                            }
                                            ?>
                                                        <li id="recognizationFilter" class="sub-menu">
                                                            <a href="javascript:;">                             
                                                                <span class="full-width "><?php print_string('browsebyrecog', 'theme_eduopen') ?></span>
                                                            </a>
                                                            <ul class="sub">
                                                                <li><a class="full-width " href="javascript:;"><input value="0" name="recognization" type="checkbox"/><?php print_string('nodeg', 'block_specialization') ?></a></li>
                                                                <li><a class="full-width " href="javascript:;"><input value="1" name="recognization" type="checkbox"/><?php print_string('mdeg', 'block_specialization') ?></a></li>
                                                                <li><a class="full-width " href="javascript:;"><input value="2" name="recognization" type="checkbox"/><?php print_string('adeg', 'block_specialization') ?></a></li>
                                                                <li><a class="full-width " href="javascript:;"><input value="3" name="recognization" type="checkbox"/><?php print_string('odeg', 'block_specialization') ?></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="CategoryFilter" class="sub-menu">
                                                            <a href="javascript:;">                             
                                                                <span class="full-width "><?php print_string('browsebycategories', 'theme_eduopen') ?></span>
                                                            </a>
                                                            <ul class="sub">
                                            <?php
                                            //Filter by course categories section

                                            $category_rs = $DB->get_records('course_categories', array('visible' => '1')); //only show the visible categories.
                                            if ($category_rs) {
                                                foreach ($category_rs as $category) {
                                                    $category_id = $category->id;
                                                    $category_name = $category->name;
                                                    $course_count = $category->coursecount;
                                                    $pathwaycount = $DB->count_records('block_eduopen_master_special', array('category' => $category_id));
                                                    echo '<li><a  class="full-width " href="javascript:;"><input name="categories[]" value="' . $category_id . '" type="checkbox"/>' . $category_name . ' (' . $pathwaycount . ')' . '</a></li>';
                                                }
                                            }
                                            ?>
                                                            </ul>
                                                        </li>
                                                        <li id="LanguageFilter" class="sub-menu">
                                                            <a href="javascript:;">                             
                                                                <span class="full-width ">
                                            <?php print_string('browsebylanguage', 'theme_eduopen') ?>
                                                                </span>
                                                            </a>
                                                            <ul class="sub">
                                            <?php
                                            $lang = get_string_manager()->get_list_of_translations();
                                            foreach ($lang as $key => $value) {
                                                ?>
                                                                                                <li><a class="full-width " href="javascript:;"><input value="<?php echo $key; ?>" name="language"
                                                                                                                                                      type="checkbox"/><?php echo $value; ?></a>
                                                                                                </li>
                                            <?php } ?>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>
                                            </div> --><!--there codes are  advance filter code.removed for temporary-->
                                        </ul>
                                        <div id="archived" class="arc_course">
                                            <li id="Arch_Crs" class="Arch_Course">
                                                <a class="full-width " href="javascript:;">
                                                    <input name="archive[]" value="88888888" type="radio">
                                                    <?php print_string('archived_course', 'theme_eduopen'); ?>
                                                </a>
                                            </li>
                                        </div>
                                    </div>
                                    <!-- sidebar menu end-->
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-9 col-xs-7 pad0A" id="rightsideTab">
                                <?php
                                echo $OUTPUT->main_content();
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </section>
        <?php include("includes/footersetting.php"); ?>
        <?php echo $OUTPUT->standard_end_of_body_html() ?>
        <?php include_once("includes/foot.php"); ?>
    </body>
</html>

<style>
    #main-content {
        margin-left: 0 !important;
    }
</style>
<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>", loginurl: "<?php echo $CFG->wwwroot . '/login/index.php' ?>"};
    var filtermode = {value: "<?php echo $mode; ?>"};
    var coursesearch = {pageurl: "<?php echo $CFG->wwwroot . '/eduopen/course_details.php?courseid=' ?>"};
    var loaderurl = "<?php echo $CFG->wwwroot . '/theme/eduopen/pix/loader.svg' ?>";
</script>
<script>
    $(window).load(function () {
        var ht = $("#rightsideTab").height();
        extraheight = ht + 2;
        $(".sidebackcolor").height(extraheight);
    });
    $("div#CategoryFilter li").click(function () {
        $("div#CategoryFilter li").removeClass("catactive");
        $(this).addClass("catactive");
        var catid = $(this).find('input[name="categories[]"]').val();
        if (typeof catid != 'undefined') {
            if (($("ul.nav-tabs li#coursefilter").attr('class') == 'active') || ($("ul.nav-tabs li#pathwayfilter").attr('class') == 'active')) {
                var datas = {catid: catid, mode: 'categorycolor'}
                $.ajax({
                    url: "<?php echo $CFG->wwwroot . '/theme/eduopen/ajax_load.php' ?>",
                    type: 'post',
                    data: datas,
                    success: function (response) {
                        var jresponse = JSON.parse(response);
                        $("#crsbackcolor,.searchdiv").css('background-color', jresponse.colorcode);
                        $(".cseacrh").css('background-color', jresponse.colorcode);
                        $(".searchdiv").css({
                            'background-color': jresponse.colorcode,
                            'opacity': "0.8"
                        });
                        $("#filteredcname").text(jresponse.catname);

                    }
                });
            } else {
                var degfaultcolor = '#D95843';
                $("#crsbackcolor,.searchdiv").css('background-color', degfaultcolor);
            }
        } else {
            $("#filteredcname").text("All Category");
            var degfaultcolor = '#D95843';
            $("#crsbackcolor,.searchdiv").css('background-color', degfaultcolor);
            $(".searchdiv").css('background-color', 'rgba(249, 165, 143, 0.5) none repeat scroll 0% 0%');
            $(".cseacrh").css('background-color', degfaultcolor);
        }
    });
</script>

<script>
    $('#coursename').bind("keypress", function (event) {
        if (event.which == 13) {
            event.preventDefault();
            return false;
        }
    });
</script>
<script>
//     $(window).load(function () {
//         var size_li = $(".box-shadow").length;
//         x = 9;
//         y =  3;
//         $('.box-shadow:lt(' + x + ')').show();
//         $('div#CourseArea #frontrowpad:lt(' + y + ')').show();

//     $(window).scroll(function(){
//         var size_li = $(".box-shadow").length;

//     if( $(window).scrollTop() == ($(document).height() - $(window).height())) {
//             x = (x + 3 <= size_li) ? x + 3 : size_li;
//             y = y + 1;


//             $('div#CourseArea #frontrowpad:lt(' + y + ')').show();
//             $('.box-shadow:lt(' + x + ')').show('slow');
//         }
//         });

//         // $('#showLess').click(function () {
//         //     x = (x - 3 < 0) ? 3 : x - 3;
//         //     $('.box-shadow').not(':lt(' + x + ')').hide();
//         // });
//     }); 

</script>