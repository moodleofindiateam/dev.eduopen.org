<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $PAGE, $DB, $CFG, $USER, $COURSE;
$PAGE->requires->jquery();
echo $OUTPUT->doctype();

$crs = $DB->get_record('course', array('id' => $COURSE->id));
$crscontext = context_course::instance($COURSE->id);
$student = get_student($COURSE->id, $USER->id, $crscontext->id);
if (($crs->startdate >= time()) && $student) {
    $CrsCls = 'notstarted';
} else {
    $CrsCls = 'started';
}
?>

<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("includes/head.php"); ?>
    </head>
    <body <?php echo $OUTPUT->body_attributes(array('full-width')); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
            <header class="header container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->

                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php include('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header>
            <!--header end-->
        </div>
        <section id="container" class="hr-menu"> 
            <section id="main-content">
                <section class="wrapper">
                    <div id="page" class="container-fluid cmediaqry frontcont">
                        <div class="Msg">
                            <center><h2><?php echo get_string('youareenrolled', 'theme_eduopen') . ' ' . $crs->fullname; ?></h2></center>
                            <?php $vday = userdate($crs->startdate, '%d/%m/%Y'); ?>
                            <center><h4><?php echo get_string('learningactivitystart', 'theme_eduopen') . ' ' . $vday ?></h4></center>
                            <center><?php echo get_string('comeback', 'theme_eduopen') ?></center>

                        </div>
                        <div class="row-fluid contbtn">
                            <a href="<?php echo $CFG->wwwroot ?>"><button>Continue</button></a>
                        </div>
                        <script>
                            $(document).ready(function () {
                                var Condition = '<?php echo $CrsCls ?>';
                                var student = '<?php echo $student ?>';
                                if (Condition == 'notstarted' && student) {
                                    var aMsg = ' ';
                                    document.getElementById('crsclassId').innerHTML = aMsg;
                                }
                            });
                        </script>
                        <div id="crsclassId">
                            <?php echo $OUTPUT->main_content(); ?>
                        </div>
                    </div>
                </section>
            </section>
        </section>
    </section>
    <?php include("includes/footersetting.php"); ?>
    <?php echo $OUTPUT->standard_end_of_body_html() ?>
    <?php include("includes/foot.php"); ?>
</body>
</html>
<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>