<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */


// Get the HTML for the settings bits.
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot.'/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/javascript/language.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/theme/eduopen/javascript/layout.js'),true);
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/eduopen/js/eduopen.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot.'/eduopen/js/easyResponsiveTabs.js'));
$PAGE->requires->css(new moodle_url($CFG->wwwroot.'/eduopen/css/easy-responsive-tabs.css'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include("includes/head.php"); ?>
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

	<?php echo $OUTPUT->standard_top_of_body_html() ?>
	<section id="container">
		<header class="header fixed-top clearfix">
			<!--logo start-->
			<div class="brand">

				<a class="logo" href="<?php echo $CFG->wwwroot;?>">
				<?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
					?>
				</a>
				<div class="sidebar-toggle-box">
					<div class="tooltips fa fa-bars " data-placement="bottom" data-toggle="tooltip"  data-original-title="Show side bar"></div>
				</div>
			</div>
			<!--logo end-->

			<div class="nav notify-row" id="top_menu">
				<!--  notification start -->			
				
				<!--  notification end -->
			</div>
			
			<div id="top_menu" class="horizontal-menu">
			<!--Custom Menu starts-->

				<ul id="CustomMenu" class="nav navbar-nav">
					<?php include('includes/main_navigation.php'); ?>
				</ul>
				<!--Custom Menu ends-->
			</div>
			
			<div class="top-nav clearfix">				
				<!--search & user info start-->
				<ul class="nav pull-right top-menu">
					<li id="SearchBox">
						<input type="text" class="form-control search" placeholder=" Search">
					</li>
					<!-- user login dropdown start-->
					<li>
						<?php echo $OUTPUT->user_menu(); ?><!--User menu-->
					</li>
					<li>
						<?php echo $OUTPUT->page_heading_menu(); ?>
					</li>
					<!-- user login dropdown end -->
					<li>
						<div class="toggle-right-box">
							<div class="fa fa-bars"></div>
						</div>
					</li>					
				</ul>
				<!--search & user info end-->
			</div>
		</header>
		<!--header end-->
		<aside>
			<div id="sidebar" class="nav-collapse">
				<!-- sidebar menu start-->
				<div class="leftside-navigation">
					<!--Left side bar items goes here-->    
					<ul class="sidebar-menu" id="nav-accordion">
						<li id="WebNav" class="sub-menu" style="display:none; ">
							<a href="javascript:;">
								<i class="fa fa-laptop"></i>
								<span>Website Navigation</span>
							</a>
							<ul class="sub">
								<?php include('includes/main_navigation.php'); ?>
							</ul>
						</li>
						<!--Left side bar items goes here-->    
					
					</ul>					
				</div>
				<!-- sidebar menu end-->
			</div>
		</aside>
		
		<section id="main-content">
			<section class="wrapper">

				<div id="page">
					<div id="page-content" class="row-fluid">
						<div id="<?php echo $regionbsid ?>" class="span9">
							<div class="row-fluid">
								<section id="region-main" class="span8 pull-right">
									<?php
									echo $OUTPUT->course_content_header();
									echo $OUTPUT->main_content();
									echo $OUTPUT->course_content_footer();
									?>
								</section>
								<?php echo $OUTPUT->blocks('side-pre', 'span4 desktop-first-column'); ?>
							</div>
						</div>
						<?php echo $OUTPUT->blocks('side-post', 'span3'); ?>
					</div>
				</div>
				
			</section>	
		</section>
		<!--right sidebar start-->
		<div class="right-sidebar">
			<!--Right side bar items goes here-->
		</div>
		<!--right sidebar end-->
			
	</section>
	
		

		<?php echo $OUTPUT->standard_end_of_body_html() ?>

	<?php include("includes/foot.php"); ?>
	<script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/layout.js'; ?>" > </script>
</body>
</html>
<script>
var page = {url:"<?php echo $CFG->wwwroot ?>"};
</script>