<?php

/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */

// Get the HTML for the settings bits.
require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '../../../../my/lib.php');
require_once(dirname(__FILE__) . '../../../eduopen/lib.php');
$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);

if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}
global $DB,$CFG;
echo $OUTPUT->doctype() ?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php include("includes/head.php"); ?>
</head>

<body <?php echo $OUTPUT->body_attributes(array('full-width')); ?>>
	<?php echo $OUTPUT->standard_top_of_body_html() ?>
	<section id="container" class="hr-menu">		 
	
	
		<header class="header fixed-top">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="fa fa-bars"></span>
				</button>
			<!--logo start-->
			<div class="brand">

				<a class="logo" href="<?php echo $CFG->wwwroot;?>">
				<?php //echo format_string($SITE->shortname, true, array('context' => context_course::instance(SITEID)));
					?>
				</a>
				
			</div>
			<!--logo end-->
			
			<div class="horizontal-menu navbar-collapse collapse ">

				<ul class="nav navbar-nav">
					<?php include('includes/main_navigation.php'); ?>
				</ul>
				
			</div>
			
			<div class="top-nav hr-top-nav">
				<ul class="nav pull-right top-menu mrg50l">
					<li id="SearchBox">
						<input type="text" class="form-control search" placeholder=" Search">
					</li>
					<!-- user login dropdown start-->
					<li>
						<?php include("includes/custom_menu1.php"); ?>
                       
					</li>
                     
					<li>
						<?php echo $OUTPUT->page_heading_menu(); ?>
					</li>
                      
					<!-- user login dropdown end -->
				
					<!-- <li>
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#custom-menu-navbar-collapse">
							<div class="fa fa-bars"></div>
						</button>
					</li> 				
				  search & user info end-->
                </ul>
			</div>
		</div>
		</header>
		<!--header end-->
		
		<section id="main-content">
			<section class="wrapper" id="main_sec1">
					<div id="homepage" >
						<div id="page" class="container-fluid">
							
							<div class="row">
                                <div id="slider1_container" class="jsocont">
									<!-- Loading Screen -->
									
                                    <div u="slides" class="jslide">
										<?php	
											$bck = "SELECT id,name,banner FROM {block_eduopen_master_inst} ORDER BY RAND()";				
											$institution = $DB->get_record_sql($bck);		 
											//$myinst=$DB->get_records('block_eduopen_master_inst');
											//foreach($bck_rm as $institution){											
											$banner = banner_images($institution);
										?>
										<div id="bck" class="jumbotron">
											<img u="image" src="<?php echo $banner ;?>" class="img-responsive" />
											<div class="message row">	
                                                <div class="col-md-12">												
												<div class="col-md-6 mdquery">	
													<h1 class="msg"> <span class="txtbgcolor"> <?php print_string('hmj','theme_eduopen')?></span></h1>
														
													<h1 class="msg"><span class="txtbgcolor"><?php print_string('hmjo','theme_eduopen')?></span>
													</h1>
													<p></p>
													
													<form id="datasearch" action="<?php echo $CFG->wwwroot.'/eduopen/courses.php'  ?>"      method="POST" >
															<label class="input-hide" for="search-input">
																<?php print_string('hmsearch','theme_eduopen')?>
															</label>
															<div class="search-input-container">
																<input name ="search-input" class="search-input" autocomplete="off" data-js="search" placeholder="Search your course..."></input>
																<button class="search-button" name="search_btn"></button>

															</div>
													</form>
													<p class="fronttextp" id="frontpadtext">
														 <span class="frontwhite">
														<?php print_string('hmjoin','theme_eduopen')?>
														<?php $users="SELECT COUNT(id) AS totalusers FROM {user} WHERE confirmed=1 AND deleted=0 AND username!='guest' AND username!='admin'";
															$user= $DB->get_record_sql($users); ?>
															
														<i class="fa fa-users"></i>&nbsp;<?php echo $user->totalusers ?>  
															<?php print_string('hmcourse','theme_eduopen')?> 
														</span>
														</p>
                                                        <p class="fronttextp">
															<span class="frontwhite">
															<?php print_string('hmlearn','theme_eduopen')?>
															<?php $ccourse= "SELECT COUNT(id) AS totalcourses FROM {course}"; 
																$cc= $DB->get_record_sql($ccourse);
																//var_dump($cc);
															?>
															
															<i class="fa fa-book"></i>&nbsp;<?php echo $cc->totalcourses ?>&nbsp; 
															
															 <?php print_string('hmc','theme_eduopen')?></span>
														</p>
																										
														<p class="fronttextp">
															 <?php $inst="SELECT COUNT(id) AS totalinstitution FROM {block_eduopen_master_inst}";
															 $ins= $DB->get_record_sql($inst);
															 ?>
															 <span class="frontwhite">
															<i class="fa fa-university"></i>&nbsp;<?php echo $ins->totalinstitution ?> &nbsp;<?php print_string('hmp','theme_eduopen')?></span>
														</p>
												</div>
												
												<div class="col-md-6 stats bgfrontbox">
                                                        
												</div>																							
												</div>		
														
											</div><!--stats-->
											<div class="message row instfrontdiv">	
												<div class="col-md-12 ">
													<p> 
														<a href="<?php echo $CFG->wwwroot.'/eduopen/institution_details.php?institutionid='.$institution->id; ?>" class="txtbginst"> <?php echo $institution->name;?> </a>
													</p>
												</div>
											
											</div>
												
											
											
											
										</div>	
                                        <?php //} ?>
                                    </div>
                                   
                                </div>
							</div>
							
						</div>
					<div class="container-fluid">
					<div class="row" id="rowmrglr1">
						
							<h2><center><?php print_string('feature','theme_eduopen') ?></center></h2>
							<hr>
							
					</div>

					<div class="row" id="rowmrglr2">
						
						<?php
							$c_details="SELECT c.id,c.fullname,c.startdate,ce.courseimage,ce.contextid, ce.institution,
							ce.featurecourse,ce.certificate2,ce.formalcredit
							FROM {course} c 
							LEFT JOIN {course_extrasettings_general} ce ON c.id=ce.courseid WHERE c.category!=0 and c.visible=1 ";
							
							$rs=$DB->get_records_sql($c_details);
							$count = 0;
							
							if($rs){
								
								foreach($rs as $details){
									$course_id = $details->id;
									$course_name = $details->fullname;
									$startdate_timestamp = $details->startdate;
									$feature = $details->featurecourse;
									$exam = $details->formalcredit;
									
									
									$institute = $details->institution;
									$c_settings_instid = $DB->get_record('block_eduopen_master_inst',array('name'=>$institute));
									if ($c_settings_instid) {
									$instid = $c_settings_instid->id; 
									}
									$cstartdate = date("F d, Y",$startdate_timestamp);
									$startdate = gmdate("M dS", $startdate_timestamp);
									$c_imgid = $details->courseimage;
									$context_id = $details->contextid;
									$default_img = $CFG->wwwroot.'/theme/eduopen/pix/default_course.png';
									
									$img_url = get_uploaded_image($c_imgid,$context_id);
									
									$img_url = $img_url ? $img_url : $default_img; ?>
									 
									 <?php 
										$count++;
										if(($count % 4) == 0){
										echo "<div class='row mrg2pcb' id =''>"; } ?>
										<div class="col-md-3 col-sm-6 col-xs-12 textcenter pad10b">
											<div class="box-shadow">
												<div class="course-img front_cimg">
													<a class="eduopen-link" href="<?php echo $CFG->wwwroot.'/eduopen/course_details.php?courseid='.$course_id ?>">
													 <img src="<?php echo $img_url ?>" width="100%" height="160px" alt='<?php echo $course_name ?>'></img>
													</a>
												</div>
												<?php if($details->certificate2=='1'){ ?>
												<div class="banner"><p class="verify-ban"><?php echo "VERIFIED";?></p></div>
												<?php }else{ ?>
												<hr class="hrule">
												<?php } ?>
												<div class="course-details">
													<a  class="eduopen-link" href="<?php echo $CFG->wwwroot.'/eduopen/course_details.php?courseid='.$course_id ?>">
														<h5 class="fontsz14">
															<?php if($exam == 1){ ?>
															<img src="<?php echo $CFG->wwwroot.'/theme/eduopen/pix/exam.png'; ?>">
															<?php } ?>
															<?php echo $course_name ?>
														</h5>
													 </a>
												</div>
												
												<p id="f_inst">
													<a  class="eduopen-link" 
													 href="<?php echo $CFG->wwwroot.'/eduopen/institution_details.php?institutionid='.$instid ?>">
														 <span class="fontsz text-wrap"><?php echo $institute ?></span>
													</a>
												</p>
												
												<?php if($startdate_timestamp>time()){ ?>
												<div class="course-date col-md-12">
													<span><i class="fa fa-calendar"></i></span>
													<b><?php $textdate = get_string('cs_start_u','theme_eduopen');
													echo $textdate.'  '.$cstartdate; ?></b>
												</div>
												<?php }else{?>
												<div class="course-date-btn col-md-12">
													<span><i class="fa fa-calendar"></i></span>
													<b>
													<?php
														$textc = get_string('cs_start_c','theme_eduopen');
														echo $textc; ?>
													</b>
													<button class="btn joinus">
														<a href="<?php echo $CFG->wwwroot.'/course/view.php?id='.$course_id; ?>">
															<?php 
															echo get_string('join_us','theme_eduopen'); ?>
														</a>
													</button>
												</div>
												<?php } ?>
												
												<!--  <?php //if ($feature == 1) { ?>
												<div class="course-details">
												<p><span class="label label-info"> 
															<?php //print_string('feature','theme_eduopen'); ?>
														</span></p>
												</div>
												<?php //} ?> -->
											
											</div>
										</div>	
									 
										<?php 
											if(($count % 4) == 0){
											echo "</div>";
										}
								}
							}
							?>	
						
					</div><!--end of rowmrglr2-->
					<div class="row" id="rowmrglr3">
						<a class="btn btn-info" href="<?php echo $CFG->wwwroot.'/eduopen/courses.php' ?> "><?php print_string('hmview','theme_eduopen') ?></a>
					</div>
					</div>
				</div>
				<?php echo $OUTPUT->main_content(); ?>
			</section>
		</section>
	</section>
	
	<?php include("includes/footersetting.php"); ?>
	<?php echo $OUTPUT->standard_end_of_body_html() ?>
	<?php include("includes/foot.php"); ?>
    
	<script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/layout.js'; ?>" > </script>
    <script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/homepage.js'; ?>" > </script>
    <script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/jssorslider.js'; ?>" > </script>
</body>
</html>
