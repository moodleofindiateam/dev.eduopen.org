<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A one column layout for the eduopen theme.
 *
 * @package   theme_eduopen
 * @copyright 2015 Moodle Of India - wwww.moodleofidia.com
 * @author    Pratim Sarangi 
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
echo $OUTPUT->doctype();
global $PAGE;
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include('includes/head.php'); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(array('login-body')); ?>>
        <?php
        echo $OUTPUT->standard_top_of_body_html();
		?>
		<div class="container-fluid fixedtophead">
		<?php
        require_once("includes/topheader.php");
        ?>
         <header class="header container-fluid he">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php
                            //echo format_string($SITE->shortname, true,
                            //array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->
                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php require('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header><!--header end-->
			</div>
        <?php echo $OUTPUT->standard_top_of_body_html() ?>
        <div id="page" class="container-fluid">
            <div id="page-content" class="row-fluid">
                <section id="region-main" class="col-md-12">
                    <?php
                    echo $OUTPUT->main_content();
                    ?>
                    <!--<div class="row center">
                            <div class="registration col-md-8 col-sm-12 col-xs-12 ">
                    <?php //print_string('noaccount','theme_eduopen'); ?>
                                    <a class="eduopen-link" href="<?php //echo $CFG->wwwroot.'/login/signup.php'; ?>">
                    <?php //print_string('createaccount','theme_eduopen'); ?>
                                    </a>
                            </div>
                    </div>-->	
                </section>
            </div> 
            <?php include("includes/footersetting.php"); ?>
            <?php include("includes/foot.php"); ?>
            <?php //echo $OUTPUT->standard_end_of_body_html() ?>
        </div>
    </body>
	<script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/jquery.ddslick.min.js';?>"></script>
	<script type="text/javascript" src="<?php echo $CFG->wwwroot.'/theme/eduopen/javascript/language.js';?>"></script>
</html>
<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>
