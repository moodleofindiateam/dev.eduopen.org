<?php
/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */
// Get the HTML for the settings bits.

require_once(dirname(__FILE__) . '/../../../config.php');
require_once(dirname(__FILE__) . '../../../../my/lib.php');
require($CFG->dirroot . '/lib/grade/grade_item.php');
require($CFG->dirroot . '/lib/grade/grade_grade.php');


$html = theme_eduopen_get_html_for_settings($OUTPUT, $PAGE);
require_once($CFG->dirroot . '/theme/eduopen/layout/includes/allscript.php');
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/jquery.ddslick.min.js'));
$PAGE->requires->js(new moodle_url($CFG->wwwroot . '/theme/eduopen/javascript/language.js'));
if (right_to_left()) {
    $regionbsid = 'region-bs-main-and-post';
} else {
    $regionbsid = 'region-bs-main-and-pre';
}

global $DB, $CFG, $USER;
$PAGE->requires->jquery();
echo $OUTPUT->doctype();
?>
<html <?php echo $OUTPUT->htmlattributes(); ?>>
    <head>
        <title><?php echo $OUTPUT->page_title(); ?></title>
        <link rel="shortcut icon" href="<?php echo $OUTPUT->favicon(); ?>" />
        <?php echo $OUTPUT->standard_head_html() ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php include("includes/head.php"); ?>
    </head>

    <body <?php echo $OUTPUT->body_attributes(); ?>>
        <?php echo $OUTPUT->standard_top_of_body_html(); ?>
        <div class="container-fluid fixedtophead">
            <?php require_once("includes/topheader.php"); ?>
            <header class="header container-fluid ">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle hr-toggle" data-toggle="collapse">
                        <span class="fa fa-bars"></span>
                    </button>
                    <!--logo start-->
                    <div class="brand">
                        <a class="logo" href="<?php echo $CFG->wwwroot; ?>">
                            <?php
                            //echo format_string($SITE->shortname, true,
                            //array('context' => context_course::instance(SITEID)));
                            ?>
                        </a>
                    </div>
                    <!--logo end-->
                    <div class="horizontal-menu navbar-collapse collapse "  id="MainMenuEle">
                        <ul class="nav navbar-nav" id="CustomMenu">
                            <?php require('includes/main_navigation.php'); ?>
                        </ul>
                    </div>
                </div>
            </header><!--header end-->
        </div>
        <?php echo $OUTPUT->standard_top_of_body_html() ?>
        <section id="container" class="my_page">
            <section class="wrapper">
                <div id="page" class="cmediaqry container-fluid frontcont dashpage">
                    <div id="page-content" class="row-fluid">
                        <div class="row-fluid ">
                            <?php
                            global $USER;
                            $enrolled_course_count = 0;
                            $learning_course_count = 0;
                            $teaching_course_count = 0;
                            $studentcourse = 0;

                            $learning_course_id = array();
                            $learning_course_name = array();
                            $learning_course_summary = array();
                            $learing_course_teacher = array();
                            $learning_course_progress = array();

                            $completed_course_id = array();
                            $completed_course_name = array();
                            $completed_course_summary = array();

                            $teaching_course_id = array();
                            $teaching_course_name = array();
                            $teaching_course_summary = array();
                            $visibleRole = visible_roles();
                            if (is_siteadmin() || $visibleRole) {
                                $course_q = "SELECT * FROM {course} WHERE category != 0"; //exclude the top level category.
                            } else {
                                $course_q = "SELECT * FROM {course} WHERE category != 0 AND visible=1"; //exclude the top level category.
                            }
                            $course_rs = $DB->get_records_sql($course_q);
                            if ($course_rs) {
                                foreach ($course_rs as $course) {
                                    $context = context_course::instance($course->id);
                                    if (is_enrolled($context, $USER)) {//Checking if the user is enrolled or not
                                        $enrolled_course_count++;
                                        //$enroled_as = is_enroled_as($course->id,$USER->id);

                                        $is_student = user_has_role_assignment($USER->id, 5, $context->id); //finds out the user is enrolled as student or teacher.

                                        if ($is_student) {//if student
                                            $enrolled_course_count++;
                                            $enrolled_course_id[] = $course->id;
                                            $enrolled_course_name[] = $course->fullname;
                                            $enrolled_course_summary[] = $course->summary;
                                            $coursecompleted = $DB->get_record('course_completions', array('userid' => $USER->id, 'course' => $course->id), 'timecompleted');
                                            $studentcourse++;
                                            if (empty($coursecompleted->timecompleted)) {
                                                $learning_course_count++;
                                                $learning_course_id[] = $course->id;
                                                $learning_course_name[] = $course->fullname;
                                                $learning_course_summary[] = $course->summary;
                                            }
                                        } else {//teacher       
                                            $teaching_course_count++;
                                            $teaching_course_id[] = $course->id;
                                            $teaching_course_name[] = $course->fullname;
                                            $teaching_course_summary[] = $course->summary;
                                        }
                                    }
                                }
                                if ($enrolled_course_count == 0) {//This user has not enrolled in any course. 
                                    ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-info" role="alert">
                                            <center><?php print_string('notenrolled', 'theme_eduopen'); ?> </center>          
                                        </div>                                              
                                    </div>
                                    <?php
                                }
                            }
                            if ($enrolled_course_count != 0) {
                                ?>
                            </div>
                            <div class="dashtabs">
                                <!-- End editing shiuli -->
                                <div id="tabs" class="noborder">
                                    <ul class="nobackground">
                                        <!-- Added by Shiuli for Pathway Tab-->
                                        <?php
                                        if ($visibleRole) {
                                            ?>
                                            <li class="noborder teachingdash">
                                                <a href="#Teaching">
                                                    <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                        <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                            <i class="fa fa-book"></i>
                                                        </div>
                                                        <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                            <?php
                                                            print_string('teaching', 'theme_eduopen');
                                                            $teachcount = count($teaching_course_id);
                                                            echo '<span class="pull-right counting">' . $teachcount . '</span>';
                                                            ?>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <li class="noborder pathwaydash">
                                            <a href="#Pathways">
                                                <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                        <i class="fa fa-dashboard"></i>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                        <?php print_string('Pathway', 'theme_eduopen'); ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li class="noborder inprogressdash">
                                            <a href="#Inprogress">
                                                <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                        <i class="fa fa-clock-o"></i>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                        <?php
                                                        $pathwayids1 = $DB->get_records('custom_pathway_enrolment', array('userid' => $USER->id));
                                                        if ($pathwayids1) {
                                                            $ij = 0;
                                                            $AllPathwayCoures = array();
                                                            foreach ($pathwayids1 as $pathwayid) {
                                                                $pathways = $DB->get_records('course_extrasettings_general', array('coursestatus' => 1), '', 'id,specializations, courseid');
                                                                $pathcourses = array();
                                                                $Arrpathcourses = array();
                                                                foreach ($pathways as $pathway) {
                                                                    $ids = explode(',', $pathway->specializations);
                                                                    if (in_array($pathwayid->pathwayid, $ids)) {
                                                                        // Show only pathway not completed courses.
                                                                        $hasCompleted = has_completed_course($pathway->courseid);
                                                                        if ($hasCompleted != 1) {
                                                                            if (!isset($pathcourses[$pathway->courseid])) {
                                                                                $pathcourses[$pathway->courseid] = $pathway->courseid;
                                                                                $Arrpathcourses[] = $pathway->courseid;
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                $ij++;
                                                                $AllPathwayCoures[$ij] = $Arrpathcourses;
                                                            }
                                                        }

                                                        echo get_string('inpro', 'theme_eduopen');
                                                        if (!empty($AllPathwayCoures)) {
                                                            $CountArr = count($AllPathwayCoures);
                                                            $UniqueCArr = array_unique($AllPathwayCoures[$CountArr]);
                                                        }
                                                        if (!empty($UniqueCArr)) {
                                                            foreach ($learning_course_id as $singleCourse) {
                                                                if (!in_array($singleCourse, $UniqueCArr)) {
                                                                    $SingleCrsVal[] = $singleCourse;
                                                                }
                                                            }
                                                            $CourseArrDiff = $SingleCrsVal;
                                                        } else {
                                                            $CourseArrDiff = $learning_course_id;
                                                        }
                                                        $inprogresscount = count($CourseArrDiff);
                                                        echo '<span class="pull-right counting">' . $inprogresscount . '</span>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="noborder pastdash">
                                            <a href="#Past">
                                                <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                        <i class="fa fa-check-circle"></i>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                        <?php
                                                        $completed1 = array();
                                                        $cpathwayids1 = $DB->get_records('custom_pathway_enrolment', array('userid' => $USER->id));
                                                        if ($cpathwayids1) {
                                                            $jk = 0;
                                                            foreach ($cpathwayids1 as $cpathwayid1) {
                                                                $cpathways1 = $DB->get_records('course_extrasettings_general', null, '', 'id,specializations, courseid');
                                                                $cpathcourses1 = array();
                                                                foreach ($cpathways1 as $cpathway1) {
                                                                    $ids1 = explode(',', $cpathway1->specializations);
                                                                    if (in_array($cpathwayid1->pathwayid, $ids1)) {
                                                                        if (!isset($cpathcourses1[$cpathway1->courseid])) {
                                                                            $cpathcourses1[] = $cpathway1->courseid;
                                                                        }
                                                                    }
                                                                }
                                                                $jk++;

                                                                for ($k = 0; $k < sizeof($cpathcourses1); $k++) {
                                                                    $has_completed = has_completed_course($cpathcourses1[$k]);
                                                                    if ($has_completed) {
                                                                        $completed1[] = $cpathcourses1[$k];
                                                                        $completed_course_name[] = array();
                                                                        $completed_course_summary[] = array();
                                                                    }
                                                                }
                                                                $AllPathwayCmpCoures1[$jk] = $completed1;
                                                            }
                                                        }

                                                        echo get_string('past', 'theme_eduopen');
                                                        if (isset($enrolled_course_id) && !empty($enrolled_course_id)) {
                                                            for ($i = 0; $i < sizeof($enrolled_course_id); $i++) {
                                                                $has_completed = has_completed_course($enrolled_course_id[$i]);
                                                                if ($has_completed) {
                                                                    $completed_course_id[] = $enrolled_course_id[$i];
                                                                    $completed_course_name[] = $enrolled_course_name[$i];
                                                                    $completed_course_summary[] = $enrolled_course_summary[$i];
                                                                }
                                                            }
                                                        }
                                                        if (!empty($AllPathwayCmpCoures1)) {
                                                            $CountComArr = count($AllPathwayCmpCoures1);
                                                            $UniqueComCArr = array_unique($AllPathwayCmpCoures1[$CountComArr]);
                                                        }
                                                        if (!empty($UniqueComCArr)) {
                                                            if ((count($completed_course_id) >= (count($UniqueComCArr)))) {
                                                                $CourseArrComDiff = array_diff($completed_course_id, $UniqueComCArr);
                                                            } else {
                                                                $CourseArrComDiff = array_diff($UniqueComCArr, $completed_course_id);
                                                            }
                                                        } else {
                                                            $CourseArrComDiff = $completed_course_id;
                                                        }
                                                        $completecount = count($CourseArrComDiff);
                                                        echo '<span class="pull-right counting">' . $completecount .
                                                        '</span>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li class="noborder mycertificatedash">
                                            <a href="#Mycertificates">
                                                <!--<i class="fa fa-certificate"></i>&nbsp;-->
                                                <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                        <span class="pull-left imgpd">
                                                            <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/certificatedash.png' ?>">
                                                        </span>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                        <?php
                                                        print_string('mycertificates', 'theme_eduopen');
                                                        $certificatecnt = 0;
                                                        if ($course_rs) {
                                                            $cnt = 0;
                                                            $sql = "SELECT DISTINCT ci.id AS certificateid, ci.userid, ci.code AS code,
                                                                ci.timecreated AS citimecreated,
                                                                crt.name AS certificatename,
                                                                cm.id AS coursemoduleid, cm.course, cm.module,
                                                                c.id AS id, c.fullname AS fullname, c.*,
                                                                                ctx.id AS contextid, ctx.instanceid AS instanceid,
                                                                                f.itemid AS itemid, f.filename AS filename
                                                            FROM {certificate_issues} ci
                                                            INNER JOIN {certificate} crt
                                                                    ON crt.id = ci.certificateid
                                                            INNER JOIN {course_modules} cm
                                                                    ON cm.course = crt.course
                                                            INNER JOIN {course} c
                                                                    ON c.id = cm.course
                                                            INNER JOIN {context} ctx
                                                                    ON ctx.instanceid = cm.id
                                                            INNER JOIN {files} f
                                                                    ON f.contextid = ctx.id
                                                            WHERE ctx.contextlevel = 70 AND
                                                                       f.mimetype = 'application/pdf' AND
                                                                       ci.userid = f.userid AND
                                                                       ci.userid = $userid
                                                            GROUP BY ci.id , ci.userid, ci.code,
                                                                ci.timecreated,
                                                                crt.name,
                                                                cm.id, cm.course, cm.module,
                                                                c.id, c.fullname, c.*,
                                                                                ctx.id, ctx.instanceid,
                                                                                f.itemid, f.filename
                                                            ORDER BY ci.timecreated ASC";
                                                            $certificates = $DB->get_records_sql($sql, array('userid' => $USER->id));
                                                            $certificatecnt += count($certificates);
                                                            echo '<span class="pull-right counting">' . $certificatecnt .
                                                            '</span>';
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="noborder mybadgedash">
                                            <a href="#Mybadges">
                                                <!--<i class="fa fa-shield " aria-hidden="true"></i>&nbsp;-->
                                                <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                        <span class="pull-left imgpd">
                                                            <img class="img-responsive" src="<?php echo $CFG->wwwroot . '/theme/eduopen/pix/badgedash.png' ?>">
                                                        </span>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                        <?php
                                                        print_string('mybadges', 'theme_eduopen');
                                                        $userbadges1 = badges_get_user_badges($USER->id);
                                                        $totalbadgecount = count($userbadges1);
                                                        $bestrclientobj = new bestrclient();
                                                        $bstrbadges = $bestrclientobj->get_badges_bymailid();
                                                        $bestrbadgecount = count($bstrbadges);
                                                        echo '<span class="pull-right counting">' . $bestrbadgecount .
                                                        '</span>';
                                                        ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li class="noborder myfiledash">
                                            <a href="#Myfiles">
                                                <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                        <i class="fa fa-file"></i>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                        <?php print_string('myprivatefiles', 'theme_eduopen'); ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>

                                        <li class="noborder myprofiledash">
                                            <a href="#Myprofile">
                                                <div class="col-md-12 col-sm-12 col-xs-12 pad0A">
                                                    <div class="col-md-2 col-sm-2 col-xs-2 pad0A">
                                                        <i class="fa fa-user "></i>
                                                    </div>
                                                    <div class="col-md-10 col-sm-10 col-xs-10 pad0A">
                                                        <?php print_string('myprofiles', 'theme_eduopen'); ?>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>

                                    <!--Added by Shiuli for Pathway--> 
                                    <div id="Pathways">
                                        <h3 class="text-center dashboardh3"><?php print_string('mypathwaycourses', 'theme_eduopen') ?></h3>
                                        <!--Inprogress Courses under Pathway--> 
                                        <h4 class="text-center dashboardh3"><?php print_string('mypathwaycourses_inprogress', 'theme_eduopen') ?></h4>
                                        <hr class="BlackHr">
                                        <?php
                                        // From Custom Table.
                                        $pathwayids = $DB->get_records('custom_pathway_enrolment', array('userid' => $USER->id));
                                        if ($pathwayids) {
                                            foreach ($pathwayids as $pathwayid) {
                                                $pathways = $DB->get_records('course_extrasettings_general', array('coursestatus' => 1), '', 'id,specializations, courseid');
                                                $pathcourses = array();
                                                $Arrpathcourses = array();
                                                foreach ($pathways as $pathway) {
                                                    $ids = explode(',', $pathway->specializations);
                                                    if (in_array($pathwayid->pathwayid, $ids)) {
                                                        // Show only pathway not completed courses.
                                                        $hasCompleted = has_completed_course($pathway->courseid);
                                                        if ($hasCompleted != 1) {
                                                            if (!isset($pathcourses[$pathway->courseid])) {
                                                                $pathcourses[$pathway->courseid] = $pathway->courseid;
                                                                $Arrpathcourses[] = $pathway->courseid;
                                                            }
                                                        }
                                                    }
                                                }
                                                $pathname = $DB->get_record('block_eduopen_master_special', array('id' => $pathwayid->pathwayid));
                                                ?>
                                                <h5 class="padL dashboardh3"><?php echo $pathname->name . ':'; ?></h5>
                                                <?php
                                                $pathwayCourseBlock = generate_course_block_dashboard($pathcourses, $learning_course_name, $learning_course_summary, true, '');
                                                if ($pathwayCourseBlock) {
                                                    echo '<div class="row-fluid">' . $pathwayCourseBlock . '</div>';
                                                } else {
                                                    echo '<div class="divcenter"><div class="alert alert-info col-md-12 center">' . get_string('nocourse_underpath', 'theme_eduopen') . '</div></div>';
                                                }
                                            }
                                        } else {
                                            echo '<div class="divcenter"><div class="alert alert-info col-md-12 center">' . get_string('mypathway_noinprogress', 'theme_eduopen') . '</div></div>';
                                        }
                                        ?>
                                        <!--Completed Courses under Pathway--> 
                                        <h3 class="text-center dashboardh3"><?php print_string('mypathwaycourses_completed', 'theme_eduopen') ?></h3>
                                        <hr class="BlackHr">
                                        <?php
                                        $completed = array();
                                        $cpathwayids = $DB->get_records('custom_pathway_enrolment', array('userid' => $USER->id));
                                        if ($cpathwayids) {
                                            $ji = 0;
                                            foreach ($cpathwayids as $cpathwayid) {
                                                $cpathways = $DB->get_records('course_extrasettings_general', null, '', 'id,specializations, courseid');
                                                $cpathcourses = array();
                                                foreach ($cpathways as $cpathway) {
                                                    $ids = explode(',', $cpathway->specializations);
                                                    if (in_array($cpathwayid->pathwayid, $ids)) {
                                                        if (!isset($cpathcourses[$cpathway->courseid])) {
                                                            $cpathcourses[] = $cpathway->courseid;
                                                        }
                                                    }
                                                }
                                                $ji++;

                                                for ($k = 0; $k < sizeof($cpathcourses); $k++) {
                                                    $has_completed = has_completed_course($cpathcourses[$k]);
                                                    if ($has_completed) {
                                                        $completed[] = $cpathcourses[$k];
                                                        $completed_course_name[] = array();
                                                        $completed_course_summary[] = array();
                                                    }
                                                }
                                                $cpathname = $DB->get_record('block_eduopen_master_special', array('id' => $cpathwayid->pathwayid));
                                                ?>
                                                <h5 class="padL dashboardh3"><?php echo $cpathname->name . ':'; ?></h5>

                                                <?php
                                                $course_block = array();
                                                if ($completed) {
                                                    $course_block = generate_course_block_dashboard($completed, $completed_course_name, $completed_course_summary, true, '');
                                                }
                                                if ($course_block) {
                                                    echo '<div class="row-fluid">' . $course_block . '</div>';
                                                } else {
                                                    echo '<div class="divcenter"><div class="alert alert-info col-md-12 center">' . get_string('mypathwaycourses_nocompleted', 'theme_eduopen') . '</div></div>';
                                                }
                                                $AllPathwayCmpCoures[$ji] = $completed;
                                            }
                                        } else {
                                            echo '<div class="divcenter"><div class="alert alert-info col-md-12 center">' . get_string('mypathway_nocompleted', 'theme_eduopen') . '</div></div>';
                                        }
                                        ?>
                                    </div>
                                    <!-- end Shiuli --> 
                                    <div id="Inprogress">
                                        <h3 class="text-center dashboardh3"><?php print_string('myinprogresscourses', 'theme_eduopen') ?></h3>
                                        <?php
                                        if (!empty($AllPathwayCoures)) {
                                            $CountArr = count($AllPathwayCoures);
                                            $UniqueCArr = array_unique($AllPathwayCoures[$CountArr]);
                                        }
                                        if (!empty($UniqueCArr)) {
                                            foreach ($learning_course_id as $singleCourse) {
                                                if (!in_array($singleCourse, $UniqueCArr)) {
                                                    $SingleCrsVal[] = $singleCourse;
                                                }
                                            }
                                            $CourseArrDiff = $SingleCrsVal;
                                        } else {
                                            $CourseArrDiff = $learning_course_id;
                                        }
                                        // Show those courses which are not present in pathway tab. OR enrolled Courses.
                                        $course_block = generate_course_block_dashboard($CourseArrDiff, $learning_course_name, $learning_course_summary, true, '');
                                        if ($course_block) {
                                            echo $course_block;
                                        } else {
                                            echo '<div class="divcenter"><div class="alert alert-info col-md-12 center">' . get_string('nocourse_inprogress', 'theme_eduopen') . '</div></div>';
                                        }
                                        ?>
                                    </div>
                                    <div id="Past">
                                        <h3 class="text-center dashboardh3">
                                            <?php print_string('mycompletedcourses', 'theme_eduopen') ?>
                                        </h3>
                                        <?php
                                        if (isset($enrolled_course_id) && !empty($enrolled_course_id)) {
                                            for ($i = 0; $i < sizeof($enrolled_course_id); $i++) {
                                                $has_completed = has_completed_course($enrolled_course_id[$i]);
                                                if ($has_completed) {
                                                    $completed_course_id[] = $enrolled_course_id[$i];
                                                    $completed_course_name[] = $enrolled_course_name[$i];
                                                    $completed_course_summary[] = $enrolled_course_summary[$i];
                                                }
                                            }
                                        }
                                        if (!empty($AllPathwayCmpCoures)) {
                                            $CountComArr = count($AllPathwayCmpCoures);
                                            $UniqueComCArr = array_unique($AllPathwayCmpCoures[$CountComArr]);
                                        }
                                        if (!empty($UniqueComCArr)) {
                                            if ((count($completed_course_id) >= (count($UniqueComCArr)))) {
                                                $CourseArrComDiff = array_diff($completed_course_id, $UniqueComCArr);
                                            } else {
                                                $CourseArrComDiff = array_diff($UniqueComCArr, $completed_course_id);
                                            }
                                        } else {
                                            $CourseArrComDiff = $completed_course_id;
                                        }

                                        // Print only if those courses which are not
                                        // present in Pathway Comppleted tab.
                                        $course_block = generate_course_block_dashboard($CourseArrComDiff, $completed_course_name, $completed_course_summary, true, '');
                                        if ($course_block) {
                                            echo $course_block;
                                        } else {
                                            echo '<div class="divcenter"><div class="alert alert-info col-md-12 center">' . get_string('nocourse_past', 'theme_eduopen') . '</div></div>';
                                        }
                                        ?>

                                    </div>
                                    <?php
                                    if ($visibleRole) {
                                        ?>
                                        <div id="Teaching">
                                            <h3 class="text-center dashboardh3"><?php print_string('myteachingcourses', 'theme_eduopen') ?></h3>
                                            <?php
                                            if ($teaching_course_count > 0) {
                                                ?>
                                                <?php
                                                $course_block = generate_course_block_dashboard($teaching_course_id, $teaching_course_name, $teaching_course_summary, false, '');
                                                if ($course_block) {
                                                    echo $course_block;
                                                }
                                                ?>
                                            <?php } ?>  
                                        </div>
                                    <?php } ?>
                                    <div id="Mycertificates">
                                        <h3 class="text-center dashboardh3"><?php print_string('myallcertificates', 'theme_eduopen') ?></h3>
                                        <?php
                                        $myallcertificates = get_my_certificates();
                                        echo $myallcertificates;
                                        ?>
                                    </div>
                                    <div id="Mybadges" class="">
                                        <!--<h3 class="text-center dashboardh3"><?php //print_string('myalldiaries', 'theme_eduopen')                                                                         ?></h3>-->
                                        <h3 class="text-center dashboardh3"><?php print_string('myallbadges', 'theme_eduopen') ?></h3>
                                        <?php
                                        //include($CFG->wwwroot . '/badges/mybadges.php');
                                        badges_setup_backpack_js();
                                        $page = optional_param('page', 0, PARAM_INT);
                                        $search = optional_param('search', '', PARAM_CLEAN);

                                        $output = $PAGE->get_renderer('core', 'badges');
                                        $badges = badges_get_user_badges($USER->id);

                                        //echo $OUTPUT->header();
                                        $totalcount = count($badges);
                                        $records = badges_get_user_badges($USER->id, null, $page, BADGE_PERPAGE, $search);

                                        $userbadges = new badge_user_collection($records, $USER->id);
                                        $userbadges->sort = 'dateissued';
                                        $userbadges->dir = 'DESC';
                                        $userbadges->page = $page;
                                        $userbadges->perpage = BADGE_PERPAGE;
                                        $userbadges->totalcount = $totalcount;
                                        $userbadges->search = $search;
                                        echo $output->render($userbadges);
//                                        $allcourses = enrol_get_all_users_courses($USER->id);
//                                        $counter = 1;
//                                        foreach ($allcourses as $allcourse) {
//                                            if ($allcourse->visible == 1) {
//                                                $myalldiaries = get_my_diaries($allcourse->id, $allcourse->fullname, $counter);
//                                                echo $myalldiaries;
//                                            }
//                                            $counter ++;
//                                        }
                                        ?>
                                    </div>
                                    <div id="Myfiles">
                                        <h3 class="text-center dashboardh3"><?php print_string('myprivatefiles', 'theme_eduopen') ?></h3>
                                        <?php
                                        require_once($CFG->libdir . '/blocklib.php');
                                        $renderer = $PAGE->get_renderer('block_private_files');
                                        echo $renderer->private_files_tree();
                                        ?>
                                    </div>
                                    <!--                                    <div id="Mymessages">
                                                                            <h3 class="text-center dashboardh3">
                                    <?php //print_string('mymessages', 'theme_eduopen')  ?>
                                                                            </h3>
                                    <?php
//                                        echo 'messages';
//                                        $user1 = $USER;
//                                        $countunreadtotal = message_count_unread_messages($user1);
//                                        message_print_contact_selector($countunreadtotal);
                                    ?>
                                                                        </div>-->
                                    <div id="Myprofile">
                                        <h3 class="text-center dashboardh3"><?php print_string('myprofiles', 'theme_eduopen') ?></h3>
                                        <?php
                                        echo '<div class="userprofile">';
                                        // Render custom blocks.
                                        $user = $DB->get_record('user', array('id' => $USER->id));
                                        $currentuser = $user;
                                        $renderer = $PAGE->get_renderer('core_user', 'myprofile');
                                        $tree = core_user\output\myprofile\manager::build_tree($user, $currentuser);
                                        echo $renderer->render($tree);
                                        echo '</div>';
                                        ?>
                                    </div>

                                </div>

                            <?php } ?>
                        </div>  

                        <!--Notification-->
                        <div id="page1" class="row" style="width:800px; display: none;">
                            <div class = "row mrg15pL"> 
                                <div class="col-md-12">  
                                    <h2><?php echo get_string('msg-not', 'theme_eduopen'); ?></h2>
                                </div>
                            </div>


                            <div class="table-responsive mrg15pL">
                                <table class="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="table-head"><h4 class="head4"><?php echo get_string('sino', 'theme_eduopen'); ?></h4></th>
                                            <th class="table-head"><h4 class="head4"><?php echo get_string('coursename', 'theme_eduopen'); ?></h4></th>
                                            <th class="table-head"><h4 class="head4"><?php echo get_string('msg', 'theme_eduopen'); ?></h4></th>
                                            <th class="table-head"><h4 class="head4"><?php echo get_string('status', 'theme_eduopen'); ?></h4></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php
                                            $crs_name = $DB->get_records_sql("SELECT crs.id,crs.fullname,app.intro,app.id FROM {course} crs
                                                JOIN {apply} app ON crs.id = app.course");
                                            $i = 1;
                                            foreach ($crs_name as $course_name) {
                                                $apply = $DB->get_records('apply_submit', array('apply_id' => $course_name->id));
                                                foreach ($apply as $apply_sub) {
                                                    if ($apply_sub->user_id == $USER->id) {
                                                        for ($i = 1; $i <= count($course_name->id); $i++) {
                                                            ?>
                                                            <td>
                                                                <?php
                                                                echo $i;
                                                                $i++;
                                                                ?>
                                                            </td>

                                                            <td><?php echo $course_name->fullname; ?></td>
                                                            <td>
                                                                <?php
                                                                $app_name = $DB->get_records('apply', array('id' => $course_name->id));
                                                                foreach ($app_name as $apply_name) {
                                                                    echo $apply_name->name;
                                                                    ?><br><br>
                                                                <?php } ?>
                                                            </td>

                                                            <td>
                                                                <?php
                                                                if ($apply_sub->acked == 0) {
                                                                    ?>
                                                                    <button class="btn btn-warning btn_wdt"><?php echo get_string('none', 'theme_eduopen'); ?></button>
                                                                <?php } elseif ($apply_sub->acked == 1) {
                                                                    ?>
                                                                    <button class="btn btn-success btn_wdt"><?php echo get_string('accept', 'theme_eduopen'); ?></button>
                                                                <?php } else { ?>
                                                                    <button class="btn btn-danger btn_wdt"><?php echo get_string('reject', 'theme_eduopen'); ?></button>
                                                                <?php } ?>
                                                            </td>

                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>      

                        </div>
                        <!--end of notification-->
                        <!-- payment histroy  start-->
                        <div id="page2" class="row" style="width:800px; display: none;">
                            <?php
                            $crs_paypal = $DB->get_records_sql("SELECT ep.item_name,ep.timeupdated,c.fullname FROM {enrol_edupay} ep
                                JOIN {course} c ON ep.courseid = c.id where ep.userid = '$USER->id'");
                            if ($crs_paypal) {
                                foreach ($crs_paypal as $course_paypal) {
                                    $srdate = $course_paypal->timeupdated;
                                    $timeupd = date("F d, Y", $srdate);
                                    $pay_course = $course_paypal->fullname;
                                    if ($course_paypal->item_name == 'attendance_of_completion') {
                                        $payitem = "Attendance of Completion";
                                        $payicon = '<span class="fa fa-book fontsz19"></span>';
                                    } elseif ($course_paypal->item_name == 'firstattend') {
                                        $payitem = "Attendance Certificate";
                                        $payicon = '<span class="fa fa-book fontsz19"></span>';
                                        //$payicon = '<img src="'.$CFG->wwwroot.'/theme/eduopen/pix/ad.png'.'">';
                                    } else {
                                        $payitem = "Verified Certificate";
                                        $payicon = '<img src="' . $CFG->wwwroot . '/theme/eduopen/pix/v.png' . '">';
                                    }
                                    ?>
                                    <div class="col-md-12">
                                        <h3><?php echo $payitem; ?></h3>
                                        <div class="col-md-12">
                                            <p class="fontsz19"><?php echo $payicon; ?> You have completed <?php echo $pay_course; ?> for <?php echo $payitem; ?> on <?php echo $timeupd; ?></p>
                                        </div>
                                    </div>
                                    <?php
                                }
                            } else {
                                ?>
                                <p class="pad20L"> You donot have any payment history</p>
                            <?php } ?>

                            <!-- payment histroy end-->
                            <div id="<?php echo $regionbsid ?>" class="col-md-9">                                              

                                <div class="row-fluid">
                                    <section id="region-main" class="col-md-8 pull-right">
                                        <?php echo $OUTPUT->main_content(); ?>
                                    </section>                              
                                </div>
                            </div>                      
                        </div>
                    </div>
            </section>
            <?php include("includes/footersetting.php"); ?> 
        </section>
    </body>
</html>

<?php echo $OUTPUT->standard_end_of_body_html() ?>
<?php include("includes/foot.php"); ?>

<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
    $('#tabs')
            .tabs()
            .addClass('ui-tabs-vertical ui-helper-clearfix');
    $(document).ready(function () {
        $('[title]').removeAttr('title');
    });
</script>

<script>
    $(window).load(function () {
        var ht = $("#Teaching").height();
        extraheight = ht + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight);
    });
    $(window).load(function () {
        var ht = $("#Pathways").height();
        extraheight = ht + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight);
    });
    $(".teachingdash").click(function () {
        var ht1 = $("#Teaching").height();
        extraheight1 = ht1 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight1);
    });
    $(".pathwaydash").click(function () {
        var ht2 = $("#Pathways").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
    $(".inprogressdash").click(function () {
        var ht2 = $("#Inprogress").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
    $(".pastdash").click(function () {
        var ht2 = $("#Past").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
    $(".mycertificatedash").click(function () {
        var ht2 = $("#Mycertificates").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
    $(".mybadgedash").click(function () {
        var ht2 = $("#Mybadges").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
    $(".myfiledash").click(function () {
        var ht2 = $("#Myfiles").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
    $(".mymessagedash").click(function () {
        var ht2 = $("#Mymessages").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
    $(".myprofiledash").click(function () {
        var ht2 = $("#Myprofile").height();
        extraheight2 = ht2 + 30;
        $(".dashtabs .ui-tabs-nav").height(extraheight2);
    });
</script>
});

<style>
    @media screen and (max-width: 484px) {
        .fa-3x {
            font-size: 1em !important;
        }
    }
</style>