<?php

/**
 * Moodle's 
 * This file is part of eduopen LMS Product
 *
 * @package   theme_eduopen
 * @copyright http://www.moodleofindia.com
 * @license   This file is copyrighted to Dhruv Infoline Pvt Ltd, http://www.moodleofindia.com
 */

/**
 * Parses CSS before it is cached.
 *
 * This function can make alterations and replace patterns within the CSS.
 *
 * @param string $css The CSS
 * @param theme_config $theme The theme config object.
 * @return string The parsed CSS The parsed CSS.
 */
function theme_eduopen_process_css($css, $theme) {

// Set the background image for the logo.
    $logo = $theme->setting_file_url('logo', 'logo');
    $css = theme_eduopen_set_logo($css, $logo);

// Set custom CSS.
    if (!empty($theme->settings->customcss)) {
        $customcss = $theme->settings->customcss;
    } else {
        $customcss = null;
    }
    $css = theme_eduopen_set_customcss($css, $customcss);
    if (!empty($theme->settings->breadcrumbs)) {
        $breadcrumbs = $theme->settings->breadcrumbs;
    } else {
        $breadcrumbs = null;
    }
    $css = theme_eduopen_set_breadcrumbs($css, $breadcrumbs);

// Set the main color.
    if (!empty($theme->settings->maincolor)) {
        $maincolor = $theme->settings->maincolor;
    } else {
        $maincolor = null;
    }
    $css = theme_eduopen_set_maincolor($css, $maincolor);
// Set the back color
    if (!empty($theme->settings->backcolor)) {
        $backcolor = $theme->settings->backcolor;
    } else {
        $backcolor = null;
    }
    $css = theme_eduopen_set_backcolor($css, $backcolor);
// Set the logo bg color.
    if (!empty($theme->settings->logobackcolor)) {
        $logobackcolor = $theme->settings->logobackcolor;
    } else {
        $logobackcolor = null;
    }
    $css = theme_eduopen_set_logobackcolor($css, $logobackcolor);
// Set the homepage font bg color.
    if (!empty($theme->settings->homefontbackcolor)) {
        $homefontbackcolor = $theme->settings->homefontbackcolor;
    } else {
        $homefontbackcolor = null;
    }
    $css = theme_eduopen_set_homefontbackcolor($css, $homefontbackcolor);
// Set the link color.
    if (!empty($theme->settings->linkcolor)) {
        $linkcolor = $theme->settings->linkcolor;
    } else {
        $linkcolor = null;
    }
    $css = theme_eduopen_set_linkcolor($css, $linkcolor);
// Set the link hover color.
    if (!empty($theme->settings->linkhover)) {
        $linkhover = $theme->settings->linkhover;
    } else {
        $linkhover = null;
    }
    $css = theme_eduopen_set_linkhover($css, $linkhover);
// Set the buttoncolour
    if (!empty($theme->settings->buttoncolour)) {
        $buttoncolour = $theme->settings->buttoncolour;
    } else {
        $buttoncolour = null;
    }
    $css = theme_eduopen_set_buttoncolour($css, $buttoncolour);
// Set buttonhovercolour
    if (!empty($theme->settings->buttonhovercolour)) {
        $buttonhovercolour = $theme->settings->buttonhovercolour;
    } else {
        $buttonhovercolour = null;
    }
    $css = theme_eduopen_set_buttonhovercolour($css, $buttonhovercolour);
// Set footer colour
    if (!empty($theme->settings->footerbgcolour)) {
        $footerbgcolour = $theme->settings->footerbgcolour;
    } else {
        $footerbgcolour = null;
    }
    $css = theme_eduopen_set_footerbgcolour($css, $footerbgcolour);
    return $css;
}

/**
 * Adds the logo to CSS.
 *
 * @param string $css The CSS.
 * @param string $logo The URL of the logo.
 * @return string The parsed CSS
 */
function theme_eduopen_set_logo($css, $logo) {
    $tag = '[[setting:logo]]';
    $replacement = $logo;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

function theme_eduopen_set_breadcrumbs($css, $breadcrumbs) {
    $tag = '[[setting:breadcrumbs]]';
    $replacement = $breadcrumbs;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_maincolor($css, $maincolor) {
    $tag = '[[setting:maincolor]]';
    $replacement = $maincolor;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_backcolor($css, $backcolor) {
    $tag = '[[setting:backcolor]]';
// $replacement = hex2rgba($backcolor, 0.5);
    $replacement = $backcolor;
    if (is_null($replacement)) {
        $replacement = '#F1EEE7';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_logobackcolor($css, $logobackcolor) {
    $tag = '[[setting:logobackcolor]]';
    $replacement = $logobackcolor;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_homefontbackcolor($css, $homefontbackcolor) {
    $tag = '[[setting:homefontbackcolor]]';
    $replacement = hex2rgba($homefontbackcolor, 0.5);
    /* $replacement = $homefontbackcolor;
      if (is_null($replacement)) {
      $replacement = '#001E3C';
      } */
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_linkcolor($css, $linkcolor) {
    $tag = '[[setting:linkcolor]]';
    $replacement = $linkcolor;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_linkhover($css, $linkhover) {
    $tag = '[[setting:linkhover]]';
    $replacement = $linkhover;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_buttoncolour($css, $buttoncolour) {
    $tag = '[[setting:buttoncolour]]';
    $replacement = $buttoncolour;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_buttonhovercolour($css, $buttonhovercolour) {
    $tag = '[[setting:buttonhovercolour]]';
    $replacement = $buttonhovercolour;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function theme_eduopen_set_footerbgcolour($css, $footerbgcolour) {
    $tag = '[[setting:footerbgcolour]]';
    $replacement = $footerbgcolour;
    if (is_null($replacement)) {
        $replacement = '#001E3C';
    }
    $css = str_replace($tag, $replacement, $css);
    return $css;
}

function hex2rgba($color, $opacity = false) {
    $default = 'rgb(0, 0, 0)';

//Return default if no color provided
    if (empty($color)) {
        return $default;
    }

//Sanitize $color if "#" is provided
    if ($color[0] == '#') {
        $color = substr($color, 1);
    }

//Check if color has 6 or 3 characters and get values
    if (strlen($color) == 6) {
        $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
    } else if (strlen($color) == 3) {
        $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
    } else {
        return $default;
    }

//Convert hexadec to rgb
    $rgb = array_map('hexdec', $hex);

//Check if opacity is set(rgba or rgb)
    if ($opacity) {
        if (abs($opacity) > 1)
            $opacity = 1.0;
        $output = 'rgba(' . implode(", ", $rgb) . ', ' . $opacity . ')';
    } else {
        $output = 'rgb(' . implode(",", $rgb) . ')';
    }
//Return rgb(a) color string
    return $output;
}

/**
 * Serves any files associated with the theme settings.
 *
 * @param stdClass $course
 * @param stdClass $cm
 * @param context $context
 * @param string $filearea
 * @param array $args
 * @param bool $forcedownload
 * @param array $options
 * @return bool
 */
function theme_eduopen_pluginfile($course, $cm, $context, $filearea, $args, $forcedownload, array $options = array()) {
    if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'logo') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('logo', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image1') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image1', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image2') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image2', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image3') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image3', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image4') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image4', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image5') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image5', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image6') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image6', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image7') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image7', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image8') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image8', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image9') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image9', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image10') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image10', $args, $forcedownload, $options);
    } if ($context->contextlevel == CONTEXT_SYSTEM and $filearea === 'image11') {
        $theme = theme_config::load('eduopen');
// By default, theme files must be cache-able by both browsers and proxies.
        if (!array_key_exists('cacheability', $options)) {
            $options['cacheability'] = 'public';
        }
        return $theme->setting_file_serve('image11', $args, $forcedownload, $options);
    } else {
        send_file_not_found();
    }
}

/**
 * Adds any custom CSS to the CSS before it is cached.
 *
 * @param string $css The original CSS.
 * @param string $customcss The custom CSS to add.
 * @return string The CSS which now contains our custom CSS.
 */
function theme_eduopen_set_customcss($css, $customcss) {
    $tag = '[[setting:customcss]]';
    $replacement = $customcss;
    if (is_null($replacement)) {
        $replacement = '';
    }

    $css = str_replace($tag, $replacement, $css);

    return $css;
}

/**
 * Returns an object containing HTML for the areas affected by settings.
 *
 * Do not add eduopen specific logic in here, child themes should be able to
 * rely on that function just by declaring settings with similar names.
 *
 * @param renderer_base $output Pass in $OUTPUT.
 * @param moodle_page $page Pass in $PAGE.
 * @return stdClass An object with the following properties:
 *      - navbarclass A CSS class to use on the navbar. By default ''.
 *      - heading HTML to use for the heading. A logo if one is selected or the default heading.
 *      - footnote HTML to use as a footnote. By default ''.
 */
function theme_eduopen_get_html_for_settings(renderer_base $output, moodle_page $page) {
    global $CFG;
    $return = new stdClass;

    $return->navbarclass = '';
    if (!empty($page->theme->settings->invert)) {
        $return->navbarclass .= ' navbar-inverse';
    }

    if (!empty($page->theme->settings->logo)) {
        $return->heading = html_writer::link($CFG->wwwroot, '', array('title' => get_string('home'), 'class' => 'logo'));
    } else {
        $return->heading = $output->page_heading();
    }

    $return->footnote = '';
    if (!empty($page->theme->settings->footnote)) {
        $return->footnote = '<div class="footnote text-center">' . format_text($page->theme->settings->footnote) . '</div>';
    }

    return $return;
}

/**
 * All theme functions should start with theme_eduopen_
 * @deprecated since 2.5.1
 */
function eduopen_process_css() {
    throw new coding_exception('Please call theme_' . __FUNCTION__ . ' instead of ' . __FUNCTION__);
}

/**
 * All theme functions should start with theme_eduopen_
 * @deprecated since 2.5.1
 */
function eduopen_set_logo() {
    throw new coding_exception('Please call theme_' . __FUNCTION__ . ' instead of ' . __FUNCTION__);
}

/**
 * All theme functions should start with theme_eduopen_
 * @deprecated since 2.5.1
 */
function eduopen_set_customcss() {
    throw new coding_exception('Please call theme_' . __FUNCTION__ . ' instead of ' . __FUNCTION__);
}

//Following function returns id of the user(s) assigned as a role of teacher into moodle course by Pratim.
function get_teacher($courseid) {
    global $DB;
    $teacherid = array();
    $return = null;
    $contextresultset = $DB->get_record('context', array('instanceid' => $courseid, 'contextlevel' => '50'));
    if ($contextresultset) {
        $contextids = $contextresultset->id;
        $roleassignmentresultset = $DB->get_records('role_assignments', array('contextid' => $contextids, 'roleid' => '3'));
        if ($roleassignmentresultset) {
            foreach ($roleassignmentresultset as $roleassignment) {
                $teacherid[] = $roleassignment->userid;
            }
            $return = $teacherid;
        }
    }
    return $return;
}

//Following function returns all the module ids and names of a course by Pratim.

function mod_info($courseid) {

    global $DB;
    $i = 1;
    $return = null;
    $coursemoduleresultset = $DB->get_records('course_modules', array('course' => $courseid, 'visible' => '1'));
    if ($coursemoduleresultset) {
        foreach ($coursemoduleresultset as $coursemodules) {
            $module = $coursemodules->module;
            $moduletype = $DB->get_field('modules', 'name', array('id' => $module), MUST_EXIST);

            if ($moduletype) {
                $return['mod ' . $i] = new stdClass();
                $return['mod ' . $i]->mod_id = $coursemodules->id;
                $return['mod ' . $i]->mod_name = $moduletype;
            }
            $i++;
        }
    }
    return $return;
}

//Following function checks that if a module has attended or not by Pratim.
function has_attended_module($moduleid) {
    global $DB, $USER;
    $return = false;
    $hasviewed = $DB->record_exists('course_modules_completion', array('coursemoduleid' => $moduleid, 'userid' => $USER->id, 'viewed' => '1'));
    if ($hasviewed) {
        $return = true;
    }
    return $return;
}

//Following function returns overall progress in percentage of a student by Pratim.

function progress($courseid) {
    global $CFG, $DB, $USER;
    $progress = 0;
    $return = null;
    $completed = null;
    $incompleted = null;
    $courseobj = get_course($courseid);

    $completioninfolist[] = new completion_info($courseobj);
    foreach ($completioninfolist as $cminfo) {
        $class = new \ReflectionClass("completion_info");
        $property = $class->getProperty("course");
        $property->setAccessible(true);
        $list = $property->getValue($cminfo);
        $listarray1[$list->id] = ['id' => $list->id, 'fullname' => $list->fullname, 'activity' => array()];

        $completions = $cminfo->get_completions($USER->id);
        foreach ($completions as $completion) {
            $criteria = $completion->get_criteria();
            $row = array();
            if ($criteria->module != '') { // check whether the type is null or not by Shiuli.
                $row['name'] = $criteria->get_title_detailed();
                $row['type'] = $criteria->module;
                $row['completionstate'] = (int) $completion->is_complete();
                $row['date'] = (int) $completion->timecompleted != 0 ? userdate((int) $completion->timecompleted, '%d %B %Y') : '-';

                $listarray1[$list->id]['activity'][] = $row;
                $completionstate = $row['completionstate'];
                //print_object($listarray1[$list->id]['activity']);
                $completionstateArray[] = $row['completionstate'];
                if ($completionstate == 1) {
                    $activitycompleted[] = $completionstate;
                }
            }
        }
    }
    if (!empty($completionstateArray) && !empty($activitycompleted)) {
        $compl = count($activitycompleted);
        $allact = count($completionstateArray);
        $progressrate = round(($compl / $allact) * 100);
    } else {
        $progressrate = 0;
    }
    $return = $progressrate;
    return $return;
}

//Following function gets the user profile picture original author Mihir, updated by Pratim.
function get_user_picture($size, $user) {
    global $PAGE, $OUTPUT;
    $username = ucfirst($user->firstname) . " " . ucfirst($user->lastname);
    $html = '';
    if (isloggedin() && !isguestuser()) {
        $html .= $OUTPUT->user_picture($user, array('size' => $size));
    } else {
        if ($size > 35) {
            $html .= "<img alt='Picture of " . $username . "' title='Picture of " . $username . "'
            class='userpicture' role='presentation' src='" . $OUTPUT->pix_url('g/f1') . "' />";
        } else {
            $html .= "<img alt='Picture of " . $username . "' title='Picture of " . $username . "'
            class='userpicture' role='presentation' src='" . $OUTPUT->pix_url('g/f2') . "' />";
        }
    }
    return $html;
}

//This function takes course details as array and returns formatted course block,
//if the last parameter is provided as null it returns the block for teacher as in teaching courses block by Pratim.
//nihar to update for new ui look
function generate_course_block_dashboard($courseidArray, $coursename, $coursesummary, $student, $inprogres = '') {
    $output = '';
    global $DB, $CFG, $USER;
    $currentLanguage = current_language();
    if (!empty($courseidArray)) {
        $cidimplode = implode(',', $courseidArray);
        $teacher = user_has_role_assignment($USER->id, 3);
        $tutor = $DB->get_record('role', array('shortname' => 'teacher'));
        $tutorrole = user_has_role_assignment($USER->id, $tutor->id);
        $roleid = $DB->get_record('role', array('shortname' => 'contenteditor'));
        $contenteditor = user_has_role_assignment($USER->id, $roleid->id); // for dev.9, for demo 12.
        if ($teacher || $contenteditor || $tutorrole) {
            $coursedetails = "SELECT c.id, c.fullname, c.startdate, c.visible,
            ce.courseimage, ce.courseid, ce.language, ce.coursestatus,
            ce.contextid, ce.institution, ce.specializations, ce.featurecourse,
            ce.engtitle, ce.certificate2,
            ce.formalcredit FROM {course} c LEFT JOIN {course_extrasettings_general} ce
            ON c.id=ce.courseid WHERE c.category!=0  AND
            c.id IN ($cidimplode)";
        } else {
            $coursedetails = "SELECT c.id, c.fullname, c.startdate, c.visible,
            ce.courseimage, ce.courseid, ce.language, ce.coursestatus,
            ce.contextid, ce.institution, ce.specializations, ce.featurecourse,
            ce.engtitle, ce.certificate2,
            ce.formalcredit FROM {course} c LEFT JOIN {course_extrasettings_general} ce
            ON c.id=ce.courseid WHERE c.category!=0 
            AND c.visible=1 AND c.id IN ($cidimplode)";
        }
        $rs = $DB->get_records_sql($coursedetails);
        $count = 0;
        if ($rs) {
            foreach ($rs as $details) {
                $CRSid[$details->id] = $details->id;
                //$coursename[$details->id] = $details->fullname; // Comes from Course edit settings.
                $coursename1[$details->id] = $details->fullname;
                $coursetitle[$details->id] = strip_tags($details->engtitle); // Comes from extra settings.
                $startdatetimestamp = $details->startdate; //course start date.
                $crshidecls[$details->id] = $details->visible;
                $dateafteramonth[$details->id] = time() + (30 * 24 * 60 * 60); // date after 1 month.
                $feature[$details->id] = $details->featurecourse;
                $exam[$details->id] = $details->formalcredit;
                $institute = $details->institution;
                $pathway[$details->id] = $details->specializations;
                $certificate2[$details->id] = $details->certificate2;
                $csettingsinstitutionid = $DB->get_record('block_eduopen_master_inst', array('id' => $institute));
                if ($csettingsinstitutionid) {
                    if ($currentLanguage == 'it') {
                        $instname[$details->id] = $csettingsinstitutionid->itname;
                    } else {
                        $instname[$details->id] = $csettingsinstitutionid->name;
                    }
                }
                if ($details->coursestatus == 0) {
                    $archivecls[$details->id] = 'ArchivedCourse';
                } else {
                    $archivecls[$details->id] = '';
                }
                $cstartdate[$details->id] = date("dS F, Y", $startdatetimestamp);
                $startdate[$details->id] = gmdate("M dS", $startdatetimestamp);
                $courseimageid = $details->courseimage;
                $contextid = $details->contextid;
                $defaultimage = $CFG->wwwroot . '/theme/eduopen/pix/default_course.png';
                $imageurls = csetting_images($details);
                $imageurl[$details->id] = $imageurls ? $imageurls : $defaultimage;
                $count++;
            }
        }
        if (!empty($CRSid)) {
            foreach ($CRSid as $key => $courseids) {
                $i = $key;
                $output .= '<div class="col-md-4 col-sm-4 col-xs-12 textcenter pad0A">';
                $output .= '<div class="box-shadow ' . $archivecls[$i] . ' dashbordbox coursehidden' . $crshidecls[$i] . '">';
                $condA = (($certificate2[$i] == '1') && ($exam[$i] == 1));
                if ($condA || ($certificate2[$i] == '1') || ($exam[$i] == 1)) {
                    $frontcls = 1;
                } else {
                    $frontcls = 0;
                }
                $output .= '<div class="course-img front_cimg' . $frontcls . '">';
                $output .= '<a class="eduopen-link" 
            href="' . $CFG->wwwroot . '/course/view.php?id=' . $CRSid[$i] . '">';
                $output .= '<img src="' . $imageurl[$i] . '" width="100%" alt="' . $coursename1[$i] . '">';
                $output .= '</a>';
                if (($certificate2[$i] == '1') || ($exam[$i] == 1)) {
                    if ($exam[$i] == 1) {
                        $banname = get_string('vernexam', 'theme_eduopen');
                        $output .= '<div class="crbanner">' . $banname . '</div>';
                    } else if ($certificate2[$i] == '1') {
                        $banname = get_string('verified', 'theme_eduopen');
                        $output .= '<div class="banner">' . $banname . '</div>';
                    } else if (($certificate2[$i] == '1') && ($exam[$i] == 1)) {
                        $banname = get_string('vernexam', 'theme_eduopen');
                        $output .= '<div class="crbanner">' . $banname . '</div>';
                    }
                } else {
                    $output .= '<div class="empty_ban"></div>';
                }
                $output .= '</div>';
                $condA = (($certificate2[$i] == '1') && ($exam[$i] == 1));
                if ($condA || ($certificate2[$i] == '1') || ($exam[$i] == 1)) {
                    $cls = 1;
                } else {
                    $cls = 0;
                }

                // rests of the divs after an image div.
                $output .= '<div class="restcontent cont' . $cls . '">';
                $output .= '<div id="f_inst" class="ban' . $certificate2[$i] . '" class="padl5">';
                $output .= '<a class="eduopen-link"
                               href="' . $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $institute . '">';
                if (!empty($instname[$i])) {
                    $institutionname = strip_tags($instname[$i]);
                } else {
                    $institutionname = '';
                }
                $output .= '<p class="instclr">' . $institutionname . '</p>';
                $output .= '</a>';
                $output .= '</div>';
                $output .= '<div class="course-details">';
                $output .= '<a class="eduopen-link" 
                               href="' . $CFG->wwwroot . '/course/view.php?id=' . $CRSid[$i] . '">';
                $output .= '<p>' . strip_tags($coursename1[$i]) . '</p>';
                $output .= '</a>';
                $output .= '</div>';
                $output .= '<div id="f_inst" class="ban htreduce" class="padl5">';

                if ($student) {
                    /* if ($inprogres = 'inprogress') {
                      $progress = progress1(($CRSid[$i]));
                      } else { */
                    // }
                    $progressPercent = eduplayer_progress_dashboard($CRSid[$i]);
                    if ($progressPercent) {
                        $progress = $progressPercent;
                    } else {
                        $progress = 0;
                    }

                    if ($progress != 100) {
                        $output .= '<div class="progress progress-xs">';
                        $output .= '<div class="progress-bar progress-bar-success eduopen-theme"
                    role="progressbar" aria-valuenow="' . $progress . '" aria-valuemin="0" aria-valuemax="100" style="width:' . $progress . '%">';
                        $output .= '<span class="sr-only">' . $progress . '%Complete (success)</span>';
                        $output .= '</div>';
                        $output .= '</div>';
                        $output .= '</div>';
                        $output .= '<div class="col-md-12">';
                        $output .= '<span class="course-percent">' . $progress . '% &nbsp;</span><span>' .
                                get_string('completed', 'theme_eduopen') . '</span>';
                    } else {
                        $output .= '<div class="DashCcomplete">' . get_string('CCompleted', 'theme_eduopen') . '</div>';
                    }
                }
                $output .= '</div>';
                /* $output .= '<div class="col-md-12 pull-left">';
                  $output .= '<a href="' . $CFG->wwwroot . '/course/view.php?id=' . $courseid[$i] . '" class="eduopen-link pull-right">';
                  if ($progress == 100) {
                  $output .= get_string('view', 'theme_eduopen');
                  } else {
                  $output .= ($student ? get_string('continuelearning', 'theme_eduopen') :
                  get_string('continueteaching', 'theme_eduopen'));
                  } */
                //$output .= '</a>';
                //$output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';
                $output .= '</div>';
            }
        }
    }
    return $output;
}

//this function checks that user has completed every module in the course or not by Pratim.
function has_completed_course($courseid) {
    global $DB, $USER, $CFG;
    $return = false;
    $modulecount = 0;
    $completedmodulecount = 0;
    $cmcm = new completion_completion(array('userid' => $USER->id, 'course' => $courseid));
    if ($cmcm->is_complete()) {
        $return = true;
    }
    /* $modules = mod_info($courseid);
      if ($modules) {
      foreach ($modules as $modinformation) {
      $moduleid = $modinformation->mod_id;
      $modulename = $modinformation->mod_name;
      if ($modulename != 'label') {
      $modulecount++;
      }
      $hascompleted = $DB->record_exists('course_modules_completion', array('coursemoduleid' => $moduleid, 'userid' => $USER->id, 'completionstate' => '1'));
      if ($hascompleted) {
      $completedmodulecount++;
      }
      }
      if ($modulecount == $completedmodulecount) {
      $return = true;
      }
      } */
    return $return;
}

//date function returns format
function customdate_format($timestamp) {
    $m = date('M', $timestamp);
    $j = date('j', $timestamp);
    $S = date('S', $timestamp);
    $y = date('Y', $timestamp);
    return $m . '&nbsp;' . $j . '<sup>' . $S . '</sup>' . ',&nbsp;' . $y;
}

//date function returns format
function catalog_customdate_format($timestamp) {
    $m = date('F', $timestamp);
    $j = date('j', $timestamp);
    $S = date('S', $timestamp);
    $y = date('Y', $timestamp);
    return $j . '<sup>' . $S . '</sup>' . '&nbsp;' . $m . ',&nbsp;' . $y;
}

//The functions get_uploaded_image(), make_draftfile_url_for_image(), make_file_url_for_image()
//are used for generating a stored file url using files id and context id
function get_uploaded_image($fileid, $contextids) {// Function to retrieve the uploaded file.
    $filearea = 'draft';
    $filepath = '/';
    $fs = get_file_storage();
    $picsfile = null;
    if (!empty($fileid)) {
        $files = $fs->get_area_files($contextids, 'user', $filearea, $fileid, 'id', false);
        foreach ($files as $file) {
            $picsfile = make_draftfile_url_for_image($contextids, $file->get_itemid(), $file->get_filepath(), $file->get_filename());
            $itemid = $file->get_itemid();
            $filename = $file->get_filename();
        }
    }
    return $picsfile;
}

function make_draftfile_url_for_image($contextid, $draftid, $pathname, $filename, $forcedownload = false) {
//function to make file url

    global $CFG, $USER;
    $urlbase = "$CFG->httpswwwroot/draftfile.php";
    return make_file_url_for_image($urlbase, "/$contextid/user/draft/$draftid" . $pathname . $filename, $forcedownload);
}

function make_file_url_for_image($urlbase, $path, $forcedownload = false) {//function to generate stored file url
    $params = array();
    if ($forcedownload) {
        $params['forcedownload'] = 1;
    }
    $url = new moodle_url($urlbase, $params);
    $url->set_slashargument($path);
    return $url;
}

function get_my_certificates() {
    global $CFG, $USER, $DB;
    require_once($CFG->dirroot . '/mod/certificate/locallib.php');

    $userid = $USER->id;
    $content = new stdClass();
    $content = '';
    $table = new html_table();
    $table->head = array(get_string('report_certificates_tblheader_coursename', 'certificate'),
        //get_string('report_certificates_tblheader_grade', 'block_report_certificates'),
        get_string('report_certificates_tblheader_code', 'certificate'),
        get_string('report_certificates_tblheader_issuedate', 'certificate'),
        get_string('report_certificates_tblheader_download', 'certificate'));
    $table->align = array("left", "center", "center", "center", "center");

    $sql = "SELECT DISTINCT ci.id AS certificateid, ci.userid, ci.code AS code,
            ci.timecreated AS citimecreated,
            crt.name AS certificatename,
            cm.id AS coursemoduleid, cm.course, cm.module,
            c.id AS id, c.fullname AS fullname, c.*,
                            ctx.id AS contextid, ctx.instanceid AS instanceid,
                            f.itemid AS itemid, f.filename AS filename
        FROM {certificate_issues} ci
        INNER JOIN {certificate} crt
                ON crt.id = ci.certificateid
        INNER JOIN {course_modules} cm
                ON cm.course = crt.course
        INNER JOIN {course} c
                ON c.id = cm.course
        INNER JOIN {context} ctx
                ON ctx.instanceid = cm.id
        INNER JOIN {files} f
                ON f.contextid = ctx.id
        WHERE ctx.contextlevel = 70 AND
                   f.mimetype = 'application/pdf' AND
                   ci.userid = f.userid AND
                   ci.userid = $userid
        GROUP BY ci.id , ci.userid, ci.code,
            ci.timecreated,
            crt.name,
            cm.id, cm.course, cm.module,
            c.id, c.fullname, c.*,
                            ctx.id, ctx.instanceid,
                            f.itemid, f.filename
        ORDER BY ci.timecreated ASC";
//print_object($sql);
// CONTEXT_MODULE (ctx.contextlevel = 70).
// PDF FILES ONLY (f.mimetype = 'application/pdf').

    $limit = " LIMIT 5"; // Limiting the output results to just five records.
    $certificates = $DB->get_records_sql($sql, array('userid' => $USER->id));
    //print_object($certificates);

    if (empty($certificates)) {
        $content .= '<div class="alert alert-warning"><center>' .
                get_string('report_certificates_noreports', 'theme_eduopen') . '</center></div>';
        return $content;
    } else {

        foreach ($certificates as $certdata) {

            $certdata->printdate = 1; // Modify printdate so that date is always printed.
            $certrecord = new stdClass();
            $certrecord->timecreated = $certdata->citimecreated;

// Date format.
            $dateformat = get_string('strftimedate', 'langconfig');

// Required variables for output.
            $userid = $certrecord->userid = $certdata->userid;
            $certificateid = $certrecord->certificateid = $certdata->certificateid;
            $contextid = $certrecord->contextid = $certdata->contextid;
            $courseid = $certrecord->id = $certdata->id;
            $coursename = $certrecord->fullname = $certdata->fullname;
            $filename = $certrecord->filename = $certdata->filename;
            $files = $certdata->certificatename;

            $code = $certrecord->code = $certdata->code;

// Retrieving grade and date for each certificate.
//$grade = certificate_get_grade($certdata, $certrecord, $userid, $valueonly = true);
            $date = $certrecord->timecreated = $certdata->citimecreated;

// Direct course link.
            $link = html_writer::link(new moodle_url('/course/view.php', array('id' => $courseid)), $coursename, array('fullname' => $coursename));

// Direct certificate download link.
            $filelink = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/'
                    . $contextid . '/mod_certificate/issue/'
                    . $certificateid . '/' . $filename);

            $outputlink = '<a href="' . $filelink . '" >'
                    . '<img src="' . $CFG->wwwroot . '/mod/certificate/pix/download.png" alt="Please download"'
                    . 'width="100px" height="29px">'
                    . '</a>';

            $table->data[] = array($link, $code, userdate($date, $dateformat), $outputlink);
        }

//$content .= html_writer::link(new moodle_url('/blocks/report_certificates/report.php', array('userid' => $USER->id)), get_string('report_certificates_footermessage', 'block_report_certificates'));
    }

    $content .= html_writer::table($table);
    return $content;
}

function get_my_diaries($courseid, $coursename, $counter) {

    global $CFG, $OUTPUT, $COURSE, $USER, $DB;
    include_once($CFG->dirroot . '/blocks/diary/locallib.php');
    include_once($CFG->dirroot . '/mod/diary/locallib.php');
    $contents = new stdClass();

    $context = context_course::instance($courseid);
    $contents = '';
    $questionlist = $DB->get_records("diary", array("course" => $courseid));
    if ($counter == 1) {
        if (has_capability('moodle/course:update', context_course::instance($courseid))) {
            $contents .= html_writer::link(new moodle_url($CFG->wwwroot . '/blocks/diary/view.php', array('courseid' => $courseid)), get_string('viewalldownloads', 'block_diary'), array('class' => 'btn'));
        }
    }
    $contents .= "<h3 class='dashboardh3'>" . $coursename . "</h3>";
    if ($questionlist == null) {
        $contents .= html_writer::div(get_string('nodiary', 'block_diary'), 'alert alert-warning');
        return $contents;
    }

    if (user_has_role_assignment($USER->id, 5, $context->id)) {

        $table = new html_table();
        $table->attributes = array('class' => 'table table-responsive table-striped', 'style' => 'font-size:9pt');
        $table->head = (array) get_strings(array('sl', 'name', 'status', 'download'), 'block_diary');
        $downloadflg = 'disabled';
        foreach ($questionlist as $id => $row) {
            static $i = 1;
            $status = get_string('notattempted', 'block_diary');
            $response = diary_get_user_responses($id, $USER->id, false);
            foreach ($response as $field) {
                if ($field->complete == 'y') {
                    $status = get_string('submitted', 'block_diary');
                    $downloadflg = "";
                } else if ($field->complete == 'n') {
                    $status = get_string('saved', 'block_diary');
                }
            }
            $link = '-';
            if ($status == 'Submitted') {
                $link = html_writer::link(
                                new moodle_url($CFG->wwwroot . '/blocks/diary/download.php', array('id' => $id)), get_string('clickhere', 'block_diary'), array('download' => 'download', 'target' => '_blank')
                );
            }

            $table->data[] = array(
                $i++,
                $row->name,
                $status,
                $link
            );
        }
        $contents .= html_writer::table($table);

        if ($downloadflg == 'disabled') {
            $contents .= html_writer::tag('button', get_string('downoadconsolidated', 'block_diary'), array('class' => 'btn btn-success ' . $downloadflg));
        } else {
            $contents .= html_writer::link(new moodle_url($CFG->wwwroot . '/blocks/diary/download.php', array('courseid' => $courseid)), get_string('downoadconsolidated', 'block_diary'), array('class' => 'btn', 'download' => 'download', 'target' => '_blank'));
        }
    }


    return $contents;
}

// Added by shiuli on 27th feb.

function featuredcourse_block($courseidArray) {
    $output = '';
    global $DB, $CFG;
    $cidimplode = implode(',', $courseidArray);
    $coursedetails = "SELECT c.id, c.fullname, c.startdate, ce.courseimage, ce.language,
        ce.contextid, ce.institution, ce.specializations, ce.featurecourse,
        ce.engtitle, ce.certificate2,
        ce.formalcredit FROM {course} c JOIN {course_extrasettings_general} ce
        ON c.id=ce.courseid WHERE c.category!=0 AND ce.coursestatus=1 AND c.visible=1 AND
        c.id IN ($cidimplode) ORDER BY RANDOM()";
    $rs = $DB->get_records_sql($coursedetails);
    $i = 0;
    $count = 0;
    $currentLanguage = current_language();
    if ($rs) {
        foreach ($rs as $details) {
            $courseid[] = $details->id;
            if ($details->language == 'en') {
                $CourseName[] = strip_tags($details->engtitle); // Comes from extra settings.
            } else {
                $CourseName[] = $details->fullname; // Comes from Course edit settings.
            }
            $startdatetimestamp = $details->startdate; //course start date.
            $dateafteramonth[] = time() + (30 * 24 * 60 * 60); // date after 1 month.
            $feature[] = $details->featurecourse;
            $exam[] = $details->formalcredit;
            $institute = $details->institution;
            $pathway[] = $details->specializations;
            $certificate2[] = $details->certificate2;
            $csettingsinstitutionid = $DB->get_record('block_eduopen_master_inst', array('id' => $institute));
            if ($csettingsinstitutionid) {
                $instituteid[] = $csettingsinstitutionid->id;
                if ($currentLanguage == 'it') {
                    $instname[] = $csettingsinstitutionid->itname;
                } else {
                    $instname[] = $csettingsinstitutionid->name;
                }
            }
            $datestamp[] = $details->startdate;
            $cstartdate[] = date("dS F, Y", $startdatetimestamp);
            $startdate[] = gmdate("M dS", $startdatetimestamp);
            $courseimageid = $details->courseimage;
            $contextid = $details->contextid;
            $defaultimage = $CFG->wwwroot . '/theme/eduopen/pix/default_course.png';
            $imageurls = csetting_images($details);
            $imageurl[] = $imageurls ? $imageurls : $defaultimage;
            $count++;
        }
    }

    if (!empty($courseid)) {
        for ($i = 0; $i < sizeof($courseid); $i++) {
            $condA = (($certificate2[$i] == '1') && ($exam[$i] == 1));
            if ($condA || ($certificate2[$i] == '1') || ($exam[$i] == 1)) {
                $cls = 1;
            } else {
                $cls = 0;
            }

            //print($courseid[$i]);
            $output .= '<div class="col-md-3 col-sm-4 col-xs-6 textcenter pad0A">';
            $output .= '<div class="box-shadow">';
            $output .= '<div class="course-img front_cimg' . $cls . '">';
            $output .= '<a class="eduopen-link" 
            href="' . $CFG->wwwroot . '/eduopen/course_details.php?courseid=' . $courseid[$i] . '">';
            $output .= '<img src="' . $imageurl[$i] . '" width="100%" alt="' . $CourseName[$i] . '">';
            $output .= '</a>';
            if (($certificate2[$i] == '1') || ($exam[$i] == 1)) {
                if ($exam[$i] == 1) {
                    $banname = get_string('vernexam', 'theme_eduopen');
                    $output .= '<div class="crbanner">' . $banname . '</div>';
                } else if ($certificate2[$i] == '1') {
                    $banname = get_string('verified', 'theme_eduopen');
                    $output .= '<div class="banner">' . $banname . '</div>';
                } else if (($certificate2[$i] == '1') && ($exam[$i] == 1)) {
                    $banname = get_string('vernexam', 'theme_eduopen');
                    $output .= '<div class="crbanner">' . $banname . '</div>';
                }
            } else {
                $output .= '<div class="empty_ban"></div>';
            }
            $output .= '</div>';
            $condA = (($certificate2[$i] == '1') && ($exam[$i] == 1));
            if ($condA || ($certificate2[$i] == '1') || ($exam[$i] == 1)) {
                $cls = 1;
            } else {
                $cls = 0;
            }

            // rests of the divs after an image div.
            $output .= '<div class="restcontent">';
            $output .= '<div id="f_inst" class="ban' . $certificate2[$i] . '" class="padl5">';
            $output .= '<a class="eduopen-link"
                               href="' . $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $instituteid[$i] . '">';
            $output .= '<p class="instclr cont' . $cls . '">' . strip_tags($instname[$i]) . '</p>';
            $output .= '</a>';
            $output .= '</div>';
            $output .= '<div class="course-details">';
            $output .= '<a class="eduopen-link" 
                               href="' . $CFG->wwwroot . '/eduopen/course_details.php?courseid=' . $courseid[$i] . '">';
            $output .= '<p>' . strip_tags($CourseName[$i]) . '</p>';
            $output .= '</a>';
            $output .= '</div>';
            // Pathway Section.
            /* $output .= '<div class="crs-pathname">';
              if ($pathway[$i] != 'none') {
              $specializationid = explode(',', $pathway[$i]);
              if ($specializationid) {
              $output .= '<div class="frontpathhead">';
              $output .= '<b>' . get_string('splz', 'theme_eduopen') . ': </b>';
              for ($ij = 0; $ij < count($specializationid); $ij++) {
              if ($specializationid[$ij] != 0) {
              $deli = $ij < count($specializationid) - 1 ? ', ' : ''; // comma between two pathways except last one
              $specialdata = $DB->get_record('block_eduopen_master_special', array('id' => $specializationid[$ij]), 'name');
              //print_object($specialdata);
              $output .= '<span class="pathlist">
              <a class="" href="' . $CFG->wwwroot . '/eduopen/pathway_details.php?specialid=' . $specializationid[$ij] . '">
              ' . strip_tags($specialdata->name) . $deli . '</a></span>';
              }
              }
              $output .= '</div>';
              }
              }
              $output .= '</div>'; */

            $output .= '<div class="course-date-btn start_time">';
            $output .= '<div class="col-md-12 col-sm-12 col-xs-12 pad0A">';
            $output .= '<div class="col-md-11 col-sm-11 col-xs-11 pad0A datestring">';
            $output .= '<div class="date cstarts padl5">';
            $output .= '<i class="fa fa-calendar"></i>&nbsp;';
            $output .= $cstartdate[$i];
            $output .= '</div>';
            $output .= '</div>';
            // Current, coming soon, Upcoming.
            $output .= '<div class="col-md-1 col-sm-1 col-xs-1 pad0A dateimg">';
            $timeafteramonth = time() + (30 * 24 * 60 * 60); // date after 1month from current date
            if ($datestamp[$i] < time()) { // Current
                $output .= '<img class="img-responsive margTop2" src="' . $CFG->wwwroot . '/theme/eduopen/pix/datecurrent.svg">';
            } else if (($datestamp[$i] > time()) && ($datestamp[$i] < $timeafteramonth)) { // Coming Soon
                $output .= '<img class="img-responsive margTop2" src="' . $CFG->wwwroot . '/theme/eduopen/pix/datecomesoon.svg">';
            } else if ($datestamp[$i] > $timeafteramonth) { //upcoming
                $output .= '<img class="img-responsive margTop2" src="' . $CFG->wwwroot . '/theme/eduopen/pix/dateupcom.svg">';
            }
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
        }
    }
    return $output;
}

//featuredcourse_block for catalog page.
function catalog_featuredcourse_block($courseid, $archiveCourse) {
    $output = '';
    $archiveCourse = '';
    global $DB, $CFG;
    $currentLang = current_language();
    //$output = '<div class="row-fluid" id="frontrowpad">';
    $coursedetails = $DB->get_record('course', array('id' => $courseid, 'visible' => '1'));
    $count = 0;
    if ($coursedetails) {
        $courseid = $coursedetails->id;
        $coursename = $coursedetails->fullname;
        $startdate = $coursedetails->startdate;
        $instructor = get_teacher($courseid);
        if ($archiveCourse == 'archive') {
            $details = $DB->get_record('course_extrasettings_general', array('courseid' => $courseid,
                'coursestatus' => 1));
        } else {
            $details = $DB->get_record('course_extrasettings_general', array('courseid' => $courseid));
        }

        if ($details) {
            //$courseid = $details->id;
            $engcoursename = strip_tags($details->engtitle); // Comes from Course edit settings.
            if ($details->language == 'en') {
                $CrsName = $engcoursename;
            } else {
                $CrsName = $coursename;
            }
            //$startdatetimestamp = $details->startdate; //course start date.
            $startdatetimestamp = $coursedetails->startdate;
            $dateafteramonth = time() + (30 * 24 * 60 * 60); // date after 1 month.
            $feature = $details->featurecourse;
            $exam = $details->formalcredit;
            $institute = $details->institution;
            $pathway = $details->specializations;
            $csettingsinstitutionid = $DB->get_record('block_eduopen_master_inst', array('id' => $institute));
            if ($csettingsinstitutionid) {
                if ($currentLang == 'it') {
                    $instname = $csettingsinstitutionid->itname;
                } else {
                    $instname = $csettingsinstitutionid->name;
                }
            }
            $cstartdate = date("dS F, Y", $startdatetimestamp);
            $startdate = gmdate("M dS", $startdatetimestamp);
            $courseimageid = $details->courseimage;
            $contextid = $details->contextid;
            $defaultimage = $CFG->wwwroot . '/theme/eduopen/pix/default_course.png';
            $imageurls = csetting_images($details);
            $imageurl = $imageurls ? $imageurls : $defaultimage;
            $count++;
            $condA = (($details->certificate2 == '1') && ($exam == 1));
            if ($condA || ($details->certificate2 == '1') || ($exam == 1)) {
                $cls = 1;
            } else {
                $cls = 0;
            }

            $output .= '<div class="col-md-4 col-sm-4 col-xs-12 textcenter pad0A">';
            $output .= '<div class="box-shadow">';
            $output .= '<div class="course-img front_cimg' . $cls . '">';
            $output .= '<a class="eduopen-link" 
                           href="' . $CFG->wwwroot . '/eduopen/course_details.php?courseid=' . $courseid . '">';
            $output .= '<img src="' . $imageurl . '" width="100%" alt="' . $CrsName . '">';
            $output .= '</a>';
            if (($details->certificate2 == '1') || ($exam == 1)) {
                if ($exam == 1) {
                    $banname = get_string('vernexam', 'theme_eduopen');
                    $output .= '<div class="crbanner">' . $banname . '</div>';
                } else if ($details->certificate2 == '1') {
                    $banname = get_string('verified', 'theme_eduopen');
                    $output .= '<div class="banner">' . $banname . '</div>';
                } else if (($details->certificate2 == '1') && ($exam == 1)) {
                    $banname = get_string('vernexam', 'theme_eduopen');
                    $output .= '<div class="crbanner">' . $banname . '</div>';
                }
            } else {
                $output .= '<div class="empty_ban"></div>';
            }
            $output .= '</div>';
            $condA = (($details->certificate2 == '1') && ($exam == 1));
            if ($condA || ($details->certificate2 == '1') || ($exam == 1)) {
                $cls = 1;
            } else {
                $cls = 0;
            }
            // rests of the divs after an image div.
            $output .= '<div class="restcontent cont' . $cls . '">';
            $output .= '<div id="f_inst" class="ban' . $details->certificate2 . '" class="padl5">';
            $output .= '<a class="eduopen-link"
                           href="' . $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $institute . '">';
            $output .= '<p class="instclr">' . strip_tags($instname) . '</p>';
            $output .= '</a>';
            $output .= '</div>';
            $output .= '<div class="course-details">';
            $output .= '<a class="eduopen-link" 
                           href="' . $CFG->wwwroot . '/eduopen/course_details.php?courseid=' . $courseid . '">';
            $output .= '<p>' . strip_tags($CrsName) . '</p>';
            $output .= '</a>';
            $output .= '</div>';
//                $output .= '<div class="crs-pathname">';
//                $pathway = $details->specializations;
//                if ($pathway != 'none') {
//                    $special = explode(",", $pathway);
//                    $output .= '<div class="frontpathhead">';
//                    $output .= '<b>' . get_string('path', 'theme_eduopen') . '</b>';
//                    $output .= '<span class="pathlist">';
//                    for ($jj = 0; $jj < count($special); $jj++) {
//                        if ($special[$jj] != 0) {
//                            $deli = $jj < count($special) - 1 ? ', ' : ''; // comma between two pathways except last one
//                            $csettingspathway = $DB->get_record('block_eduopen_master_special', array('id' => $special[$jj]), 'name');
//                            $output .= '<a href="' . $CFG->wwwroot . '/eduopen/pathway_details.php?specialid=' .
//                                    $special[$jj] . '"class="">' . strip_tags($csettingspathway->name) . $deli . '</a>';
//                        }
//                    }
//                    $output .= '</div>';
//                }
//                $output .= '</div>';

            $output .= '<div class="course-date-btn start_time">';
            $output .= '<div class="col-md-12 col-sm-12 col-xs-12 pad0A">';
            $output .= '<div class="col-md-11 col-sm-11 col-xs-11 pad0A datestring">';
            $output .= '<div class="date cstarts padl5">';
            $output .= '<i class="fa fa-calendar"></i>&nbsp;';
            $output .= $cstartdate;
            $output .= '</div>';
            $output .= '</div>';
            // Current, coming soon, upcoming.
            $output .= '<div class="col-md-1 col-sm-1 col-xs-1 pad0A dateimg">';
            $timeafteramonth = time() + (30 * 24 * 60 * 60); // date after 1month from current date
            if ($startdatetimestamp < time()) { // Current
                $output .= '<img class="img-responsive margTop2" src="' . $CFG->wwwroot . '/theme/eduopen/pix/datecurrent.svg">';
            } else if (($startdatetimestamp > time()) && ($startdatetimestamp < $timeafteramonth)) { // Coming Soon
                $output .= '<img class="img-responsive margTop2" src="' . $CFG->wwwroot . '/theme/eduopen/pix/datecomesoon.svg">';
            } else if ($startdatetimestamp > $timeafteramonth) { //upcoming
                $output .= '<img class="img-responsive margTop2" src="' . $CFG->wwwroot . '/theme/eduopen/pix/dateupcom.svg">';
            }
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
        }
    }
    return $output;
}

//featuredpahway_block for catalog page.
function catalog_featuredpathway_block($pathwayid) {
    $output = '';
    global $DB, $CFG;
    $currentLanguage = current_language();
    $blockcount = 0;
    $specialdetails = $DB->get_record_sql("SELECT * FROM {block_eduopen_master_special}
        WHERE status='1' AND id=$pathwayid");
    if ($specialdetails) {

        if ($specialdetails->pathlanguage == 'it') {
            $pathwayName = $specialdetails->localname; // show the local pathway name
        } else {
            $pathwayName = $specialdetails->name; // show the en pathway name
        }

        $specialimgs = specialization_images($specialdetails);
        $defaultcourseimage = $CFG->wwwroot . '/theme/eduopen/pix/default_pathway.png';
        $specialimg = $specialimgs ? $specialimgs : $defaultcourseimage;
        $blockcount++;
        $output .= '<div class="col-md-4 col-sm-4 col-xs-12 special_imgs frontpagepath pad0A">';
        $output .= '<div class="cleanimg box-shadow-spz pad0A">';
        $output .= '<div class= "path-img' . $specialdetails->degree . '">';
        $output .= ' <a href = "' . $CFG->wwwroot . '/eduopen/pathway_details.php?specialid=' . $specialdetails->id . '">';
        $output .= '<img src = "' . $specialimg . '" class = "img-responsive" width = "100%"/>';

        if ($specialdetails->degree == 1) {
            $output .= '<div class = "mbanner">';
            $output .= $degree = get_string('mdeg', 'theme_eduopen');
            $output .= '</div>';
        } else if ($specialdetails->degree == 2) {
            $output .= '<div class="abanner">';
            $output .= $degree = get_string('adeg', 'theme_eduopen');
            $output .= '</div>';
        } else if ($specialdetails->degree == 3) {
            $output .= '<div class="obanner">';
            $output .= $degree = get_string('odeg', 'theme_eduopen');
            $output .= '</div>';
        } else {
            $output .= '<div class="empty_ban"></div>';
        }
        $output .= '</a>';
        $output .= '</div>';
        //<!-- Rests of the divs -->

        if ($specialdetails->degree == 0) {
            $classid = 0;
        } else {
            $classid = 1;
        }
        $output .= '<div class = "restpathdivs' . $classid . '">';
        $output .= '<div id= "path_name" class = "special_nms' . $specialdetails->degree . '">';
        $output .= '<a href = "' . $CFG->wwwroot . '/eduopen/pathway_details.php?specialid=' . $specialdetails->id . ' ">';
        $output .= '<p>' . $pathwayName . '</p>';
        $output .= '</a>';
        $output .= '</div>';
        $gen = $DB->get_records_sql("SELECT DISTINCT institution FROM {course_extrasettings_general}
            WHERE specializations @@ to_tsquery('$specialdetails->id') AND coursestatus=1 LIMIT 3");
        $gencount = count($gen);
        foreach ($gen as $new) {
            $newinst = $new->institution;
        }
        $output .= '<div class="inst_right">';
        if ($gencount == 1) {
            $ins = $DB->get_record_sql("SELECT * FROM {block_eduopen_master_inst} WHERE id=$newinst");
            if ($currentLanguage == 'it') {
                $instituteName = $ins->itname;
            } else {
                $instituteName = $ins->name;
            }
            $output .= '<div class="institutnm">';
            $output .= '<a class = "instclr" href="' . $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $ins->id . '">';
            $output .= $instituteName;
            $output .= '</a>';
            $output .= '</div>';
        } else if ($gencount > 1) {
            $output .= '<div class="institutnm">';
            $output .= '<a class = "instclr">';
            if ($currentLanguage == 'it') {
                $output .= get_string('instlist_it', 'theme_eduopen');
            } else {
                $output .= get_string('instlist', 'theme_eduopen');
            }
            $output .= '</a>';
            $output .= '</div>';
        }
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        $output .= '</div>';
        if (($blockcount % 4) == 0) {
            echo '</div>';
        }
        if (($blockcount % 4) == 0) {
            echo '<div class ="row-fluid dsrow" id="frontrowpad">';
        }
    }
    return $output;
}

function featured_pathway_block($pathway) {
    $output = '';
    global $DB, $CFG;
    $pathwayids = implode(',', $pathway);
    $blockcount = 0;
    $currentLanguage = current_language();
    $allspecial = $DB->get_records_sql("SELECT * FROM {block_eduopen_master_special} WHERE status='1'
        AND id IN ($pathwayids)");
    // print_object($allspecial);
    if (!empty($allspecial)) {
        foreach ($allspecial as $specialdetails) {
            if ($specialdetails->pathlanguage == 'it') {
                $PathwayName = $specialdetails->localname;
            } else {
                $PathwayName = $specialdetails->name;
            }
            $specialimg = specialization_images($specialdetails);
            $defaultcourseimage = $CFG->wwwroot . '/theme/eduopen/pix/default_pathway.png';
            $specialimg = $specialimg ? $specialimg : $defaultcourseimage;
            $blockcount++;
            $output .= '<div class="col-md-3 col-sm-4 col-xs-6 special_imgs frontpagepath pad0A">';
            $output .= '<div class="cleanimg box-shadow-spz pad0A">';
            $output .= '<div class= "path-img' . $specialdetails->degree . '">';
            $output .= ' <a href = "' . $CFG->wwwroot . '/eduopen/pathway_details.php?specialid=' . $specialdetails->id . '">';
            $output .= '<img src = "' . $specialimg . '" class = "img-responsive" width = "100%"/>';
            if ($specialdetails->degree == 1) {
                $output .= '<div class = "mbanner">';
                $output .= $degree = get_string('mdeg', 'theme_eduopen');
                $output .= '</div>';
            } else if ($specialdetails->degree == 2) {
                $output .= '<div class="abanner">';
                $output .= $degree = get_string('adeg', 'theme_eduopen');
                $output .= '</div>';
            } else if ($specialdetails->degree == 3) {
                $output .= '<div class="obanner">';
                $output .= $degree = get_string('odeg', 'theme_eduopen');
                $output .= '</div>';
            } else {
                $output .= '<div class="empty_ban"></div>';
            }
            $output .= '</a>';
            $output .= '</div>';
            //                <!-- Rests of the divs -->

            if ($specialdetails->degree == 0) {
                $classid = 0;
            } else {
                $classid = 1;
            }
            $output .= '<div class = "restpathdivs' . $classid . '">';
            $output .= '<div id= "path_name" class = "special_nms' . $specialdetails->degree . '">';
            $output .= '<a href = "' . $CFG->wwwroot . '/eduopen/pathway_details.php?specialid=' . $specialdetails->id . ' ">';
            $output .= '<p>' . $PathwayName . '</p>';
            $output .= '</a>';
            $output .= '</div>';
            $gen = $DB->get_records_sql("SELECT DISTINCT institution FROM {course_extrasettings_general}
                WHERE specializations @@ to_tsquery('$specialdetails->id') AND coursestatus=1");
            $gencount = count($gen);
            foreach ($gen as $new) {
                $newinst = $new->institution;
            }
            //print_object($gencount);
            $output .= '<div class="inst_right">';
            if ($gencount == 1) {
                $ins = $DB->get_record_sql("SELECT * FROM {block_eduopen_master_inst} WHERE id=$newinst");
                if ($currentLanguage == 'it') {
                    $instName = $ins->itname;
                } else {
                    $instName = $ins->name;
                }
                $output .= '<div class="institutnm">';
                $output .= '<a class = "instclr" href="' . $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $ins->id . '">';
                $output .= $instName;
                $output .= '</a>';
                $output .= '</div>';
            } else if ($gencount > 1) {
                $output .= '<div class="institutnm">';
                $output .= '<a class = "instclr">';
                if ($currentLanguage == 'it') {
                    $output .= get_string('instlist_it', 'theme_eduopen');
                } else {
                    $output .= get_string('instlist', 'theme_eduopen');
                }
                $output .= '</a>';
                $output .= '</div>';
            }
//            foreach ($gen as $value1) {
//                $ins = $DB->get_record_sql("SELECT * FROM {block_eduopen_master_inst} WHERE id=$value1->institution");
//                if ($gencount == 1) {
//                    $output .= '<div class="institutnm">';
//                    $output .= '<a href="' . $CFG->wwwroot . '/eduopen/institution_details.php?institutionid=' . $ins->id . '">';
//                    $output .= $ins->name;
//                    $output .= '</a>';
//                    $output .= '</div>';
//                } else if ($gencount > 1) {
//                    $output .= get_string('instlist', 'theme_eduopen');
//                }
//            }

            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            if (($blockcount % 4) == 0) {
                echo '</div>';
            }
            if (($blockcount % 4) == 0) {
                echo '<div class ="row-fluid dsrow" id="frontrowpad">';
            }
        }
    }
    return $output;
}

// Function to print instructors. 
function instructor_block($cidArray, $flag) {
    $output = '';
    global $DB, $CFG;
    $output .= '<div class = "row-fluid cstmmrgn">';
    $blockcount = 0;
    $duplicatedata = array();
    if ($cidArray) {
        $cArr = implode(',', $cidArray);
        if ($flag == 'editflag') {
            $teacherflag = 'editingteacher';
        } else if ($flag == 'noneditflag') {
            $teacherflag = 'teacher';
        }
        $instruct = $DB->get_records_sql("SELECT DISTINCT u.id, c.visible, u.firstname, u.lastname, u.department, u.picture,
            r.name FROM {course} c JOIN {context} ct ON c.id = ct.instanceid
            JOIN {role_assignments} ra ON ra.contextid = ct.id JOIN {user} u ON u.id = ra.userid
            JOIN {role} r ON r.id = ra.roleid WHERE r.shortname = '$teacherflag' AND c.visible=1 AND c.id IN ($cArr)");
    }
    //print_object(count($instruct));
    if ($instruct) {
        foreach ($instruct as $instdetails) {
            if (!in_array($instdetails->id, $duplicatedata)) {
                $userpicture = $DB->get_record('user', array('id' => $instdetails->id));
                $blockcount++;
                $output .= '<div class="col-md-4 col-sm-4 col-xs-12 pad0A">';
                $output .= '<div class="col-md-12 col-sm-12 col-xs-12">';

                $output .= '<div class="col-md-4 col-sm-6 col-xs-4">';
                $output .= '<div class="cbx">';
                $output .= '<a href="' . $CFG->wwwroot . '/eduopen/instructor.php?id=' . $instdetails->id . '">';
                $output .= '<img src="' . $CFG->wwwroot . '/user/pix.php?file=/' . $instdetails->id . '/f1.jpg"
                        width="80px" height="80px" class="img-circle"/></a>';
                $output .= '</div>';
                $output .= '</div>';

                $output .= '<div class="col-md-8 col-sm-6 col-xs-8">';
                $output .= '<div class="userdetails">';
                $output .= '<p id="instruct_nm"><strong>';
                $output .= '<a href="' . $CFG->wwwroot . '/eduopen/instructor.php?id=' . $instdetails->id . '">';
                $output .= $instdetails->firstname . ' ' . $instdetails->lastname;
                $output .= '</a>';
                $output .= '</strong></p>';
                $output .= '<p>' . $instdetails->department . '</p>';
                $output .= '</div>';
                $output .= '</div>';

                $output .= '</div>';
                $output .= '</div>';
                if (($blockcount % 3) == 0) {
                    $output .= '</div>';
                }
                if (( $blockcount % 3) == 0) {//if two blocks are printed at a time then create a new row
                    $output .= '<div class="row-fluid cstmmrgn">';
                }
            }
            $duplicatedata[] = $instdetails->id;
        }
    }
    $output .= '</div>';
    return $output;
}

function progress1($courseid) {
    global $CFG, $DB, $USER;
    $progress = 0;
    $return = null;
    $completed = null;
    $incompleted = null;
    $courseobj = get_course($courseid);
    $modinfo = get_fast_modinfo($courseobj);

    $completioninfolist[] = new completion_info($courseobj);
    foreach ($modinfo->cms as $mod) {
        foreach ($completioninfolist as $cminfo) {
            $modCm = $cminfo->is_enabled($mod);
        }
        // If module has completion enabled.
        if ($modCm != 0) {
            $modId[] = $mod->id;
        }
    }
//print_object($modCm);
    $modCompl = $DB->get_records('course_modules_completion', array('userid' => $USER->id,
        'completionstate' => 1));
    foreach ($modCompl as $modCompletion) {
        $cmids[] = $modCompletion->coursemoduleid;
    }
    if (!empty($modId)) {
        $ArrIntersect = array_intersect($modId, $cmids);
        $allMods = count($modId);
        $completeMods = count($ArrIntersect);
        $percent = round((($completeMods / $allMods) * 100), 1);
    } else {
        $percent = '0';
    }
    return $percent;
}

//Following function returns whether an user is student or not in a course.
function get_student($courseid, $userid, $contextid = 0) {
    global $DB;
    $student = '';
    $crscontext = context_course::instance($courseid);
    $contextid = $crscontext->id;
    $enrolled = is_enrolled($crscontext);
    if ($enrolled && !(is_siteadmin())) {
        $student = user_has_role_assignment($userid, 5, $contextid);
    }
    return $student;
}

// get all roles except student.
function visible_roles() {
    global $DB, $USER;
    $contentrole = $DB->get_record('role', array('shortname' => 'contenteditor'));
    $tutorrole = $DB->get_record('role', array('shortname' => 'teacher'));
    $student = user_has_role_assignment($USER->id, 5);
    $editingTeacher = user_has_role_assignment($USER->id, 3);
    $contentEditor = user_has_role_assignment($USER->id, $contentrole->id);
    $tutor = user_has_role_assignment($USER->id, $tutorrole->id);
    if ($editingTeacher || $contentEditor || $tutor) {
        $visibleUsers = 1;
    } else {
        $visibleUsers = 0;
    }
    return $visibleUsers;
}

// Added by Shiuli for course page section0 forum discussion.
function section0_forum_discussion($course, $cm, $forum) {
    global $DB, $CFG, $USER;
    echo '<div class="row-fluid courseForum" id="ForumDiscuss-' . $cm->id . '">';
    if ($DB->get_record('forum_subscriptions', array('userid' => $USER->id, 'forum' => $forum->id))) {
        echo html_writer::tag('button', get_string('cunsubscribe', 'mod_forum'), array('class' => 'forumunsubscribe pull-right', 'onclick' => 'forum_unsubscribe(' . $forum->id . ')'));
    }
    if (!$DB->get_record('forum_subscriptions', array('userid' => $USER->id, 'forum' => $forum->id))) {
        echo html_writer::tag('button', get_string('csubscribe', 'mod_forum'), array('class' => 'forumsubscribe pull-right', 'onclick' => 'forum_subscribe(' . $forum->id . ')'));
    }

    switch ($forum->type) {
        case 'single':
            if (!empty($discussions) && count($discussions) > 1) {
                echo $OUTPUT->notification(get_string('warnformorepost', 'forum'));
            }
            if (!$post = forum_get_post_full($discussion->firstpost)) {
                print_error('cannotfindfirstpost', 'forum');
            }
            if ($mode) {
                set_user_preference("forum_displaymode", $mode);
            }

            $canreply = forum_user_can_post($forum, $discussion, $USER, $cm, $course, $context);
            $canrate = has_capability('mod/forum:rate', $context);
            $displaymode = get_user_preferences("forum_displaymode", $CFG->forum_displaymode);

            echo '&nbsp;'; // this should fix the floating in FF
            forum_print_discussion($course, $cm, $forum, $discussion, $post, $displaymode, $canreply, $canrate);
            break;

        case 'eachuser':
            echo '<p class="mdl-align">';
            if (forum_user_can_post_discussion($forum, null, -1, $cm)) {
                print_string("allowsdiscussions", "forum");
            } else {
                echo '&nbsp;';
            }
            echo '</p>';
            if (!empty($showall)) {
                forum_print_latest_discussions($course, $forum, 0, 'header', '', -1, -1, -1, 0, $cm);
            } else {
                forum_print_latest_discussions($course, $forum, -1, 'header', '', -1, -1, $page, $CFG->forum_manydiscussions, $cm);
            }
            break;

        case 'teacher':
            if (!empty($showall)) {
                forum_print_latest_discussions($course, $forum, 0, 'header', '', -1, -1, -1, 0, $cm);
            } else {
                forum_print_latest_discussions($course, $forum, -1, 'header', '', -1, -1, $page, $CFG->forum_manydiscussions, $cm);
            }
            break;

        case 'blog':
            echo '<br />';
            if (!empty($showall)) {
                forum_print_latest_discussions($course, $forum, 0, 'plain', '', -1, -1, -1, 0, $cm);
            } else {
                forum_print_latest_discussions($course, $forum, -1, 'plain', '', -1, -1, $page, $CFG->forum_manydiscussions, $cm);
            }
            break;

        default:
            echo '<br />';
            if (!empty($showall)) {
                forum_print_latest_discussions($course, $forum, 0, 'header', '', -1, -1, -1, 0, $cm);
            } else {
                forum_print_latest_discussions($course, $forum, -1, 'header', '', -1, -1, 0, $CFG->forum_manydiscussions, $cm);
            }
            break;
    }
    echo '</div>';
}

// Eduplayer completion status for course view page by Shiuli.
function eduplayer_complete($courseid, $sectionid) {
    global $CFG, $DB, $USER;
    $progress = 0;
    $return = null;
    $completedEdu = array();
    $courseobj = get_course($courseid);
    $modinfo = get_fast_modinfo($courseobj);
    $secInfo = $modinfo->get_section_info($sectionid);
    $crsmodid = explode(',', $secInfo->sequence);
    if ($crsmodid) {
        foreach ($crsmodid as $crmid) {
            if (isset($crmid) && !empty($crmid)) {
                $mod = $DB->get_record('course_modules', array('id' => $crmid));
                $module = $DB->get_record('modules', array('id' => $mod->module));
                if (($module->name == 'eduplayer') && ($mod->visible == 1) && ($mod->completion == 2)) {
                    $totalEduid[] = $mod->id;
                    $modCompl = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id,
                        'userid' => $USER->id, 'completionstate' => 1));
                    if ($modCompl) {
                        $completedEdu[] = $modCompl;
                    }
                }
            }
        }
        $completedEduplayer = count($completedEdu);
        if (!empty($totalEduid)) {
            $totalEduplayer = count($totalEduid);
        } else {
            $totalEduplayer = 0;
        }
        if ($totalEduplayer != 0) {
            if ($totalEduplayer == $completedEduplayer) {
                $eduplayerProgress = '✓';
            } else {
                $eduplayerProgress = $completedEduplayer . '/' . $totalEduplayer;
            }
            return $eduplayerProgress;
        }
    }
}

function eduplayer_progress_dashboard($courseid) {
    global $CFG, $DB, $USER;
    $progress = 0;
    $return = null;
    $completedEdu = array();
    $courseobj = get_course($courseid);
    $modinfo = get_fast_modinfo($courseobj);
    foreach ($modinfo->cms as $mod) {
        $moduleName = $mod->get_module_type_name();
        if (($moduleName == 'Eduplayer') && ($mod->visible == 1) && ($mod->completion == 2)) {
            $totalEduid[] = $mod->id;
            $modCompl = $DB->get_record('course_modules_completion', array('coursemoduleid' => $mod->id,
                'userid' => $USER->id, 'completionstate' => 1));
            if ($modCompl) {
                $completedEdu[] = $modCompl;
            }
        }
    }
    if (!empty($totalEduid)) {
        $completedEduplayer = count($completedEdu);
        $totalEduplayer = count($totalEduid);
        $eduplayerProgress = round(($completedEduplayer / $totalEduplayer) * 95);
    } else {
        $eduplayerProgress = 0;
    }
    return $eduplayerProgress;
}
