<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's eduopen theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_eduopen
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/* defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    // Invert Navbar to dark background.
    $name = 'theme_eduopen/invert';
    $title = get_string('invert', 'theme_eduopen');
    $description = get_string('invertdesc', 'theme_eduopen');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Logo file setting.
    $name = 'theme_eduopen/logo';
    $title = get_string('logo','theme_eduopen');
    $description = get_string('logodesc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Custom CSS file.
    $name = 'theme_eduopen/customcss';
    $title = get_string('customcss', 'theme_eduopen');
    $description = get_string('customcssdesc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_eduopen/footnote';
    $title = get_string('footnote', 'theme_eduopen');
    $description = get_string('footnotedesc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
}
 */
$settings = null;

defined('MOODLE_INTERNAL') || die;
if (is_siteadmin()) {
   $ADMIN->add('themes', new admin_category('theme_eduopen', 'eduopen'));
   //$ADMIN->add('themes', new admin_category('theme_eduopen', 'BCU'));

    $temp = new admin_settingpage('theme_eduopen_colour', get_string('coloursettings', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_colour', get_string('coloursettingsheading', 'theme_eduopen'),
        format_text(get_string('colourdesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/breadcrumbs';
    $title = get_string('breadcrumbs', 'theme_eduopen');
    $description = get_string('breadcrumbsdesc', 'theme_eduopen');
    $default = '#001E3C';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $temp->add($setting);
    
    $name = 'theme_eduopen/maincolor';
    $title = get_string('maincolor', 'theme_eduopen');
    $description = get_string('maincolordesc', 'theme_eduopen');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#001E3C', $previewconfig);
    $temp->add($setting);

    $name = 'theme_eduopen/backcolor';
    $title = get_string('backcolor', 'theme_eduopen');
    $description = get_string('backcolordesc', 'theme_eduopen');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#FFF', $previewconfig);
    $temp->add($setting);
    
    $name = 'theme_eduopen/logobackcolor';
    $title = get_string('logobackcolor', 'theme_eduopen');
    $description = get_string('logobackcolordesc', 'theme_eduopen');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#FFF', $previewconfig);
    $temp->add($setting);
    
    $name = 'theme_eduopen/homefontbackcolor';
    $title = get_string('homefontbackcolor', 'theme_eduopen');
    $description = get_string('homefontbackcolordesc', 'theme_eduopen');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#FFF', $previewconfig);
    $temp->add($setting);

     $name = 'theme_eduopen/linkcolor';
    $title = get_string('linkcolor', 'theme_eduopen');
    $description = get_string('linkcolordesc', 'theme_eduopen');
    $default = '#001E3C';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $temp->add($setting);
     
    $name = 'theme_eduopen/linkhover';
    $title = get_string('linkhover', 'theme_eduopen');
    $description = get_string('linkhoverdesc', 'theme_eduopen');
    $default = '#001E3C';
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, $default, $previewconfig);
    $temp->add($setting);
    
     $name = 'theme_eduopen/buttoncolour';
    $title = get_string('buttoncolour', 'theme_eduopen');
    $description = get_string('buttoncolourdesc', 'theme_eduopen');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#00aeef', $previewconfig);
    $temp->add($setting);
    
    $name = 'theme_eduopen/buttonhovercolour';
    $title = get_string('buttonhovercolour', 'theme_eduopen');
    $description = get_string('buttonhovercolourdesc', 'theme_eduopen');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#0084c2', $previewconfig);
    $temp->add($setting); 
    
    $name = 'theme_eduopen/footerbgcolour';
    $title = get_string('footerbgcolour', 'theme_eduopen');
    $description = get_string('footerbgcolourdesc', 'theme_eduopen');
    $previewconfig = null;
    $setting = new admin_setting_configcolourpicker($name, $title, $description, '#0084c2', $previewconfig);
    $temp->add($setting);

   $ADMIN->add('theme_eduopen', $temp);
    /* //footer setting page
    $temp = new admin_settingpage('theme_eduopen_footer', get_string('footersettings', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_footer', get_string('footersettingsheading', 'theme_eduopen'),
    format_text(get_string('footerdesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/footer1header';
    $title = get_string('footer1header', 'theme_eduopen');
    $description = get_string('footer1desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/footer1content';
    $title = get_string('footer1content', 'theme_eduopen');
    $description = get_string('footer1contentdesc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/footer2header';
    $title = get_string('footer2header', 'theme_eduopen');
    $description = get_string('footer2desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/footer2content';
    $title = get_string('footer2content', 'theme_eduopen');
    $description = get_string('footer2contentdesc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/footer3header';
    $title = get_string('footer3header', 'theme_eduopen');
    $description = get_string('footer3desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/footer3content';
    $title = get_string('footer3content', 'theme_eduopen');
    $description = get_string('footer3contentdesc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/footer4header';
    $title = get_string('footer4header', 'theme_eduopen');
    $description = get_string('footer4desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/footer4content';
    $title = get_string('footer4content', 'theme_eduopen');
    $description = get_string('footer4contentdesc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/footnote';
    $title = get_string('footnote', 'theme_eduopen');
    $description = get_string('footnotedesc', 'theme_eduopen');
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);

    $ADMIN->add('theme_eduopen', $temp); */
    //for logo setting
    $temp = new admin_settingpage('theme_eduopen_header', get_string('headersettings', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_header', get_string('headersettingsheading', 'theme_eduopen'),
    format_text(get_string('headerdesc', 'theme_eduopen'), FORMAT_MARKDOWN)));
    
    $name = 'theme_eduopen/sitetitle';
    $title = get_string('sitetitle', 'theme_eduopen');
    $description = get_string('sitetitledesc', 'theme_eduopen');
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
        
    $name = 'theme_eduopen/logo';
    $title = get_string('logo', 'theme_eduopen');
    $description = get_string('logodesc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    $ADMIN->add('theme_eduopen', $temp);
   //==================================For general setting========================================================
    $temp = new admin_settingpage('theme_eduopen_general', get_string('generalsettings', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_general', get_string('generalsettingsheading', 'theme_eduopen'),
    format_text(get_string('generaldesc', 'theme_eduopen'), FORMAT_MARKDOWN)));
    
    $name = 'theme_eduopen/toggleblock';
    $title = get_string('toggleblock', 'theme_eduopen');
    $description = get_string('toggleblockdesc', 'theme_eduopen');
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
     $name = 'theme_eduopen/blockicons';
    $title = get_string('blockicons', 'theme_eduopen');
    $description = get_string('blockiconsdesc', 'theme_eduopen');
    $setting = new admin_setting_configcheckbox($name, $title, $description, $default, true, false);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    
    $ADMIN->add('theme_eduopen', $temp);

    //=================================================================================================================
	
	
	  //==================  certificate page change in 23 sept 15===================start================================
	  
 $temp = new admin_settingpage('theme_eduopen_certificate', get_string('certificatesettings', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_certificate', get_string('certificatesettingsheading', 'theme_eduopen'),
    format_text(get_string('certificatedesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/image3';
    $title = get_string('image3', 'theme_eduopen');
    $description = get_string('image3desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image3');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_eduopen/image4';
    $title = get_string('image4', 'theme_eduopen');
    $description = get_string('image4desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image4');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add('theme_eduopen', $temp);
	
	//==================  certificate page change in 23 sept 15========================end===========================
	
     //==================  exampage page change in 23 sept 15========================start===========================
	 
 $temp = new admin_settingpage('theme_eduopen_exam', get_string('examsettings', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_exam', get_string('examsettingsheading', 'theme_eduopen'),
    format_text(get_string('examdesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/image1';
    $title = get_string('image1', 'theme_eduopen');
    $description = get_string('image1desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image1');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_eduopen/image2';
    $title = get_string('image2', 'theme_eduopen');
    $description = get_string('image2desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image2');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $ADMIN->add('theme_eduopen', $temp);
	 //==================  exampage page change in 23 sept 15========================end===========================
	 
      //==================  about us page change in 23 sept 15========================end===========================
	 
    $temp = new admin_settingpage('theme_eduopen_aboutus', get_string('aboutussettings', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_certificate', get_string('aboutussettingsheading', 'theme_eduopen'),
    format_text(get_string('aboutusdesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/image5';
    $title = get_string('image5', 'theme_eduopen');
    $description = get_string('image5desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image5');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_eduopen/image6';
    $title = get_string('image6', 'theme_eduopen');
    $description = get_string('image6desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image6');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
 

    $name = 'theme_eduopen/image7';
    $title = get_string('image7', 'theme_eduopen');
    $description = get_string('image7desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image7');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_eduopen/image8';
    $title = get_string('image8', 'theme_eduopen');
    $description = get_string('image8desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image8');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

 $name = 'theme_eduopen/image9';
    $title = get_string('image9', 'theme_eduopen');
    $description = get_string('image9desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image9');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
 

    $name = 'theme_eduopen/image10';
    $title = get_string('image10', 'theme_eduopen');
    $description = get_string('image10desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image10');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);

    $name = 'theme_eduopen/image11';
    $title = get_string('image11', 'theme_eduopen');
    $description = get_string('image11desc', 'theme_eduopen');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'image11');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $temp->add($setting);
    $ADMIN->add('theme_eduopen', $temp);
	
	 //==================  policy page change in 13 nov 15========================end===========================
	 /*policy page-infomation notice for privacy policy(en)*/
    
    $temp = new admin_settingpage('theme_eduopen_policy_en_infonotice_setting', get_string('policy_en_infonotice_privacy_setting', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_policy_en_infonotice_setting', get_string('policy_en_infonotice_settingsheading', 'theme_eduopen'),
    format_text(get_string('policy_en_infonoticedesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/policy_en_infonotice_header';
    $title = get_string('policy_en_infonotice_header', 'theme_eduopen');
    $description = get_string('policy_infonotice_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_en_infonotice_content1title';
    $title = get_string('policy_en_infonotice_content1title', 'theme_eduopen');
    $description = get_string('policy_infonotice_content1title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);

    $name = 'theme_eduopen/policy_en_infonotice_content1';
    $title = get_string('policy_en_infonotice_content1', 'theme_eduopen');
    $description = get_string('policy_infonotice_content1_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_en_infonotice_content2title';
    $title = get_string('policy_en_infonotice_content2title', 'theme_eduopen');
    $description = get_string('policy_infonotice_content2title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_en_infonotice_content2';
    $title = get_string('policy_en_infonotice_content2', 'theme_eduopen');
    $description = get_string('policy_infonotice_content2_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    $ADMIN->add('theme_eduopen', $temp);
    
    /*policy page-infomation notice for privacy policy(it)*/
    
    $temp = new admin_settingpage('theme_eduopen_policy_it_infonotice_setting', get_string('policy_it_infonotice_privacy_setting', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_policy_it_infonotice_setting', get_string('policy_it_infonotice_settingsheading', 'theme_eduopen'),
    format_text(get_string('policy_it_infonoticedesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/policy_it_infonotice_header';
    $title = get_string('policy_it_infonotice_header', 'theme_eduopen');
    $description = get_string('policy_infonotice_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_it_infonotice_content1title';
    $title = get_string('policy_it_infonotice_content1title', 'theme_eduopen');
    $description = get_string('policy_infonotice_content1title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_it_infonotice_content1';
    $title = get_string('policy_it_infonotice_content1', 'theme_eduopen');
    $description = get_string('policy_infonotice_content1_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    
    
    $name = 'theme_eduopen/policy_it_infonotice_content2title';
    $title = get_string('policy_it_infonotice_content2title', 'theme_eduopen');
    $description = get_string('policy_infonotice_content2title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_it_infonotice_content2';
    $title = get_string('policy_it_infonotice_content2', 'theme_eduopen');
    $description = get_string('policy_infonotice_content2_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    $ADMIN->add('theme_eduopen', $temp);
    
    /*policy page-condition for using elearning platform policy(en)*/
    
    $temp = new admin_settingpage('theme_eduopen_policy_en_condition_setting', get_string('policy_en_condition_elearn_setting', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_policy_en_condition_setting', get_string('policy_en_condition_settingsheading', 'theme_eduopen'),
    format_text(get_string('policy_en_conditiondesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/policy_en_condition_header';
    $title = get_string('policy_en_condition_header', 'theme_eduopen');
    $description = get_string('policy_condition_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_en_condition_content1title';
    $title = get_string('policy_en_condition_content1title', 'theme_eduopen');
    $description = get_string('policy_condition_content1title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_en_condition_content1';
    $title = get_string('policy_en_condition_content1', 'theme_eduopen');
    $description = get_string('policy_condition_content1_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_en_condition_content2title';
    $title = get_string('policy_en_condition_content2title', 'theme_eduopen');
    $description = get_string('policy_condition_content2title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_en_condition_content2';
    $title = get_string('policy_en_condition_content2', 'theme_eduopen');
    $description = get_string('policy_condition_content2_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    $ADMIN->add('theme_eduopen', $temp);
    
     /*policy page-condition for using elearning platform policy(it)*/
    
    $temp = new admin_settingpage('theme_eduopen_policy_it_condition_setting', get_string('policy_it_condition_elearn_setting', 'theme_eduopen'));
    $temp->add(new admin_setting_heading('theme_eduopen_policy_it_condition_setting', get_string('policy_it_condition_settingsheading', 'theme_eduopen'),
    format_text(get_string('policy_en_conditiondesc', 'theme_eduopen'), FORMAT_MARKDOWN)));

    $name = 'theme_eduopen/policy_it_condition_header';
    $title = get_string('policy_it_condition_header', 'theme_eduopen');
    $description = get_string('policy_condition_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_it_condition_content1title';
    $title = get_string('policy_it_condition_content1title', 'theme_eduopen');
    $description = get_string('policy_condition_content1title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_it_condition_content1';
    $title = get_string('policy_it_condition_content1', 'theme_eduopen');
    $description = get_string('policy_condition_content1_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_it_condition_content2title';
    $title = get_string('policy_it_condition_content2title', 'theme_eduopen');
    $description = get_string('policy_condition_content2title_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_configtext($name, $title, $description, $default);
    $temp->add($setting);
    
    $name = 'theme_eduopen/policy_it_condition_content2';
    $title = get_string('policy_it_condition_content2', 'theme_eduopen');
    $description = get_string('policy_condition_content2_desc', 'theme_eduopen');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $temp->add($setting);
    $ADMIN->add('theme_eduopen', $temp);
	
}