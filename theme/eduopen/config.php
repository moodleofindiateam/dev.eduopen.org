<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Moodle's eduopen theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_eduopen
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$THEME->name = 'eduopen';

/////////////////////////////////
// The only thing you need to change in this file when copying it to
// create a new theme is the name above. You also need to change the name
// in version.php and lang/en/theme_eduopen.php as well.
//////////////////////////////////
//
$THEME->doctype = 'html5';
$THEME->parents = array('bootstrapbase');
$THEME->parentsheets = false;
$THEME->sheets = array('custom', 'bootstrap_min', 'bootstrap-reset', 'style', 'style-responsive', 'eduopen_custom', 'media_eduopen', 'font_local','block');
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();
$THEME->enable_dock = true;
$THEME->editor_sheets = array();

$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_eduopen_process_css';

global $COURSE, $DB, $USER;
// Check course Start date.
$crs = $DB->get_record('course', array('id' => $COURSE->id));
// Check whether an user is a student and not admin.
$crscontext = context_course::instance($COURSE->id);
$enrolled = is_enrolled($crscontext);
if ($enrolled && !(is_siteadmin())) {
    $student = user_has_role_assignment($USER->id, 5, $crscontext->id);
}  else {
    $student = '';
}

if (($crs->startdate >= time()) && $student) {
    $crslayout = 'eduopen_notstarted_course.php';
} else {
    $crslayout = 'eduopen_course.php';
}

$THEME->layouts = array(
    // Most backwards compatible layout without the blocks - this is the layout used by default.
    'base' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array(),
    ),
    // Standard layout with blocks, this is recommended for most pages with general information.
    'standard' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array(),
    ),
    //Institution layout
    'eduopen_institution' => array(
        'file' => 'eduopen_institution.php',
        'regions' => array(),
    ),
    //Specialization layout
    'eduopen' => array(
        'file' => 'eduopen_special.php',
        'regions' => array(),
    ),
    // Main course page.
    'course' => array(
        'file' => $crslayout,
        'regions' => array('side-post', 'center-post'),
        'defaultregion' => 'side-post',
        'options' => array('langmenu' => true),
    ),
    'coursecategory' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
    ),
    // Part of course, typical for modules - default page layout if $cm specified in require_login().
    'incourse' => array(
        'file' => 'eduopen_modcourse.php',
        'regions' => array('side-pre', 'side-post'),
        'options' => array('langmenu' => true),
        'defaultregion' => 'side-post',
    ),
    // for enrol index page.
    'enrol_index' => array(
        'file' => 'enrol_index.php',
        'regions' => array(),
        'options' => array('nonavbar' => true),
    //'defaultregion' => 'side-pre',
    ),
    // Part of course, typical for eduvideo - default page layout if $cm specified in require_login().
    'edustreamincourse' => array(
        'file' => 'eduopen_incourse.php',
        'regions' => array(),
        'options' => array('nofooter' => true, 'nonavbar' => true),
    //'defaultregion' => 'side-pre',
    ),
    // The site home page.
    'frontpage' => array(
        'file' => 'frontpage.php',
        'regions' => array(),
    ),
    // Server administration scripts.
    'admin' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
    ),
    // My dashboard page.
    'mydashboard' => array(
        'file' => 'dashboard.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
        'options' => array('langmenu' => true),
    ),
    // My public page.
    'mypublic' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre',
    ),
    'login' => array(
        'file' => 'login.php',
        'regions' => array(),
        'options' => array('langmenu' => true),
    ),
    'register' => array(
        'file' => 'signup.php',
    /* 'regions' => array(),
      'options' => array('langmenu' => true), */
    ),
    // Pages that appear in pop-up windows - no navigation, no blocks, no header.
    'popup' => array(
        'file' => 'popup.php',
        'regions' => array(),
        'options' => array('nofooter' => true, 'nonavbar' => true),
    ),
    // No blocks and minimal footer - used for legacy frame layouts only!
    'frametop' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array(),
        'options' => array('nofooter' => true, 'nocoursefooter' => true),
    ),
    // Embeded pages, like iframe/object embeded in moodleform - it needs as much space as possible.
    'embedded' => array(
        'file' => 'embedded.php',
        'regions' => array()
    ),
    // Used during upgrade and install, and for the 'This site is undergoing maintenance' message.
    // This must not have any blocks, links, or API calls that would lead to database or cache interaction.
    // Please be extremely careful if you are modifying this layout.
    'maintenance' => array(
        'file' => 'maintenance.php',
        'regions' => array(),
    ),
    // Should display the content and basic headers only.
    'print' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array(),
        'options' => array('nofooter' => true, 'nonavbar' => false),
    ),
    // The pagelayout used when a redirection is occuring.
    'redirect' => array(
        'file' => 'embedded.php',
        'regions' => array(),
    ),
    // The pagelayout used for reports.
    'report' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array('side-pre'),
        'defaultregion' => 'side-pre',
    ),
    // The pagelayout used for safebrowser and securewindow.
    'secure' => array(
        'file' => 'secure.php',
        'regions' => array('side-pre', 'side-post'),
        'defaultregion' => 'side-pre'
    ),
    'allcourse' => array(
        'file' => 'all_courses.php',
        'regions' => array(),
    ),
    // This pagelayout is used to show the report of myanalytics plugin.
    'analytics_report' => array(
        'file' => 'eduopen_nosidebars.php',
        'regions' => array(),
    ),
    // This pagelayout is used to show the report of myanalytics plugin.
    'mod_eduplaer' => array(
        'file' => 'eduopen_modcourse.php',
        'regions' => array(),
    ),
);

$THEME->blockrtlmanipulations = array(
    'side-pre' => 'side-post',
    'side-post' => 'side-pre'
);
