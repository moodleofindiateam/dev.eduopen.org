<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * print the form to add or edit a diary-instance
 *
 * @author Mike Churchward
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package diary
 */

require_once($CFG->dirroot.'/course/moodleform_mod.php');

class diary_settings_form extends moodleform {

    public function definition() {
        global $diary, $diaryrealms, $CFG;

        $mform    =& $this->_form;

        $mform->addElement('header', 'contenthdr', get_string('contentoptions', 'diary'));

        $capabilities = diary_load_capabilities($diary->cm->id);
        if (!$capabilities->createtemplates) {
            unset($diaryrealms['template']);
        }
        if (!$capabilities->createpublic) {
            unset($diaryrealms['public']);
        }
        if (isset($diaryrealms['public']) || isset($diaryrealms['template'])) {
            $mform->addElement('select', 'realm', get_string('realm', 'diary'), $diaryrealms);
            $mform->setDefault('realm', $diary->survey->realm);
            $mform->addHelpButton('realm', 'realm', 'diary');
        } else {
            $mform->addElement('hidden', 'realm', 'private');
        }
        $mform->setType('realm', PARAM_RAW);

        $mform->addElement('text', 'title', get_string('title', 'diary'), array('size' => '60'));
        $mform->setDefault('title', $diary->survey->title);
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', null, 'required', null, 'client');
        $mform->addHelpButton('title', 'title', 'diary');

        $mform->addElement('text', 'subtitle', get_string('subtitle', 'diary'), array('size' => '60'));
        $mform->setDefault('subtitle', $diary->survey->subtitle);
        $mform->setType('subtitle', PARAM_TEXT);
        $mform->addHelpButton('subtitle', 'subtitle', 'diary');

        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'trusttext' => true);
        $mform->addElement('editor', 'info', get_string('additionalinfo', 'diary'), null, $editoroptions);
        $mform->setDefault('info', $diary->survey->info);
        $mform->setType('info', PARAM_RAW);
        $mform->addHelpButton('info', 'additionalinfo', 'diary');

        $mform->addElement('header', 'submithdr', get_string('submitoptions', 'diary'));

        $mform->addElement('text', 'thanks_page', get_string('url', 'diary'), array('size' => '60'));
        $mform->setType('thanks_page', PARAM_TEXT);
        $mform->setDefault('thanks_page', $diary->survey->thanks_page);
        $mform->addHelpButton('thanks_page', 'url', 'diary');

        $mform->addElement('static', 'confmes', get_string('confalts', 'diary'));
        $mform->addHelpButton('confmes', 'confpage', 'diary');

        $mform->addElement('text', 'thank_head', get_string('headingtext', 'diary'), array('size' => '30'));
        $mform->setType('thank_head', PARAM_TEXT);
        $mform->setDefault('thank_head', $diary->survey->thank_head);

        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'trusttext' => true);
        $mform->addElement('editor', 'thank_body', get_string('bodytext', 'diary'), null, $editoroptions);
        $mform->setType('thank_body', PARAM_RAW);
        $mform->setDefault('thank_body', $diary->survey->thank_body);

        $mform->addElement('text', 'email', get_string('email', 'diary'), array('size' => '75'));
        $mform->setType('email', PARAM_TEXT);
        $mform->setDefault('email', $diary->survey->email);
        $mform->addHelpButton('email', 'sendemail', 'diary');

        $defaultsections = get_config('diary', 'maxsections');

        // We cannot have more sections than available (required) questions with a choice value.
        $nbquestions = 0;
        foreach ($diary->questions as $question) {
            $qtype = $question->type_id;
            $qname = $question->name;
            $required = $question->required;
            // Question types accepted for feedback; DIARYQUESRATE ok except noduplicates.
            if (($qtype == DIARYQUESRADIO || $qtype == DIARYQUESDROP || ($qtype == DIARYQUESRATE && $question->precise != 2))
                            && $required == 'y' && $qname != '') {
                foreach ($question->choices as $choice) {
                    if (isset($choice->value) && $choice->value != null && $choice->value != 'NULL') {
                        $nbquestions ++;
                        break;
                    }
                }
            }
            if ($qtype == DIARYQUESYESNO && $required == 'y' && $qname != '') {
                $nbquestions ++;
            }
        }

        // Questionnaire Feedback Sections and Messages.
        if ($nbquestions != 0) {
            $maxsections = min ($nbquestions, $defaultsections);
            $feedbackoptions = array();
            $feedbackoptions[0] = get_string('feedbacknone', 'diary');
            $mform->addElement('header', 'submithdr', get_string('feedbackoptions', 'diary'));
            $feedbackoptions[1] = get_string('feedbackglobal', 'diary');
            for ($i = 2; $i <= $maxsections; ++$i) {
                $feedbackoptions[$i] = get_string('feedbacksections', 'diary', $i);
            }
            $mform->addElement('select', 'feedbacksections', get_string('feedbackoptions', 'diary'), $feedbackoptions);
            $mform->setDefault('feedbacksections', $diary->survey->feedbacksections);
            $mform->addHelpButton('feedbacksections', 'feedbackoptions', 'diary');

            $options = array('0' => get_string('no'), '1' => get_string('yes'));
            $mform->addElement('select', 'feedbackscores', get_string('feedbackscores', 'diary'), $options);
            $mform->addHelpButton('feedbackscores', 'feedbackscores', 'diary');

            // Is the RGraph library enabled at level site?
            $usergraph = get_config('diary', 'usergraph');
            if ($usergraph) {
                $chartgroup = array();
                $charttypes = array (null => get_string('none'),
                        'bipolar' => get_string('chart:bipolar', 'diary'),
                        'vprogress' => get_string('chart:vprogress', 'diary'));
                $chartgroup[] = $mform->createElement('select', 'chart_type_global',
                        get_string('chart:type', 'diary').' ('.
                                get_string('feedbackglobal', 'diary').')', $charttypes);
                if ($diary->survey->feedbacksections == 1) {
                    $mform->setDefault('chart_type_global', $diary->survey->chart_type);
                }
                $mform->disabledIf('chart_type_global', 'feedbacksections', 'eq', 0);
                $mform->disabledIf('chart_type_global', 'feedbacksections', 'neq', 1);

                $charttypes = array (null => get_string('none'),
                        'bipolar' => get_string('chart:bipolar', 'diary'),
                        'hbar' => get_string('chart:hbar', 'diary'),
                        'rose' => get_string('chart:rose', 'diary'));
                $chartgroup[] = $mform->createElement('select', 'chart_type_two_sections',
                        get_string('chart:type', 'diary').' ('.
                                get_string('feedbackbysection', 'diary').')', $charttypes);
                if ($diary->survey->feedbacksections > 1) {
                    $mform->setDefault('chart_type_two_sections', $diary->survey->chart_type);
                }
                $mform->disabledIf('chart_type_two_sections', 'feedbacksections', 'neq', 2);

                $charttypes = array (null => get_string('none'),
                        'bipolar' => get_string('chart:bipolar', 'diary'),
                        'hbar' => get_string('chart:hbar', 'diary'),
                        'radar' => get_string('chart:radar', 'diary'),
                        'rose' => get_string('chart:rose', 'diary'));
                $chartgroup[] = $mform->createElement('select', 'chart_type_sections',
                        get_string('chart:type', 'diary').' ('.
                                get_string('feedbackbysection', 'diary').')', $charttypes);
                if ($diary->survey->feedbacksections > 1) {
                    $mform->setDefault('chart_type_sections', $diary->survey->chart_type);
                }
                $mform->disabledIf('chart_type_sections', 'feedbacksections', 'eq', 0);
                $mform->disabledIf('chart_type_sections', 'feedbacksections', 'eq', 1);
                $mform->disabledIf('chart_type_sections', 'feedbacksections', 'eq', 2);

                $mform->addGroup($chartgroup, 'chartgroup',
                        get_string('chart:type', 'diary'), null, false);
                $mform->addHelpButton('chartgroup', 'chart:type', 'diary');
            }
            $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'trusttext' => true);
            $mform->addElement('editor', 'feedbacknotes', get_string('feedbacknotes', 'diary'), null, $editoroptions);
            $mform->setType('feedbacknotes', PARAM_RAW);
            $mform->setDefault('feedbacknotes', $diary->survey->feedbacknotes);
            $mform->addHelpButton('feedbacknotes', 'feedbacknotes', 'diary');

            $mform->addElement('submit', 'feedbackeditbutton', get_string('feedbackeditsections', 'diary'));
            $mform->disabledIf('feedbackeditbutton', 'feedbacksections', 'eq', 0);
        }

        // Hidden fields.
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'sid', 0);
        $mform->setType('sid', PARAM_INT);
        $mform->addElement('hidden', 'name', '');
        $mform->setType('name', PARAM_TEXT);
        $mform->addElement('hidden', 'owner', '');
        $mform->setType('owner', PARAM_RAW);

        // Buttons.

        $submitlabel = get_string('savechangesanddisplay');
        $submit2label = get_string('savechangesandreturntocourse');
        $mform = $this->_form;

        // Elements in a row need a group.
        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton2', $submit2label);
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', $submitlabel);
        $buttonarray[] = &$mform->createElement('cancel');

        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->setType('buttonar', PARAM_RAW);
        $mform->closeHeaderBefore('buttonar');

    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }
}