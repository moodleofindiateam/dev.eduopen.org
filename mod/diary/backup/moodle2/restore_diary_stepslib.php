<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package moodlecore
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_diary_activity_task
 */

/**
 * Structure step to restore one diary activity
 */
class restore_diary_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('diary', '/activity/diary');
        $paths[] = new restore_path_element('diary_survey', '/activity/diary/surveys/survey');
        $paths[] = new restore_path_element('diary_fb_sections',
                        '/activity/diary/surveys/survey/fb_sections/fb_section');
        $paths[] = new restore_path_element('diary_feedback',
                        '/activity/diary/surveys/survey/fb_sections/fb_section/feedbacks/feedback');
        $paths[] = new restore_path_element('diary_question',
                        '/activity/diary/surveys/survey/questions/question');
        $paths[] = new restore_path_element('diary_quest_choice',
                        '/activity/diary/surveys/survey/questions/question/quest_choices/quest_choice');

        if ($userinfo) {
            $paths[] = new restore_path_element('diary_attempt', '/activity/diary/attempts/attempt');
            $paths[] = new restore_path_element('diary_response',
                            '/activity/diary/attempts/attempt/responses/response');
            $paths[] = new restore_path_element('diary_response_bool',
                            '/activity/diary/attempts/attempt/responses/response/response_bools/response_bool');
            $paths[] = new restore_path_element('diary_response_date',
                            '/activity/diary/attempts/attempt/responses/response/response_dates/response_date');
            $paths[] = new restore_path_element('diary_response_multiple',
                            '/activity/diary/attempts/attempt/responses/response/response_multiples/response_multiple');
            $paths[] = new restore_path_element('diary_response_other',
                            '/activity/diary/attempts/attempt/responses/response/response_others/response_other');
            $paths[] = new restore_path_element('diary_response_rank',
                            '/activity/diary/attempts/attempt/responses/response/response_ranks/response_rank');
            $paths[] = new restore_path_element('diary_response_single',
                            '/activity/diary/attempts/attempt/responses/response/response_singles/response_single');
            $paths[] = new restore_path_element('diary_response_text',
                            '/activity/diary/attempts/attempt/responses/response/response_texts/response_text');
        }

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    protected function process_diary($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // Insert the diary record.
        $newitemid = $DB->insert_record('diary', $data);
        // Immediately after inserting "activity" record, call this.
        $this->apply_activity_instance($newitemid);
    }

    protected function process_diary_survey($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->owner = $this->get_courseid();

        // Insert the diary_survey record.
        $newitemid = $DB->insert_record('diary_survey', $data);
        $this->set_mapping('diary_survey', $oldid, $newitemid, true);

        // Update the diary record we just created with the new survey id.
        $DB->set_field('diary', 'sid', $newitemid, array('id' => $this->get_new_parentid('diary')));
    }

    protected function process_diary_question($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->survey_id = $this->get_new_parentid('diary_survey');

        if (isset($data->dependquestion)) {
            // Dependquestion.
            $data->dependquestion = $this->get_mappingid('diary_question', $data->dependquestion);

            // Dependchoice.
            // Only change mapping for RADIO and DROP question types, not for YESNO question.
            $dependquestion = $DB->get_record('diary_question', array('id' => $data->dependquestion), $fields = 'type_id');
            if (is_object($dependquestion)) {
                if ($dependquestion->type_id != 1) {
                    $data->dependchoice = $this->get_mappingid('diary_quest_choice', $data->dependchoice);
                }
            }
        }

        // Insert the diary_question record.
        $newitemid = $DB->insert_record('diary_question', $data);
        $this->set_mapping('diary_question', $oldid, $newitemid, true);
    }

    protected function process_diary_fb_sections($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->survey_id = $this->get_new_parentid('diary_survey');

        // If this diary has separate sections feedbacks.
        if (isset($data->scorecalculation)) {
            $scorecalculation = unserialize($data->scorecalculation);
            $newscorecalculation = array();
            foreach ($scorecalculation as $key => $qid) {
                $newqid = $this->get_mappingid('diary_question', $key);
                $newscorecalculation[$newqid] = null;
            }
            $data->scorecalculation = serialize($newscorecalculation);
        }

        // Insert the diary_fb_sections record.
        $newitemid = $DB->insert_record('diary_fb_sections', $data);
        $this->set_mapping('diary_fb_sections', $oldid, $newitemid, true);
    }

    protected function process_diary_feedback($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->section_id = $this->get_new_parentid('diary_fb_sections');

        // Insert the diary_feedback record.
        $newitemid = $DB->insert_record('diary_feedback', $data);
        $this->set_mapping('diary_feedback', $oldid, $newitemid, true);
    }

    protected function process_diary_quest_choice($data) {
        global $CFG, $DB;

        $data = (object)$data;

        // Replace the = separator with :: separator in quest_choice content.
        // This fixes radio button options using old "value"="display" formats.
        require_once($CFG->dirroot.'/mod/diary/locallib.php');

        if (($data->value == null || $data->value == 'NULL') && !preg_match("/^([0-9]{1,3}=.*|!other=.*)$/", $data->content)) {
            $content = diary_choice_values($data->content);
            if ($pos = strpos($content->text, '=')) {
                $data->content = str_replace('=', '::', $content->text);
            }
        }

        $oldid = $data->id;
        $data->question_id = $this->get_new_parentid('diary_question');

        if (isset($data->dependquestion)) {
            // Dependquestion.
            $data->dependquestion = $this->get_mappingid('diary_question', $data->dependquestion);

            // Dependchoice.
            // Only change mapping for RADIO and DROP question types, not for YESNO question.
            $dependquestion = $DB->get_record('diary_question',
                            array('id' => $data->dependquestion), $fields = 'type_id');
            if (is_object($dependquestion)) {
                if ($dependquestion->type_id != 1) {
                    $data->dependchoice = $this->get_mappingid('diary_quest_choice', $data->dependchoice);
                }
            }
        }

        // Insert the diary_quest_choice record.
        $newitemid = $DB->insert_record('diary_quest_choice', $data);
        $this->set_mapping('diary_quest_choice', $oldid, $newitemid);
    }

    protected function process_diary_attempt($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->qid = $this->get_new_parentid('diary');
        $data->userid = $this->get_mappingid('user', $data->userid);

        // Insert the diary_attempts record.
        $newitemid = $DB->insert_record('diary_attempts', $data);
        $this->set_mapping('diary_attempt', $oldid, $newitemid);
    }

    protected function process_diary_response($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->survey_id = $this->get_mappingid('diary_survey', $data->survey_id);
        $data->username = $this->get_mappingid('user', $data->username);

        // Insert the diary_response record.
        $newitemid = $DB->insert_record('diary_response', $data);
        $this->set_mapping('diary_response', $oldid, $newitemid);

        // Update the diary_attempts record we just created with the new response id.
        $DB->set_field('diary_attempts', 'rid', $newitemid,
                        array('id' => $this->get_new_parentid('diary_attempt')));
    }

    protected function process_diary_response_bool($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('diary_response');
        $data->question_id = $this->get_mappingid('diary_question', $data->question_id);

        // Insert the diary_response_bool record.
        $newitemid = $DB->insert_record('diary_response_bool', $data);
    }

    protected function process_diary_response_date($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('diary_response');
        $data->question_id = $this->get_mappingid('diary_question', $data->question_id);

        // Insert the diary_response_date record.
        $newitemid = $DB->insert_record('diary_response_date', $data);
    }

    protected function process_diary_response_multiple($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('diary_response');
        $data->question_id = $this->get_mappingid('diary_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('diary_quest_choice', $data->choice_id);

        // Insert the diary_resp_multiple record.
        $newitemid = $DB->insert_record('diary_resp_multiple', $data);
    }

    protected function process_diary_response_other($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('diary_response');
        $data->question_id = $this->get_mappingid('diary_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('diary_quest_choice', $data->choice_id);

        // Insert the diary_response_other record.
        $newitemid = $DB->insert_record('diary_response_other', $data);
    }

    protected function process_diary_response_rank($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('diary_response');
        $data->question_id = $this->get_mappingid('diary_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('diary_quest_choice', $data->choice_id);

        // Insert the diary_response_rank record.
        $newitemid = $DB->insert_record('diary_response_rank', $data);
    }

    protected function process_diary_response_single($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('diary_response');
        $data->question_id = $this->get_mappingid('diary_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('diary_quest_choice', $data->choice_id);

        // Insert the diary_resp_single record.
        $newitemid = $DB->insert_record('diary_resp_single', $data);
    }

    protected function process_diary_response_text($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('diary_response');
        $data->question_id = $this->get_mappingid('diary_question', $data->question_id);

        // Insert the diary_response_text record.
        $newitemid = $DB->insert_record('diary_response_text', $data);
    }

    protected function after_execute() {
        // Add diary related files, no need to match by itemname (just internally handled context).
        $this->add_related_files('mod_diary', 'intro', null);
        $this->add_related_files('mod_diary', 'info', 'diary_survey');
        $this->add_related_files('mod_diary', 'thankbody', 'diary_survey');
        $this->add_related_files('mod_diary', 'feedbacknotes', 'diary_survey');
        $this->add_related_files('mod_diary', 'question', 'diary_question');
        $this->add_related_files('mod_diary', 'sectionheading', 'diary_fb_sections');
        $this->add_related_files('mod_diary', 'feedback', 'diary_feedback');
    }
}
