<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script lists all the instances of diary in a particular course
 *
 * @package    mod
 * @subpackage diary
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once("../../config.php");
require_once($CFG->dirroot.'/mod/diary/locallib.php');

$id = required_param('id', PARAM_INT);
$PAGE->set_url('/mod/diary/index.php', array('id' => $id));
if (! $course = $DB->get_record('course', array('id' => $id))) {
    print_error('incorrectcourseid', 'diary');
}
$coursecontext = context_course::instance($id);
require_login($course->id);
$PAGE->set_pagelayout('incourse');

$event = \mod_diary\event\course_module_instance_list_viewed::create(array(
                'context' => context_course::instance($course->id)));
$event->trigger();

// Print the header.
$strdiarys = get_string("modulenameplural", "diary");
$PAGE->navbar->add($strdiarys);
$PAGE->set_title("$course->shortname: $strdiarys");
$PAGE->set_heading(format_string($course->fullname));
echo $OUTPUT->header();

// Get all the appropriate data.
if (!$diarys = get_all_instances_in_course("diary", $course)) {
    notice(get_string('thereareno', 'moodle', $strdiarys), "../../course/view.php?id=$course->id");
    die;
}

// Check if we need the closing date header.
$showclosingheader = false;
foreach ($diarys as $diary) {
    if ($diary->closedate != 0) {
        $showclosingheader = true;
    }
    if ($showclosingheader) {
        break;
    }
}

// Configure table for displaying the list of instances.
$headings = array(get_string('name'));
$align = array('left');

if ($showclosingheader) {
    array_push($headings, get_string('diarycloses', 'diary'));
    array_push($align, 'left');
}

array_unshift($headings, get_string('sectionname', 'format_'.$course->format));
array_unshift($align, 'left');

$showing = '';

// Current user role == admin or teacher.
if (has_capability('mod/diary:viewsingleresponse', $coursecontext)) {
    array_push($headings, get_string('responses', 'diary'));
    array_push($align, 'center');
    $showing = 'stats';
    array_push($headings, get_string('realm', 'diary'));
    array_push($align, 'left');
    // Current user role == student.
} else if (has_capability('mod/diary:submit', $coursecontext)) {
    array_push($headings, get_string('status'));
    array_push($align, 'left');
    $showing = 'responses';
}

$table = new html_table();
$table->head = $headings;
$table->align = $align;

// Populate the table with the list of instances.
$currentsection = '';
foreach ($diarys as $diary) {
    $cmid = $diary->coursemodule;
    $data = array();
    $realm = $DB->get_field('diary_survey', 'realm', array('id' => $diary->sid));

    $realm = $DB->get_field('diary_survey', 'realm', array('id' => $diary->sid));
    // Template surveys should NOT be displayed as an activity to students.
    if (!($realm == 'template' && !has_capability('mod/diary:manage', context_module::instance($cmid)))) {
        // Section number if necessary.
        $strsection = '';
        if ($diary->section != $currentsection) {
            $strsection = get_section_name($course, $diary->section);
            $currentsection = $diary->section;
        }
        $data[] = $strsection;
        // Show normal if the mod is visible.
        $class = '';
        if (!$diary->visible) {
            $class = ' class="dimmed"';
        }
        $data[] = "<a$class href=\"view.php?id=$cmid\">$diary->name</a>";

        // Close date.
        if ($diary->closedate) {
            $data[] = userdate($diary->closedate);
        } else if ($showclosingheader) {
            $data[] = '';
        }

        if ($showing == 'responses') {
            $status = '';
            if ($responses = diary_get_user_responses($diary->sid, $USER->id, $complete = false)) {
                foreach ($responses as $response) {
                    if ($response->complete == 'y') {
                        $status .= get_string('submitted', 'diary').' '.userdate($response->submitted).'<br />';
                    } else {
                        $status .= get_string('attemptstillinprogress', 'diary').' '.
                            userdate($response->submitted).'<br />';
                    }
                }
            }
            $data[] = $status;
        } else if ($showing == 'stats') {
            $data[] = $DB->count_records('diary_response', array('survey_id' => $diary->sid, 'complete' => 'y'));
            if ($survey = $DB->get_record('diary_survey', array('id' => $diary->sid))) {
                // For a public diary, look for the original public diary that it is based on.
                if ($survey->realm == 'public') {
                    $strpreview = get_string('preview_diary', 'diary');
                    if ($survey->owner != $course->id) {
                        $publicoriginal = '';
                        $originalcourse = $DB->get_record('course', array('id' => $survey->owner));
                        $originalcoursecontext = context_course::instance($survey->owner);
                        $originaldiary = $DB->get_record('diary',
                                        array('sid' => $survey->id, 'course' => $survey->owner));
                        $cm = get_coursemodule_from_instance("diary", $originaldiary->id, $survey->owner);
                        $context = context_course::instance($survey->owner, MUST_EXIST);
                        $canvieworiginal = has_capability('mod/diary:preview', $context, $USER->id, true);
                        // If current user can view diarys in original course,
                        // provide a link to the original public diary.
                        if ($canvieworiginal) {
                            $publicoriginal = '<br />'.get_string('publicoriginal', 'diary').'&nbsp;'.
                                '<a href="'.$CFG->wwwroot.'/mod/diary/preview.php?id='.
                                $cm->id.'" title="'.$strpreview.']">'.$originaldiary->name.' ['.
                                $originalcourse->fullname.']</a>';
                        } else {
                            // If current user is not enrolled as teacher in original course,
                            // only display the original public diary's name and course name.
                            $publicoriginal = '<br />'.get_string('publicoriginal', 'diary').'&nbsp;'.
                                $originaldiary->name.' ['.$originalcourse->fullname.']';
                        }
                        $data[] = get_string($realm, 'diary').' '.$publicoriginal;
                    } else {
                        // Original public diary was created in current course.
                        // Find which courses it is used in.
                        $publiccopy = '';
                        $select = 'course != '.$course->id.' AND sid = '.$diary->sid;
                        if ($copies = $DB->get_records_select('diary', $select, null,
                                $sort = 'course ASC', $fields = 'id, course, name')) {
                            foreach ($copies as $copy) {
                                $copycourse = $DB->get_record('course', array('id' => $copy->course));
                                $select = 'course = '.$copycourse->id.' AND sid = '.$diary->sid;
                                $copydiary = $DB->get_record('diary',
                                    array('id' => $copy->id, 'sid' => $survey->id, 'course' => $copycourse->id));
                                $cm = get_coursemodule_from_instance("diary", $copydiary->id, $copycourse->id);
                                $context = context_course::instance($copycourse->id, MUST_EXIST);
                                $canviewcopy = has_capability('mod/diary:view', $context, $USER->id, true);
                                if ($canviewcopy) {
                                    $publiccopy .= '<br />'.get_string('publiccopy', 'diary').'&nbsp;:&nbsp;'.
                                        '<a href = "'.$CFG->wwwroot.'/mod/diary/preview.php?id='.
                                        $cm->id.'" title = "'.$strpreview.'">'.
                                        $copydiary->name.' ['.$copycourse->fullname.']</a>';
                                } else {
                                    // If current user does not have "view" capability in copy course,
                                    // only display the copied public diary's name and course name.
                                    $publiccopy .= '<br />'.get_string('publiccopy', 'diary').'&nbsp;:&nbsp;'.
                                        $copydiary->name.' ['.$copycourse->fullname.']';
                                }
                            }
                        }
                        $data[] = get_string($realm, 'diary').' '.$publiccopy;
                    }
                } else {
                    $data[] = get_string($realm, 'diary');
                }
            } else {
                // If a diary is a copy of a public diary which has been deleted.
                $data[] = get_string('removenotinuse', 'diary');
            }
        }
    }
    $table->data[] = $data;
} // End of loop over diary instances.

echo html_writer::table($table);

// Finish the page.
echo $OUTPUT->footer();