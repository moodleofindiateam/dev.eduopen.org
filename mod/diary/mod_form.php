<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * print the form to add or edit a diary-instance
 *
 * @author Mike Churchward
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package diary
 */

require_once($CFG->dirroot.'/course/moodleform_mod.php');
require_once($CFG->dirroot.'/mod/diary/diary.class.php');
require_once($CFG->dirroot.'/mod/diary/locallib.php');

class mod_diary_mod_form extends moodleform_mod {

    protected function definition() {
        global $COURSE;

        $diary = new diary($this->_instance, null, $COURSE, $this->_cm);

        $mform    =& $this->_form;

        $mform->addElement('header', 'general', get_string('general', 'form'));

        $mform->addElement('text', 'name', get_string('name', 'diary'), array('size' => '64'));
        $mform->setType('name', PARAM_TEXT);
        $mform->addRule('name', null, 'required', null, 'client');

        $this->add_intro_editor(false, get_string('description'));

        $mform->addElement('header', 'timinghdr', get_string('timing', 'form'));

        $enableopengroup = array();
        $enableopengroup[] =& $mform->createElement('checkbox', 'useopendate', get_string('opendate', 'diary'));
        $enableopengroup[] =& $mform->createElement('date_time_selector', 'opendate', '');
        $mform->addGroup($enableopengroup, 'enableopengroup', get_string('opendate', 'diary'), ' ', false);
        $mform->addHelpButton('enableopengroup', 'opendate', 'diary');
        $mform->disabledIf('enableopengroup', 'useopendate', 'notchecked');

        $enableclosegroup = array();
        $enableclosegroup[] =& $mform->createElement('checkbox', 'useclosedate', get_string('closedate', 'diary'));
        $enableclosegroup[] =& $mform->createElement('date_time_selector', 'closedate', '');
        $mform->addGroup($enableclosegroup, 'enableclosegroup', get_string('closedate', 'diary'), ' ', false);
        $mform->addHelpButton('enableclosegroup', 'closedate', 'diary');
        $mform->disabledIf('enableclosegroup', 'useclosedate', 'notchecked');

        global $diarytypes, $diaryrespondents, $diaryresponseviewers, $diaryrealms, $autonumbering;
        $mform->addElement('header', 'diaryhdr', get_string('responseoptions', 'diary'));

        $mform->addElement('select', 'qtype', get_string('qtype', 'diary'), $diarytypes);
        $mform->addHelpButton('qtype', 'qtype', 'diary');

        $mform->addElement('hidden', 'cannotchangerespondenttype');
        $mform->setType('cannotchangerespondenttype', PARAM_INT);
        $mform->addElement('select', 'respondenttype', get_string('respondenttype', 'diary'), $diaryrespondents);
        $mform->addHelpButton('respondenttype', 'respondenttype', 'diary');
        $mform->disabledIf('respondenttype', 'cannotchangerespondenttype', 'eq', 1);

        $mform->addElement('select', 'resp_view', get_string('responseview', 'diary'), $diaryresponseviewers);
        $mform->addHelpButton('resp_view', 'responseview', 'diary');

        $options = array('0' => get_string('no'), '1' => get_string('yes'));
        $mform->addElement('select', 'resume', get_string('resume', 'diary'), $options);
        $mform->addHelpButton('resume', 'resume', 'diary');

        $options = array('0' => get_string('no'), '1' => get_string('yes'));
        $mform->addElement('select', 'navigate', get_string('navigate', 'diary'), $options);
        $mform->addHelpButton('navigate', 'navigate', 'diary');

        $mform->addElement('select', 'autonum', get_string('autonumbering', 'diary'), $autonumbering);
        $mform->addHelpButton('autonum', 'autonumbering', 'diary');
        // Default = autonumber both questions and pages.
        $mform->setDefault('autonum', 3);

        // Removed potential scales from list of grades. CONTRIB-3167.
        $grades[0] = get_string('nograde');
        for ($i = 100; $i >= 1; $i--) {
            $grades[$i] = $i;
        }
        $mform->addElement('select', 'grade', get_string('grade', 'diary'), $grades);

        if (empty($diary->sid)) {
            if (!isset($diary->id)) {
                $diary->id = 0;
            }

            $mform->addElement('header', 'contenthdr', get_string('contentoptions', 'diary'));
            $mform->addHelpButton('contenthdr', 'createcontent', 'diary');

            $mform->addElement('radio', 'create', get_string('createnew', 'diary'), '', 'new-0');

            // Retrieve existing private diarys from current course.
            $surveys = diary_get_survey_select($diary->id, $COURSE->id, 0, 'private');
            if (!empty($surveys)) {
                $prelabel = get_string('useprivate', 'diary');
                foreach ($surveys as $value => $label) {
                    $mform->addElement('radio', 'create', $prelabel, $label, $value);
                    $prelabel = '';
                }
            }
            // Retrieve existing template diarys from this site.
            $surveys = diary_get_survey_select($diary->id, $COURSE->id, 0, 'template');
            if (!empty($surveys)) {
                $prelabel = get_string('usetemplate', 'diary');
                foreach ($surveys as $value => $label) {
                    $mform->addElement('radio', 'create', $prelabel, $label, $value);
                    $prelabel = '';
                }
            } else {
                $mform->addElement('static', 'usetemplate', get_string('usetemplate', 'diary'),
                                '('.get_string('notemplatesurveys', 'diary').')');
            }

            // Retrieve existing public diarys from this site.
            $surveys = diary_get_survey_select($diary->id, $COURSE->id, 0, 'public');
            if (!empty($surveys)) {
                $prelabel = get_string('usepublic', 'diary');
                foreach ($surveys as $value => $label) {
                    $mform->addElement('radio', 'create', $prelabel, $label, $value);
                    $prelabel = '';
                }
            } else {
                $mform->addElement('static', 'usepublic', get_string('usepublic', 'diary'),
                                   '('.get_string('nopublicsurveys', 'diary').')');
            }

            $mform->setDefault('create', 'new-0');
        }

        $this->standard_coursemodule_elements();

        // Buttons.
        $this->add_action_buttons();
    }

    public function data_preprocessing(&$defaultvalues) {
        global $DB;
        if (empty($defaultvalues['opendate'])) {
            $defaultvalues['useopendate'] = 0;
        } else {
            $defaultvalues['useopendate'] = 1;
        }
        if (empty($defaultvalues['closedate'])) {
            $defaultvalues['useclosedate'] = 0;
        } else {
            $defaultvalues['useclosedate'] = 1;
        }
        // Prevent diary set to "anonymous" to be reverted to "full name".
        $defaultvalues['cannotchangerespondenttype'] = 0;
        if (!empty($defaultvalues['respondenttype']) && $defaultvalues['respondenttype'] == "anonymous") {
            // If this diary has responses.
            $numresp = $DB->count_records('diary_response',
                            array('survey_id' => $defaultvalues['sid'], 'complete' => 'y'));
            if ($numresp) {
                $defaultvalues['cannotchangerespondenttype'] = 1;
            }
        }
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }

    public function add_completion_rules() {
        $mform =& $this->_form;
        $mform->addElement('checkbox', 'completionsubmit', '', get_string('completionsubmit', 'diary'));
        return array('completionsubmit');
    }

    public function completion_rule_enabled($data) {
        return !empty($data['completionsubmit']);
    }

}