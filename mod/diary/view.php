<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once("../../config.php");
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot.'/mod/diary/diary.class.php');

if (!isset($SESSION->diary)) {
    $SESSION->diary = new stdClass();
}
$SESSION->diary->current_tab = 'view';

$id = optional_param('id', null, PARAM_INT);    // Course Module ID.
$a = optional_param('a', null, PARAM_INT);      // Or diary ID.

$sid = optional_param('sid', null, PARAM_INT);  // Survey id.

if ($id) {
    if (! $cm = get_coursemodule_from_id('diary', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }

    if (! $diary = $DB->get_record("diary", array("id" => $cm->instance))) {
        print_error('invalidcoursemodule');
    }

} else {
    if (! $diary = $DB->get_record("diary", array("id" => $a))) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id" => $diary->course))) {
        print_error('coursemisconf');
    }
    if (! $cm = get_coursemodule_from_instance("diary", $diary->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
}

// Check login and get context.
require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);

$url = new moodle_url($CFG->wwwroot.'/mod/diary/view.php');
if (isset($id)) {
    $url->param('id', $id);
} else {
    $url->param('a', $a);
}
if (isset($sid)) {
    $url->param('sid', $sid);
}

$PAGE->set_url($url);
$PAGE->set_context($context);
$diary = new diary(0, $diary, $course, $cm);

$PAGE->set_title(format_string($diary->name));

$PAGE->set_heading(format_string($course->fullname));

echo $OUTPUT->header();

echo $OUTPUT->heading(format_text($diary->name));

// Print the main part of the page.
if ($diary->intro) {
    echo $OUTPUT->box(format_module_intro('diary', $diary, $cm->id), 'generalbox', 'intro');
}

echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');

$cm = $diary->cm;
$currentgroupid = groups_get_activity_group($cm);
if (!groups_is_member($currentgroupid, $USER->id)) {
    $currentgroupid = 0;
}

if (!$diary->is_active()) {
    if ($diary->capabilities->manage) {
        $msg = 'removenotinuse';
    } else {
        $msg = 'notavail';
    }
    echo '<div class="message">'
    .get_string($msg, 'diary')
    .'</div>';

} else if (!$diary->is_open()) {
    echo '<div class="message">'
    .get_string('notopen', 'diary', userdate($diary->opendate))
    .'</div>';
} else if ($diary->is_closed()) {
    echo '<div class="message">'
    .get_string('closed', 'diary', userdate($diary->closedate))
    .'</div>';
} else if ($diary->survey->realm == 'template') {
    print_string('templatenotviewable', 'diary');
    echo $OUTPUT->box_end();
    echo $OUTPUT->footer($diary->course);
    exit();
} else if (!$diary->user_is_eligible($USER->id)) {
    if ($diary->questions) {
        echo '<div class="message">'.get_string('noteligible', 'diary').'</div>';
    }
} else if (!$diary->user_can_take($USER->id)) {
    switch ($diary->qtype) {
        case DIARYDAILY:
            $msgstring = ' '.get_string('today', 'diary');
            break;
        case DIARYWEEKLY:
            $msgstring = ' '.get_string('thisweek', 'diary');
            break;
        case DIARYMONTHLY:
            $msgstring = ' '.get_string('thismonth', 'diary');
            break;
        default:
            $msgstring = '';
            break;
    }
    echo ('<div class="message">'.get_string("alreadyfilled", "diary", $msgstring).'</div>');
} else if ($diary->user_can_take($USER->id)) {
    $select = 'survey_id = '.$diary->survey->id.' AND username = \''.$USER->id.'\' AND complete = \'n\'';
    $resume = $DB->get_record_select('diary_response', $select, null) !== false;
    if (!$resume) {
        $complete = get_string('answerquestions', 'diary');
    } else {
        $complete = get_string('resumesurvey', 'diary');
    }
    if ($diary->questions) { // Sanity check.
        echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/diary/complete.php?'.
        'id='.$diary->cm->id.'&resume='.$resume).'">'.$complete.'</a>';
    }
}
if ($diary->is_active() && !$diary->questions) {
    echo '<p>'.get_string('noneinuse', 'diary').'</p>';
}
if ($diary->is_active() && $diary->capabilities->editquestions && !$diary->questions) { // Sanity check.
    echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/diary/questions.php?'.
                'id='.$diary->cm->id).'">'.'<strong>'.get_string('addquestions', 'diary').'</strong></a>';
}
echo $OUTPUT->box_end();
if (isguestuser()) {
    $output = '';
    $guestno = html_writer::tag('p', get_string('noteligible', 'diary'));
    $liketologin = html_writer::tag('p', get_string('liketologin'));
    $output .= $OUTPUT->confirm($guestno."\n\n".$liketologin."\n", get_login_url(),
            get_referer(false));
    echo $output;
}

// Log this course module view.
// Needed for the event logging.
$context = context_module::instance($diary->cm->id);
$anonymous = $diary->respondenttype == 'anonymous';

$event = \mod_diary\event\course_module_viewed::create(array(
                'objectid' => $diary->id,
                'anonymous' => $anonymous,
                'context' => $context
));
$event->trigger();

    $usernumresp = $diary->count_submissions($USER->id);

if ($diary->capabilities->readownresponses && ($usernumresp > 0)) {
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    $argstr = 'instance='.$diary->id.'&user='.$USER->id;
    if ($usernumresp > 1) {
        $titletext = get_string('viewyourresponses', 'diary', $usernumresp);
    } else {
        $titletext = get_string('yourresponse', 'diary');
        $argstr .= '&byresponse=1&action=vresp';
    }

    echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/diary/myreport.php?'.
        $argstr).'">'.$titletext.'</a>';
    echo $OUTPUT->box_end();
}

if ($survey = $DB->get_record('diary_survey', array('id' => $diary->sid))) {
    $owner = (trim($survey->owner) == trim($course->id));
} else {
    $survey = false;
    $owner = true;
}
$numresp = $diary->count_submissions();

// Number of Responses in currently selected group (or all participants etc.).
if (isset($SESSION->diary->numselectedresps)) {
    $numselectedresps = $SESSION->diary->numselectedresps;
} else {
    $numselectedresps = $numresp;
}

// If diary is set to separate groups, prevent user who is not member of any group
// to view All responses.
$canviewgroups = true;
$groupmode = groups_get_activity_groupmode($cm, $course);
if ($groupmode == 1) {
    $canviewgroups = groups_has_membership($cm, $USER->id);;
}

$canviewallgroups = has_capability('moodle/site:accessallgroups', $context);
if (( (
            // Teacher or non-editing teacher (if can view all groups).
            $canviewallgroups ||
            // Non-editing teacher (with canviewallgroups capability removed), if member of a group.
            ($canviewgroups && $diary->capabilities->readallresponseanytime))
            && $numresp > 0 && $owner && $numselectedresps > 0) ||
            $diary->capabilities->readallresponses && ($numresp > 0) && $canviewgroups &&
            ($diary->resp_view == DIARY_STUDENTVIEWRESPONSES_ALWAYS ||
                    ($diary->resp_view == DIARY_STUDENTVIEWRESPONSES_WHENCLOSED
                            && $diary->is_closed()) ||
                    ($diary->resp_view == DIARY_STUDENTVIEWRESPONSES_WHENANSWERED
                            && $usernumresp > 0)) &&
            $diary->is_survey_owner()) {
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    $argstr = 'instance='.$diary->id.'&group='.$currentgroupid;
    echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.
            $argstr).'">'.get_string('viewallresponses', 'diary').'</a>';
    echo $OUTPUT->box_end();
}

echo $OUTPUT->footer();
