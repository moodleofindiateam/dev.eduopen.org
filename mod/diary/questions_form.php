<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @authors Mike Churchward & Joseph Rézeau
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package diary
 */
require_once($CFG->dirroot . '/course/moodleform_mod.php');
require_once($CFG->dirroot . '/mod/diary/questiontypes/diarycls.class.php');

class diary_questions_form extends moodleform {

    public function __construct($action, $moveq = false) {
        $this->moveq = $moveq;
        return parent::__construct($action);
    }

    public function definition() {
        global $CFG, $diary, $SESSION, $OUTPUT;
        global $DB;

        $sid = $diary->survey->id;
        $mform = & $this->_form;

        $mform->addElement('header', 'questionhdr', get_string('addquestions', 'diary'));
        $mform->addHelpButton('questionhdr', 'questiontypes', 'diary');

        $stredit = get_string('edit', 'diary');
        $strremove = get_string('remove', 'diary');
        $strmove = get_string('move');
        $strmovehere = get_string('movehere');
        $stryes = get_string('yes');
        $strno = get_string('no');
        $strposition = get_string('position', 'diary');

        if (!isset($diary->questions)) {
            $diary->questions = array();
        }
        if ($this->moveq) {
            $moveqposition = $diary->questions[$this->moveq]->position;
        }

        $pos = 0;
        $select = '';
        if (!($qtypes = $DB->get_records_select_menu('diary_question_type', $select, null, '', 'typeid,type'))) {
            $qtypes = array();
        }
        //$qtypes[3] = $qtypes[3];
        // Get the names of each question type in the appropriate language.
        foreach ($qtypes as $key => $qtype) {
            // Do not allow "Page Break" to be selected as first element of a Questionnaire.
            if (empty($diary->questions) && ($qtype == 'Page Break')) {
                unset($qtypes[$key]);
            } else {
                $qtypes[$key] = diary_get_type($key);
            }
        }
        natsort($qtypes);
        $addqgroup = array();
        $addqgroup[] = & $mform->createElement('select', 'type_id', '', $qtypes);

        // The 'sticky' type_id value for further new questions.
        if (isset($SESSION->diary->type_id)) {
            $mform->setDefault('type_id', $SESSION->diary->type_id);
        }

        $addqgroup[] = & $mform->createElement('submit', 'addqbutton', get_string('addselqtype', 'diary'));

        $diaryhasdependencies = diary_has_dependencies($diary->questions);

        $mform->addGroup($addqgroup, 'addqgroup', '', ' ', false);

        if (isset($SESSION->diary->validateresults) && $SESSION->diary->validateresults != '') {
            $mform->addElement('static', 'validateresult', '', '<div class="qdepend warning">' .
                    $SESSION->diary->validateresults . '</div>');
            $SESSION->diary->validateresults = '';
        }

        $qnum = 0;

        // JR skip logic :: to prevent moving child higher than parent OR parent lower than child
        // we must get now the parent and child positions.

        if ($diaryhasdependencies) {
            $parentpositions = diary_get_parent_positions($diary->questions);
            $childpositions = diary_get_child_positions($diary->questions);
        }

        $mform->addElement('header', 'manageq', get_string('managequestions', 'diary'));
        $mform->addHelpButton('manageq', 'managequestions', 'diary');

        $mform->addElement('html', '<div class="qcontainer">');

        foreach ($diary->questions as $question) {

            $manageqgroup = array();

            $qid = $question->id;
            $tid = $question->type_id;
            $qtype = $question->type;
            $required = $question->required;

            // Does this diary contain branching questions already?
            $dependency = '';
            if ($diaryhasdependencies) {
                if ($question->dependquestion != 0) {
                    $parent = diary_get_parent($question);
                    $dependency = '<strong>' . get_string('dependquestion', 'diary') . '</strong> : ' .
                            $strposition . ' ' . $parent[$qid]['parentposition'] . ' (' . $parent[$qid]['parent'] . ')';
                }
            }

            $pos = $question->position;

            // No page break in first position!
            if ($tid == DIARYQUESPAGEBREAK && $pos == 1) {
                $DB->set_field('diary_question', 'deleted', 'y', array('id' => $qid, 'survey_id' => $sid));
                if ($records = $DB->get_records_select('diary_question', $select, null, 'position ASC')) {
                    foreach ($records as $record) {
                        $DB->set_field('diary_question', 'position', $record->position - 1, array('id' => $record->id));
                    }
                }
                redirect($CFG->wwwroot . '/mod/diary/questions.php?id=' . $diary->cm->id);
            }

            if ($tid != DIARYQUESPAGEBREAK && $tid != DIARYQUESSECTIONTEXT) {
                $qnum++;
            }

            // Needed for non-English languages JR.
            $qtype = '[' . diary_get_type($tid) . ']';
            $content = '';
            // If question text is "empty", i.e. 2 non-breaking spaces were inserted, do not display any question text.
            if ($question->content == '<p>  </p>') {
                $question->content = '';
            }
            if ($tid != DIARYQUESPAGEBREAK) {
                // Needed to print potential media in question text.
                $content = format_text(file_rewrite_pluginfile_urls($question->content, 'pluginfile.php', $question->context->id, 'mod_diary', 'question', $question->id), FORMAT_HTML);
            }
            $moveqgroup = array();

            $spacer = $OUTPUT->pix_url('spacer');

            if (!$this->moveq) {
                $mform->addElement('html', '<div class="qn-container">'); // Begin div qn-container.
                $mextra = array('value' => $question->id,
                    'alt' => $strmove,
                    'title' => $strmove);
                $eextra = array('value' => $question->id,
                    'alt' => get_string('edit', 'diary'),
                    'title' => get_string('edit', 'diary'));
                $rextra = array('value' => $question->id,
                    'alt' => $strremove,
                    'title' => $strremove);

                if ($tid == DIARYQUESPAGEBREAK) {
                    $esrc = $CFG->wwwroot . '/mod/diary/images/editd.gif';
                    $eextra = array('disabled' => 'disabled');
                } else {
                    $esrc = $CFG->wwwroot . '/mod/diary/images/edit.gif';
                }

                if ($tid == DIARYQUESPAGEBREAK) {
                    $esrc = $spacer;
                    $eextra = array('disabled' => 'disabled');
                } else {
                    $esrc = $OUTPUT->pix_url('t/edit');
                }
                $rsrc = $OUTPUT->pix_url('t/delete');
                $qreq = '';

                // Question numbers.
                $manageqgroup[] = & $mform->createElement('static', 'qnums', '', '<div class="qnums">' . $strposition . ' ' . $pos . '</div>');

                // Need to index by 'id' since IE doesn't return assigned 'values' for image inputs.
                $manageqgroup[] = & $mform->createElement('static', 'opentag_' . $question->id, '', '');
                $msrc = $OUTPUT->pix_url('t/move');

                if ($diaryhasdependencies) {
                    // Do not allow moving parent question at position #1 to be moved down if it has a child at position < 4.
                    if ($pos == 1) {
                        if (isset($childpositions[$qid])) {
                            $maxdown = $childpositions[$qid];
                            if ($maxdown < 4) {
                                $strdisabled = get_string('movedisabled', 'diary');
                                $msrc = $OUTPUT->pix_url('t/block');
                                $mextra = array('value' => $question->id,
                                    'alt' => $strdisabled,
                                    'title' => $strdisabled);
                                $mextra += array('disabled' => 'disabled');
                            }
                        }
                    }
                    // Do not allow moving or deleting a page break if immediately followed by a child question
                    // or immediately preceded by a question with a dependency and followed by a non-dependent question.
                    if ($tid == DIARYQUESPAGEBREAK) {
                        if ($nextquestion = $DB->get_record('diary_question', array('survey_id' => $sid,
                            'position' => $pos + 1, 'deleted' => 'n'), $fields = 'dependquestion, name, content')) {
                            if ($previousquestion = $DB->get_record('diary_question', array('survey_id' => $sid,
                                'position' => $pos - 1, 'deleted' => 'n'), $fields = 'dependquestion, name, content')) {
                                if ($nextquestion->dependquestion != 0 || ($previousquestion->dependquestion != 0 && $nextquestion->dependquestion == 0)) {
                                    $strdisabled = get_string('movedisabled', 'diary');
                                    $msrc = $OUTPUT->pix_url('t/block');
                                    $mextra = array('value' => $question->id,
                                        'alt' => $strdisabled,
                                        'title' => $strdisabled);
                                    $mextra += array('disabled' => 'disabled');

                                    $rsrc = $msrc;
                                    $strdisabled = get_string('deletedisabled', 'diary');
                                    $rextra = array('value' => $question->id,
                                        'alt' => $strdisabled,
                                        'title' => $strdisabled);
                                    $rextra += array('disabled' => 'disabled');
                                }
                            }
                        }
                    }
                }
                $manageqgroup[] = & $mform->createElement('image', 'movebutton[' . $question->id . ']', $msrc, $mextra);
                $manageqgroup[] = & $mform->createElement('image', 'editbutton[' . $question->id . ']', $esrc, $eextra);
                $manageqgroup[] = & $mform->createElement('image', 'removebutton[' . $question->id . ']', $rsrc, $rextra);

                if ($tid != DIARYQUESPAGEBREAK && $tid != DIARYQUESSECTIONTEXT) {
                    if ($required == 'y') {
                        $reqsrc = $OUTPUT->pix_url('t/stop');
                        $strrequired = get_string('required', 'diary');
                    } else {
                        $reqsrc = $OUTPUT->pix_url('t/go');
                        $strrequired = get_string('notrequired', 'diary');
                    }
                    $strrequired .= ' ' . get_string('clicktoswitch', 'diary');
                    $reqextra = array('value' => $question->id,
                        'alt' => $strrequired,
                        'title' => $strrequired);
                    $manageqgroup[] = & $mform->createElement('image', 'requiredbutton[' . $question->id . ']', $reqsrc, $reqextra);
                }
                $manageqgroup[] = & $mform->createElement('static', 'closetag_' . $question->id, '', '');
            } else {
                $manageqgroup[] = & $mform->createElement('static', 'qnum', '', '<div class="qnums">' . $strposition . ' ' . $pos . '</div>');
                $moveqgroup[] = & $mform->createElement('static', 'qnum', '', '');

                $display = true;
                if ($diaryhasdependencies) {
                    // Prevent moving child to higher position than its parent.
                    if (isset($parentpositions[$this->moveq])) {
                        $maxup = $parentpositions[$this->moveq];
                        if ($pos <= $maxup) {
                            $display = false;
                        }
                    }
                    // Prevent moving parent to lower position than its (first) child.
                    if (isset($childpositions[$this->moveq])) {
                        $maxdown = $childpositions[$this->moveq];
                        if ($pos >= $maxdown) {
                            $display = false;
                        }
                    }
                }

                $typeid = $DB->get_field('diary_question', 'type_id', array('id' => $this->moveq));

                if ($display) {
                    // Do not move a page break to first position.
                    if ($typeid == DIARYQUESPAGEBREAK && $pos == 1) {
                        $manageqgroup[] = & $mform->createElement('static', 'qnums', '', '');
                    } else {
                        if ($this->moveq == $question->id) {
                            $moveqgroup[] = & $mform->createElement('cancel', 'cancelbutton', get_string('cancel'));
                        } else {
                            $mextra = array('value' => $question->id,
                                'alt' => $strmove,
                                'title' => $strmovehere . ' (position ' . $pos . ')');
                            $msrc = $OUTPUT->pix_url('movehere');
                            $moveqgroup[] = & $mform->createElement('static', 'opentag_' . $question->id, '', '');
                            $moveqgroup[] = & $mform->createElement('image', 'moveherebutton[' . $pos . ']', $msrc, $mextra);
                            $moveqgroup[] = & $mform->createElement('static', 'closetag_' . $question->id, '', '');
                        }
                    }
                } else {
                    $manageqgroup[] = & $mform->createElement('static', 'qnums', '', '');
                    $moveqgroup[] = & $mform->createElement('static', 'qnums', '', '');
                }
            }
            if ($question->name) {
                $qname = '(' . $question->name . ')';
            } else {
                $qname = '';
            }
            $manageqgroup[] = & $mform->createElement('static', 'qtype_' . $question->id, '', $qtype);
            $manageqgroup[] = & $mform->createElement('static', 'qname_' . $question->id, '', $qname);

            if ($dependency) {
                $mform->addElement('static', 'qdepend_' . $question->id, '', '<div class="qdepend">' . $dependency . '</div>');
            }
            if ($tid != DIARYQUESPAGEBREAK) {
                if ($tid != DIARYQUESSECTIONTEXT) {
                    $qnumber = '<div class="qn-info"><h2 class="qn-number">' . $qnum . '</h2></div>';
                } else {
                    $qnumber = '';
                }
            }

            if ($this->moveq && $pos < $moveqposition) {
                $mform->addGroup($moveqgroup, 'moveqgroup', '', '', false);
            }
            if ($this->moveq) {
                if ($this->moveq == $question->id && $display) {
                    $mform->addElement('html', '<div class="moving" title="' . $strmove . '">'); // Begin div qn-container.
                } else {
                    $mform->addElement('html', '<div class="qn-container">'); // Begin div qn-container.
                }
            }
            $mform->addGroup($manageqgroup, 'manageqgroup', '', '&nbsp;', false);
            if ($tid != DIARYQUESPAGEBREAK) {
                $mform->addElement('static', 'qcontent_' . $question->id, '', $qnumber . '<div class="qn-question">' . $content . '</div>');
            }
            $mform->addElement('html', '</div>'); // End div qn-container.

            if ($this->moveq && $pos >= $moveqposition) {
                $mform->addGroup($moveqgroup, 'moveqgroup', '', '', false);
            }
        }

        if ($this->moveq) {
            $mform->addElement('hidden', 'moveq', $this->moveq);
        }

        // Hidden fields.
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'sid', 0);
        $mform->setType('sid', PARAM_INT);
        $mform->addElement('hidden', 'action', 'main');
        $mform->setType('action', PARAM_RAW);
        $mform->setType('moveq', PARAM_RAW);

        $mform->addElement('html', '</div>');
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }

}

class diary_edit_question_form extends moodleform {

    public function definition() {
        global $CFG, $COURSE, $diary, $question, $diaryrealms, $SESSION;
        global $DB;

        // The 'sticky' required response value for further new questions.
        if (isset($SESSION->diary->required) && !isset($question->qid)) {
            $question->required = $SESSION->diary->required;
        }
        if (!isset($question->type_id)) {
            print_error('undefinedquestiontype', 'diary');
        }

        // Initialize question type defaults.
        switch ($question->type_id) {
            case DIARYQUESTEXT:
                $deflength = 20;
                $defprecise = 25;
                $lhelpname = 'fieldlength';
                $phelpname = 'maxtextlength';
                break;
            case DIARYQUESESSAY:
                $deflength = '';
                $defprecise = '';
                $lhelpname = 'textareacolumns';
                $phelpname = 'textarearows';
                break;
            case DIARYQUESCHECK:
                $deflength = 0;
                $defprecise = 0;
                $lhelpname = 'minforcedresponses';
                $phelpname = 'maxforcedresponses';
                $olabelname = 'possibleanswers';
                $ohelpname = 'checkboxes';
                break;
            case DIARYQUESRADIO:
                $deflength = 0;
                $defprecise = 0;
                $lhelpname = 'alignment';
                $olabelname = 'possibleanswers';
                $ohelpname = 'radiobuttons';
                break;
            case DIARYQUESRATE:
                $deflength = 5;
                $defprecise = 0;
                $lhelpname = 'numberscaleitems';
                $phelpname = 'kindofratescale';
                $olabelname = 'possibleanswers';
                $ohelpname = 'ratescale';
                break;
            case DIARYQUESNUMERIC:
                $deflength = 10;
                $defprecise = 0;
                $lhelpname = 'maxdigitsallowed';
                $phelpname = 'numberofdecimaldigits';
                break;
            case DIARYQUESDROP:
                $deflength = 0;
                $defprecise = 0;
                $olabelname = 'possibleanswers';
                $ohelpname = 'dropdown';
                break;
            default:
                $deflength = 0;
                $defprecise = 0;
        }

        $defdependquestion = 0;
        $defdependchoice = 0;
        $dlabelname = 'dependquestion';

        $mform = & $this->_form;

        // Display different messages for new question creation and existing question modification.
        if (isset($question->qid)) {
            $streditquestion = get_string('editquestion', 'diary', diary_get_type($question->type_id));
        } else {
            $streditquestion = get_string('addnewquestion', 'diary', diary_get_type($question->type_id));
        }
        switch ($question->type_id) {
            case DIARYQUESYESNO:
                $qtype = 'yesno';
                break;
            case DIARYQUESTEXT:
                $qtype = 'textbox';
                break;
            case DIARYQUESESSAY:
                $qtype = 'essaybox';
                break;
            case DIARYQUESRADIO:
                $qtype = 'radiobuttons';
                break;
            case DIARYQUESCHECK:
                $qtype = 'checkboxes';
                break;
            case DIARYQUESDROP:
                $qtype = 'dropdown';
                break;
            case DIARYQUESRATE:
                $qtype = 'ratescale';
                break;
            case DIARYQUESDATE:
                $qtype = 'date';
                break;
            case DIARYQUESNUMERIC:
                $qtype = 'numeric';
                break;
            case DIARYQUESSECTIONTEXT:
                $qtype = 'sectiontext';
                break;
            case DIARYQUESPAGEBREAK:
                $qtype = 'sectionbreak';
        }

        $mform->addElement('header', 'questionhdredit', $streditquestion);
        $mform->addHelpButton('questionhdredit', $qtype, 'diary');

        // Name and required fields.
        if ($question->type_id != DIARYQUESSECTIONTEXT && $question->type_id != '') {
            $stryes = get_string('yes');
            $strno = get_string('no');

            $mform->addElement('text', 'name', get_string('optionalname', 'diary'), array('size' => '30', 'maxlength' => '30'));
            $mform->setType('name', PARAM_TEXT);
            $mform->addHelpButton('name', 'optionalname', 'diary');

            $reqgroup = array();
            $reqgroup[] = & $mform->createElement('radio', 'required', '', $stryes, 'y');
            $reqgroup[] = & $mform->createElement('radio', 'required', '', $strno, 'n');
            $mform->addGroup($reqgroup, 'reqgroup', get_string('required', 'diary'), ' ', false);
            $mform->addHelpButton('reqgroup', 'required', 'diary');
        }

        // Length field.
        if ($question->type_id == DIARYQUESYESNO || $question->type_id == DIARYQUESDROP || $question->type_id == DIARYQUESDATE ||
                $question->type_id == DIARYQUESSECTIONTEXT) {
            $mform->addElement('hidden', 'length', $deflength);
        } else if ($question->type_id == DIARYQUESRADIO) {
            $lengroup = array();
            $lengroup[] = & $mform->createElement('radio', 'length', '', get_string('vertical', 'diary'), '0');
            $lengroup[] = & $mform->createElement('radio', 'length', '', get_string('horizontal', 'diary'), '1');
            $mform->addGroup($lengroup, 'lengroup', get_string($lhelpname, 'diary'), ' ', false);
            $mform->addHelpButton('lengroup', $lhelpname, 'diary');
        } else if ($question->type_id == DIARYQUESTEXT || $question->type_id == DIARYQUESRATE) {
            $question->length = isset($question->length) ? $question->length : $deflength;
            $mform->addElement('text', 'length', get_string($lhelpname, 'diary'), array('size' => '1'));
            $mform->setType('length', PARAM_TEXT);
            $mform->addHelpButton('length', $lhelpname, 'diary');
        } else if ($question->type_id == DIARYQUESESSAY) {
            $responseformats = array(
                "0" => get_string('formateditor', 'diary'),
                "1" => get_string('formatplain', 'diary'));
            $mform->addElement('select', 'precise', get_string('responseformat', 'diary'), $responseformats);
        } else if ($question->type_id == DIARYQUESCHECK || $question->type_id == DIARYQUESNUMERIC) {
            $question->length = isset($question->length) ? $question->length : $deflength;
            $mform->addElement('text', 'length', get_string($lhelpname, 'diary'), array('size' => '1'));
        }

        $mform->setType('length', PARAM_INT);

        // Precision field.
        if ($question->type_id == DIARYQUESYESNO || $question->type_id == DIARYQUESDROP || $question->type_id == DIARYQUESDATE ||
                $question->type_id == DIARYQUESSECTIONTEXT || $question->type_id == DIARYQUESRADIO) {
            $mform->addElement('hidden', 'precise', $defprecise);
        } else if ($question->type_id == DIARYQUESRATE) {
            $precoptions = array("0" => get_string('normal', 'diary'),
                "1" => get_string('notapplicablecolumn', 'diary'),
                "2" => get_string('noduplicates', 'diary'),
                "3" => get_string('osgood', 'diary'));
            $mform->addElement('select', 'precise', get_string($phelpname, 'diary'), $precoptions);
            $mform->addHelpButton('precise', $phelpname, 'diary');
        } else if ($question->type_id == DIARYQUESESSAY) {
            $choices = array();
            for ($lines = 5; $lines <= 40; $lines += 5) {
                $choices[$lines] = get_string('nlines', 'diary', $lines);
            }
            $mform->addElement('select', 'length', get_string('responsefieldlines', 'diary'), $choices);
        } else if ($question->type_id == DIARYQUESCHECK || $question->type_id == DIARYQUESNUMERIC || $question->type_id == DIARYQUESTEXT) {
            $question->precise = isset($question->precise) ? $question->precise : $defprecise;
            $mform->addElement('text', 'precise', get_string($phelpname, 'diary'), array('size' => '1'));
        }

        $mform->setType('precise', PARAM_INT);

        // Dependence fields.

        if ($diary->navigate) {
            $position = isset($question->position) ? $question->position : count($diary->questions) + 1;
            $dependencies = diary_get_dependencies($diary->questions, $position);
            $canchangeparent = true;
            if (count($dependencies) > 1) {
                if (isset($question->qid)) {
                    $haschildren = diary_get_descendants($diary->questions, $question->qid);
                    if (count($haschildren) !== 0) {
                        $canchangeparent = false;
                        $parent = diary_get_parent($question);
                        $fixeddependency = $parent [$question->id]['parent'];
                    }
                }
                if ($canchangeparent) {
                    $question->dependquestion = isset($question->dependquestion) ? $question->dependquestion . ',' .
                            $question->dependchoice : '0,0';
                    $group = array($mform->createElement('selectgroups', 'dependquestion', '', $dependencies));
                    $mform->addGroup($group, 'selectdependency', get_string('dependquestion', 'diary'), '', false);
                    $mform->addHelpButton('selectdependency', 'dependquestion', 'diary');
                } else {
                    $mform->addElement('static', 'selectdependency', get_string('dependquestion', 'diary'), '<div class="dimmed_text">' . $fixeddependency . '</div>');
                }
                $mform->addHelpButton('selectdependency', 'dependquestion', 'diary');
            }
        }

        // Content field.
        $modcontext = $this->_customdata['modcontext'];
        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'trusttext' => true, 'context' => $modcontext);
        $mform->addElement('editor', 'content', get_string('text', 'diary'), null, $editoroptions);
        $mform->setType('content', PARAM_RAW);
        $mform->addRule('content', null, 'required', null, 'client');

        // Options section:
        // has answer options ... so show that part of the form.
        if ($DB->get_field('diary_question_type', 'has_choices', array('typeid' => $question->type_id)) == 'y') {
            if (!empty($question->choices)) {
                $numchoices = count($question->choices);
            } else {
                $numchoices = 0;
            }

            if (!empty($question->choices)) {
                foreach ($question->choices as $choiceid => $choice) {
                    if (!empty($question->allchoices)) {
                        $question->allchoices .= "\n";
                    }
                    $question->allchoices .= $choice->content;
                }
            } else {
                $question->allchoices = '';
            }

            $mform->addElement('html', '<div class="qoptcontainer">');

            $options = array('wrap' => 'virtual', 'class' => 'qopts');
            $mform->addElement('textarea', 'allchoices', get_string('possibleanswers', 'diary'), $options);
            $mform->setType('allchoices', PARAM_RAW);
            $mform->addRule('allchoices', null, 'required', null, 'client');
            $mform->addHelpButton('allchoices', $ohelpname, 'diary');

            $mform->addElement('html', '</div>');

            $mform->addElement('hidden', 'num_choices', $numchoices);
            $mform->setType('num_choices', PARAM_INT);
        }

        // Hidden fields.
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'qid', 0);
        $mform->setType('qid', PARAM_INT);
        $mform->addElement('hidden', 'sid', 0);
        $mform->setType('sid', PARAM_INT);
        $mform->addElement('hidden', 'type_id', $question->type_id);
        $mform->setType('type_id', PARAM_INT);
        $mform->addElement('hidden', 'action', 'question');
        $mform->setType('action', PARAM_RAW);

        // Buttons.
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('savechanges'));
        if (isset($question->qid)) {
            $buttonarray[] = &$mform->createElement('submit', 'makecopy', get_string('saveasnew', 'diary'));
        }
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);

        // If this is a rate question.
        if ($data['type_id'] == DIARYQUESRATE) {
            if ($data['length'] < 2) {
                $errors["length"] = get_string('notenoughscaleitems', 'diary');
            }
            // If this is a rate question with no duplicates option.
            if ($data['precise'] == 2) {
                $allchoices = $data['allchoices'];
                $allchoices = explode("\n", $allchoices);
                $nbnameddegrees = 0;
                $nbvalues = 0;
                foreach ($allchoices as $choice) {
                    if ($choice && !preg_match("/^[0-9]{1,3}=/", $choice)) {
                        $nbvalues++;
                    }
                }
                if ($nbvalues < 2) {
                    $errors["allchoices"] = get_string('noduplicateschoiceserror', 'diary');
                }
            }
        }

        return $errors;
    }

}
