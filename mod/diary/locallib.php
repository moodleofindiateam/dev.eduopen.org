<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This library replaces the phpESP application with Moodle specific code. It will eventually
 * replace all of the phpESP application, removing the dependency on that.
 */

/**
 * Updates the contents of the survey with the provided data. If no data is provided,
 * it checks for posted data.
 *
 * @param int $survey_id The id of the survey to update.
 * @param string $old_tab The function that was being executed.
 * @param object $sdata The data to update the survey with.
 *
 * @return string|boolean The function to go to, or false on error.
 *
 */

require_once($CFG->libdir.'/eventslib.php');
require_once($CFG->dirroot.'/calendar/lib.php');
require_once($CFG->dirroot.'/mod/diary/questiontypes/diarycls.class.php');
// Constants.

define ('DIARYUNLIMITED', 0);
define ('DIARYONCE', 1);
define ('DIARYDAILY', 2);
define ('DIARYWEEKLY', 3);
define ('DIARYMONTHLY', 4);

define ('DIARY_STUDENTVIEWRESPONSES_NEVER', 0);
define ('DIARY_STUDENTVIEWRESPONSES_WHENANSWERED', 1);
define ('DIARY_STUDENTVIEWRESPONSES_WHENCLOSED', 2);
define ('DIARY_STUDENTVIEWRESPONSES_ALWAYS', 3);

define('DIARY_MAX_EVENT_LENGTH', 5 * 24 * 60 * 60);   // 5 days maximum.

define('DIARY_DEFAULT_PAGE_COUNT', 20);

global $diarytypes;
$diarytypes = array (DIARYUNLIMITED => get_string('qtypeunlimited', 'diary'),
                              DIARYONCE => get_string('qtypeonce', 'diary'),
                              DIARYDAILY => get_string('qtypedaily', 'diary'),
                              DIARYWEEKLY => get_string('qtypeweekly', 'diary'),
                              DIARYMONTHLY => get_string('qtypemonthly', 'diary'));

global $diaryrespondents;
$diaryrespondents = array ('fullname' => get_string('respondenttypefullname', 'diary'),
                                    'anonymous' => get_string('respondenttypeanonymous', 'diary'));

global $diaryrealms;
$diaryrealms = array ('private' => get_string('private', 'diary'),
                               'public' => get_string('public', 'diary'),
                               'template' => get_string('template', 'diary'));

global $diaryresponseviewers;
$diaryresponseviewers =
    array ( DIARY_STUDENTVIEWRESPONSES_WHENANSWERED => get_string('responseviewstudentswhenanswered', 'diary'),
            DIARY_STUDENTVIEWRESPONSES_WHENCLOSED => get_string('responseviewstudentswhenclosed', 'diary'),
            DIARY_STUDENTVIEWRESPONSES_ALWAYS => get_string('responseviewstudentsalways', 'diary'));

global $autonumbering;
$autonumbering = array (0 => get_string('autonumberno', 'diary'),
        1 => get_string('autonumberquestions', 'diary'),
        2 => get_string('autonumberpages', 'diary'),
        3 => get_string('autonumberpagesandquestions', 'diary'));

function diary_check_date ($thisdate, $insert=false) {
    $dateformat = get_string('strfdate', 'diary');
    if (preg_match('/(%[mdyY])(.+)(%[mdyY])(.+)(%[mdyY])/', $dateformat, $matches)) {
        $datepieces = explode($matches[2], $thisdate);
        foreach ($datepieces as $datepiece) {
            if (!is_numeric($datepiece)) {
                return 'wrongdateformat';
            }
        }
        $pattern = "/[^dmy]/i";
        $dateorder = strtolower(preg_replace($pattern, '', $dateformat));
        $countpieces = count($datepieces);
        if ($countpieces == 1) { // Assume only year entered.
            switch ($dateorder) {
                case 'dmy': // Most countries.
                case 'mdy': // USA.
                    $datepieces[2] = $datepieces[0]; // year
                    $datepieces[0] = '1'; // Assumed 1st month of year.
                    $datepieces[1] = '1'; // Assumed 1st day of month.
                    break;
                case 'ymd': // ISO 8601 standard
                    $datepieces[1] = '1'; // Assumed 1st month of year.
                    $datepieces[2] = '1'; // Assumed 1st day of month.
                    break;
            }
        }
        if ($countpieces == 2) { // Assume only month and year entered.
            switch ($dateorder) {
                case 'dmy': // Most countries.
                    $datepieces[2] = $datepieces[1]; // Year.
                    $datepieces[1] = $datepieces[0]; // Month.
                    $datepieces[0] = '1'; // Assumed 1st day of month.
                    break;
                case 'mdy': // USA
                    $datepieces[2] = $datepieces[1]; // Year.
                    $datepieces[0] = $datepieces[0]; // Month.
                    $datepieces[1] = '1'; // Assumed 1st day of month.
                    break;
                case 'ymd': // ISO 8601 standard
                    $datepieces[2] = '1'; // Assumed 1st day of month.
                    break;
            }
        }
        if (count($datepieces) > 1) {
            if ($matches[1] == '%m') {
                $month = $datepieces[0];
            }
            if ($matches[1] == '%d') {
                $day = $datepieces[0];
            }
            if ($matches[1] == '%y') {
                $year = strftime('%C').$datepieces[0];
            }
            if ($matches[1] == '%Y') {
                $year = $datepieces[0];
            }

            if ($matches[3] == '%m') {
                $month = $datepieces[1];
            }
            if ($matches[3] == '%d') {
                $day = $datepieces[1];
            }
            if ($matches[3] == '%y') {
                $year = strftime('%C').$datepieces[1];
            }
            if ($matches[3] == '%Y') {
                $year = $datepieces[1];
            }

            if ($matches[5] == '%m') {
                $month = $datepieces[2];
            }
            if ($matches[5] == '%d') {
                $day = $datepieces[2];
            }
            if ($matches[5] == '%y') {
                $year = strftime('%C').$datepieces[2];
            }
            if ($matches[5] == '%Y') {
                $year = $datepieces[2];
            }

            $month = min(12, $month);
            $month = max(1, $month);
            if ($month == 2) {
                $day = min(29, $day);
            } else if ($month == 4 || $month == 6 || $month == 9 || $month == 11) {
                $day = min(30, $day);
            } else {
                $day = min(31, $day);
            }
            $day = max(1, $day);
            if (!$thisdate = gmmktime(0, 0, 0, $month, $day, $year)) {
                return 'wrongdaterange';
            } else {
                if ($insert) {
                    $thisdate = trim(userdate ($thisdate, '%Y-%m-%d', '1', false));
                } else {
                    $thisdate = trim(userdate ($thisdate, $dateformat, '1', false));
                }
            }
            return $thisdate;
        }
    } else {
        return ('wrongdateformat');
    }
}


// A variant of Moodle's notify function, with a different formatting.
function diary_notify($message) {
    $message = clean_text($message);
    $errorstart = '<div class="notifyproblem">';
    $errorend = '</div>';
    $output = $errorstart.$message.$errorend;
    echo $output;
}

function diary_choice_values($content) {

    // If we run the content through format_text first, any filters we want to use (e.g. multilanguage) should work.
    // examines the content of a possible answer from radio button, check boxes or rate question
    // returns ->text to be displayed, ->image if present, ->modname name of modality, image ->title.
    $contents = new stdClass();
    $contents->text = '';
    $contents->image = '';
    $contents->modname = '';
    $contents->title = '';
    // Has image.
    if ($count = preg_match('/(<img)\s .*(src="(.[^"]{1,})")/isxmU', $content, $matches)) {
        $contents->image = $matches[0];
        $imageurl = $matches[3];
        // Image has a title or alt text: use one of them.
        if (preg_match('/(title=.)([^"]{1,})/', $content, $matches)
             || preg_match('/(alt=.)([^"]{1,})/', $content, $matches) ) {
            $contents->title = $matches[2];
        } else {
            // Image has no title nor alt text: use its filename (without the extension).
            preg_match("/.*\/(.*)\..*$/", $imageurl, $matches);
            $contents->title = $matches[1];
        }
        // Content has text or named modality plus an image.
        if (preg_match('/(.*)(<img.*)/', $content, $matches)) {
            $content = $matches[1];
        } else {
            // Just an image.
            return $contents;
        }
    }

    // Check for score value first (used e.g. by personality test feature).
    $r = preg_match_all("/^(\d{1,2}=)(.*)$/", $content, $matches);
    if ($r) {
        $content = $matches[2][0];
    }

    // Look for named modalities.
    $contents->text = $content;
    // DEV JR from version 2.5, a double colon :: must be used here instead of the equal sign.
    if ($pos = strpos($content, '::')) {
        $contents->text = substr($content, $pos + 2);
        $contents->modname = substr($content, 0, $pos);
    }
    return $contents;
}

/**
 * Get the information about the standard diary JavaScript module.
 * @return array a standard jsmodule structure.
 */
function diary_get_js_module() {
    global $PAGE;
    return array(
            'name' => 'mod_diary',
            'fullpath' => '/mod/diary/module.js',
            'requires' => array('base', 'dom', 'event-delegate', 'event-key',
                    'core_question_engine', 'moodle-core-formchangechecker'),
            'strings' => array(
                    array('cancel', 'moodle'),
                    array('flagged', 'question'),
                    array('functiondisabledbysecuremode', 'quiz'),
                    array('startattempt', 'quiz'),
                    array('timesup', 'quiz'),
                    array('changesmadereallygoaway', 'moodle'),
            ),
    );
}

/**
 * Get all the diary responses for a user
 */
function diary_get_user_responses($surveyid, $userid, $complete=true) {
    global $DB;
    $andcomplete = '';
    if ($complete) {
        $andcomplete = " AND complete = 'y' ";
    }
    return $DB->get_records_sql ("SELECT *
        FROM {diary_response}
        WHERE survey_id = ?
        AND username = ?
        ".$andcomplete."
        ORDER BY submitted ASC ", array($surveyid, $userid));
}

/**
 * get the capabilities for the diary
 * @param int $cmid
 * @return object the available capabilities from current user
 */
function diary_load_capabilities($cmid) {
    static $cb;

    if (isset($cb)) {
        return $cb;
    }

    $context = diary_get_context($cmid);

    $cb = new object;
    $cb->view                   = has_capability('mod/diary:view', $context);
    $cb->submit                 = has_capability('mod/diary:submit', $context);
    $cb->viewsingleresponse     = has_capability('mod/diary:viewsingleresponse', $context);
    $cb->downloadresponses      = has_capability('mod/diary:downloadresponses', $context);
    $cb->deleteresponses        = has_capability('mod/diary:deleteresponses', $context);
    $cb->manage                 = has_capability('mod/diary:manage', $context);
    $cb->editquestions          = has_capability('mod/diary:editquestions', $context);
    $cb->createtemplates        = has_capability('mod/diary:createtemplates', $context);
    $cb->createpublic           = has_capability('mod/diary:createpublic', $context);
    $cb->readownresponses       = has_capability('mod/diary:readownresponses', $context);
    $cb->readallresponses       = has_capability('mod/diary:readallresponses', $context);
    $cb->readallresponseanytime = has_capability('mod/diary:readallresponseanytime', $context);
    $cb->printblank             = has_capability('mod/diary:printblank', $context);
    $cb->preview                = has_capability('mod/diary:preview', $context);

    $cb->viewhiddenactivities   = has_capability('moodle/course:viewhiddenactivities', $context, null, false);

    return $cb;
}

/**
 * returns the context-id related to the given coursemodule-id
 * @param int $cmid the coursemodule-id
 * @return object $context
 */
function diary_get_context($cmid) {
    static $context;

    if (isset($context)) {
        return $context;
    }

    if (!$context = context_module::instance($cmid)) {
            print_error('badcontext');
    }
    return $context;
}

// This function *really* shouldn't be needed, but since sometimes we can end up with
// orphaned surveys, this will clean them up.
function diary_cleanup() {
    global $DB;

    // Find surveys that don't have diarys associated with them.
    $sql = 'SELECT qs.* FROM {diary_survey} qs '.
           'LEFT JOIN {diary} q ON q.sid = qs.id '.
           'WHERE q.sid IS NULL';

    if ($surveys = $DB->get_records_sql($sql)) {
        foreach ($surveys as $survey) {
            diary_delete_survey($survey->id, 0);
        }
    }
    // Find deleted questions and remove them from database (with their associated choices, etc.).
    return true;
}

function diary_record_submission(&$diary, $userid, $rid=0) {
    global $DB;

    $attempt['qid'] = $diary->id;
    $attempt['userid'] = $userid;
    $attempt['rid'] = $rid;
    $attempt['timemodified'] = time();
    return $DB->insert_record("diary_attempts", (object)$attempt, false);
}

function diary_delete_survey($sid, $diaryid) {
    global $DB;
    $status = true;
    // Delete all survey attempts and responses.
    if ($responses = $DB->get_records('diary_response', array('survey_id' => $sid), 'id')) {
        foreach ($responses as $response) {
            $status = $status && diary_delete_response($response);
        }
    }

    // There really shouldn't be any more, but just to make sure...
    $DB->delete_records('diary_response', array('survey_id' => $sid));
    $DB->delete_records('diary_attempts', array('qid' => $diaryid));

    // Delete all question data for the survey.
    if ($questions = $DB->get_records('diary_question', array('survey_id' => $sid), 'id')) {
        foreach ($questions as $question) {
            $DB->delete_records('diary_quest_choice', array('question_id' => $question->id));
        }
        $status = $status && $DB->delete_records('diary_question', array('survey_id' => $sid));
    }

    // Delete all feedback sections and feedback messages for the survey.
    if ($fbsections = $DB->get_records('diary_fb_sections', array('survey_id' => $sid), 'id')) {
        foreach ($fbsections as $fbsection) {
            $DB->delete_records('diary_feedback', array('section_id' => $fbsection->id));
        }
        $status = $status && $DB->delete_records('diary_fb_sections', array('survey_id' => $sid));
    }

    $status = $status && $DB->delete_records('diary_survey', array('id' => $sid));

    return $status;
}

function diary_delete_response($response, $diary='') {
    global $DB;
    $status = true;
    $cm = '';
    $rid = $response->id;
    // The diary_delete_survey function does not send the diary array.
    if ($diary != '') {
        $cm = get_coursemodule_from_instance("diary", $diary->id, $diary->course->id);
    }

    // Delete all of the response data for a response.
    $DB->delete_records('diary_response_bool', array('response_id' => $rid));
    $DB->delete_records('diary_response_date', array('response_id' => $rid));
    $DB->delete_records('diary_resp_multiple', array('response_id' => $rid));
    $DB->delete_records('diary_response_other', array('response_id' => $rid));
    $DB->delete_records('diary_response_rank', array('response_id' => $rid));
    $DB->delete_records('diary_resp_single', array('response_id' => $rid));
    $DB->delete_records('diary_response_text', array('response_id' => $rid));

    $status = $status && $DB->delete_records('diary_response', array('id' => $rid));
    $status = $status && $DB->delete_records('diary_attempts', array('rid' => $rid));

    if ($status && $cm) {
        // Update completion state if necessary.
        $completion = new completion_info($diary->course);
        if ($completion->is_enabled($cm) == COMPLETION_TRACKING_AUTOMATIC && $diary->completionsubmit) {
            $completion->update_state($cm, COMPLETION_INCOMPLETE, $response->username);
        }
    }

    return $status;
}

function diary_delete_responses($qid) {
    global $DB;

    $status = true;

    // Delete all of the response data for a question.
    $DB->delete_records('diary_response_bool', array('question_id' => $qid));
    $DB->delete_records('diary_response_date', array('question_id' => $qid));
    $DB->delete_records('diary_resp_multiple', array('question_id' => $qid));
    $DB->delete_records('diary_response_other', array('question_id' => $qid));
    $DB->delete_records('diary_response_rank', array('question_id' => $qid));
    $DB->delete_records('diary_resp_single', array('question_id' => $qid));
    $DB->delete_records('diary_response_text', array('question_id' => $qid));

    $status = $status && $DB->delete_records('diary_response', array('id' => $qid));
    $status = $status && $DB->delete_records('diary_attempts', array('rid' => $qid));

    return $status;
}

function diary_get_survey_list($courseid=0, $type='') {
    global $DB;

    if ($courseid == 0) {
        if (isadmin()) {
            $sql = "SELECT id,name,owner,realm,status " .
                   "{diary_survey} " .
                   "ORDER BY realm,name ";
            $params = null;
        } else {
            return false;
        }
    } else {
        $castsql = $DB->sql_cast_char2int('s.owner');
        if ($type == 'public') {
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,s.title,q.id as qid,q.name as qname " .
                   "FROM {diary} q " .
                   "INNER JOIN {diary_survey} s ON s.id = q.sid AND ".$castsql." = q.course " .
                   "WHERE realm = ? " .
                   "ORDER BY realm,name ";
            $params = array($type);
        } else if ($type == 'template') {
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,s.title,q.id as qid,q.name as qname " .
                   "FROM {diary} q " .
                   "INNER JOIN {diary_survey} s ON s.id = q.sid AND ".$castsql." = q.course " .
                   "WHERE (realm = ?) " .
                   "ORDER BY realm,name ";
            $params = array($type);
        } else if ($type == 'private') {
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,q.id as qid,q.name as qname " .
                "FROM {diary} q " .
                "INNER JOIN {diary_survey} s ON s.id = q.sid " .
                "WHERE owner = ? and realm = ? " .
                "ORDER BY realm,name ";
            $params = array($courseid, $type);

        } else {
            // Current get_survey_list is called from function diary_reset_userdata so we need to get a
            // complete list of all diarys in current course to reset them.
            $sql = "SELECT s.id,s.name,s.owner,s.realm,s.status,q.id as qid,q.name as qname " .
                   "FROM {diary} q " .
                    "INNER JOIN {diary_survey} s ON s.id = q.sid AND ".$castsql." = q.course " .
                   "WHERE owner = ? " .
                   "ORDER BY realm,name ";
            $params = array($courseid);
        }
    }
    return $DB->get_records_sql($sql, $params);
}

function diary_get_survey_select($instance, $courseid=0, $sid=0, $type='') {
    global $OUTPUT, $DB;

    $surveylist = array();

    if ($surveys = diary_get_survey_list($courseid, $type)) {
        $strpreview = get_string('preview_diary', 'diary');
        foreach ($surveys as $survey) {
            $originalcourse = $DB->get_record('course', array('id' => $survey->owner));
            if (!$originalcourse) {
                // This should not happen, but we found a case where a public survey
                // still existed in a course that had been deleted, and so this
                // code lead to a notice, and a broken link. Since that is useless
                // we just skip surveys like this.
                continue;
            }

            // Prevent creating a copy of a public diary IN THE SAME COURSE as the original.
            if ($type == 'public' && $survey->owner == $courseid) {
                continue;
            } else {
                $args = "sid={$survey->id}&popup=1";
                if (!empty($survey->qid)) {
                    $args .= "&qid={$survey->qid}";
                }
                $link = new moodle_url("/mod/diary/preview.php?{$args}");
                $action = new popup_action('click', $link);
                $label = $OUTPUT->action_link($link, $survey->qname.' ['.$originalcourse->fullname.']',
                    $action, array('title' => $strpreview));
                $surveylist[$type.'-'.$survey->id] = $label;
            }
        }
    }
    return $surveylist;
}

function diary_get_type ($id) {
    switch ($id) {
        case 1:
            return get_string('yesno', 'diary');
        case 2:
            return get_string('textbox', 'diary');
        case 3:
            return get_string('essaybox', 'diary');
        case 4:
            return get_string('radiobuttons', 'diary');
        case 5:
            return get_string('checkboxes', 'diary');
        case 6:
            return get_string('dropdown', 'diary');
        case 8:
            return get_string('ratescale', 'diary');
        case 9:
            return get_string('date', 'diary');
        case 10:
            return get_string('numeric', 'diary');
        case 100:
            return get_string('sectiontext', 'diary');
        case 99:
            return get_string('sectionbreak', 'diary');
        default:
        return $id;
    }
}

/**
 * This creates new events given as opendate and closedate by $diary.
 * @param object $diary
 * @return void
 */
 /* added by JR 16 march 2009 based on lesson_process_post_save script */

function diary_set_events($diary) {
    // Adding the diary to the eventtable.
    global $DB;
    if ($events = $DB->get_records('event', array('modulename' => 'diary', 'instance' => $diary->id))) {
        foreach ($events as $event) {
            $event = calendar_event::load($event);
            $event->delete();
        }
    }

    // The open-event.
    $event = new stdClass;
    $event->description = $diary->name;
    $event->courseid = $diary->course;
    $event->groupid = 0;
    $event->userid = 0;
    $event->modulename = 'diary';
    $event->instance = $diary->id;
    $event->eventtype = 'open';
    $event->timestart = $diary->opendate;
    $event->visible = instance_is_visible('diary', $diary);
    $event->timeduration = ($diary->closedate - $diary->opendate);

    if ($diary->closedate and $diary->opendate and $event->timeduration <= DIARY_MAX_EVENT_LENGTH) {
        // Single event for the whole diary.
        $event->name = $diary->name;
        calendar_event::create($event);
    } else {
        // Separate start and end events.
        $event->timeduration  = 0;
        if ($diary->opendate) {
            $event->name = $diary->name.' ('.get_string('diaryopens', 'diary').')';
            calendar_event::create($event);
            unset($event->id); // So we can use the same object for the close event.
        }
        if ($diary->closedate) {
            $event->name = $diary->name.' ('.get_string('diarycloses', 'diary').')';
            $event->timestart = $diary->closedate;
            $event->eventtype = 'close';
            calendar_event::create($event);
        }
    }
}

/**
 * Get users who have not completed the diary
 *
 * @global object
 * @uses CONTEXT_MODULE
 * @param object $cm
 * @param int $group single groupid
 * @param string $sort
 * @param int $startpage
 * @param int $pagecount
 * @return object the userrecords
 */
function diary_get_incomplete_users($cm, $sid,
                $group = false,
                $sort = '',
                $startpage = false,
                $pagecount = false) {

    global $DB;

    $context = context_module::instance($cm->id);

    // First get all users who can complete this diary.
    $cap = 'mod/diary:submit';
    $fields = 'u.id, u.username';
    if (!$allusers = get_users_by_capability($context,
                    $cap,
                    $fields,
                    $sort,
                    '',
                    '',
                    $group,
                    '',
                    true)) {
        return false;
    }
    $allusers = array_keys($allusers);

    // Nnow get all completed diarys.
    $params = array('survey_id' => $sid, 'complete' => 'y');
    $sql = "SELECT username FROM {diary_response} "
                    . "WHERE survey_id = $sid AND complete = 'y' "
    . "GROUP BY username ";

    if (!$completedusers = $DB->get_records_sql($sql)) {
        return $allusers;
    }
    $completedusers = array_keys($completedusers);
    // Now strike all completedusers from allusers.
    $allusers = array_diff($allusers, $completedusers);
    // For paging I use array_slice().
    if ($startpage !== false AND $pagecount !== false) {
        $allusers = array_slice($allusers, $startpage, $pagecount);
    }
    return $allusers;
}

/**
 * Called by HTML editor in showrespondents and Essay question. Based on question/essay/renderer.
 * Pending general solution to using the HTML editor outside of moodleforms in Moodle pages.
 */
function diary_get_editor_options($context) {
    return array(
                    'subdirs' => 0,
                    'maxbytes' => 0,
                    'maxfiles' => -1,
                    'context' => $context,
                    'noclean' => 0,
                    'trusttext' => 0
    );
}

// Skip logic: we need to find out how many questions will actually be displayed on next page/section.
function diary_nb_questions_on_page ($questionsindiary, $questionsinsection, $rid) {
    global $DB;
    $questionstodisplay = array();
    foreach ($questionsinsection as $question) {
        if ($question->dependquestion != 0) {
            switch ($questionsindiary[$question->dependquestion]->type_id) {
                case DIARYQUESYESNO:
                    if ($question->dependchoice == 0) {
                        $questiondependchoice = "'y'";
                    } else {
                        $questiondependchoice = "'n'";
                    }
                    $responsetable = 'response_bool';
                    break;
                default:
                    $questiondependchoice = $question->dependchoice;
                    $responsetable = 'resp_single';
            }
            $sql = 'SELECT * FROM {diary}_'.$responsetable.' WHERE response_id = '.$rid.
            ' AND question_id = '.$question->dependquestion.' AND choice_id = '.$questiondependchoice;
            if ($DB->get_record_sql($sql)) {
                $questionstodisplay [] = $question->id;
            }
        } else {
            $questionstodisplay [] = $question->id;
        }
    }
    return $questionstodisplay;
}

function diary_get_dependencies($questions, $position) {
    $dependencies = array();
    $dependencies[''][0] = get_string('choosedots');

    foreach ($questions as $question) {
        if (($question->type_id == DIARYQUESRADIO || $question->type_id == DIARYQUESDROP || $question->type_id == DIARYQUESYESNO)
                        && $question->position < $position) {
            if (($question->type_id == DIARYQUESRADIO || $question->type_id == DIARYQUESDROP) && $question->name != '') {
                foreach ($question->choices as $key => $choice) {
                    $contents = diary_choice_values($choice->content);
                    if ($contents->modname) {
                        $choice->content = $contents->modname;
                    } else if ($contents->title) { // Must be an image; use its title for the dropdown list.
                        $choice->content = $contents->title;
                    } else {
                        $choice->content = $contents->text;
                    }
                    $dependencies[$question->name][$question->id.','.$key] = $question->name.'->'.$choice->content;
                }
            }
            if ($question->type_id == DIARYQUESYESNO && $question->name != '') {
                $dependencies[$question->name][$question->id.',0'] = $question->name.'->'.get_string('yes');
                $dependencies[$question->name][$question->id.',1'] = $question->name.'->'.get_string('no');
            }
        }
    }
    return $dependencies;
}

// Get the parent of a child question.
function diary_get_parent ($question) {
    global $DB;
    $qid = $question->id;
    $parent = array();
    $dependquestion = $DB->get_record('diary_question', array('id' => $question->dependquestion),
                    $fields = 'id, position, name, type_id');
    if (is_object($dependquestion)) {
        $qdependchoice = '';
        switch ($dependquestion->type_id) {
            case DIARYQUESRADIO:
            case DIARYQUESDROP:
                $dependchoice = $DB->get_record('diary_quest_choice', array('id' => $question->dependchoice),
                    $fields = 'id,content');
                $qdependchoice = $dependchoice->id;
                $dependchoice = $dependchoice->content;

                $contents = diary_choice_values($dependchoice);
                if ($contents->modname) {
                    $dependchoice = $contents->modname;
                }
                break;
            case DIARYQUESYESNO:
                switch ($question->dependchoice) {
                    case 0:
                        $dependchoice = get_string('yes');
                        $qdependchoice = 'y';
                        break;
                    case 1:
                        $dependchoice = get_string('no');
                        $qdependchoice = 'n';
                        break;
                }
                break;
        }
        // Qdependquestion, parenttype and qdependchoice fields to be used in preview mode.
        $parent [$qid]['qdependquestion'] = 'q'.$dependquestion->id;
        $parent [$qid]['qdependchoice'] = $qdependchoice;
        $parent [$qid]['parenttype'] = $dependquestion->type_id;
        // Other fields to be used in Questions edit mode.
        $parent [$qid]['position'] = $question->position;
        $parent [$qid]['name'] = $question->name;
        $parent [$qid]['content'] = $question->content;
        $parent [$qid]['parentposition'] = $dependquestion->position;
        $parent [$qid]['parent'] = $dependquestion->name.'->'.$dependchoice;
    }
    return $parent;
}

// Get parent position of all child questions in current diary.
function diary_get_parent_positions ($questions) {
    global $DB;
    $parentpositions = array();
    foreach ($questions as $question) {
        $dependquestion = $question->dependquestion;
        if ($dependquestion != 0) {
            $childid = $question->id;
            $parentpos = $questions[$dependquestion]->position;
            $parentpositions[$childid] = $parentpos;
        }
    }
    return $parentpositions;
}

// Get child position of all parent questions in current diary.
function diary_get_child_positions ($questions) {
    global $DB;
    $childpositions = array();
    foreach ($questions as $question) {
        $dependquestion = $question->dependquestion;
        if ($dependquestion != 0) {
            $parentid = $questions[$dependquestion]->id;
            if (!isset($firstchildfound[$parentid])) {
                $firstchildfound[$parentid] = true;
                $childpos = $question->position;
                $childpositions[$parentid] = $childpos;
            }
        }
    }
    return $childpositions;
}

// Check if current diary contains child questions.
function diary_has_dependencies($questions) {
    foreach ($questions as $question) {
        if ($question->dependquestion != 0) {
            return true;
            break;
        }
    }
    return false;
}

// Check that the needed page breaks are present to separate child questions.
function diary_check_page_breaks($diary) {
    global $DB;
    $msg = '';
    // Store the new page breaks ids.
    $newpbids = array();
    $delpb = 0;
    $sid = $diary->survey->id;
    $questions = $DB->get_records('diary_question', array('survey_id' => $sid, 'deleted' => 'n'), 'id');
    $positions = array();
    foreach ($questions as $key => $qu) {
        $positions[$qu->position]['question_id'] = $key;
        $positions[$qu->position]['dependquestion'] = $qu->dependquestion;
        $positions[$qu->position]['dependchoice'] = $qu->dependchoice;
        $positions[$qu->position]['type_id'] = $qu->type_id;
        $positions[$qu->position]['qname'] = $qu->name;
        $positions[$qu->position]['qpos'] = $qu->position;
    }
    $count = count($positions);

    for ($i = $count; $i > 0; $i--) {
        $qu = $positions[$i];
        $questionnb = $i;
        if ($qu['type_id'] == DIARYQUESPAGEBREAK) {
            $questionnb--;
            // If more than one consecutive page breaks, remove extra one(s).
            $prevqu = null;
            $prevtypeid = null;
            if ($i > 1) {
                $prevqu = $positions[$i - 1];
                $prevtypeid = $prevqu['type_id'];
            }
            // If $i == $count then remove that extra page break in last position.
            if ($prevtypeid == DIARYQUESPAGEBREAK || $i == $count || $qu['qpos'] == 1) {
                $qid = $qu['question_id'];
                $delpb ++;
                $msg .= get_string("checkbreaksremoved", "diary", $delpb).'<br />';
                // Need to reload questions.
                $questions = $DB->get_records('diary_question', array('survey_id' => $sid, 'deleted' => 'n'), 'id');
                $DB->set_field('diary_question', 'deleted', 'y', array('id' => $qid, 'survey_id' => $sid));
                $select = 'survey_id = '.$sid.' AND deleted = \'n\' AND position > '.
                                $questions[$qid]->position;
                if ($records = $DB->get_records_select('diary_question', $select, null, 'position ASC')) {
                    foreach ($records as $record) {
                        $DB->set_field('diary_question', 'position', $record->position - 1, array('id' => $record->id));
                    }
                }
            }
        }
        // Add pagebreak between question child and not dependent question that follows.
        if ($qu['type_id'] != DIARYQUESPAGEBREAK) {
            $qname = $positions[$i]['qname'];
            $j = $i - 1;
            if ($j != 0) {
                $prevtypeid = $positions[$j]['type_id'];
                $prevdependquestion = $positions[$j]['dependquestion'];
                $prevdependchoice = $positions[$j]['dependchoice'];
                $prevdependquestionname = $positions[$j]['qname'];
                $prevqname = $positions[$j]['qname'];
                if (($prevtypeid != DIARYQUESPAGEBREAK && ($prevdependquestion != $qu['dependquestion']
                                || $prevdependchoice != $qu['dependchoice']))
                                || ($qu['dependquestion'] == 0 && $prevdependquestion != 0)) {
                    $sql = 'SELECT MAX(position) as maxpos FROM {diary_question} '.
                                    'WHERE survey_id = '.$diary->survey->id.' AND deleted = \'n\'';
                    if ($record = $DB->get_record_sql($sql)) {
                        $pos = $record->maxpos + 1;
                    } else {
                        $pos = 1;
                    }
                    $question = new Object();
                    $question->survey_id = $diary->survey->id;
                    $question->type_id = DIARYQUESPAGEBREAK;
                    $question->position = $pos;
                    $question->content = 'break';
                    if (!($newqid = $DB->insert_record('diary_question', $question))) {
                        return(false);
                    }
                    $newpbids[] = $newqid;
                    $movetopos = $i;
                    $diary = new diary($diary->id, null, $course, $cm);
                    $diary->move_question($newqid, $movetopos);
                }
            }
        }
    }
    if (empty($newpbids) && !$msg) {
        $msg = get_string('checkbreaksok', 'diary');
    } else if ($newpbids) {
        $msg .= get_string('checkbreaksadded', 'diary').'&nbsp;';
        $newpbids = array_reverse ($newpbids);
        $diary = new diary($diary->id, null, $course, $cm);
        foreach ($newpbids as $newpbid) {
            $msg .= $diary->questions[$newpbid]->position.'&nbsp;';
        }
    }
    return($msg);
}

// Get all descendants and choices for questions with descendants.
function diary_get_descendants_and_choices ($questions) {
    global $DB;
    $questions = array_reverse($questions, true);
    $qu = array();
    foreach ($questions as $question) {
        if ($question->dependquestion) {
            $dq = $question->dependquestion;
            $dc = $question->dependchoice;
            $qid = $question->id;

            $qu['descendants'][$dq][] = 'qn-'.$qid;
            if (array_key_exists($qid, $qu['descendants'])) {
                foreach ($qu['descendants'][$qid] as $q) {
                    $qu['descendants'][$dq][] = $q;
                }
            }
            $qu['choices'][$dq][$dc][] = 'qn-'.$qid;
        }
    }
    return($qu);
}

// Get all descendants for a question to be deleted.
function diary_get_descendants ($questions, $questionid) {
    global $DB;
    $questions = array_reverse($questions, true);
    $qu = array();
    foreach ($questions as $question) {
        if ($question->dependquestion) {
            $dq = $question->dependquestion;
            $qid = $question->id;
            $qpos = $question->position;
            $qu[$dq][] = $qid;
            if (array_key_exists($qid, $qu)) {
                foreach ($qu[$qid] as $q) {
                    $qu[$dq][] = $q;
                }
            }
        }
    }
    $descendants = array();
    if (isset($qu[$questionid])) {
        foreach ($qu[$questionid] as $descendant) {
            $childquestion = $questions[$descendant];
            $descendants += diary_get_parent ($childquestion);
        }
        uasort($descendants, 'diary_cmp');
    }
    return($descendants);
}

// Function to sort descendants array in diary_get_descendants function.
function diary_cmp($a, $b) {
    if ($a == $b) {
        return 0;
    } else if ($a < $b) {
        return -1;
    } else {
        return 1;
    }
}
