<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * prints the tabbed bar
 *
 * @author Mike Churchward
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package diary
 */
global $DB, $SESSION;
$tabs = array();
$row  = array();
$inactive = array();
$activated = array();
if (!isset($SESSION->diary)) {
    $SESSION->diary = new stdClass();
}
$currenttab = $SESSION->diary->current_tab;

// If this diary has a survey, get the survey and owner.
// In a diary instance created "using" a PUBLIC diary, prevent anyone from editing settings, editing questions,
// viewing all responses...except in the course where that PUBLIC diary was originally created.

$courseid = $diary->course->id;
if ($survey = $DB->get_record('diary_survey', array('id' => $diary->sid))) {
    $owner = (trim($survey->owner) == trim($courseid));
} else {
    $survey = false;
    $owner = true;
}
if ($diary->capabilities->manage  && $owner) {
    $row[] = new tabobject('settings', $CFG->wwwroot.htmlspecialchars('/mod/diary/qsettings.php?'.
            'id='.$diary->cm->id), get_string('advancedsettings'));
}

if ($diary->capabilities->editquestions && $owner) {
    $row[] = new tabobject('questions', $CFG->wwwroot.htmlspecialchars('/mod/diary/questions.php?'.
            'id='.$diary->cm->id), get_string('questions', 'diary'));
}

if ($diary->capabilities->preview && $owner) {
    if (!empty($diary->questions)) {
        $row[] = new tabobject('preview', $CFG->wwwroot.htmlspecialchars('/mod/diary/preview.php?'.
                        'id='.$diary->cm->id), get_string('preview_label', 'diary'));
    }
}

$usernumresp = $diary->count_submissions($USER->id);

if ($diary->capabilities->readownresponses && ($usernumresp > 0)) {
    $argstr = 'instance='.$diary->id.'&user='.$USER->id.'&group='.$currentgroupid;
    if ($usernumresp == 1) {
        $argstr .= '&byresponse=1&action=vresp';
        $yourrespstring = get_string('yourresponse', 'diary');
    } else {
        $yourrespstring = get_string('yourresponses', 'diary');
    }
    $row[] = new tabobject('myreport', $CFG->wwwroot.htmlspecialchars('/mod/diary/myreport.php?'.
                           $argstr), $yourrespstring);

    if ($usernumresp > 1 && in_array($currenttab, array('mysummary', 'mybyresponse', 'myvall', 'mydownloadcsv'))) {
        $inactive[] = 'myreport';
        $activated[] = 'myreport';
        $row2 = array();
        $argstr2 = $argstr.'&action=summary';
        $row2[] = new tabobject('mysummary', $CFG->wwwroot.htmlspecialchars('/mod/diary/myreport.php?'.$argstr2),
                                get_string('summary', 'diary'));
        $argstr2 = $argstr.'&byresponse=1&action=vresp';
        $row2[] = new tabobject('mybyresponse', $CFG->wwwroot.htmlspecialchars('/mod/diary/myreport.php?'.$argstr2),
                                get_string('viewindividualresponse', 'diary'));
        $argstr2 = $argstr.'&byresponse=0&action=vall&group='.$currentgroupid;
        $row2[] = new tabobject('myvall', $CFG->wwwroot.htmlspecialchars('/mod/diary/myreport.php?'.$argstr2),
                                get_string('myresponses', 'diary'));
        if ($diary->capabilities->downloadresponses) {
            $argstr2 = $argstr.'&action=dwnpg';
            $link  = $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2);
            $row2[] = new tabobject('mydownloadcsv', $link, get_string('downloadtext'));
        }
    } else if (in_array($currenttab, array('mybyresponse', 'mysummary'))) {
        $inactive[] = 'myreport';
        $activated[] = 'myreport';
    }
}

$numresp = $diary->count_submissions();
// Number of responses in currently selected group (or all participants etc.).
if (isset($SESSION->diary->numselectedresps)) {
    $numselectedresps = $SESSION->diary->numselectedresps;
} else {
    $numselectedresps = $numresp;
}

// If diary is set to separate groups, prevent user who is not member of any group
// to view All responses.
$canviewgroups = true;
$groupmode = groups_get_activity_groupmode($cm, $course);
if ($groupmode == 1) {
    $canviewgroups = groups_has_membership($cm, $USER->id);
}
$canviewallgroups = has_capability('moodle/site:accessallgroups', $context);

if (($canviewallgroups || ($canviewgroups && $diary->capabilities->readallresponseanytime))
                && $numresp > 0 && $owner && $numselectedresps > 0) {
    $argstr = 'instance='.$diary->id;
    $row[] = new tabobject('allreport', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.
                           $argstr.'&action=vall'), get_string('viewallresponses', 'diary'));
    if (in_array($currenttab, array('vall', 'vresp', 'valldefault', 'vallasort', 'vallarsort', 'deleteall', 'downloadcsv',
                                     'vrespsummary', 'individualresp', 'printresp', 'deleteresp'))) {
        $inactive[] = 'allreport';
        $activated[] = 'allreport';
        if ($currenttab == 'vrespsummary' || $currenttab == 'valldefault') {
            $inactive[] = 'vresp';
        }
        $row2 = array();
        $argstr2 = $argstr.'&action=vall&group='.$currentgroupid;
        $row2[] = new tabobject('vall', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                get_string('summary', 'diary'));
        if ($diary->capabilities->viewsingleresponse) {
            $argstr2 = $argstr.'&byresponse=1&action=vresp&group='.$currentgroupid;
            $row2[] = new tabobject('vrespsummary', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                get_string('viewbyresponse', 'diary'));
            if ($currenttab == 'individualresp' || $currenttab == 'deleteresp') {
                $argstr2 = $argstr.'&byresponse=1&action=vresp';
                $row2[] = new tabobject('vresp', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                        get_string('viewindividualresponse', 'diary'));
            }
        }
    }
    if (in_array($currenttab, array('valldefault',  'vallasort', 'vallarsort', 'deleteall', 'downloadcsv'))) {
        $activated[] = 'vall';
        $row3 = array();

        $argstr2 = $argstr.'&action=vall&group='.$currentgroupid;
        $row3[] = new tabobject('valldefault', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                get_string('order_default', 'diary'));
        if ($currenttab != 'downloadcsv' && $currenttab != 'deleteall') {
            $argstr2 = $argstr.'&action=vallasort&group='.$currentgroupid;
            $row3[] = new tabobject('vallasort', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                    get_string('order_ascending', 'diary'));
            $argstr2 = $argstr.'&action=vallarsort&group='.$currentgroupid;
            $row3[] = new tabobject('vallarsort', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                    get_string('order_descending', 'diary'));
        }
        if ($diary->capabilities->deleteresponses) {
            $argstr2 = $argstr.'&action=delallresp&group='.$currentgroupid;
            $row3[] = new tabobject('deleteall', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                    get_string('deleteallresponses', 'diary'));
        }

        if ($diary->capabilities->downloadresponses) {
            $argstr2 = $argstr.'&action=dwnpg&group='.$currentgroupid;
            $link  = $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2);
            $row3[] = new tabobject('downloadcsv', $link, get_string('downloadtext'));
        }
    }

    if (in_array($currenttab, array('individualresp', 'deleteresp'))) {
        $inactive[] = 'vresp';
        if ($currenttab != 'deleteresp') {
            $activated[] = 'vresp';
        }
        if ($diary->capabilities->deleteresponses) {
            $argstr2 = $argstr.'&action=dresp&rid='.$rid.'&individualresponse=1';
            $row2[] = new tabobject('deleteresp', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                            get_string('deleteresp', 'diary'));
        }

    }
} else if ($canviewgroups && $diary->capabilities->readallresponses && ($numresp > 0) && $canviewgroups &&
           ($diary->resp_view == DIARY_STUDENTVIEWRESPONSES_ALWAYS ||
            ($diary->resp_view == DIARY_STUDENTVIEWRESPONSES_WHENCLOSED
                && $diary->is_closed()) ||
            ($diary->resp_view == DIARY_STUDENTVIEWRESPONSES_WHENANSWERED
                && $usernumresp > 0 )) &&
           $diary->is_survey_owner()) {
    $argstr = 'instance='.$diary->id.'&sid='.$diary->sid;
    $row[] = new tabobject('allreport', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.
                           $argstr.'&action=vall&group='.$currentgroupid), get_string('viewallresponses', 'diary'));
    if (in_array($currenttab, array('valldefault',  'vallasort', 'vallarsort', 'deleteall', 'downloadcsv'))) {
        $inactive[] = 'vall';
        $activated[] = 'vall';
        $row2 = array();
        $argstr2 = $argstr.'&action=vall&group='.$currentgroupid;
        $row2[] = new tabobject('valldefault', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                get_string('summary', 'diary'));
        $inactive[] = $currenttab;
        $activated[] = $currenttab;
        $row3 = array();
        $argstr2 = $argstr.'&action=vall&group='.$currentgroupid;
        $row3[] = new tabobject('valldefault', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                get_string('order_default', 'diary'));
        $argstr2 = $argstr.'&action=vallasort&group='.$currentgroupid;
        $row3[] = new tabobject('vallasort', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                get_string('order_ascending', 'diary'));
        $argstr2 = $argstr.'&action=vallarsort&group='.$currentgroupid;
        $row3[] = new tabobject('vallarsort', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                get_string('order_descending', 'diary'));
        if ($diary->capabilities->deleteresponses) {
            $argstr2 = $argstr.'&action=delallresp';
            $row2[] = new tabobject('deleteall', $CFG->wwwroot.htmlspecialchars('/mod/diary/report.php?'.$argstr2),
                                    get_string('deleteallresponses', 'diary'));
        }

        if ($diary->capabilities->downloadresponses) {
            $argstr2 = $argstr.'&action=dwnpg';
            $link  = htmlspecialchars('/mod/diary/report.php?'.$argstr2);
            $row2[] = new tabobject('downloadcsv', $link, get_string('downloadtext'));
        }
        if (count($row2) <= 1) {
            $currenttab = 'allreport';
        }
    }
}

if ($diary->capabilities->viewsingleresponse && ($canviewallgroups || $canviewgroups)) {
    $nonrespondenturl = new moodle_url('/mod/diary/show_nonrespondents.php', array('id' => $diary->cm->id));
    $row[] = new tabobject('nonrespondents',
                    $nonrespondenturl->out(),
                    get_string('show_nonrespondents', 'diary'));
}

if ((count($row) > 1) || (!empty($row2) && (count($row2) > 1))) {
    $tabs[] = $row;

    if (!empty($row2) && (count($row2) > 1)) {
        $tabs[] = $row2;
    }

    if (!empty($row3) && (count($row3) > 1)) {
        $tabs[] = $row3;
    }

    print_tabs($tabs, $currenttab, $inactive, $activated);

}