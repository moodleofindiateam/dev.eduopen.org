<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// This page prints a particular instance of diary.

require_once("../../config.php");
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot.'/mod/diary/diary.class.php');

if (!isset($SESSION->diary)) {
    $SESSION->diary = new stdClass();
}
$SESSION->diary->current_tab = 'view';

$id = optional_param('id', null, PARAM_INT);    // Course Module ID.
$a = optional_param('a', null, PARAM_INT);      // diary ID.

$sid = optional_param('sid', null, PARAM_INT);  // Survey id.
$resume = optional_param('resume', null, PARAM_INT);    // Is this attempt a resume of a saved attempt?

if ($id) {
    if (! $cm = get_coursemodule_from_id('diary', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }

    if (! $diary = $DB->get_record("diary", array("id" => $cm->instance))) {
        print_error('invalidcoursemodule');
    }

} else {
    if (! $diary = $DB->get_record("diary", array("id" => $a))) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id" => $diary->course))) {
        print_error('coursemisconf');
    }
    if (! $cm = get_coursemodule_from_instance("diary", $diary->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
}

// Check login and get context.
require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/diary:view', $context);

$url = new moodle_url($CFG->wwwroot.'/mod/diary/complete.php');
if (isset($id)) {
    $url->param('id', $id);
} else {
    $url->param('a', $a);
}

$PAGE->set_url($url);
$PAGE->set_context($context);
$diary = new diary(0, $diary, $course, $cm);

$diary->strdiarys = get_string("modulenameplural", "diary");
$diary->strdiary  = get_string("modulename", "diary");

// Mark as viewed.
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

if ($resume) {
    $context = context_module::instance($diary->cm->id);
    $anonymous = $diary->respondenttype == 'anonymous';

    $event = \mod_diary\event\attempt_resumed::create(array(
                    'objectid' => $diary->id,
                    'anonymous' => $anonymous,
                    'context' => $context
    ));
    $event->trigger();
}

$diary->view();
