<?php
//header("Cache-Control: no-cache, must-revalidate");
require_once("../../config.php");
require_once($CFG->dirroot . '/lib/accesslib.php');
global $CFG, $DB, $USER, $COURSE;
require_login();
$cid = required_param('courseid', PARAM_INT);
$course = get_course($cid);
$loggedinuser = $USER->id;
$editingteacher = user_has_role_assignment($loggedinuser, 3, 0);
purge_all_caches();
$PAGE->set_url('/mod/selfchecklist/graphs.php', array('courseid' => $cid));
$PAGE->set_pagelayout('incourse');
// Print the header.
$strselfchecklists = get_string("modulenameplural", "selfchecklist");
$selfcheck = get_string('selfchecklistgraph', 'selfchecklist');
$PAGE->set_context(context_course::instance($cid));
$PAGE->set_title($selfcheck);
$PAGE->set_heading($selfcheck);
// For Breadcrumb.
$selfgraph = get_string('selfchecklistgraph', 'selfchecklist');
$PAGE->navbar->add($course->fullname, new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $course->id)));
if (is_siteadmin() || $editingteacher) {
    $PAGE->navbar->add($strselfchecklists, new moodle_url($CFG->wwwroot . '/mod/selfchecklist/graphs.php', array('id' => $cid)));
}
$PAGE->navbar->add($selfgraph);
echo $OUTPUT->header("Cache-Control: no-cache, must-revalidate");
?>

<script type="text/javascript" src="<?php echo $CFG->wwwroot ?>/mod/selfchecklist/custom/jquery.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot ?>/mod/selfchecklist/custom/gchart_loader.js"></script>
<script type="text/javascript" src="<?php echo $CFG->wwwroot ?>/mod/selfchecklist/custom/visualization.js"></script>
<script>
    google.charts.load('current', {'packages': ['corechart']});
</script>
<?php

function chart($data) {
    $cols = array();
    $col1 = ["id" => "", "label" => "Y", "type" => "string"];
    $col2 = ["id" => "", "label" => "Beginning Graph", "type" => "number"];
    $col3 = ["id" => "", "label" => "Ending Graph", "type" => "number"];
    $col4 = ["id" => "", "label" => "Initial Average Line", "type" => "number"];
    $cols = array($col1, $col2, $col3, $col4);

    $rowdata = array();
    foreach ($data as $var) {
        $cell0["v"] = $var['name'];
        $cell0["f"] = null;
        $cell1["v"] = $var['begrank'];
        $cell1["f"] = null;
        $cell2["v"] = $var['endrank'];
        $cell2["f"] = null;
        $cell3["v"] = $var['avgrank'];
        $cell3["f"] = null;
        $rowdata[]["c"] = array($cell0, $cell1, $cell2, $cell3);
    }
    return array("cols" => $cols, "rows" => $rowdata);
}

// Fuction to sum values of the array of the same key.
function array_add_by_key($array1, $array2) {
    foreach ($array2 as $k => $a) {
        if (array_key_exists($k, $array1)) {
            $array1[$k] += $a;
        } else {
            $array1[$k] = $a;
        }
    }
    return $array1;
}

// Fuction to difference values of the array of the same key.
function array_diff_by_key($array2, $array1) {
    foreach ($array1 as $k => $a) {
        if (array_key_exists($k, $array2)) {
            $array2[$k] -= $a;
        } else {
            $array2[$k] = $a;
        }
    }
    return $array2;
}

if (is_siteadmin() || $editingteacher) {
    $userids = optional_param('userid', '*', PARAM_INT);
    $user = $DB->get_record_sql("SELECT * FROM {user} WHERE id=$userids");
    $ufullname = fullname($user);
    // Admin Beggining Graph starts.
    $adminbegsql = "SELECT DISTINCT qrr.id, q.course, qq.content AS blockcontent, qqc.content, qrr.rank, q.addtype, qrr.question_id AS quesid, qrr.response_id,
    qrr.choice_id, qr.username
    FROM {selfchecklist_question} qq
    JOIN {selfchecklist_response_rank} qrr ON qrr.question_id=qq.id
    JOIN {selfchecklist_response} qr ON qr.id=qrr.response_id
    JOIN {selfchecklist} q ON q.id=qq.survey_id
    JOIN {selfchecklist_quest_choice} qqc ON qqc.question_id=qrr.question_id
    WHERE q.addtype='beg' AND qr.username='$userids' AND q.course=$cid AND qrr.choice_id=qqc.id
    ORDER BY qrr.id ASC";
    $adminbegrs = $DB->get_records_sql($adminbegsql);
    // Admin Ending Graph starts.
    $adminendsql = "SELECT DISTINCT qrr.id, q.course, qq.content AS blockcontent, qqc.content, qrr.rank, q.addtype, qrr.question_id AS quesid, qrr.response_id,
    qrr.choice_id, qr.username
    FROM {selfchecklist_question} qq
    JOIN {selfchecklist_response_rank} qrr ON qrr.question_id=qq.id
    JOIN {selfchecklist_response} qr ON qr.id=qrr.response_id
    JOIN {selfchecklist} q ON q.id=qq.survey_id
    JOIN {selfchecklist_quest_choice} qqc ON qqc.question_id=qrr.question_id
    WHERE q.addtype='end' AND qr.username='$userids' AND q.course=$cid AND qrr.choice_id=qqc.id
    ORDER BY qrr.id ASC";
    $adminendrs = $DB->get_records_sql($adminendsql);
    $adminendranks = array();
    foreach ($adminendrs as $adminendrsval) {
        $adminendranks[] = $adminendrsval->rank;
    }
    echo '<h2>Response Table for ' . $ufullname . '</h2>';
    $checklistname = $DB->get_records_sql("SELECT name FROM {selfchecklist} WHERE course='$cid'");
    $i = 0;
    $reccount = count($checklistname);
    foreach ($checklistname as $chname) {
        $checklists[] = $chname->name;
    }
    echo '<h4 class="padbott">Checkists: ' . $checklists[0] . ' & ' . $checklists[1] . '</h4>';
    echo '<a href="' . $CFG->wwwroot . '/mod/selfchecklist/download.php?userid=' . $userids . '&courseid=' . $cid . '">
    <div class="btn btn-success redbtn">' . get_string('clicktodownload', 'selfchecklist') . '</div>
    </a>';

    //Average rank portion.
    $adminbegarrrank = array();
    foreach ($adminbegrs as $adminbegresult) {
        $adminbegarrrank[] = $adminbegresult->rank;
    }
    $begrankarr = $adminbegarrrank;
    $endrankarr = $adminendranks;
    $arraysum = array_add_by_key($begrankarr, $endrankarr);
    foreach ($arraysum as $k => $a) {
        if (array_key_exists($k, $arraysum)) {
            $arraysum[$k] = $a / 2;
        } else {
            $arraysum[$k] = $a;
        }
        $arravg = $arraysum;
    }
    foreach ($adminbegrs as $adminbegresult) {
        $adminbegarrrank[] = $adminbegresult->rank;
    }
    // Rank difference between End Ranks and Beginning Ranks.
    $diffArr = array_diff_by_key($endrankarr, $begrankarr);
    // Sum of begging ranks, ending ranks, avg ranks.
    $begrankAvg = array_sum($begrankarr) / 15;
    $endrankAvg = array_sum($endrankarr) / 15;
    $diffrankAvg = array_sum($diffArr) / 15;

    $table = new html_table();
    //$table->head = (array) get_strings(['blocks', 'qno', 'begranks', 'endranks', 'diffranks'], 'selfchecklist');
    $avgi = 0;
    $endi = 0;
    foreach ($adminbegrs as $val) {
        $arrkeys = array_key_exists($endi, $adminendranks);
        if (!$arrkeys) {
            $adminendranks[$endi] = 0;
        }
        $arrkey = array_key_exists($avgi, $arravg);
        if (!$arrkey) {
            $arravg[$avgi] = 0;
        }
        $table->data[] = array(
            $val->blockcontent, $val->content, $val->rank, $adminendranks[$endi], $diffArr[$avgi]
        );
        $endi++;
        $avgi++;
    }
    $lastrow = array(
        array(
            0 => '<p>' . get_string('colavg', 'selfchecklist') . '<br></p>',
            1 => get_string('colavg1', 'selfchecklist'),
            2 => round($begrankAvg, 1),
            3 => round($endrankAvg, 1),
            4 => round($diffrankAvg, 1)
        )
    );
    $merge = array_merge($table->data, $lastrow);
    $tabobject = new html_table();
    foreach ($merge as $key => $value) {
        $tabobject->attributes = array('class' => 'generaltable customtab');
        $tabobject->head = (array) get_strings(['blocks', 'qno', 'begranks', 'endranks', 'diffranks'], 'selfchecklist');
        $tabobject->data[] = $value;
    }
    echo html_writer::table($tabobject);

    if ($adminbegrs || $adminendrs) {
        $i = 0;
        foreach ($adminbegrs as $adminbegrss) {
            $adminbegarr[] = $adminbegrss->rank;
            $strl = strlen($adminbegrss->content);
            //Take first two letters to print haxis as A1, A2, A3....
            $arr = $adminbegrss->content[0] . $adminbegrss->content[1];
            $qnum = $arr;
            $adminrankbeg = $adminbegrss->rank;
            $courseid = $adminbegrss->course;
            $arrkey = array_key_exists($i, $adminendranks);
            if (!$arrkey) {
                $adminendranks[$i] = 0;
            }
            $graphdata[] = array('name' => "$qnum", 'begrank' => $adminrankbeg, 'endrank' => $adminendranks[$i], 'avgrank' => round($begrankAvg, 1));
            $i++;
        }
        //print_object($graphdata);
        $cidd = (int) $courseid;
        $cname = $DB->get_record('course', array('id' => $cidd));

        echo '</head>
            <body>
                <div id="buttons1" class="buttons"></div>
                <hr/>
                <div id="JSFiddle">
                    <div id="admin_curve_chart_beg"></div>
                </div>
            </body>
          </html>';
        ?>
        <script type="text/javascript">
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var json = <?php echo json_encode(chart($graphdata)) ?>;
                var data = new google.visualization.DataTable(json);
                var options = {
                    title: 'Response Graph of <?php echo $ufullname ?> for <?php echo $cname->fullname ?>',
                    legend: {position: 'top', alignment: 'center', textStyle: {fontSize: 14}},
                    hAxis: {
                        title: 'Question',
                        logScale: true,
                        textStyle: {color: 'blue'},
                    },
                    vAxis: {
                        title: 'Rank',
                        logScale: false,
                        textStyle: {color: 'blue'},
                        ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    },
                    colors: ['#097138', '#000066', '#a52714'],
                    height: 500,
                    width: '100%',
                    chartArea: {width: '85%', height: '75%'},
                    //series: {1: {type: "steppedArea", color: '#FF0000', visibleInLegend: false, areaOpacity: 0}}
                };
                var chart = new google.visualization.LineChart(document.getElementById('admin_curve_chart_beg'));
                // Wait for the chart to finish drawing before calling the getIm geURI() method.
                google.visualization.events.addListener(chart, 'ready', function () {
                    admin_curve_chart_beg.innerHTML = '<img id="imgstore" src="' + chart.getImageURI() + '">';
                    $.post("download_ajax.php", {imgsrc: chart.getImageURI(), CourseID: <?php echo $cidd ?>, UserId: <?php echo $userids ?>});
                });
                chart.draw(data, options);
            }
        </script>

        <?php
    } else {
        echo '<div class ="alert alert-info">No records found for this user.</div>';
    }
    // Admin Graph portion end.
// Student portion starts here.
} else {
    $usersid = $USER->id;
    $user = $DB->get_record_sql("SELECT * FROM {user} WHERE id=$usersid");
    $ufullname = fullname($user);
    // Beginning graph starts.
    $begsql = "SELECT DISTINCT qrr.id, q.course, qq.content AS blockcontent, qqc.content, qrr.rank, q.addtype, qrr.question_id AS quesid, qrr.response_id,
    qrr.choice_id, qr.username
    FROM {selfchecklist_question} qq
    JOIN {selfchecklist_response_rank} qrr ON qrr.question_id=qq.id
    JOIN {selfchecklist_response} qr ON qr.id=qrr.response_id
    JOIN {selfchecklist} q ON q.id=qq.survey_id
    JOIN {selfchecklist_quest_choice} qqc ON qqc.question_id=qrr.question_id
    WHERE q.addtype='beg' AND qr.username='$usersid' AND q.course=$cid AND qrr.choice_id=qqc.id
    ORDER BY qrr.id ASC";
    $begrs = $DB->get_records_sql($begsql);
    // Ending Graph Starts.
    $endsql = "SELECT DISTINCT qrr.id, q.course, qq.content AS blockcontent, qqc.content, qrr.rank, q.addtype, qrr.question_id AS quesid, qrr.response_id,
    qrr.choice_id, qr.username
    FROM {selfchecklist_question} qq
    JOIN {selfchecklist_response_rank} qrr ON qrr.question_id=qq.id
    JOIN {selfchecklist_response} qr ON qr.id=qrr.response_id
    JOIN {selfchecklist} q ON q.id=qq.survey_id
    JOIN {selfchecklist_quest_choice} qqc ON qqc.question_id=qrr.question_id
    WHERE q.addtype='end' AND qr.username='$usersid' AND q.course=$cid AND qrr.choice_id=qqc.id
    ORDER BY qrr.id ASC";
    $endrs = $DB->get_records_sql($endsql);
    $endranks = array();
    foreach ($endrs as $endrsvalue) {
        $endranks[] = $endrsvalue->rank;
    }
    echo '<h2>Response Table for ' . $ufullname . '</h2>';
    $checklistname = $DB->get_records_sql("SELECT name FROM {selfchecklist} WHERE course='$cid'");
    $i = 0;
    $reccount = count($checklistname);
    foreach ($checklistname as $chname) {
        $checklists[] = $chname->name;
    }
    echo '<h4 class="padbott">Checkists: ' . $checklists[0] . ' & ' . $checklists[1] . '</h4>';

    echo '<a href="' . $CFG->wwwroot . '/mod/selfchecklist/download.php?userid=' . $usersid . '&courseid=' . $cid . '">
    <div class="btn btn-success redbtn">Click to download as PDF</div>
    </a>';

    // Average rank portion.
    $begarrrank = array();
    foreach ($begrs as $begresult) {
        $begarrrank[] = $begresult->rank;
    }
    $begrankarr = $begarrrank;
    $endrankarr = $endranks;
    $arraysum = array_add_by_key($begrankarr, $endrankarr);
    foreach ($arraysum as $k => $a) {
        if (array_key_exists($k, $arraysum)) {
            $arraysum[$k] = $a / 2;
        } else {
            $arraysum[$k] = $a;
        }
        $arravg = $arraysum;
    }

    // Rank difference between End Ranks and Beginning Ranks.
    $diffArr = array_diff_by_key($endrankarr, $begrankarr);

    // Sum of begging ranks, ending ranks, avg ranks.
    $begrankAvg = array_sum($begrankarr) / 15;
    $endrankAvg = array_sum($endrankarr) / 15;
    $diffrankAvg = (array_sum($diffArr)) / 15;

    $table = new html_table();
    //$table->head = (array) get_strings(['blocks', 'qno', 'begranks', 'endranks', 'avgranks'], 'selfchecklist');
    $endi = 0;
    $avgi = 0;
    foreach ($begrs as $val) {
        $arrkeys = array_key_exists($endi, $endranks);
        if (!$arrkeys) {
            $endranks[$endi] = 0;
        }
        $arrkey = array_key_exists($avgi, $arravg);
        if (!$arrkey) {
            $endranks[$avgi] = 0;
        }
        $table->data[] = array(
            $val->blockcontent, $val->content, $val->rank, $endranks[$endi], $diffArr[$avgi]
        );
        $endi++;
        $avgi++;
    }
    $lastrow = array(
        array(
            0 => '<p>' . get_string('colavg', 'selfchecklist') . '<br></p>',
            1 => get_string('colavg1', 'selfchecklist'),
            2 => round($begrankAvg, 1),
            3 => round($endrankAvg, 1),
            4 => round($diffrankAvg, 1)
        )
    );
    $merge = array_merge($table->data, $lastrow);
    $tabobject = new html_table();
    $tabobject->attributes = array('class' => 'generaltable customtab');
    $tabobject->head = (array) get_strings(['blocks', 'qno', 'begranks', 'endranks', 'avgranks'], 'selfchecklist');
    foreach ($merge as $key => $value) {
        $tabobject->data[] = $value;
    }
    echo html_writer::table($tabobject);

    if ($begrs || $endrs) {
        $j = 0;
        foreach ($begrs as $begrsvalue) {
            $begranks = $begrsvalue->rank;
            $rankarr[] = $begrsvalue->rank;
            $courseid = $begrsvalue->course;
            $arr = $begrsvalue->content[0] . $begrsvalue->content[1];
            $qnum = $arr;
            $arrkey = array_key_exists($j, $endranks);
            if (!$arrkey) {
                $endranks[$j] = 0;
            }
            $graphdata[] = array('name' => "$qnum", 'begrank' => $begranks, 'endrank' => $endranks[$j], 'avgrank' => round($begrankAvg, 1));
            $j++;
        }
        $cidd = (int) $courseid;
        $cname = $DB->get_record('course', array('id' => $cid));
        echo '</head>
          <body>
            <div id="buttons" class="buttons"></div>
            <hr/>
            <div id="JSFiddle">
                <div id="curve_chart_beg"></div>
            </div>
          </body>
        </html>';
        ?>
        <script type="text/javascript">
            google.charts.setOnLoadCallback(drawChart);
            function drawChart() {
                var json = <?php echo json_encode(chart($graphdata)); ?>;
                var data = new google.visualization.DataTable(json);
                var options = {
                    title: 'Response Graph for <?php echo $cname->fullname ?>',
                    legend: {position: 'top', alignment: 'center', textStyle: {fontSize: 14}},
                    hAxis: {
                        title: 'Question',
                        logScale: true,
                        textStyle: {color: 'blue'}
                    },
                    vAxis: {
                        title: 'Rank',
                        logScale: false,
                        textStyle: {color: 'blue'},
                        ticks: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10],
                    },
                    colors: ['#097138', '#000066', '#a52714'],
                    height: 500,
                    width: '100%',
                    chartArea: {width: '85%', height: '75%'}
                };
                var chart = new google.visualization.LineChart(document.getElementById('curve_chart_beg'));
                // Wait for the chart to finish drawing before calling the getIm geURI() method.
                google.visualization.events.addListener(chart, 'ready', function () {
                    curve_chart_beg.innerHTML = '<img id="imgstore" src="' + chart.getImageURI() + '">';
                    $.post("download_ajax.php", {imgsrc: chart.getImageURI(), CourseID: <?php echo $cid ?>, UserId: <?php echo $usersid ?>});
                });
                chart.draw(data, options);
            }
        </script>
        <?php
    } else {
        echo '<div class="alert alert-info">No Data Found</div>';
    }
}// End of else.
echo $OUTPUT->footer();
