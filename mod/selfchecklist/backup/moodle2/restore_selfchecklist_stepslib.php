<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package moodlecore
 * @subpackage backup-moodle2
 * @copyright 2010 onwards Eloy Lafuente (stronk7) {@link http://stronk7.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Define all the restore steps that will be used by the restore_selfchecklist_activity_task
 */

/**
 * Structure step to restore one selfchecklist activity
 */
class restore_selfchecklist_activity_structure_step extends restore_activity_structure_step {

    protected function define_structure() {

        $paths = array();
        $userinfo = $this->get_setting_value('userinfo');

        $paths[] = new restore_path_element('selfchecklist', '/activity/selfchecklist');
        $paths[] = new restore_path_element('selfchecklist_survey', '/activity/selfchecklist/surveys/survey');
        $paths[] = new restore_path_element('selfchecklist_fb_sections',
                        '/activity/selfchecklist/surveys/survey/fb_sections/fb_section');
        $paths[] = new restore_path_element('selfchecklist_feedback',
                        '/activity/selfchecklist/surveys/survey/fb_sections/fb_section/feedbacks/feedback');
        $paths[] = new restore_path_element('selfchecklist_question',
                        '/activity/selfchecklist/surveys/survey/questions/question');
        $paths[] = new restore_path_element('selfchecklist_quest_choice',
                        '/activity/selfchecklist/surveys/survey/questions/question/quest_choices/quest_choice');

        if ($userinfo) {
            $paths[] = new restore_path_element('selfchecklist_attempt', '/activity/selfchecklist/attempts/attempt');
            $paths[] = new restore_path_element('selfchecklist_response',
                            '/activity/selfchecklist/attempts/attempt/responses/response');
            $paths[] = new restore_path_element('selfchecklist_response_bool',
                            '/activity/selfchecklist/attempts/attempt/responses/response/response_bools/response_bool');
            $paths[] = new restore_path_element('selfchecklist_response_date',
                            '/activity/selfchecklist/attempts/attempt/responses/response/response_dates/response_date');
            $paths[] = new restore_path_element('selfchecklist_response_multiple',
                            '/activity/selfchecklist/attempts/attempt/responses/response/response_multiples/response_multiple');
            $paths[] = new restore_path_element('selfchecklist_response_other',
                            '/activity/selfchecklist/attempts/attempt/responses/response/response_others/response_other');
            $paths[] = new restore_path_element('selfchecklist_response_rank',
                            '/activity/selfchecklist/attempts/attempt/responses/response/response_ranks/response_rank');
            $paths[] = new restore_path_element('selfchecklist_response_single',
                            '/activity/selfchecklist/attempts/attempt/responses/response/response_singles/response_single');
            $paths[] = new restore_path_element('selfchecklist_response_text',
                            '/activity/selfchecklist/attempts/attempt/responses/response/response_texts/response_text');
        }

        // Return the paths wrapped into standard activity structure.
        return $this->prepare_activity_structure($paths);
    }

    protected function process_selfchecklist($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->course = $this->get_courseid();

        $data->timemodified = $this->apply_date_offset($data->timemodified);

        // Insert the selfchecklist record.
        $newitemid = $DB->insert_record('selfchecklist', $data);
        // Immediately after inserting "activity" record, call this.
        $this->apply_activity_instance($newitemid);
    }

    protected function process_selfchecklist_survey($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->owner = $this->get_courseid();

        // Insert the selfchecklist_survey record.
        $newitemid = $DB->insert_record('selfchecklist_survey', $data);
        $this->set_mapping('selfchecklist_survey', $oldid, $newitemid, true);

        // Update the selfchecklist record we just created with the new survey id.
        $DB->set_field('selfchecklist', 'sid', $newitemid, array('id' => $this->get_new_parentid('selfchecklist')));
    }

    protected function process_selfchecklist_question($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->survey_id = $this->get_new_parentid('selfchecklist_survey');

        if (isset($data->dependquestion)) {
            // Dependquestion.
            $data->dependquestion = $this->get_mappingid('selfchecklist_question', $data->dependquestion);

            // Dependchoice.
            // Only change mapping for RADIO and DROP question types, not for YESNO question.
            $dependquestion = $DB->get_record('selfchecklist_question', array('id' => $data->dependquestion), $fields = 'type_id');
            if (is_object($dependquestion)) {
                if ($dependquestion->type_id != 1) {
                    $data->dependchoice = $this->get_mappingid('selfchecklist_quest_choice', $data->dependchoice);
                }
            }
        }

        // Insert the selfchecklist_question record.
        $newitemid = $DB->insert_record('selfchecklist_question', $data);
        $this->set_mapping('selfchecklist_question', $oldid, $newitemid, true);
    }

    protected function process_selfchecklist_fb_sections($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->survey_id = $this->get_new_parentid('selfchecklist_survey');

        // If this selfchecklist has separate sections feedbacks.
        if (isset($data->scorecalculation)) {
            $scorecalculation = unserialize($data->scorecalculation);
            $newscorecalculation = array();
            foreach ($scorecalculation as $key => $qid) {
                $newqid = $this->get_mappingid('selfchecklist_question', $key);
                $newscorecalculation[$newqid] = null;
            }
            $data->scorecalculation = serialize($newscorecalculation);
        }

        // Insert the selfchecklist_fb_sections record.
        $newitemid = $DB->insert_record('selfchecklist_fb_sections', $data);
        $this->set_mapping('selfchecklist_fb_sections', $oldid, $newitemid, true);
    }

    protected function process_selfchecklist_feedback($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->section_id = $this->get_new_parentid('selfchecklist_fb_sections');

        // Insert the selfchecklist_feedback record.
        $newitemid = $DB->insert_record('selfchecklist_feedback', $data);
        $this->set_mapping('selfchecklist_feedback', $oldid, $newitemid, true);
    }

    protected function process_selfchecklist_quest_choice($data) {
        global $CFG, $DB;

        $data = (object)$data;

        // Replace the = separator with :: separator in quest_choice content.
        // This fixes radio button options using old "value"="display" formats.
        require_once($CFG->dirroot.'/mod/selfchecklist/locallib.php');

        if (($data->value == null || $data->value == 'NULL') && !preg_match("/^([0-9]{1,3}=.*|!other=.*)$/", $data->content)) {
            $content = selfchecklist_choice_values($data->content);
            if ($pos = strpos($content->text, '=')) {
                $data->content = str_replace('=', '::', $content->text);
            }
        }

        $oldid = $data->id;
        $data->question_id = $this->get_new_parentid('selfchecklist_question');

        if (isset($data->dependquestion)) {
            // Dependquestion.
            $data->dependquestion = $this->get_mappingid('selfchecklist_question', $data->dependquestion);

            // Dependchoice.
            // Only change mapping for RADIO and DROP question types, not for YESNO question.
            $dependquestion = $DB->get_record('selfchecklist_question',
                            array('id' => $data->dependquestion), $fields = 'type_id');
            if (is_object($dependquestion)) {
                if ($dependquestion->type_id != 1) {
                    $data->dependchoice = $this->get_mappingid('selfchecklist_quest_choice', $data->dependchoice);
                }
            }
        }

        // Insert the selfchecklist_quest_choice record.
        $newitemid = $DB->insert_record('selfchecklist_quest_choice', $data);
        $this->set_mapping('selfchecklist_quest_choice', $oldid, $newitemid);
    }

    protected function process_selfchecklist_attempt($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->qid = $this->get_new_parentid('selfchecklist');
        $data->userid = $this->get_mappingid('user', $data->userid);

        // Insert the selfchecklist_attempts record.
        $newitemid = $DB->insert_record('selfchecklist_attempts', $data);
        $this->set_mapping('selfchecklist_attempt', $oldid, $newitemid);
    }

    protected function process_selfchecklist_response($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->survey_id = $this->get_mappingid('selfchecklist_survey', $data->survey_id);
        $data->username = $this->get_mappingid('user', $data->username);

        // Insert the selfchecklist_response record.
        $newitemid = $DB->insert_record('selfchecklist_response', $data);
        $this->set_mapping('selfchecklist_response', $oldid, $newitemid);

        // Update the selfchecklist_attempts record we just created with the new response id.
        $DB->set_field('selfchecklist_attempts', 'rid', $newitemid,
                        array('id' => $this->get_new_parentid('selfchecklist_attempt')));
    }

    protected function process_selfchecklist_response_bool($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('selfchecklist_response');
        $data->question_id = $this->get_mappingid('selfchecklist_question', $data->question_id);

        // Insert the selfchecklist_response_bool record.
        $newitemid = $DB->insert_record('selfchecklist_response_bool', $data);
    }

    protected function process_selfchecklist_response_date($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('selfchecklist_response');
        $data->question_id = $this->get_mappingid('selfchecklist_question', $data->question_id);

        // Insert the selfchecklist_response_date record.
        $newitemid = $DB->insert_record('selfchecklist_response_date', $data);
    }

    protected function process_selfchecklist_response_multiple($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('selfchecklist_response');
        $data->question_id = $this->get_mappingid('selfchecklist_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('selfchecklist_quest_choice', $data->choice_id);

        // Insert the selfchecklist_resp_multiple record.
        $newitemid = $DB->insert_record('selfchecklist_resp_multiple', $data);
    }

    protected function process_selfchecklist_response_other($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('selfchecklist_response');
        $data->question_id = $this->get_mappingid('selfchecklist_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('selfchecklist_quest_choice', $data->choice_id);

        // Insert the selfchecklist_response_other record.
        $newitemid = $DB->insert_record('selfchecklist_response_other', $data);
    }

    protected function process_selfchecklist_response_rank($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('selfchecklist_response');
        $data->question_id = $this->get_mappingid('selfchecklist_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('selfchecklist_quest_choice', $data->choice_id);

        // Insert the selfchecklist_response_rank record.
        $newitemid = $DB->insert_record('selfchecklist_response_rank', $data);
    }

    protected function process_selfchecklist_response_single($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('selfchecklist_response');
        $data->question_id = $this->get_mappingid('selfchecklist_question', $data->question_id);
        $data->choice_id = $this->get_mappingid('selfchecklist_quest_choice', $data->choice_id);

        // Insert the selfchecklist_resp_single record.
        $newitemid = $DB->insert_record('selfchecklist_resp_single', $data);
    }

    protected function process_selfchecklist_response_text($data) {
        global $DB;

        $data = (object)$data;
        $oldid = $data->id;
        $data->response_id = $this->get_new_parentid('selfchecklist_response');
        $data->question_id = $this->get_mappingid('selfchecklist_question', $data->question_id);

        // Insert the selfchecklist_response_text record.
        $newitemid = $DB->insert_record('selfchecklist_response_text', $data);
    }

    protected function after_execute() {
        // Add selfchecklist related files, no need to match by itemname (just internally handled context).
        $this->add_related_files('mod_selfchecklist', 'intro', null);
        $this->add_related_files('mod_selfchecklist', 'info', 'selfchecklist_survey');
        $this->add_related_files('mod_selfchecklist', 'thankbody', 'selfchecklist_survey');
        $this->add_related_files('mod_selfchecklist', 'feedbacknotes', 'selfchecklist_survey');
        $this->add_related_files('mod_selfchecklist', 'question', 'selfchecklist_question');
        $this->add_related_files('mod_selfchecklist', 'sectionheading', 'selfchecklist_fb_sections');
        $this->add_related_files('mod_selfchecklist', 'feedback', 'selfchecklist_feedback');
    }
}
