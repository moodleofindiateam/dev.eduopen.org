<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the parent class for selfchecklist question types.
 *
 * @author Mike Churchward
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package questiontypes
 */
/**
 * Class for describing a question
 *
 * @author Mike Churchward
 * @package questiontypes
 */
// Constants.
define('SELFQUESCHOOSE', 0);
define('SELFQUESYESNO', 1);
define('SELFQUESTEXT', 2);
define('SELFQUESESSAY', 3);
define('SELFQUESRADIO', 4);
define('SELFQUESCHECK', 5);
define('SELFQUESDROP', 6);
define('SELFQUESRATE', 8);
define('SELFQUESDATE', 9);
define('SELFQUESNUMERIC', 10);
define('SELFQUESPAGEBREAK', 99);
define('SELFQUESSECTIONTEXT', 100);

GLOBAL $qtypenames;
$qtypenames = array(
    SELFQUESYESNO => 'yesno',
    SELFQUESTEXT => 'text',
    SELFQUESESSAY => 'essay',
    SELFQUESRADIO => 'radio',
    SELFQUESCHECK => 'check',
    SELFQUESDROP => 'drop',
    SELFQUESRATE => 'rate',
    SELFQUESDATE => 'date',
    SELFQUESNUMERIC => 'numeric',
    SELFQUESPAGEBREAK => 'pagebreak',
    SELFQUESSECTIONTEXT => 'sectiontext'
);
GLOBAL $idcounter, $CFG;
$idcounter = 0;

require_once($CFG->dirroot . '/mod/selfchecklist/locallib.php');

class selfchecklist_question {

    // Class Properties.
    /*
     * The database id of this question.
     * @var int $id
     */
    public $id = 0;

    /**
     * The database id of the survey this question belongs to.
     * @var int $survey_id
     */
    public $surveyid = 0;

    /**
     * The name of this question.
     * @var string $name
     */
    public $name = '';

    /**
     * The alias of the number of this question.
     * @var string $numberalias
     */

    /**
     * The name of the question type.
     * @var string $type
     */
    public $type = '';

    /**
     * Array holding any choices for this question.
     * @var array $choices
     */
    public $choices = array();

    /**
     * The table name for responses.
     * @var string $response_table
     */
    public $responsetable = '';

    /**
     * The length field.
     * @var int $length
     */
    public $length = 0;

    /**
     * The precision field.
     * @var int $precise
     */
    public $precise = 0;

    /**
     * Position in the selfchecklist
     * @var int $position
     */
    public $position = 0;

    /**
     * The question's content.
     * @var string $content
     */
    public $content = '';

    /**
     * The list of all question's choices.
     * @var string $allchoices
     */
    public $allchoices = '';

    /**
     * The required flag.
     * @var boolean $required
     */
    public $required = 'n';

    /**
     * The deleted flag.
     * @var boolean $deleted
     */
    public $deleted = 'n';

    // Class Methods.

    /**
     * The class constructor
     *
     */
    public function __construct($id = 0, $question = null, $context = null) {
        global $DB;
        static $qtypes = null;

        if (is_null($qtypes)) {
            $qtypes = $DB->get_records('selfchecklist_question_type', array(), 'typeid', 'typeid, type, has_choices, response_table');
        }

        if ($id) {
            $question = $DB->get_record('selfchecklist_question', array('id' => $id));
        }

        if (is_object($question)) {
            $this->id = $question->id;
            $this->survey_id = $question->survey_id;
            $this->name = $question->name;
            // Added f