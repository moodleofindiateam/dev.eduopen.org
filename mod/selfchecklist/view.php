<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once("../../config.php");
require_once($CFG->libdir . '/completionlib.php');
require_once($CFG->dirroot.'/mod/selfchecklist/selfchecklist.class.php');

if (!isset($SESSION->selfchecklist)) {
    $SESSION->selfchecklist = new stdClass();
}
$SESSION->selfchecklist->current_tab = 'view';

$id = optional_param('id', null, PARAM_INT);    // Course Module ID.
$a = optional_param('a', null, PARAM_INT);      // Or selfchecklist ID.

$sid = optional_param('sid', null, PARAM_INT);  // Survey id.

if ($id) {
    if (! $cm = get_coursemodule_from_id('selfchecklist', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record("course", array("id" => $cm->course))) {
        print_error('coursemisconf');
    }

    if (! $selfchecklist = $DB->get_record("selfchecklist", array("id" => $cm->instance))) {
        print_error('invalidcoursemodule');
    }

} else {
    if (! $selfchecklist = $DB->get_record("selfchecklist", array("id" => $a))) {
        print_error('invalidcoursemodule');
    }
    if (! $course = $DB->get_record("course", array("id" => $selfchecklist->course))) {
        print_error('coursemisconf');
    }
    if (! $cm = get_coursemodule_from_instance("selfchecklist", $selfchecklist->id, $course->id)) {
        print_error('invalidcoursemodule');
    }
}

// Check login and get context.
require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);

$url = new moodle_url($CFG->wwwroot.'/mod/selfchecklist/view.php');
if (isset($id)) {
    $url->param('id', $id);
} else {
    $url->param('a', $a);
}
if (isset($sid)) {
    $url->param('sid', $sid);
}

$PAGE->set_url($url);
$PAGE->set_context($context);
$selfchecklist = new selfchecklist(0, $selfchecklist, $course, $cm);

$PAGE->set_title(format_string($selfchecklist->name));

$PAGE->set_heading(format_string($course->fullname));

echo $OUTPUT->header();

echo $OUTPUT->heading(format_text($selfchecklist->name));

// Print the main part of the page.
if ($selfchecklist->intro) {
    echo $OUTPUT->box(format_module_intro('selfchecklist', $selfchecklist, $cm->id), 'generalbox', 'intro');
}

echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');

$cm = $selfchecklist->cm;
$currentgroupid = groups_get_activity_group($cm);
if (!groups_is_member($currentgroupid, $USER->id)) {
    $currentgroupid = 0;
}

if (!$selfchecklist->is_active()) {
    if ($selfchecklist->capabilities->manage) {
        $msg = 'removenotinuse';
    } else {
        $msg = 'notavail';
    }
    echo '<div class="message">'
    .get_string($msg, 'selfchecklist')
    .'</div>';

} else if (!$selfchecklist->is_open()) {
    echo '<div class="message">'
    .get_string('notopen', 'selfchecklist', userdate($selfchecklist->opendate))
    .'</div>';
} else if ($selfchecklist->is_closed()) {
    echo '<div class="message">'
    .get_string('closed', 'selfchecklist', userdate($selfchecklist->closedate))
    .'</div>';
} else if ($selfchecklist->survey->realm == 'template') {
    print_string('templatenotviewable', 'selfchecklist');
    echo $OUTPUT->box_end();
    echo $OUTPUT->footer($selfchecklist->course);
    exit();
} else if (!$selfchecklist->user_is_eligible($USER->id)) {
    if ($selfchecklist->questions) {
        echo '<div class="message">'.get_string('noteligible', 'selfchecklist').'</div>';
    }
} else if (!$selfchecklist->user_can_take($USER->id)) {
    switch ($selfchecklist->qtype) {
        case SELFCHECKLISTDAILY:
            $msgstring = ' '.get_string('today', 'selfchecklist');
            break;
        case SELFCHECKLISTWEEKLY:
            $msgstring = ' '.get_string('thisweek', 'selfchecklist');
            break;
        case SELFCHECKLISTMONTHLY:
            $msgstring = ' '.get_string('thismonth', 'selfchecklist');
            break;
        default:
            $msgstring = '';
            break;
    }
    echo ('<div class="message">'.get_string("alreadyfilled", "selfchecklist", $msgstring).'</div>');
} else if ($selfchecklist->user_can_take($USER->id)) {
    $select = 'survey_id = '.$selfchecklist->survey->id.' AND username = \''.$USER->id.'\' AND complete = \'n\'';
    $resume = $DB->get_record_select('selfchecklist_response', $select, null) !== false;
    if (!$resume) {
        $complete = get_string('answerquestions', 'selfchecklist');
    } else {
        $complete = get_string('resumesurvey', 'selfchecklist');
    }
    if ($selfchecklist->questions) { // Sanity check.
        echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/selfchecklist/complete.php?'.
        'id='.$selfchecklist->cm->id.'&resume='.$resume).'">'.$complete.'</a>';
    }
}
if ($selfchecklist->is_active() && !$selfchecklist->questions) {
    echo '<p>'.get_string('noneinuse', 'selfchecklist').'</p>';
}
if ($selfchecklist->is_active() && $selfchecklist->capabilities->editquestions && !$selfchecklist->questions) { // Sanity check.
    echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/selfchecklist/questions.php?'.
                'id='.$selfchecklist->cm->id).'">'.'<strong>'.get_string('addquestions', 'selfchecklist').'</strong></a>';
}
echo $OUTPUT->box_end();
if (isguestuser()) {
    $output = '';
    $guestno = html_writer::tag('p', get_string('noteligible', 'selfchecklist'));
    $liketologin = html_writer::tag('p', get_string('liketologin'));
    $output .= $OUTPUT->confirm($guestno."\n\n".$liketologin."\n", get_login_url(),
            get_referer(false));
    echo $output;
}

// Log this course module view.
// Needed for the event logging.
$context = context_module::instance($selfchecklist->cm->id);
$anonymous = $selfchecklist->respondenttype == 'anonymous';

$event = \mod_selfchecklist\event\course_module_viewed::create(array(
                'objectid' => $selfchecklist->id,
                'anonymous' => $anonymous,
                'context' => $context
));
$event->trigger();

$usernumresp = $selfchecklist->count_submissions($USER->id);

if ($selfchecklist->capabilities->readownresponses && ($usernumresp > 0)) {
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    $argstr = 'instance='.$selfchecklist->id.'&user='.$USER->id;
    if ($usernumresp > 1) {
        $titletext = get_string('viewyourresponses', 'selfchecklist', $usernumresp);
    } else {
        $titletext = get_string('yourresponse', 'selfchecklist');
        $argstr .= '&byresponse=1&action=vresp';
    }

    echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/selfchecklist/myreport.php?'.
        $argstr).'">'.$titletext.'</a>';
    echo $OUTPUT->box_end();
}

if ($survey = $DB->get_record('selfchecklist_survey', array('id' => $selfchecklist->sid))) {
    $owner = (trim($survey->owner) == trim($course->id));
} else {
    $survey = false;
    $owner = true;
}
$numresp = $selfchecklist->count_submissions();

// Number of Responses in currently selected group (or all participants etc.).
if (isset($SESSION->selfchecklist->numselectedresps)) {
    $numselectedresps = $SESSION->selfchecklist->numselectedresps;
} else {
    $numselectedresps = $numresp;
}

// If selfchecklist is set to separate groups, prevent user who is not member of any group
// to view All responses.
$canviewgroups = true;
$groupmode = groups_get_activity_groupmode($cm, $course);
if ($groupmode == 1) {
    $canviewgroups = groups_has_membership($cm, $USER->id);;
}

$canviewallgroups = has_capability('moodle/site:accessallgroups', $context);
if (( (
            // Teacher or non-editing teacher (if can view all groups).
            $canviewallgroups ||
            // Non-editing teacher (with canviewallgroups capability removed), if member of a group.
            ($canviewgroups && $selfchecklist->capabilities->readallresponseanytime))
            && $numresp > 0 && $owner && $numselectedresps > 0) ||
            $selfchecklist->capabilities->readallresponses && ($numresp > 0) && $canviewgroups &&
            ($selfchecklist->resp_view == SELFCHECKLIST_STUDENTVIEWRESPONSES_ALWAYS ||
                    ($selfchecklist->resp_view == SELFCHECKLIST_STUDENTVIEWRESPONSES_WHENCLOSED
                            && $selfchecklist->is_closed()) ||
                    ($selfchecklist->resp_view == SELFCHECKLIST_STUDENTVIEWRESPONSES_WHENANSWERED
                            && $usernumresp > 0)) &&
            $selfchecklist->is_survey_owner()) {
    echo $OUTPUT->box_start('generalbox boxaligncenter boxwidthwide');
    $argstr = 'instance='.$selfchecklist->id.'&group='.$currentgroupid;
    echo '<a href="'.$CFG->wwwroot.htmlspecialchars('/mod/selfchecklist/report.php?'.
            $argstr).'">'.get_string('viewallresponses', 'selfchecklist').'</a>';
    echo $OUTPUT->box_end();
}

echo $OUTPUT->footer();
