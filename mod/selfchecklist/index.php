<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This script lists all the instances of selfchecklist in a particular course
 *
 * @package    mod
 * @subpackage selfchecklist
 * @copyright  1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


require_once("../../config.php");
require_once($CFG->dirroot.'/mod/selfchecklist/locallib.php');

$id = required_param('id', PARAM_INT);
$PAGE->set_url('/mod/selfchecklist/index.php', array('id' => $id));
if (! $course = $DB->get_record('course', array('id' => $id))) {
    print_error('incorrectcourseid', 'selfchecklist');
}
$coursecontext = context_course::instance($id);
require_login($course->id);
$PAGE->set_pagelayout('incourse');

$event = \mod_selfchecklist\event\course_module_instance_list_viewed::create(array(
                'context' => context_course::instance($course->id)));
$event->trigger();

// Print the header.
$strselfchecklists = get_string("modulenameplural", "selfchecklist");
$PAGE->navbar->add($strselfchecklists);
$PAGE->set_title("$course->shortname: $strselfchecklists");
$PAGE->set_heading(format_string($course->fullname));
echo $OUTPUT->header();

// Get all the appropriate data.
if (!$selfchecklists = get_all_instances_in_course("selfchecklist", $course)) {
    notice(get_string('thereareno', 'moodle', $strselfchecklists), "../../course/view.php?id=$course->id");
    die;
}

// Check if we need the closing date header.
$showclosingheader = false;
foreach ($selfchecklists as $selfchecklist) {
    if ($selfchecklist->closedate != 0) {
        $showclosingheader = true;
    }
    if ($showclosingheader) {
        break;
    }
}

// Configure table for displaying the list of instances.
$headings = array(get_string('name'));
$align = array('left');

if ($showclosingheader) {
    array_push($headings, get_string('selfchecklistcloses', 'selfchecklist'));
    array_push($align, 'left');
}

array_unshift($headings, get_string('sectionname', 'format_'.$course->format));
array_unshift($align, 'left');

$showing = '';

// Current user role == admin or teacher.
if (has_capability('mod/selfchecklist:viewsingleresponse', $coursecontext)) {
    array_push($headings, get_string('responses', 'selfchecklist'));
    array_push($align, 'center');
    $showing = 'stats';
    array_push($headings, get_string('realm', 'selfchecklist'));
    array_push($align, 'left');
    // Current user role == student.
} else if (has_capability('mod/selfchecklist:submit', $coursecontext)) {
    array_push($headings, get_string('status'));
    array_push($align, 'left');
    $showing = 'responses';
}

$table = new html_table();
$table->head = $headings;
$table->align = $align;

// Populate the table with the list of instances.
$currentsection = '';
foreach ($selfchecklists as $selfchecklist) {
    $cmid = $selfchecklist->coursemodule;
    $data = array();
    $realm = $DB->get_field('selfchecklist_survey', 'realm', array('id' => $selfchecklist->sid));

    $realm = $DB->get_field('selfchecklist_survey', 'realm', array('id' => $selfchecklist->sid));
    // Template surveys should NOT be displayed as an activity to students.
    if (!($realm == 'template' && !has_capability('mod/selfchecklist:manage', context_module::instance($cmid)))) {
        // Section number if necessary.
        $strsection = '';
        if ($selfchecklist->section != $currentsection) {
            $strsection = get_section_name($course, $selfchecklist->section);
            $currentsection = $selfchecklist->section;
        }
        $data[] = $strsection;
        // Show normal if the mod is visible.
        $class = '';
        if (!$selfchecklist->visible) {
            $class = ' class="dimmed"';
        }
        $data[] = "<a$class href=\"view.php?id=$cmid\">$selfchecklist->name</a>";

        // Close date.
        if ($selfchecklist->closedate) {
            $data[] = userdate($selfchecklist->closedate);
        } else if ($showclosingheader) {
            $data[] = '';
        }

        if ($showing == 'responses') {
            $status = '';
            if ($responses = selfchecklist_get_user_responses($selfchecklist->sid, $USER->id, $complete = false)) {
                foreach ($responses as $response) {
                    if ($response->complete == 'y') {
                        $status .= get_string('submitted', 'selfchecklist').' '.userdate($response->submitted).'<br />';
                    } else {
                        $status .= get_string('attemptstillinprogress', 'selfchecklist').' '.
                            userdate($response->submitted).'<br />';
                    }
                }
            }
            $data[] = $status;
        } else if ($showing == 'stats') {
            $data[] = $DB->count_records('selfchecklist_response', array('survey_id' => $selfchecklist->sid, 'complete' => 'y'));
            if ($survey = $DB->get_record('selfchecklist_survey', array('id' => $selfchecklist->sid))) {
                // For a public selfchecklist, look for the original public selfchecklist that it is based on.
                if ($survey->realm == 'public') {
                    $strpreview = get_string('preview_selfchecklist', 'selfchecklist');
                    if ($survey->owner != $course->id) {
                        $publicoriginal = '';
                        $originalcourse = $DB->get_record('course', array('id' => $survey->owner));
                        $originalcoursecontext = context_course::instance($survey->owner);
                        $originalselfchecklist = $DB->get_record('selfchecklist',
                                        array('sid' => $survey->id, 'course' => $survey->owner));
                        $cm = get_coursemodule_from_instance("selfchecklist", $originalselfchecklist->id, $survey->owner);
                        $context = context_course::instance($survey->owner, MUST_EXIST);
                        $canvieworiginal = has_capability('mod/selfchecklist:preview', $context, $USER->id, true);
                        // If current user can view selfchecklists in original course,
                        // provide a link to the original public selfchecklist.
                        if ($canvieworiginal) {
                            $publicoriginal = '<br />'.get_string('publicoriginal', 'selfchecklist').'&nbsp;'.
                                '<a href="'.$CFG->wwwroot.'/mod/selfchecklist/preview.php?id='.
                                $cm->id.'" title="'.$strpreview.']">'.$originalselfchecklist->name.' ['.
                                $originalcourse->fullname.']</a>';
                        } else {
                            // If current user is not enrolled as teacher in original course,
                            // only display the original public selfchecklist's name and course name.
                            $publicoriginal = '<br />'.get_string('publicoriginal', 'selfchecklist').'&nbsp;'.
                                $originalselfchecklist->name.' ['.$originalcourse->fullname.']';
                        }
                        $data[] = get_string($realm, 'selfchecklist').' '.$publicoriginal;
                    } else {
                        // Original public selfchecklist was created in current course.
                        // Find which courses it is used in.
                        $publiccopy = '';
                        $select = 'course != '.$course->id.' AND sid = '.$selfchecklist->sid;
                        if ($copies = $DB->get_records_select('selfchecklist', $select, null,
                                $sort = 'course ASC', $fields = 'id, course, name')) {
                            foreach ($copies as $copy) {
                                $copycourse = $DB->get_record('course', array('id' => $copy->course));
                                $select = 'course = '.$copycourse->id.' AND sid = '.$selfchecklist->sid;
                                $copyselfchecklist = $DB->get_record('selfchecklist',
                                    array('id' => $copy->id, 'sid' => $survey->id, 'course' => $copycourse->id));
                                $cm = get_coursemodule_from_instance("selfchecklist", $copyselfchecklist->id, $copycourse->id);
                                $context = context_course::instance($copycourse->id, MUST_EXIST);
                                $canviewcopy = has_capability('mod/selfchecklist:view', $context, $USER->id, true);
                                if ($canviewcopy) {
                                    $publiccopy .= '<br />'.get_string('publiccopy', 'selfchecklist').'&nbsp;:&nbsp;'.
                                        '<a href = "'.$CFG->wwwroot.'/mod/selfchecklist/preview.php?id='.
                                        $cm->id.'" title = "'.$strpreview.'">'.
                                        $copyselfchecklist->name.' ['.$copycourse->fullname.']</a>';
                                } else {
                                    // If current user does not have "view" capability in copy course,
                                    // only display the copied public selfchecklist's name and course name.
                                    $publiccopy .= '<br />'.get_string('publiccopy', 'selfchecklist').'&nbsp;:&nbsp;'.
                                        $copyselfchecklist->name.' ['.$copycourse->fullname.']';
                                }
                            }
                        }
                        $data[] = get_string($realm, 'selfchecklist').' '.$publiccopy;
                    }
                } else {
                    $data[] = get_string($realm, 'selfchecklist');
                }
            } else {
                // If a selfchecklist is a copy of a public selfchecklist which has been deleted.
                $data[] = get_string('removenotinuse', 'selfchecklist');
            }
        }
    }
    $table->data[] = $data;
} // End of loop over selfchecklist instances.

echo html_writer::table($table);

// Finish the page.
echo $OUTPUT->footer();