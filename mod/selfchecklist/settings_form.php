<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * print the form to add or edit a selfchecklist-instance
 *
 * @author Mike Churchward
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package selfchecklist
 */

require_once($CFG->dirroot.'/course/moodleform_mod.php');

class selfchecklist_settings_form extends moodleform {

    public function definition() {
        global $selfchecklist, $selfchecklistrealms, $CFG;

        $mform    =& $this->_form;

        $mform->addElement('header', 'contenthdr', get_string('contentoptions', 'selfchecklist'));

        $capabilities = selfchecklist_load_capabilities($selfchecklist->cm->id);
        if (!$capabilities->createtemplates) {
            unset($selfchecklistrealms['template']);
        }
        if (!$capabilities->createpublic) {
            unset($selfchecklistrealms['public']);
        }
        if (isset($selfchecklistrealms['public']) || isset($selfchecklistrealms['template'])) {
            $mform->addElement('select', 'realm', get_string('realm', 'selfchecklist'), $selfchecklistrealms);
            $mform->setDefault('realm', $selfchecklist->survey->realm);
            $mform->addHelpButton('realm', 'realm', 'selfchecklist');
        } else {
            $mform->addElement('hidden', 'realm', 'private');
        }
        $mform->setType('realm', PARAM_RAW);

        $mform->addElement('text', 'title', get_string('title', 'selfchecklist'), array('size' => '60'));
        $mform->setDefault('title', $selfchecklist->survey->title);
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', null, 'required', null, 'client');
        $mform->addHelpButton('title', 'title', 'selfchecklist');

        $mform->addElement('text', 'subtitle', get_string('subtitle', 'selfchecklist'), array('size' => '60'));
        $mform->setDefault('subtitle', $selfchecklist->survey->subtitle);
        $mform->setType('subtitle', PARAM_TEXT);
        $mform->addHelpButton('subtitle', 'subtitle', 'selfchecklist');

        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'trusttext' => true);
        $mform->addElement('editor', 'info', get_string('additionalinfo', 'selfchecklist'), null, $editoroptions);
        $mform->setDefault('info', $selfchecklist->survey->info);
        $mform->setType('info', PARAM_RAW);
        $mform->addHelpButton('info', 'additionalinfo', 'selfchecklist');

        $mform->addElement('header', 'submithdr', get_string('submitoptions', 'selfchecklist'));

        $mform->addElement('text', 'thanks_page', get_string('url', 'selfchecklist'), array('size' => '60'));
        $mform->setType('thanks_page', PARAM_TEXT);
        $mform->setDefault('thanks_page', $selfchecklist->survey->thanks_page);
        $mform->addHelpButton('thanks_page', 'url', 'selfchecklist');

        $mform->addElement('static', 'confmes', get_string('confalts', 'selfchecklist'));
        $mform->addHelpButton('confmes', 'confpage', 'selfchecklist');

        $mform->addElement('text', 'thank_head', get_string('headingtext', 'selfchecklist'), array('size' => '30'));
        $mform->setType('thank_head', PARAM_TEXT);
        $mform->setDefault('thank_head', $selfchecklist->survey->thank_head);

        $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'trusttext' => true);
        $mform->addElement('editor', 'thank_body', get_string('bodytext', 'selfchecklist'), null, $editoroptions);
        $mform->setType('thank_body', PARAM_RAW);
        $mform->setDefault('thank_body', $selfchecklist->survey->thank_body);

        $mform->addElement('text', 'email', get_string('email', 'selfchecklist'), array('size' => '75'));
        $mform->setType('email', PARAM_TEXT);
        $mform->setDefault('email', $selfchecklist->survey->email);
        $mform->addHelpButton('email', 'sendemail', 'selfchecklist');

        $defaultsections = get_config('selfchecklist', 'maxsections');

        // We cannot have more sections than available (required) questions with a choice value.
        $nbquestions = 0;
        foreach ($selfchecklist->questions as $question) {
            $qtype = $question->type_id;
            $qname = $question->name;
            $required = $question->required;
            // Question types accepted for feedback; QUESRATE ok except noduplicates.
            if (($qtype == QUESRADIO || $qtype == QUESDROP || ($qtype == QUESRATE && $question->precise != 2))
                            && $required == 'y' && $qname != '') {
                foreach ($question->choices as $choice) {
                    if (isset($choice->value) && $choice->value != null && $choice->value != 'NULL') {
                        $nbquestions ++;
                        break;
                    }
                }
            }
            if ($qtype == QUESYESNO && $required == 'y' && $qname != '') {
                $nbquestions ++;
            }
        }

        // selfchecklist Feedback Sections and Messages.
        if ($nbquestions != 0) {
            $maxsections = min ($nbquestions, $defaultsections);
            $feedbackoptions = array();
            $feedbackoptions[0] = get_string('feedbacknone', 'selfchecklist');
            $mform->addElement('header', 'submithdr', get_string('feedbackoptions', 'selfchecklist'));
            $feedbackoptions[1] = get_string('feedbackglobal', 'selfchecklist');
            for ($i = 2; $i <= $maxsections; ++$i) {
                $feedbackoptions[$i] = get_string('feedbacksections', 'selfchecklist', $i);
            }
            $mform->addElement('select', 'feedbacksections', get_string('feedbackoptions', 'selfchecklist'), $feedbackoptions);
            $mform->setDefault('feedbacksections', $selfchecklist->survey->feedbacksections);
            $mform->addHelpButton('feedbacksections', 'feedbackoptions', 'selfchecklist');

            $options = array('0' => get_string('no'), '1' => get_string('yes'));
            $mform->addElement('select', 'feedbackscores', get_string('feedbackscores', 'selfchecklist'), $options);
            $mform->addHelpButton('feedbackscores', 'feedbackscores', 'selfchecklist');

            // Is the RGraph library enabled at level site?
            $usergraph = get_config('selfchecklist', 'usergraph');
            if ($usergraph) {
                $chartgroup = array();
                $charttypes = array (null => get_string('none'),
                        'bipolar' => get_string('chart:bipolar', 'selfchecklist'),
                        'vprogress' => get_string('chart:vprogress', 'selfchecklist'));
                $chartgroup[] = $mform->createElement('select', 'chart_type_global',
                        get_string('chart:type', 'selfchecklist').' ('.
                                get_string('feedbackglobal', 'selfchecklist').')', $charttypes);
                if ($selfchecklist->survey->feedbacksections == 1) {
                    $mform->setDefault('chart_type_global', $selfchecklist->survey->chart_type);
                }
                $mform->disabledIf('chart_type_global', 'feedbacksections', 'eq', 0);
                $mform->disabledIf('chart_type_global', 'feedbacksections', 'neq', 1);

                $charttypes = array (null => get_string('none'),
                        'bipolar' => get_string('chart:bipolar', 'selfchecklist'),
                        'hbar' => get_string('chart:hbar', 'selfchecklist'),
                        'rose' => get_string('chart:rose', 'selfchecklist'));
                $chartgroup[] = $mform->createElement('select', 'chart_type_two_sections',
                        get_string('chart:type', 'selfchecklist').' ('.
                                get_string('feedbackbysection', 'selfchecklist').')', $charttypes);
                if ($selfchecklist->survey->feedbacksections > 1) {
                    $mform->setDefault('chart_type_two_sections', $selfchecklist->survey->chart_type);
                }
                $mform->disabledIf('chart_type_two_sections', 'feedbacksections', 'neq', 2);

                $charttypes = array (null => get_string('none'),
                        'bipolar' => get_string('chart:bipolar', 'selfchecklist'),
                        'hbar' => get_string('chart:hbar', 'selfchecklist'),
                        'radar' => get_string('chart:radar', 'selfchecklist'),
                        'rose' => get_string('chart:rose', 'selfchecklist'));
                $chartgroup[] = $mform->createElement('select', 'chart_type_sections',
                        get_string('chart:type', 'selfchecklist').' ('.
                                get_string('feedbackbysection', 'selfchecklist').')', $charttypes);
                if ($selfchecklist->survey->feedbacksections > 1) {
                    $mform->setDefault('chart_type_sections', $selfchecklist->survey->chart_type);
                }
                $mform->disabledIf('chart_type_sections', 'feedbacksections', 'eq', 0);
                $mform->disabledIf('chart_type_sections', 'feedbacksections', 'eq', 1);
                $mform->disabledIf('chart_type_sections', 'feedbacksections', 'eq', 2);

                $mform->addGroup($chartgroup, 'chartgroup',
                        get_string('chart:type', 'selfchecklist'), null, false);
                $mform->addHelpButton('chartgroup', 'chart:type', 'selfchecklist');
            }
            $editoroptions = array('maxfiles' => EDITOR_UNLIMITED_FILES, 'trusttext' => true);
            $mform->addElement('editor', 'feedbacknotes', get_string('feedbacknotes', 'selfchecklist'), null, $editoroptions);
            $mform->setType('feedbacknotes', PARAM_RAW);
            $mform->setDefault('feedbacknotes', $selfchecklist->survey->feedbacknotes);
            $mform->addHelpButton('feedbacknotes', 'feedbacknotes', 'selfchecklist');

            $mform->addElement('submit', 'feedbackeditbutton', get_string('feedbackeditsections', 'selfchecklist'));
            $mform->disabledIf('feedbackeditbutton', 'feedbacksections', 'eq', 0);
        }

        // Hidden fields.
        $mform->addElement('hidden', 'id', 0);
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'sid', 0);
        $mform->setType('sid', PARAM_INT);
        $mform->addElement('hidden', 'name', '');
        $mform->setType('name', PARAM_TEXT);
        $mform->addElement('hidden', 'owner', '');
        $mform->setType('owner', PARAM_RAW);

        // Buttons.

        $submitlabel = get_string('savechangesanddisplay');
        $submit2label = get_string('savechangesandreturntocourse');
        $mform = $this->_form;

        // Elements in a row need a group.
        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton2', $submit2label);
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', $submitlabel);
        $buttonarray[] = &$mform->createElement('cancel');

        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->setType('buttonar', PARAM_RAW);
        $mform->closeHeaderBefore('buttonar');

    }

    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }
}