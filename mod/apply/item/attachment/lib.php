<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

defined('MOODLE_INTERNAL') OR die('not allowed');
require_once($CFG->dirroot.'/mod/apply/item/apply_item_class.php');



class apply_item_attachment extends apply_item_base {
    protected $type = "attachment";
    private $commonparams;
    private $itemform;
    private $item;

    public function init() {

    }

    public function build_editform($item, $apply, $cm) {
        global $DB, $CFG;
        require_once('attachment_form.php');

        //get the lastposition number of the apply_items
        $position = $item->position;
        $lastposition = $DB->count_records('apply_item', array('apply_id' => $apply->id));
        if ($position == -1) {
            $iformselectlast = $lastposition + 1;
            $iformselectvalue = $lastposition + 1;
            $item->position = $lastposition + 1;
        } else {
            $iformselectlast = $lastposition;
            $iformselectvalue = $item->position;
        }
        //the elements for position dropdownlist
        $positionlist = array_slice(range(0, $iformselectlast), 1, $iformselectlast, true);

        $item->presentation = empty($item->presentation) ? '' : $item->presentation;

        $sizeandlength = explode('|', $item->presentation);

        if (isset($sizeandlength[0]) AND $sizeandlength[0] >= 5) {
            $itemsize = $sizeandlength[0];
        } else {
            $itemsize = 30;
        }

        $itemlength = isset($sizeandlength[1]) ? $sizeandlength[1] : 30;

        $item->itemsize = $itemsize;
        $item->itemmaxlength = $itemlength;

        //all items for dependitem
        $applyitems = apply_get_depend_candidates_for_item($apply, $item);
        $commonparams = array('cmid' => $cm->id,
                             'id' => isset($item->id) ? $item->id : null,
                             'typ' => $item->typ,
                             'items' => $applyitems,
                             'apply_id' => $apply->id);

        //build the form
        $customdata = array('item' => $item,
                            'common' => $commonparams,
                            'positionlist' => $positionlist,
                            'position' => $position);

        $this->item_form = new apply_attachment_form('edit_item.php', $customdata);
    }

    //this function only can used after the call of build_editform()
    public function show_editform() {
        $this->item_form->display();
    }

    public function is_cancelled() {
        return $this->item_form->is_cancelled();
    }

    public function get_data() {
        if ($this->item = $this->item_form->get_data()) {
            return true;
        }
        return false;
    }

    public function save_item() {
        global $DB;

        if (!$item = $this->item_form->get_data()) {
            return false;
        }

        if (isset($item->clone_item) AND $item->clone_item) {
            $item->id = ''; //to clone this item
            $item->position++;
        }

        $item->hasvalue = $this->get_hasvalue();
        if (!$item->id) {
            $item->id = $DB->insert_record('apply_item', $item);
        } else {
            $DB->update_record('apply_item', $item);
        }

        return $DB->get_record('apply_item', array('id' => $item->id));
    }


    //liefert eine Struktur ->name, ->data = array(mit Antworten)
    public function get_analysed($item, $groupid = false, $courseid = false) {
        global $DB;

        $analysedvalue = new stdClass();
        $analysedvalue->data = null;
        $analysedvalue->name = $item->name;

        $values = apply_get_group_values($item, $groupid, $courseid);
        if ($values) {
            $data = array();
            foreach ($values as $value) {
                $data[] = str_replace("\n", '<br />', $value->value);
            }
            $analysedvalue->data = $data;
        }
        return $analysedvalue;
    }

    public function get_printval($item, $value) {

        if (!isset($value->value)) {
            return '';
        }
        return $value->value;
    }

    public function print_analysed($item, $itemnr = '', $groupid = false, $courseid = false) {
        $values = apply_get_group_values($item, $groupid, $courseid);
        if ($values) {
            echo '<tr><th colspan="2" align="left">';
            echo $itemnr.'&nbsp;('.$item->label.') '.$item->name;
            echo '</th></tr>';
            foreach ($values as $value) {
                echo '<tr><td colspan="2" valign="top" align="left">';
                echo '-&nbsp;&nbsp;'.str_replace("\n", '<br />', $value->value);
                echo '</td></tr>';
            }
        }
    }

    public function excelprint_item(&$worksheet, $rowoffset, $xlsformats, $item, $groupid, $courseid = false) {

        $analyseditem = $this->get_analysed($item, $groupid, $courseid);

        $worksheet->write_string($rowoffset, 0, $item->label, $xlsformats->head2);
        $worksheet->write_string($rowoffset, 1, $item->name, $xlsformats->head2);
        $data = $analyseditem->data;
        if (is_array($data)) {
            $worksheet->write_string($rowoffset, 2, $data[0], $xlsformats->value_bold);
            $rowoffset++;
            $sizeofdata = count($data);
            for ($i = 1; $i < $sizeofdata; $i++) {
                $worksheet->write_string($rowoffset, 2, $data[$i], $xlsformats->default);
                $rowoffset++;
            }
        }
        $rowoffset++;
        return $rowoffset;
    }

    /**     
     * print the item at the edit-page of apply
     *
     * @global object
     * @param object $item
     * @return void
     */
    public function print_item_preview($item) {
        global $OUTPUT, $DB, $myattachmentid;
        $myattachmentid = $item->typ.'_'.$item->id;
        $align = right_to_left() ? 'right' : 'left';
        $stringrequiredmark = '<span class="apply_required_mark">*</span>';

        $presentation = explode ("|", $item->presentation);
        $requiredmark = ($item->required == 1) ? $stringrequiredmark : '';
        //print the question and label
        echo '<div class="apply_item_label_'.$align.'">';
        echo '('.$item->label.') ';
        echo format_text($item->name.$requiredmark, true, false, false);
        if ($item->dependitem) {
            if ($dependitem = $DB->get_record('apply_item', array('id' => $item->dependitem))) {
                echo ' <span class="apply_depend">';
                echo '('.$dependitem->label.'-&gt;'.$item->dependvalue.')';
                echo '</span>';
            }
        }
        echo '</div>';

        //print the presentation
        echo '<div class="apply_item_presentation_'.$align.'">';
        echo '<span class="apply_item_attachment">';
        //display of file picker
        global $CFG;
        require_once('filepicker_form.php');
        //Instantiate simplehtml_form 
        $mform = new filepicker_form();
        //Form processing and displaying is done here
        if ($mform->is_cancelled()) {
            //Handle form cancel operation, if cancel button is present on form
        } else if ($fromform = $mform->get_data()) {
            //In this case you process validated data. $mform->get_data() returns data posted in form.
        } else {
            // this branch is executed if the form is submitted but the data doesn't validate and the form should be redisplayed
            // or on the first display of the form.

            //Set default data (if any)
            //$mform->set_data($toform);
            //displays the form
            $mform->display();
        }
        /* echo '<input type="text" '.
                    'name="'.$item->typ.'_'.$item->id.'" '.
                    'size="'.$presentation[0].'" '.
                    'maxlength="'.$presentation[1].'" '.
                    'value="" />'; */
        echo '</span>';
        echo '</div>';
    }

    /**     
     * print the item at the complete-page of apply
     *
     * @global object
     * @param object $item
     * @param string $value
     * @param bool $highlightrequire
     * @return void
     */
    public function print_item_submit($item, $value = '', $highlightrequire = false) {
        global $OUTPUT, $myattachmentid;
        $myattachmentid = $item->typ.'_'.$item->id;//to create the name for file picker in the file picker form--added by nihar	
        $setform = new stdClass();
        $setform->$myattachmentid = $value;//set file picker value--added by nihar
        $align = right_to_left() ? 'right' : 'left';
        $stringrequiredmark = '<span class="apply_required_mark">*</span>';

        $presentation = explode ("|", $item->presentation);
        if ($highlightrequire AND $item->required AND strval($value) == '') {
            $highlight = ' missingrequire';
        } else {
            $highlight = '';
        }
        $requiredmark = ($item->required == 1) ? $stringrequiredmark : '';

        //print the question and label
        echo '<div class="apply_item_label_'.$align.$highlight.'">';
        echo format_text($item->name.$requiredmark, true, false, false);
        echo '</div>';

        //print the presentation
        echo '<div class="apply_item_presentation_'.$align.$highlight.'">';
        echo '<span class="apply_item_attachment">';
        //display of file picker
        global $CFG;
        require_once('filepicker_form.php');

        //Instantiate simplehtml_form
        $mform = new filepicker_form();
        $mform->set_data($setform);
        //Form processing and displaying is done here
        if ($mform->is_cancelled()) {
            //Handle form cancel operation, if cancel button is present on form
        } else if ($fromform = $mform->get_data()) {
            //In this case you process validated data. $mform->get_data() returns data posted in form.
        } else {

            $mform->display();
        }
        /*  echo '<input type="text" '.      //commented due to addition of attachement
                    'name="'.$item->typ.'_'.$item->id.'" '.
                    'size="'.$presentation[0].'" '.
                    'maxlength="'.$presentation[1].'" '.
                    'value="'.$value.'" />'; */
        echo '</span>';
        echo '</div>';
    }

    /**     
     * print the item at the complete-page of apply
     *
     * @global object
     * @param object $item
     * @param string $value
     * @return void
     */

    public function print_item_show_value($item, $value = '') {
        global $OUTPUT, $DB, $CFG;
        $align = right_to_left() ? 'right' : 'left';
        $stringrequiredmark = '<span class="apply_required_mark">*</span>';

        $presentation = explode ("|", $item->presentation);
        $requiredmark =  ($item->required == 1) ? $stringrequiredmark : '';

        //print the question and label
        echo '<div class="apply_item_label_'.$align.'">';
        //    echo '('.$item->label.') ';
        echo format_text($item->name . $requiredmark, true, false, false);
        echo '</div>';
        echo $OUTPUT->box_start('generalbox boxalign'.$align);
        //to get the attachment--by nihar
        //echo $value ? $value : '&nbsp;';
        //added by nihar--to show the image by url
        //$results = $DB->get_records('files', array('itemid' => $value, 'sortorder' => 0,'mimetype'=>'application/pdf'));
        $results = $DB->get_records_sql("select * from {files} where itemid='$value' and sortorder='0' and mimetype != 'NULL'");

        foreach ($results as $result) {
            $value = new stdClass();
            $value->contextid = $result->contextid;
            $value->component = $result->component;
            $value->filearea = $result->filearea;
            $value->itemid = $result->itemid;
            $value->filepath = $result->filepath;
            $value->filename = $result->filename;
            $value->mimetype = $result->mimetype;
        }
        $url = moodle_url::make_pluginfile_url($value->contextid, $value->component, $value->filearea,
        $value->itemid, $value->filepath, $value->filename);
        $fileurl = str_replace("pluginfile.php", "draftfile.php", $url);
        $mimetype = explode("/", $value->mimetype);
        if ($mimetype[0] == 'image') {
            echo "<img src='$fileurl' name='uploadedimage' alt='image'>";
        } else {
            echo "<p>View File -</p> <a href='$fileurl'>$fileurl</a>";
        }
        //echo $url;
        echo $OUTPUT->box_end();
    }

    public function check_value($value, $item) {
        //if the item is not required, so the check is true if no value is given
        if ((!isset($value) OR $value == '') AND $item->required != 1) {
            return true;
        }
        if ($value == "") {
            return false;
        }
        return true;
    }

    public function create_value($data) {
        $data = s($data);
        return $data;
    }

    //compares the dbvalue with the dependvalue
    //dbvalue is the value put in by the user
    //dependvalue is the value that is compared
    public function compare_value($item, $dbvalue, $dependvalue) {
        if ($dbvalue == $dependvalue) {
            return true;
        }
        return false;
    }

    public function get_presentation($data) {
        return $data->itemsize . '|'. $data->itemmaxlength;
    }

    public function get_hasvalue() {
        return 1;
    }

    public function can_switch_require() {
        return true;
    }

    public function value_type() {
        return PARAM_RAW;
    }

    public function clean_input_value($value) {
        return s($value);
    }
}
