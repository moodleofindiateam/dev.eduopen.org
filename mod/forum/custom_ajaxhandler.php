<?php
define('AJAX_SCRIPT', true);
header('Content-Type:application\json');
require_once(dirname(dirname(__DIR__)) . '/config.php');

require_once($CFG->dirroot . '/mod/forum/lib.php');

require_once ($CFG->dirroot.'/mod/forum/classes/subscriptions.php');

$mode           =  required_param('mode', PARAM_TEXT);
$forumid        = required_param('forumid', PARAM_INT);
$response = array();
require_sesskey();
$forum = new stdClass();
$forum->id = $forumid ;
$subscriptionsobj = new \mod_forum\subscriptions();
global $DB, $USER;
switch ($mode) {
        case 'subscribe':
        //$subscribe = $DB->insert_record('forum_subscriptions', (object)['userid'=>$USER->id,'forum'=>$forumid]);
        $subscribe = $subscriptionsobj->subscribe_user($USER->id, $forum);
        if($subscribe){
            $response = ['status'=>true, 'message'=>'Success:you are subscribed to this forum'];
            echo json_encode($response);
        } else {
            $response = ['status'=>true, 'message'=>'Error:Some problem occured'];
            echo json_encode($response);
        }
        break;
        case 'unsubscribe':
        //$unsubscribe = $DB->delete_records('forum_subscriptions', array('forum'=> $forumid, 'userid' => $USER->id));
        $unsubscribe = $subscriptionsobj->unsubscribe_user($USER->id, $forum);
            if($unsubscribe){
            $response = ['status'=>true, 'message'=>'Success:you are unsubscribed from this forum'];
            echo json_encode($response);
        } else {
            $response = ['status'=>true, 'message'=>'Error:Some problem occured'];
            echo json_encode($response);
        }
        break;

    default:
        break;
}
