<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'edustream', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   mod_edustream
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['configdisplayoptions'] = 'Select all options that should be available, existing settings are not modified. Hold CTRL key to select multiple fields.';
$string['content'] = 'edustream content';
$string['contentheader'] = 'Content';
$string['createedustream'] = 'Create a new edustream resource';
$string['displayoptions'] = 'Available display options';
$string['displayselect'] = 'Display';
$string['displayselectexplain'] = 'Select display type.';
$string['legacyfiles'] = 'Migration of old course file';
$string['legacyfilesactive'] = 'Active';
$string['legacyfilesdone'] = 'Finished';
$string['modulename'] = 'edustream';
$string['modulename_help'] = 'The edustream module enables a teacher to create a web educational video resource using the text editor. 

An edustream can, mainly, display video from an external source (as Youtube or Vimeo) using an embedding code (iframe or direct link).  
edustream is not different from Page module and can include  text, images, sound, video, web links and embedded code, such as Google maps. 

Advantages of using the edustream module rather than the standard Page module are: <br>
- the activity icon is related to video resources; <br>
- the activities are included into the Course Syllabus as single lessons in the EduOpen LMS; <br>
- more accessible (for example to users of mobile devices) and easier to update. <br>
';
$string['modulename_link'] = 'mod/edustream/view';
$string['modulenameplural'] = 'edustreams';
$string['optionsheader'] = 'Display options';
$string['edustream-mod-edustream-x'] = 'Any edustream module edustream';
$string['edustream:addinstance'] = 'Add a new edustream resource';
$string['edustream:view'] = 'View edustream content';
$string['pluginadministration'] = 'edustream module administration';
$string['pluginname'] = 'edustream';
$string['popupheight'] = 'Pop-up height (in pixels)';
$string['popupheightexplain'] = 'Specifies default height of popup windows.';
$string['popupwidth'] = 'Pop-up width (in pixels)';
$string['popupwidthexplain'] = 'Specifies default width of popup windows.';
$string['printheading'] = 'Display edustream name';
$string['printheadingexplain'] = 'Display edustream name above content?';
$string['printintro'] = 'Display edustream description';
$string['printintroexplain'] = 'Display edustream description above content?';
