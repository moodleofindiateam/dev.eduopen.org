<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * edustream module version information
 *
 * @package mod_edustream
 * @copyright  2009 Petr Skoda (http://skodak.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require('../../config.php');
require_once($CFG->dirroot.'/mod/edustream/locallib.php');
require_once($CFG->libdir.'/completionlib.php');

$id      = optional_param('id', 0, PARAM_INT); // Course Module ID
$p       = optional_param('p', 0, PARAM_INT);  // edustream instance ID
$inpopup = optional_param('inpopup', 0, PARAM_BOOL);

if ($p) {
    if (!$edustream = $DB->get_record('edustream', array('id'=>$p))) {
        print_error('invalidaccessparameter');
    }
    $cm = get_coursemodule_from_instance('edustream', $edustream->id, $edustream->course, false, MUST_EXIST);

} else {
    if (!$cm = get_coursemodule_from_id('edustream', $id)) {
        print_error('invalidcoursemodule');
    }
    $edustream = $DB->get_record('edustream', array('id'=>$cm->instance), '*', MUST_EXIST);
}

$course = $DB->get_record('course', array('id'=>$cm->course), '*', MUST_EXIST);

require_course_login($course, true, $cm);
$context = context_module::instance($cm->id);
require_capability('mod/edustream:view', $context);

// Trigger module viewed event.
$event = \mod_edustream\event\course_module_viewed::create(array(
   'objectid' => $edustream->id,
   'context' => $context
));
$event->add_record_snapshot('course_modules', $cm);
$event->add_record_snapshot('course', $course);
$event->add_record_snapshot('edustream', $edustream);
$event->trigger();

// Update 'viewed' state if required by completion system
require_once($CFG->libdir . '/completionlib.php');
$completion = new completion_info($course);
$completion->set_module_viewed($cm);

$PAGE->set_url('/mod/edustream/view.php', array('id' => $cm->id));

$options = empty($edustream->displayoptions) ? array() : unserialize($edustream->displayoptions);

if ($inpopup and $edustream->display == RESOURCELIB_DISPLAY_POPUP) {
    $PAGE->set_pagelayout('popup');
    $PAGE->set_title($course->shortname.': '.$edustream->name);
    $PAGE->set_heading($course->fullname);
} else {
    $PAGE->set_title($course->shortname.': '.$edustream->name);
   // $PAGE->set_pagelayout('edustreamincourse');
$PAGE->set_pagelayout('incourse');
    $PAGE->set_heading($course->fullname);
    $PAGE->set_activity_record($edustream);
}
echo $OUTPUT->header();
if (!isset($options['printheading']) || !empty($options['printheading'])) {
    echo $OUTPUT->heading(format_string($edustream->name), 2);
}

if (!empty($options['printintro'])) {
    if (trim(strip_tags($edustream->intro))) {
        echo $OUTPUT->box_start('mod_introbox', 'edustreamintro');
        echo format_module_intro('edustream', $edustream, $cm->id);
        echo $OUTPUT->box_end();
    }
}

$content = file_rewrite_pluginfile_urls($edustream->content, 'pluginfile.php', $context->id, 'mod_edustream', 'content', $edustream->revision);
$formatoptions = new stdClass;
$formatoptions->noclean = true;
$formatoptions->overflowdiv = true;
$formatoptions->context = $context;
$content = format_text($content, $edustream->contentformat, $formatoptions);
echo $OUTPUT->box($content, "generalbox center clearfix");

$strlastmodified = get_string("lastmodified");
echo "<div class=\"modified\">$strlastmodified: ".userdate($edustream->timemodified)."</div>";

echo $OUTPUT->footer();
