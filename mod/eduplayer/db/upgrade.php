<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the eduplayer module
 *
 * Sometimes, changes between versions involve alterations to database
 * structures and other major things that may break installations. The upgrade
 * function in this file will attempt to perform all the necessary actions to
 * upgrade your older installation to the current version. If there's something
 * it cannot do itself, it will tell you what you need to do.  The commands in
 * here will all be database-neutral, using the functions defined in DLL libraries.
 */

/**
 * @package    mod
 * @subpackage eduplayer
 * @author     Humanage Srl <info@humanage.it>
 * @copyright  2013 Humanage Srl <info@humanage.it>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Execute eduplayer upgrade from the given old version
 *
 * @param int $oldversion
 * @return bool
 */
function xmldb_eduplayer_upgrade($oldversion) {
    global $DB;

    $dbman = $DB->get_manager(); // loads ddl manager and xmldb classes

     if ($oldversion < 2013100804) {
     	 $table1 = new xmldb_table('eduplayer_comment');
     	$table1->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null, null);
		$table1->add_field('cmid', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null, null, '');
		$table1->add_field('userid', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
		$table1->add_field('comment', XMLDB_TYPE_CHAR, '1000', null, null, null, null, null, null);
		$table1->add_field('timecreated', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
		$table1->add_field('timemodified', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
 
		$table1->add_key('primary', XMLDB_KEY_PRIMARY, array('id'), null, null);

     	$table2 = new xmldb_table('eduplayer_post');
     	$table2->add_field('id', XMLDB_TYPE_INTEGER, '10', XMLDB_UNSIGNED, XMLDB_NOTNULL, XMLDB_SEQUENCE, null, null, null);
     	$table2->add_field('commentid', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null, null, '');
		$table2->add_field('parent', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, null, null, '');
		$table2->add_field('userid', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
		$table2->add_field('message', XMLDB_TYPE_CHAR, '1000', null, null, null, null, null, null);
		$table2->add_field('timecreated', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
		$table2->add_field('timemodified', XMLDB_TYPE_INTEGER, '20', null, null, null, null, null, null);
 
		$table2->add_key('primary', XMLDB_KEY_PRIMARY, array('id'), null, null);

     	 // Conditionally launch create table for assignment_upgrade.
        if (!$dbman->table_exists($table1)) {
            $dbman->create_table($table1);
        }
        // Conditionally launch create table for assignment_upgrade.
        if (!$dbman->table_exists($table2)) {
            $dbman->create_table($table2);
        }
     }
      // Assignment savepoint reached.
        upgrade_mod_savepoint(true, 2013100804, 'eduplayer');
    // Final return of upgrade result (true, all went good) to Moodle.
    return true;
}
