
$(document).on('click', '.panel-heading span.icon_minim', function (e) {
    var $this = $(this);
    if (!$this.hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideUp();
        $this.addClass('panel-collapsed');
        $this.removeClass('glyphicon-minus').addClass('glyphicon-plus');
    } else {
        $this.parents('.panel').find('.panel-body').slideDown();
        $this.removeClass('panel-collapsed');
        $this.removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});
$(document).on('focus', '.panel-footer input.chat_input', function (e) {
    var $this = $(this);
    if ($('#minim_chat_window').hasClass('panel-collapsed')) {
        $this.parents('.panel').find('.panel-body').slideDown();
        $('#minim_chat_window').removeClass('panel-collapsed');
        $('#minim_chat_window').removeClass('glyphicon-plus').addClass('glyphicon-minus');
    }
});

$(document).ready(function () {

    $(".msg_container_base").load(chatconfig.url + '?callback=conversation_list&touser=' + chatconfig.touser, function (response, status, xhr) {
        var json = JSON.parse(response);
        if (json.status) {
            var content = '';
            for (var prop in json.conversations) {
                content += '<div class="row msg_container ' + json.conversations[prop].class + '">';
                if (json.conversations[prop].class === 'base_sent') {
                    content += '<div class="col-md-10 col-xs-10">' +
                            '<div class="messages ' + json.conversations[prop].subclass + '">' +
                            '<p>' + json.conversations[prop].text + '</p>' +
                            '<time datetime="2009-11-13T20:00">' + json.conversations[prop].time + '</time>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-md-2 col-xs-2 avatar">' +
                            '<img src="' + json.conversations[prop].imgurl + '" class=" img-responsive ">' +
                            '</div>';
                } else {
                    content += '<div class="col-md-2 col-xs-2 avatar">' +
                            '<img src="' + json.conversations[prop].imgurl + '" class=" img-responsive ">' +
                            '</div>' +
                            '<div class="col-md-10 col-xs-10">' +
                            '<div class="messages ' + json.conversations[prop].subclass + '">' +
                            '<p>' + json.conversations[prop].text + '</p>' +
                            '<time datetime="2009-11-13T20:00">' + json.conversations[prop].time + '</time>' +
                            '</div>' +
                            '</div>';
                }
                content += '</div>';
            }
            $(".msg_container_base").html(content);
            $('.msg_container_base').animate({scrollTop: $('.msg_container_base')[0].scrollHeight}, 500);

        } else {
            $(".msg_container_base").html('<div class="row msg_container empty-conversation">No conversation yet</div>');
        }
    });

    $(".contact-list").load(chatconfig.url + '?callback=userslist&courseid=' + chatconfig.courseid, function (response, status, xhr) {
        var json = JSON.parse(response);
        if (json.status) {
            var content = '';
            for (var prop in json.userlist) {
                content += '<li class="media">' +
                        '<a href="#" class="pull-left">' +
                        '<img src="' + json.userlist[prop].imgurl + '" alt="" class="img-circle">' +
                        '</a>' +
                        '<div class="media-body">' +
                        '<span class="text-muted pull-right">' +
                        '</span>' +
                        '<strong class="text-success"><a href="' + chatconfig.basepath + json.userlist[prop].id + '">' + json.userlist[prop].name + '</a></strong>' +
                        '<p>' +
                        '</p>' +
                        '</div>' +
                        '</li>';

            }
            $(".contact-list").html(content);
        } else {
            $(".contact-list").html('<li class="media">' + json.message + '</li>');
        }
        contact_chatroom();
    });

    $(".msg-wrap").load(chatconfig.url + '?callback=conversation_list&touser=' + chatconfig.touser, function (response, status, xhr) {
        var json = JSON.parse(response);
        if (json.status) {
            var content = '';
            for (var prop in json.conversations) {
                if (json.conversations[prop].class === 'base_sent') {
                    content += '<li class="media">' +
                            '<a href="#" class="pull-left">' +
                            '<img src="' + json.conversations[prop].imgurl + '" alt="" class="img-circle">' +
                            '</a>' +
                            '<div class="media-body">' +
                            '<span class="text-muted pull-right">' +
                            '<small class="text-muted">' + json.conversations[prop].time + '</small>' +
                            '</span>' +
                            '<strong class="text-success">' + json.conversations[prop].name + '</strong>' +
                            '<p>' + json.conversations[prop].text +
                            '</p>' +
                            '</div>' +
                            '</li>';
                } else {
                    content += '<li class="media">' +
                            '<a href="#" class="pull-left">' +
                            '<img src="' + json.conversations[prop].imgurl + '" alt="" class="img-circle">' +
                            '</a>' +
                            '<div class="media-body">' +
                            '<span class="text-muted pull-right">' +
                            '<small class="text-muted">' + json.conversations[prop].time + '</small>' +
                            '</span>' +
                            '<strong class="text-success">' + json.conversations[prop].name + '</strong>' +
                            '<p>' + json.conversations[prop].text +
                            '</p>' +
                            '</div>' +
                            '</li>';
                }
            }
            $(".msg-wrap").html(content);
            $('.message-wrap').animate({scrollTop: $('.message-wrap')[0].scrollHeight}, 1000);
        } else {
            $(".msg-wrap").html('<div class="row msg_container empty-conversation"><p class="text-center text-muted" style="line-height:60px;">' + json.message + '</p></div>');
        }
    });


    $("#btn-chat").on('click', function () {
        var message = $("#btn-input").val();
        $.ajax({
            method: "POST",
            url: chatconfig.url + '?callback=sending',
            data: {session: chatconfig.session, touser: chatconfig.touser, fromuser: chatconfig.fromuser, message: message}
        }).done(function (json) {
            if (json.status) {
                $("#btn-input").val('');
                refresh_chat();
            } else {

            }
        });
    });

    $("#btn-chatroom").on('click', function () {
        var message = $("#btn-textarea").val();
        $.ajax({
            method: "POST",
            url: chatconfig.url + '?callback=sending',
            data: {session: chatconfig.session, touser: chatconfig.touser, fromuser: chatconfig.fromuser, message: message}
        }).done(function (json) {
            if (json.status) {
                $("#btn-textarea").val('');
                refresh_chatroom();
            } else {

            }
        });
    });

    var typingTimer;
    var doneTypingInterval = 2000;
    var $input = $('#btn-input');

    $input.on('keyup', function () {
        clearTimeout(typingTimer);
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });
    console.log(typingTimer);
    $input.on('keydown', function () {
        clearTimeout(typingTimer);
    });

    function doneTyping() {
        $.ajax({
            method: "POST",
            url: chatconfig.url + '?callback=typing'
        }).done(function (msg) {
            alert("Data Saved: " + msg);
        });
    }

    $input.on('keyup', function (e) {
        if (e.keyCode == 13) {
            message = $(this).val();
            $.ajax({
                method: "POST",
                url: chatconfig.url + '?callback=send_message',
                data: {message: message}
            }).done(function (msg) {
                alert("Data Saved: " + msg);
            });
        }
    });
});

$(document).on('click', '#new_chat', function (e) {
    var size = $(".chat-window:last-child").css("margin-left");
    size_total = parseInt(size) + 400;
    alert(size_total);
    var clone = $("#chat_window_1").clone().appendTo(".container");
    clone.css("margin-left", size_total);
});
$(document).on('click', '.icon_close', function (e) {
    //$(this).parent().parent().parent().parent().remove();
    $("#chat_window_1").remove();
});


function contact_chatroom() {
    $(".contact-list").html("<img src='" + chatconfig.pixurl + '/ajax-loader.gif' + "'>");
    
    $.ajax({
        method: "POST",
        url: chatconfig.url + '?callback=userslist&courseid=' + chatconfig.courseid,
    }).done(function (json) {
        if (json.status) {
            var content = '';
            for (var prop in json.userlist) {
                content += '<li class="media">' +
                        '<a href="#" class="pull-left">' +
                        '<img src="' + json.userlist[prop].imgurl + '" alt="" class="img-circle">' +
                        '</a>' +
                        '<div class="media-body">' +
                        '<span class="text-muted pull-right">' +
                        '</span>' +
                        '<strong class="text-success"><a href="' + chatconfig.basepath + json.userlist[prop].id + '">' + json.userlist[prop].name + '</a></strong>' +
                        '<p>' +
                        '</p>' +
                        '</div>' +
                        '</li>';

            }

            $(".contact-list").html(content);
        } else {
            $(".contact-list").html('<li class="media">' + json.message + '</li>');
        }
    });
    setTimeout('contact_chatroom()', 5000);
}
function refresh_chatroom() {
    $.ajax({
        method: "POST",
        url: chatconfig.url + '?callback=conversation_list&touser=' + chatconfig.touser,
    }).done(function (data) {
        if (data.status) {
            var content = '';
            for (var prop in data.conversations) {
                if (data.conversations[prop].class === 'base_sent') {
                    content += '<li class="media">' +
                            '<a href="#" class="pull-left">' +
                            '<img src="' + data.conversations[prop].imgurl + '" alt="" class="img-circle">' +
                            '</a>' +
                            '<div class="media-body">' +
                            '<span class="text-muted pull-right">' +
                            '<small class="text-muted">' + data.conversations[prop].time + '</small>' +
                            '</span>' +
                            '<strong class="text-success">' + data.conversations[prop].name + '</strong>' +
                            '<p>' + data.conversations[prop].text +
                            '</p>' +
                            '</div>' +
                            '</li>';
                } else {
                    content += '<li class="media">' +
                            '<a href="#" class="pull-left">' +
                            '<img src="' + data.conversations[prop].imgurl + '" alt="" class="img-circle">' +
                            '</a>' +
                            '<div class="media-body">' +
                            '<span class="text-muted pull-right">' +
                            '<small class="text-muted">' + data.conversations[prop].time + '</small>' +
                            '</span>' +
                            '<strong class="text-primary">' + data.conversations[prop].name + '</strong>' +
                            '<p>' + data.conversations[prop].text +
                            '</p>' +
                            '</div>' +
                            '</li>';
                }
            }
            $(".msg-wrap").html(content);
            $('.message-wrap').animate({scrollTop: $('.message-wrap')[0].scrollHeight}, 500);
        } else {
            $(".msg-wrap").html('<div class="row msg_container empty-conversation">No conversation yet</div>');
        }
    });
    setTimeout('refresh_chatroom()', 5000);
}

function refresh_chat() {

    $.ajax({
        method: "POST",
        url: chatconfig.url + '?callback=conversation_list&touser=' + chatconfig.touser,
    }).done(function (json) {
        if (json.status) {
            var content = '';
            for (var prop in json.conversations) {
                content += '<div class="row msg_container ' + json.conversations[prop].class + '">';
                if (json.conversations[prop].class === 'base_sent') {
                    content += '<div class="col-md-10 col-xs-10">' +
                            '<div class="messages ' + json.conversations[prop].subclass + '">' +
                            '<p>' + json.conversations[prop].text + '</p>' +
                            '<time datetime="2009-11-13T20:00">' + json.conversations[prop].time + '</time>' +
                            '</div>' +
                            '</div>' +
                            '<div class="col-md-2 col-xs-2 avatar">' +
                            '<img src="' + json.conversations[prop].imgurl + '" class=" img-responsive ">' +
                            '</div>';
                } else {
                    content += '<div class="col-md-2 col-xs-2 avatar">' +
                            '<img src="' + json.conversations[prop].imgurl + '" class=" img-responsive ">' +
                            '</div>' +
                            '<div class="col-md-10 col-xs-10">' +
                            '<div class="messages ' + json.conversations[prop].subclass + '">' +
                            '<p>' + json.conversations[prop].text + '</p>' +
                            '<time datetime="2009-11-13T20:00">' + json.conversations[prop].time + '</time>' +
                            '</div>' +
                            '</div>';
                }
                content += '</div>';
            }
            $(".msg_container_base").html(content);
            $('.msg_container_base').animate({scrollTop: $('.msg_container_base')[0].scrollHeight}, 500);
        } else {
            $(".msg_container_base").html('<div class="row msg_container empty-conversation">No conversation yet</div>');
        }
    });
    setTimeout('refresh_chat()', 15000);
}




function set_chat_msg()
{
    if (typeof XMLHttpRequest != "undefined")
    {
        oxmlHttpSend = new XMLHttpRequest();
    }
    else if (window.ActiveXObject)
    {
        oxmlHttpSend = new ActiveXObject("Microsoft.XMLHttp");
    }
    if (oxmlHttpSend == null)
    {
        alert("Browser does not support XML Http Request");
        return;
    }

    var url = "chat_send_ajax.php";
    var strname = "noname";
    var strmsg = "";
    if (document.getElementById("txtname") != null)
    {
        strname = document.getElementById("txtname").value;
        document.getElementById("txtname").readOnly = true;
    }
    if (document.getElementById("txtmsg") != null)
    {
        strmsg = document.getElementById("txtmsg").value;
        document.getElementById("txtmsg").value = "";
    }

    url += "?name=" + strname + "&msg=" + strmsg;
    oxmlHttpSend.open("GET", url, true);
    oxmlHttpSend.send(null);
}