<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once('../../../config.php');
require_once($CFG->dirroot.'/mod/eduplayer/lib.php');
global $USER, $DB, $PAGE;
define(AJAX_SCRIPT, true);
header('Content-Type:application\json');
$PAGE->requires->js('/mod/eduplayer/scripts.js');
$response = array();
$mode = required_param('mode', PARAM_TEXT);
if(confirm_sesskey() && data_submitted()){
    switch($mode) { 
        case "insertpost":
            $comment = required_param('comment', PARAM_RAW);
            $parent = required_param('parent', PARAM_RAW);
            $commentid = required_param('commentid', PARAM_RAW);
            if($parent == 0) {
                $cmid = required_param('cmid', PARAM_INT);
                try {
                    if(insert_comment($cmid, $comment)) {
                        //insert_post($commentid, $parent, $comment);
                        $response = ['status'=>true, 'message' => 'Thank you for your feeback!!']; 
                    }
                } catch (Exception $ex) {
                    print_object($ex);
                    $response = ['status'=>true, 'message' => 'Temporary problem occured!!'];
                }   
                echo json_encode($response);
            } else {//if it is a general reply 
                $lastinsetpost = insert_post($commentid, $parent, $comment);
                if($lastinsetpost) {
                    $countpost = count_post($commentid);
                    $countpoststring = $countpost.' '.get_string('posts', 'mod_eduplayer');
                    $response = ['status'=>true, 'countpost' => $countpoststring,'message' => 'Thank you for your feeback!!'];
                    echo json_encode($response);
                }
            }
            break; 
        case "allpost":
            $commentid = required_param('commentid', PARAM_RAW);

            $getpost = $DB->get_records_sql("SELECT * FROM {eduplayer_post} 
                                             WHERE commentid=$commentid  
                                             AND parent != '0' 
                                             ORDER BY timecreated DESC");

     
//echo html_writer::tag('h4','View All Post', array('class' =>'viewalldiscussheader divleft' ));     
            
echo html_writer::tag('div', html_writer::tag('textarea', '', array('name' => 'replybox','id' => 'replybox')).''.html_writer::tag('a', get_string('reply','mod_eduplayer'), array('href' => 'javascript:;', 'onclick' => 'insert_reply_post('.$commentid.', 1); return false;','class' => 'btn btn-primary replybutton')),array('class' => 'show_textbox'));          
echo '<div class="editpost"></div>';
            foreach($getpost as $allpost) {
                $user = $DB->get_record('user', array('id' => $allpost->userid),'id,firstname,lastname');
                get_all_post($allpost, $user);           
            }
            break;
        case "searchpost" :
            $data = required_param('data', PARAM_RAW);
            $fetchsql = "SELECT id FROM {user} 
                         WHERE firstname 
                         LIKE '%$data%' 
                         OR lastname LIKE '%$data%'";
            $fetchrecord = $DB->get_records_sql($fetchsql);

            foreach($fetchrecord as $record) {
                $latestthread = $DB->get_records('eduplayer_comment', 
                                          array('userid' => $record->id));
                foreach ($latestthread as $newthread) {
                    $user = $DB->get_record('user', 
                                     array('id' => $newthread->userid),'id,firstname,lastname');
                    get_parent_discussion_div($newthread, $user);  
                } 
            }
            break;
        case 'updatepost':
            $postid = required_param('postid', PARAM_RAW);
            $message = required_param('message', PARAM_RAW);
            $lastupdatepost = update_post($postid, $message);
            if ($lastupdatepost) {
                $response = ['status' => true, 'message' => 'post has been updated'];
                echo json_encode($response);
            }
            break;
        case 'deletepost':
            $postid = required_param('postid', PARAM_RAW);
            $lastdeletedpost = delete_post($postid);
            if($lastdeletedpost){
                $response = ['status' => true, 'message' => 'post has been deleted'];
                echo json_encode($response);
            } else {
              $response = ['status' => true, 'message' => 'some problem occured'];
              echo json_encode($response);  
            }
            break;
        case 'deletecomment':
            $commentid = required_param('commentid', PARAM_RAW);
            $lastdeletedcomment = delete_comment($commentid);
            if($lastdeletedcomment){
                $response = ['status' => true, 'message' => 'Thread has been deleted'];
                echo json_encode($response);
            } else {
              $response = ['status' => true, 'message' => 'some problem occured'];
              echo json_encode($response);  
            }
            break;
        case 'updatecomment':
            $commentid = required_param('commentid', PARAM_INT);
            $comment = required_param('comment', PARAM_RAW);
            $lastupdatecomment = update_comment($commentid, $comment);
             if($lastupdatecomment){
                $response = ['status' => true, 'message' => 'comment has been deleted'];
                echo json_encode($response);
            } else {
              $response = ['status' => true, 'message' => 'some problem occured'];
              echo json_encode($response);  
            }
            break;
        case 'fetchcomment':
            $commentid = required_param('commentid', PARAM_INT);
            global $DB;
            $eduplayer = $DB->get_record('eduplayer_comment', array('id' => $commentid),'comment');
            if($eduplayer){
                $response = ['status'=>true, 'comment'=> $eduplayer->comment]; 
                echo json_encode($response);
            } else {
                $response = ['status'=>false, 'comment'=> ' ']; 
                echo json_encode($response); 
            }
            break;
        case 'fetchpost':
            $postid = required_param('postid', PARAM_INT);
            global $DB;
            $eduplayer = $DB->get_record('eduplayer_post', array('id' => $postid), 'message');
            if ($eduplayer) {
                $response = ['status' => true, 'post' => $eduplayer->message];
                echo json_encode($response);
            } else {
                $response = ['status' => false, 'post' => ''];
                echo json_encode($response);
            }
            break;
        default:
            "not to be excecuted"; 
    }
}
function get_all_post($allpost, $user) {
    global $CFG, $DB, $USER;
    $ago = get_hours_ago($allpost);

    echo '<div class="media subpost col-md-10 mrg10L">
                <div class="media-left  col-md-1">
                    <a href="#">
                        <img src="' . $CFG->wwwroot . '/user/pix.php?file=/' . $allpost->userid . '/f1.jpg" class="img-circle" data-holder-rendered="true" style="width: 50px; height: 50px;" class="media-object" data-src="holder.js/64x64" alt="64x64" />
                    </a>
                </div>';
    echo '<div class="media-body col-md-9">
                <p class="font15">' . $allpost->message . '</p>
                <span class="media-heading lead">' . html_writer::link(new moodle_url($CFG->wwwroot . '/user/profile.php', array('id' => $allpost->userid)), 'By: ' . $user->firstname . ' ' . $user->lastname) .'<span class="ago"> , '. $ago . '</span> </span>
                
              </div>';
    if ($USER->id == $user->id || is_siteadmin()) {
        echo '<span class="ago pull-right postaction" onclick="delete_click(' . $allpost->id . ',' . $allpost->commentid . ')">' . get_string('delete', 'mod_eduplayer') . '</span>
                    <span class="ago pull-right postaction" onclick="edit_click(' . $allpost->id . ',' . $allpost->commentid . ')">' . get_string('edit', 'mod_eduplayer') . '</span>';
    }
    echo '</div>';
}

function get_parent_discussion_div($newthread,$user) {
    global $CFG, $DB;
    $ago = get_hours_ago($newthread);
    echo '<div class="media mainthread">
                <div class="media-left pull-left">
                  <a href="javascript:;">
                    <img src="' . $CFG->wwwroot . '/user/pix.php?file=/' . $newthread->userid . '/f1.jpg" class="img-circle" data-holder-rendered="true" style="width: 64px; height: 64px;" class="media-object" data-src="holder.js/64x64" alt="64x64" />
                  </a>
            </div>';
         echo html_writer::tag('div',html_writer::empty_tag('img',array('src'=>$CFG->wwwroot.'/mod/eduplayer/pix/post.png','onclick' => 'get_all_discussionpost('.$newthread->id.'); return false;')),array('class' =>'viewpost pull-right'));
        echo '<div class="media-body">
                <p class="font15">' . $newthread->comment . '</p>
                <span class="ago pull-right">'.$ago.'</span>
                <p class="media-heading lead">' . html_writer::link(new moodle_url($CFG->wwwroot . '/user/profile.php', array('id' => $newthread->userid)), $user->firstname . ' ' . $user->lastname) . '</p>      
            </div></div>';
}
function insert_post($commentid, $parent, $comment) {
    global $USER, $DB;
    $postobj = new stdClass();
    $postobj->commentid = $commentid;
    $postobj->parent = $parent;
    $postobj->userid = $USER->id;
    $postobj->message = $comment;
    $postobj->timecreated = time();
    $postobj->timemodified = NULL;
    $lastinsertpostid = $DB->insert_record('eduplayer_post', $postobj);
    if($lastinsertpostid) {
        return true;
    }
}
function update_post($postid, $message) {
    global $USER, $DB;
    $postobj = new stdClass();
    $postobj->id = $postid;
    $postobj->message = $message;
    $postobj->timemodified = time();
    $lastupdatepostid = $DB->update_record('eduplayer_post', $postobj);
    if($lastupdatepostid) {
        return true;
    }
}
function delete_post($postid){
    global $USER, $DB;
    $deletepostobj = $DB->delete_records('eduplayer_post', array('id' => $postid));
    if($deletepostobj) {
        return true;
    }
}
function delete_comment($commentid){
    global $USER, $DB;
    $deletecommentobj = $DB->delete_records('eduplayer_comment', array('id' => $commentid));
    if($deletecommentobj) {
        return true;
    }
}
function insert_comment($cmid,$comment) {
    global $CFG,$DB,$USER;
    $commentobj = new stdClass();
    $commentobj->cmid = $cmid;
    $commentobj->userid = $USER->id;
    $commentobj->comment = $comment;
    $commentobj->timecreated = time();
    $commentobj->timemodified = NULL;
    $lastinsertcommentid = $DB->insert_record('eduplayer_comment', $commentobj);
    if($lastinsertcommentid) {
        return true;
    }
}
function update_comment($commentid,$comment) {
    global $CFG, $DB, $USER;
    $updatecommentobj = new stdClass();
    $updatecommentobj->id = $commentid;
    $updatecommentobj->comment = $comment;
    $lastupdatecommentid = $DB->update_record('eduplayer_comment', $updatecommentobj);
    if($lastupdatecommentid) {
        return true;
    }
    
}
