<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Prints a particular instance of eduplayer
 *
 *
 * @package    mod
 * @subpackage eduplayer
 * @author     Humanage Srl <info@humanage.it>
 * @copyright  2013 Humanage Srl <info@humanage.it>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once(dirname(__FILE__) . '/lib.php');

$id = optional_param('id', 0, PARAM_INT);
$n = optional_param('n', 0, PARAM_INT);
$forceDownload = optional_param('forceDownload', 0, PARAM_INT);

if ($id) {
    $cm = get_coursemodule_from_id('eduplayer', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $eduplayer = $DB->get_record('eduplayer', array('id' => $cm->instance), '*', MUST_EXIST);
} elseif ($n) {
    $eduplayer = $DB->get_record('eduplayer', array('id' => $n), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $eduplayer->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('eduplayer', $eduplayer->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}


require_login($course, true, $cm);
//$context = get_context_instance(CONTEXT_MODULE, $cm->id);
$context = context_module::instance($cm->id);

if ($forceDownload == 1) {
   // eduplayer_pluginfile($course, $cm, $context, 'file', array('itemid' => $id, 'filename' => $eduplayer->eduplayerfile), true, array());
    $lectureresource = $DB->get_record_sql("SELECT filename from {files} WHERE contextid = $context->id AND source != 'NULL'");
    eduplayer_pluginfile($course, $cm, $context, 'lectresources', array('itemid' => 0, 'filename' => $lectureresource->filename), true, array());
   
    die();
}
//add_to_log($course->id, 'eduplayer', 'view', "view.php?id={$cm->id}", $eduplayer->name, $cm->id);
$manager = get_log_manager();
    if (method_exists($manager, 'legacy_add_to_log')) {
        $manager->legacy_add_to_log($course->id, 'eduplayer', 'view', "view.php?id={$cm->id}", $eduplayer->name, $cm->id);
    }

$completion = new completion_info($course);
$completion->set_module_viewed($cm);

$PAGE->set_url('/mod/eduplayer/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($eduplayer->name));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($context);
$PAGE->requires->jquery();
$PAGE->requires->js('/mod/eduplayer/js/bootstrap.min.js', true);
$PAGE->requires->js('/mod/eduplayer/js/bootstrap-dialog.min.js', true);
$PAGE->requires->js('/mod/eduplayer/scripts.js');
$PAGE->requires->js('/mod/eduplayer/js/custom.js', true);
$PAGE->requires->css('/mod/eduplayer/css/bootstrap-dialog.min.css', true);
$PAGE->requires->css('/mod/eduplayer/css/custom.css', true);
$PAGE->set_pagelayout('incourse');
$PAGE->set_cacheable(true);

if ($eduplayer->sharelink) {
    require_once('./share_form.php');
    $f = new shareform_form('?id=' . $id);
    if (isset($_POST['ajax']) && $_POST['ajax'] == 1) {
        if ($f->get_data()) {
            if ($f->shareEmailLink($eduplayer)) {
                die(json_encode(array('result' => 'success', 'message' => get_string('emailsent', 'eduplayer', $f->get_data()->email))));
            } else {
                die(json_encode(array('result' => 'error', 'message' => get_string('emailnotsent', 'eduplayer'))));
            }
        } else {
            die(json_encode(array('result' => 'error', 'message' => get_string('emailnotcorrect', 'eduplayer'))));
        }
    }
}

// Output starts here
echo $OUTPUT->header();
echo '<script type="text/javascript" src="./jwplayer/jwplayer.js"></script>';
echo '<script type="text/javascript">jwplayer.key="t9LAmH0jS/ef+nCpjaENR7X9IDnWOdafl90okQ=="</script>';
if ($eduplayer->intro) {
    echo $OUTPUT->box(format_module_intro('eduplayer', $eduplayer, $cm->id), 'generalbox mod_introbox', 'eduplayerintro');
}
?>
<style>
    .jwlogo {
        display: none;
    }
    
    .myvid {height:400px;border:1px solid transparent;}
    .jw-display-icon-container {display:none !important;};
  
  
</style>

<script>
  $(function() {
    $( "#tabs" ).tabs();
  });
  </script>
<body>
    <?php
    echo eduplayer_video($eduplayer);
    if ($eduplayer->sharelink) {
        echo '<a id="share" href="#" class="btn btn-success">' . get_string('share', 'eduplayer') . '</a>&nbsp&nbsp';
    }

    if ($eduplayer->downloadenabled) {
        echo '<a id="download" href="./view.php?id=' . $id . '&forceDownload=1" class="btn btn-success">' . get_string('download', 'eduplayer') . '</a>';
        if ($eduplayer->disclaimer) {
            ?>	
            <script type='text/javascript'>
                YUI().use('event', 'panel', function (Y) {
                    var downloadbutton = Y.one('#download');
                    downloadbutton.on('click', function (e) {
                        e.preventDefault();
                        try {
                            if (downloadpanel.get('visible'))
                                return false;
                        } catch (err) {
                        }
                        downloadpanel = new Y.Panel({
                            bodyContent: '<?php echo addslashes(str_replace(array("\r\n", "\r", "\n"), '<br />', $eduplayer->disclaimer)); ?>',
                            width: 400,
                            centered: true,
                            buttons: [
                                {value: "no",
                                    label: '<?php echo get_string('cancel', 'eduplayer'); ?>',
                                    action: function (i) {
                                        i.preventDefault();
                                        downloadpanel.hide();
                                        return false;
                                    },
                                    section: Y.WidgetStdMod.FOOTER
                                },
                                {value: "yes",
                                    label: '<?php echo get_string('download', 'eduplayer'); ?>',
                                    action: function (i) {
                                        i.preventDefault();
                                        downloadpanel.hide();
                                        location.href = downloadbutton.getAttribute('href');
                                    },
                                    section: Y.WidgetStdMod.FOOTER
                                }
                            ]
                        });

                        downloadpanel.render();
                    });
                });
            </script>
            <?php
        }
    }
    if (isset($f)) {
        echo '<div id="sharebox" class="hidden">';
        if ($f->is_cancelled()) {
            $f->display();
        } else if ($f->get_data() && $f->shareEmailLink($eduplayer)) {
            echo get_string('emailsent', 'eduplayer', $f->get_data()->email);
        } else if ($f->get_data()) {
            echo get_string('emailnotsent', 'eduplayer');
            $f->display();
        } else {
            $f->displayCustom();
        }
        echo ('</div>');
        ?>
        <script type='text/javascript'>
            function makeajaxemail() {
                YUI().use('transition', 'io-form', 'node', function (Y) {
                    var cfg = {
                        method: 'POST',
                        data: {ajax: '1'},
                        form: {id: 'mform1'},
                    };
                    function complete(transactionid, response, arguments) {
                        var r = JSON.parse(response.responseText);
                        Y.one('span#loading').replace('<span id="result" class="' + r.result + '">' + r.message + '</span>');
                        setTimeout(function () {
                            Y.one('span#result').hide(true);
                        }, 5000);
                    }
                    function start(transactionid, arguments) {
                        if (Y.one('#loading'))
                            Y.one('#loading').remove();
                        if (Y.one('#result'))
                            Y.one('#result').remove();
                        Y.one('input[type=submit]').insert('<span id="loading"><img src="./pix/ajax-loader.gif" />loading ....</span>', 'after');
                    }
                    Y.on('io:start', start, Y, ['lorem', 'ipsum']);
                    Y.on('io:complete', complete, Y, ['lorem', 'ipsum']);
                    var request = Y.io(window.location.href, cfg);
                });
            }
            YUI().use('node', 'event', 'panel', function (Y) {
                Y.one('a#share').on('click', function (e) {
                    e.preventDefault();
                    Y.one('#sharebox').toggleClass('hidden');
                });

                var shareform = Y.one('#mform1');
                shareform.on('submit', function (e) {
                    e.preventDefault();
                    if (Y.one('input[name=email]').get('value') == '') {
                        Y.one('input[name=email]').focus();
                        return false;
                    }
    <?php if ($eduplayer->disclaimer) { ?>
                        try {
                            if (sharepanel.get('visible'))
                                return false;
                        } catch (err) {
                        }
                        sharepanel = new Y.Panel({
                            bodyContent: '<?php echo addslashes(str_replace(array("\r\n", "\r", "\n"), '<br />', $eduplayer->disclaimer)); ?>',
                            width: 400,
                            centered: true,
                            buttons: [
                                {value: "no",
                                    label: '<?php echo get_string('cancel', 'eduplayer'); ?>',
                                    action: function (i) {
                                        i.preventDefault();
                                        sharepanel.hide();
                                    },
                                    section: Y.WidgetStdMod.FOOTER
                                },
                                {value: "yes",
                                    label: '<?php echo get_string('share', 'eduplayer') ?>',
                                    action: function (i) {
                                        i.preventDefault();
                                        sharepanel.hide();
                                        makeajaxemail();
                                    },
                                    section: Y.WidgetStdMod.FOOTER
                                }
                            ]
                        });

                        sharepanel.render();
    <?php } else { ?>
                        makeajaxemail();
    <?php } ?>
                });

            });
        </script>
        <?php
    }
  $useragent  = check_useragent();
if($useragent == 'is_desktop'){
?>
<style>
       iframe#videoElement_youtube {
              pointer-events: none;         
      }
</style>

<?php
}
    ?>
    <style> 

@media screen and (min-width: 320px)  and (max-width: 609px) {
  
a.downloadresource {
       top: -120px;
    left: 67%;
}
.allpost {
    margin-top: 4%;

}
  
}

@media screen and (min-width: 610px)  and (max-width: 615px) {
  
a.downloadresource {
       top: -120px;
    left: 67%;
}
.allpost {
    margin-top: 4%;

}
  
}

@media screen and (min-width: 615px)  and (max-width: 700px) {
  
a.downloadresource {
         top: -120px;
    left: 73%;
}
.allpost {
    margin-top: 4%;

}
  
}

@media screen and (min-width: 701px)  and (max-width: 800px) {
a.downloadresource {
        top: -91px;
    left: 69%;
}
.allpost {
    margin-top: 4%;

}
}
@media screen and (min-width: 801px)  and (max-width: 1000px) {
a.downloadresource {
    top: -128px;
    left: 82%;
}
.allpost {
    margin-top: 4%;

}
}
@media screen and (min-width: 1001px)  and (max-width: 2000px) {
a.downloadresource {
    top: -91px;
    left: 82%;
}
}
	div#videoElement {
   outline : 2px solid white !important;
   }

.jw_controlbar {
    background-color: #252525 !important;
    height: 40px !important;
    padding: 4px 0.25em !important;
   width: 1200px !important;
}

#videoElement_controlbar {
bottom: 13px !important;
max-width: 1092px !important;
}
.jw-progress {
    background-color: #FF0046 !important;
}
.jw-slider-time {
    background-color: transparent;
}
    </style>	
	<script>
		$(document).ready(function(){
                        $(".jw-controlbar").css("display", "none !important");
                         $("#videoElement").mouseover(function(){
                                 $(".jw-controlbar").show();
                         });
    
                       $("#videoElement").mouseout(function(){
                               $(".jw-controlbar").hide();
                         });
                  });

	</script>
<?php
 $cm = optional_param('id', 0, PARAM_INT);
  $eduplayer->coursemodule =  $cm;
  $resourceurl = eduplayer_lecture_resource($eduplayer);

  if($resourceurl) {
echo '<a id="download" href="./view.php?id=' . $id . '&forceDownload=1" class="btn btn-success downloadresource">' . get_string('downloadresource', 'eduplayer') . '</a>';
  }
require_once($CFG->dirroot.'/mod/eduplayer/forum_eduplayer.php');
 echo $OUTPUT->footer();
?>
  <script type="text/javascript">
        var ratepath = '<?php echo $CFG->wwwroot . '/mod/eduplayer/ajax/ratecomment.php'; ?>';
        var cmid = "<?php echo $id; ?>";
        var sesskey = "<?php echo $USER->sesskey; ?>";
    </script>