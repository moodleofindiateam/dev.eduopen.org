    <?php
    $latestthread = $DB->get_records_sql("SELECT * FROM {eduplayer_comment} WHERE cmid=$id ORDER BY timecreated ASC" );
    $allthread = $DB->get_records_sql("SELECT * FROM {eduplayer_comment} WHERE cmid=$id" );
    foreach ($allthread as $allthreads) {
        $reccount[$allthreads->id] = $DB->count_records('eduplayer_post', array('commentid' => $allthreads->id, 'parent' => 1));
        arsort($reccount);
    }

    $userslist = null;
    //this is to show all new thread in particular eduplayer
    
?>
<?php  echo html_writer::tag('button', 'Create a new thread', array('id' => 'ratethis', 'class' => 'newthread pull-right', 'onclick' => 'show_modal("",0,"insertpost")')) ?> 
<div class="col-md-12 forumdiv pad0A">
    
<div id="tabs" class="edupayertabs">
  <ul>
    <li><a href="#tabs-1"><?php print_string('latest', 'mod_eduplayer')?></a></li>
    <li><a href="#tabs-2"><?php print_string('top', 'mod_eduplayer')?></a></li>
    <li><a href="#tabs-3"><?php print_string('unanswered', 'mod_eduplayer')?></a></li>
    <!--<li><a href="#tabs-4"><input type="text" id="searchpost"></a></li>-->
  </ul>
  <?php echo "<h4 class='viewalldiscussheader divleft'>View All discussion</h4>"; ?>
  <div class="col-md-12 allmainthread row">
  <div id="tabs-1" class="latestdiscuss">
    <?php
    foreach ($latestthread as $newthread) {
        $user = $DB->get_record('user', array('id' => $newthread->userid));
         get_main_thread($newthread,$user);
    }
?>

  </div>
  <div id="tabs-2" class="topdiscuss">
    <?php
    if(isset($reccount)) {
    foreach ($reccount as $key => $reccounts) {
        
        $newthread = $DB->get_record('eduplayer_comment', array('id' => $key));
        $user = $DB->get_record('user', array('id' => $newthread->userid));
         get_main_thread($newthread,$user);
    }
  }
?>
  </div>
  <div id="tabs-3" class="unanswered">
    <?php
        if(isset($reccount)) {
        foreach ($reccount as $key => $reccounts) {
        if($reccounts == 0) {
        
        $newthread = $DB->get_record('eduplayer_comment', array('id' => $key));
        $user = $DB->get_record('user', array('id' => $newthread->userid));
        get_main_thread($newthread,$user);

    }
  }
}
?>
  </div>
  <!--<div id="tabs-4" class="searchpost">


  </div>-->

</div>
</div>

</div>