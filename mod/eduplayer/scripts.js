function show_modal(commentid, parent, mode) {
            BootstrapDialog.show({
                title: 'Please leave us comment',
                message: $('<div class="col-md-12"><div class="col-md-6"><div class="error"></div><br/><textarea class="form-control" cols="45" rows="3" id="comment-box" placeholder="Leave us your comment.."></textarea></div></div>'),
                buttons: [{
                        label: 'Submit',
                        cssClass: 'btn-primary',
                        hotkey: 13, // Enter.
                        action: function (dialogItself) {
                            var comment = $("#comment-box").val();
                            if (comment === '') {
                                $(".error").html('<div class="alert alert-warning">Please write some comments..</div>');
                                return;
                            }
                            if (mode == 'insertpost') {
                                $.post(ratepath, {mode: 'insertpost', cmid: cmid, comment: comment, commentid: commentid, parent: parent, sesskey: sesskey}, function (json) {
                                    if (json.status) {
                                        $(".error").html();
                                        dialogItself.close();
                                        BootstrapDialog.show({
                                            message: json.message
                                        });
                                        window.location.reload();
                                    } else {
                                        $(".login-error").addClass('alert alert-warning');
                                        $(".login-error").text(json.message);
                                        console.log('not logged in');
                                    }
                                }, 'json');
                            } else {
                                $.post(ratepath, {mode: 'updatecomment', cmid: cmid, comment: comment, commentid: commentid, parent: parent, sesskey: sesskey}, function (json) {
                                    if (json.status) {
                                        $(".error").html();
                                        dialogItself.close();
                                        BootstrapDialog.show({
                                            message: json.message
                                        });
                                        window.location.reload();
                                    } else {
                                        $(".login-error").addClass('alert alert-warning');
                                        $(".login-error").text(json.message);
                                        console.log('not logged in');
                                    }
                                }, 'json');
                            }


                        }
                    }]
            });

}

function edit_modal(commentid, parent, mode) {
    $.post(ratepath, {mode: 'fetchcomment', commentid: commentid, sesskey: sesskey}, function (json) {
        if (json.status) {
            var comment = json.comment;
            BootstrapDialog.show({
                title: 'Please leave us comment',
                message: $('<div class="col-md-12"><div class="col-md-6"><div class="error"></div><br/><textarea class="form-control" cols="45" rows="3" id="comment-box" placeholder="Leave us your comment..">' + comment + '</textarea></div></div>'),
                buttons: [{
                        label: 'Submit',
                        cssClass: 'btn-primary',
                        hotkey: 13, // Enter.
                        action: function (dialogItself) {
                            var comment = $("#comment-box").val();
                            if (comment === '') {
                                $(".error").html('<div class="alert alert-warning">Please write some comments..</div>');
                                return;
                            }
                            if (mode == 'insertpost') {
                                $.post(ratepath, {mode: 'insertpost', cmid: cmid, comment: comment, commentid: commentid, parent: parent, sesskey: sesskey}, function (json) {
                                    if (json.status) {
                                        $(".error").html();
                                        dialogItself.close();
                                        BootstrapDialog.show({
                                            message: json.message
                                        });
                                        window.location.reload();
                                    } else {
                                        $(".login-error").addClass('alert alert-warning');
                                        $(".login-error").text(json.message);
                                        console.log('not logged in');
                                    }
                                }, 'json');
                            } else {
                                $.post(ratepath, {mode: 'updatecomment', cmid: cmid, comment: comment, commentid: commentid, parent: parent, sesskey: sesskey}, function (json) {
                                    if (json.status) {
                                        $(".error").html();
                                        dialogItself.close();
                                        BootstrapDialog.show({
                                            message: json.message
                                        });
                                        window.location.reload();
                                    } else {
                                        $(".login-error").addClass('alert alert-warning');
                                        $(".login-error").text(json.message);
                                        console.log('not logged in');
                                    }
                                }, 'json');
                            }


                        }
                    }]
            });
        }
    });
}

function get_all_discussionpost(commentid) {
    $("div[class^='allpost'],div[class*='allpost']").hide();
    $(".allpost"+commentid).load(ratepath,{mode:'allpost',commentid:commentid, sesskey: sesskey});
    $(".allpost"+commentid).show();
    return true;
}

$(".mainthread").click(function(){
  $(".mainthread").css({"border-left":"3px solid #d95843","background": "#F5F5F5"});
  $(this).css({"border-left":"3px solid #45AD1A","background": "#FFFFFF"});
});

function insert_reply_post(commentid,parent) {
  //$(".").load(ratepath,{mode:'insertpost',commentid:commentid,parent:parent});
  var comment = $("#replybox").val();
  $.post(ratepath,{mode:'insertpost',comment:comment,commentid:commentid,parent:parent,sesskey: sesskey},function(json){
    if(json.status) {  
      get_all_discussionpost(commentid);
     $(".noofpost"+commentid).text(json.countpost);
    }
  });
}

$("#tabs li,#searchpost").click(function(){
  $("div[class^='allpost'],div[class*='allpost']").empty();
});
$(document).ready(function(){
  $("#searchpost").keyup(function(){
      var data = $(this).val();
     if(data != '') {
      $(".searchpost").load(ratepath,{mode:'searchpost', data:data,sesskey: sesskey},function(){
        $(".mainthread").click(function(){
        $(".mainthread").css({"border-left":"3px solid #d95843","background": "#F5F5F5"});
        $(this).css({"border-left":"3px solid #45AD1A","background": "#FFFFFF"});
});

      });
} else {
$(".searchpost").empty();
}
  });
});

function edit_click(postid,commentid) {
    $(".allpost").animate({ scrollTop: 0 }, "fast");
    $("html, body").animate({ scrollTop: 600 }, "fast");
    $(".show_textbox").addClass('hide');
    $.post(ratepath,{mode:'fetchpost', postid:postid,sesskey: sesskey}, function(json){
        if (json.status) {
            var messagepost = json.post;
            $(".editpost").html("<div class='show_updatepost_textbox'><textarea name='updatebox' id='updatebox'>"+messagepost+"</textarea><a href='javascript:;' class='btn btn-primary updatebutton' onclick='edit_post("+postid+","+commentid+")'; retun false;'>Update</a></div>");
        }
    });
    
 
    
    //$(".editpost").html("<?php echo html_writer::tag('div', html_writer::tag('textarea', '', array('name' => 'updatebox','id' => 'updatebox')).''.html_writer::tag('a', 'Update', array('href' => 'javascript:;', 'onclick' => 'edit_post('.$commentid.', 1); return false;','class' => 'btn btn-primary updatebutton')),array('class' => 'show_updatepost_textbox hide')); ?>");
}
function delete_click(postid, commentid){
    var pressed = confirm('Do you want to delete this post');
    if(pressed) {
        $.post(ratepath,{mode:'deletepost', postid:postid, sesskey: sesskey}, function(json){
            if(json.status) {
                alert(json.message);
            }
        });
        get_all_discussionpost(commentid);
    }
}
function delete_click_comment(commentid){
    var pressed = confirm('Do you want to delete this thread');
    if(pressed) {
        $.post(ratepath,{mode:'deletecomment', commentid:commentid, sesskey: sesskey}, function(json){
            if(json.status) {
                alert(json.message);
            }
        });
        window.location.reload();
    }
}

function edit_post(postid,commentid) {
        var message = $("#updatebox").val();
        $.post(ratepath, {mode: 'updatepost', message: message, postid: postid, sesskey: sesskey}, function (json) {
            if (json.status) {
                get_all_discussionpost(commentid);
            }
        });
}
