<?php

defined('MOODLE_INTERNAL') || die();

function cinstitution_images($csettings) {
    global $CFG, $USER, $DB;

    /* $imagedt = $DB->get_records('files', array('itemid' => $csettings->logo));
      foreach ($imagedt as $dataimg) {
      $formitemname = 'logo';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/clean/pix/logo.jpg';//edited by nihar
      if (!empty($csettings->logo)) {
      $files = $fs->get_area_files($dataimg->contextid, 'user', $filearea , $csettings->logo, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($dataimg->contextid,$file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile;
      } */
    $instlogo = $DB->get_records('files', array('itemid' => $csettings->logo));
    foreach ($instlogo as $institutelogo) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($institutelogo->contextid, 'block_institution', 'content', $csettings->logo, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $csettings->logo, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

function cinstitution_logo($csettingslogo) {
    global $CFG, $USER, $DB;

    /* $imagedtlogo = $DB->get_records('files', array('itemid' => $csettingslogo->logo1));
      foreach ($imagedtlogo as $dataimg) {
      $formitemname = 'logo1';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/clean/pix/logo1.jpg';//edited by nihar
      if(!empty($csettingslogo->logo1)){
      $files = $fs->get_area_files($dataimg->contextid, 'user', $filearea ,
      $csettingslogo->logo1, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($dataimg->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile;
      } */
    //$contextmodule = context_block::instance(); //TODO-need to make dynamic--block instance id
    $instlogo1 = $DB->get_records('files', array('itemid' => $csettingslogo->logo1));
    foreach ($instlogo1 as $institutelogo1) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($institutelogo1->contextid, 'block_institution', 'content', $csettingslogo->logo1, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $csettingslogo->logo1, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

//function to get the course extra setting badge 
function course_extrasetting_badgeimage($csettingsbadge) {
    global $CFG, $USER, $DB;

    /* $imagedtlogo = $DB->get_records('files', array('itemid' => $csettingslogo->logo1));
      foreach ($imagedtlogo as $dataimg) {
      $formitemname = 'logo1';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/clean/pix/logo1.jpg';//edited by nihar
      if(!empty($csettingslogo->logo1)){
      $files = $fs->get_area_files($dataimg->contextid, 'user', $filearea ,
      $csettingslogo->logo1, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($dataimg->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile;
      } */
    //$contextmodule = context_block::instance(); //TODO-need to make dynamic--block instance id
    $courseextrasettingbadge = $DB->get_records('files', array('itemid' => $csettingsbadge->badgeimage));
    foreach ($courseextrasettingbadge as $badgeimage) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($badgeimage->contextid, 'local_course_extrasettings', 'content', $csettingsbadge->badgeimage, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $csettingsbadge->badgeimage, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

function banner_images($banner) {
    global $CFG, $DB, $USER;

    /* $banerimg = $DB->get_records('files', array('itemid' => $banner->banner));
      foreach ($banerimg as $imgbaner) {
      $formitemname = 'banner';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/clean/pix/banner.jpg';//edited by nihar
      if(!empty($banner->banner)){
      $files = $fs->get_area_files($imgbaner->contextid, 'user', $filearea , $banner->banner, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($imgbaner->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile;
      } */
    // $contextmodule = context_block::instance(); //TODO-need to make dynamic--block instance id
    $instban = $DB->get_records('files', array('itemid' => $banner->banner));
    foreach ($instban as $instbanner) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($instbanner->contextid, 'block_institution', 'content', $banner->banner, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $banner->banner, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

function specialization_images($spcimg) {
    global $CFG, $USER, $DB;
    /* $splizationimg = $DB->get_records('files', array('itemid' => $spcimg->specialization_picture));
      foreach ($splizationimg as $imgsplization) {

      $formitemname = 'specialization_picture';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/clean/pix/logo.jpg';//edited by nihar
      if (!empty($spcimg->specialization_picture)) {
      $files = $fs->get_area_files($imgsplization->contextid, 'user', $filearea ,
      $spcimg->specialization_picture, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($imgsplization->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile;
      } */
    $splizationimg = $DB->get_records('files', array('itemid' => $spcimg->specialization_picture));
    foreach ($splizationimg as $imgsplization) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($imgsplization->contextid, 'block_specialization', 'content', $spcimg->specialization_picture, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $spcimg->specialization_picture, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

function specialization_certificate($certificate) {
    global $CFG, $USER, $DB;
    //$contextid = context_user::instance($USER->id);
    //$certificate->contextid=$contextid->id;
    /* $crficateimg = $DB->get_records('files', array('itemid' => $certificate->certificate));
      foreach ($crficateimg as $imgcrficate) {
      $formitemname = 'certificate';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/clean/pix/logo.jpg';//edited by nihar
      if (!empty($certificate->certificate)) {
      $files = $fs->get_area_files($imgcrficate->contextid, 'user', $filearea ,
      $certificate->certificate, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($imgcrficate->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile;
      } */
    $certificates = $DB->get_records('files', array('itemid' => $certificate->certificate));
    foreach ($certificates as $certificateimg) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($certificateimg->contextid, 'block_specialization', 'content', $certificate->certificate, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $certificate->certificate, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

// Attendance Certificate
function certificate_images1($extrasettingscertificate1) {
    global $CFG, $DB;

    /* $formitemname = 'certificatedownload1';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      //$picsfile = $CFG->wwwroot.'/theme/elegance/pix/book.jpg';//edited by nihar
      if (!empty($extrasettingscertificate1->certificatedownload1)) {
      $files = $fs->get_area_files($extrasettingscertificate1->contextid, 'user', $filearea ,
      $extrasettingscertificate1->certificatedownload1, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($extrasettingscertificate1->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile; */
    $certificate1 = $DB->get_records('files', array('itemid' => $extrasettingscertificate1->certificatedownload1));
    foreach ($certificate1 as $certificateimg1) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($certificateimg1->contextid, 'local_course_extrasettings', 'content', $extrasettingscertificate1->certificatedownload1, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $extrasettingscertificate1->certificatedownload1, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

// Verified Certificate
function certificate_images2($extrasettingscertificate2) {
    global $CFG, $DB;

    /* $formitemname = 'certificatedownload2';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      //$picsfile = $CFG->wwwroot.'/theme/elegance/pix/book.jpg';//edited by nihar
      if (!empty($extrasettingscertificate2->certificatedownload2)) {
      $files = $fs->get_area_files($extrasettingscertificate2->contextid, 'user', $filearea ,
      $extrasettingscertificate2->certificatedownload2, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($extrasettingscertificate2->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile; */
    $certificate2 = $DB->get_records('files', array('itemid' => $extrasettingscertificate2->certificatedownload2));
    foreach ($certificate2 as $certificateimg2) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($certificateimg1->contextid, 'local_course_extrasettings', 'content', $extrasettingscertificate2->certificatedownload2, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $extrasettingscertificate2->certificatedownload2, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

// Exam Certificate
function certificate_images($extrasettingscertificate) {
    global $CFG, $DB;

    /* $formitemname = 'certificatedownload';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/elegance/pix/book.jpg';//edited by nihar
      if (!empty($extrasettingscertificate->certificatedownload)) {
      $files = $fs->get_area_files($extrasettingscertificate->contextid, 'user', $filearea ,
      $extrasettingscertificate->certificatedownload, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($extrasettingscertificate->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile; */
    $certificate = $DB->get_records('files', array('itemid' => $extrasettingscertificate->certificatedownload));
    foreach ($certificate as $certificateimg) {
        $fs = get_file_storage();
        $files = $fs->get_area_files($certificateimg->contextid, 'local_course_extrasettings', 'content', $extrasettingscertificate->certificatedownload, 'id', false);
        foreach ($files as $file) {
            $filename = $file->get_filename();
            if (!$filename <> '.') {
                $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $extrasettingscertificate->certificatedownload, $file->get_filepath(), $filename);
                return $url;
            }
        }
    }
}

function csetting_images($crsextrasettings) {
    global $CFG;
    /* $formitemname = 'courseimage';
      $filearea = 'draft';
      $filepath = '/';
      $fs = get_file_storage();
      //$picsfile = 'NA';
      $picsfile = $CFG->wwwroot.'/theme/elegance/pix/book.jpg';//edited by nihar
      if (!empty($crsextrasettings->courseimage)) {
      $files = $fs->get_area_files($crsextrasettings->contextid, 'user', $filearea ,
      $crsextrasettings->courseimage, 'id', false);
      foreach ($files as $file) {
      $picsfile = make_draftfileinstitution_url($crsextrasettings->contextid, $file->get_itemid(),
      $file->get_filepath(), $file->get_filename());
      $itemid = $file->get_itemid();
      $filename = $file->get_filename();
      }
      }
      return $picsfile; */
    /* var_dump($url); */
    $fs = get_file_storage();
    $files = $fs->get_area_files($crsextrasettings->contextid, 'local_course_extrasettings', 'content', $crsextrasettings->courseimage, 'id', false);

    foreach ($files as $file) {
        $filename = $file->get_filename();

        if (!$filename <> '.') {
            $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $crsextrasettings->courseimage, $file->get_filepath(), $filename);
            return $url;
        }
    }
}

/* added by Shiuli on 4th Oct for course page Icon */

function csetting_icons($crsextrasettings) {
    global $CFG;

    $fs = get_file_storage();
    $files = $fs->get_area_files($crsextrasettings->contextid, 'local_course_extrasettings', 'content', $crsextrasettings->courseicon, 'id', false);

    foreach ($files as $file) {
        $filename = $file->get_filename();

        if (!$filename <> '.') {
            $url = moodle_url::make_pluginfile_url($file->get_contextid(), $file->get_component(), $file->get_filearea(), $crsextrasettings->courseicon, $file->get_filepath(), $filename);
            return $url;
        }
    }
}

function make_draftfileinstitution_url($contextid, $draftid, $pathname, $filename, $forcedownload = false) {
    global $CFG, $USER;

    $urlbase = "$CFG->httpswwwroot/draftfile.php";
    return make_fileinstitution_url($urlbase, "/$contextid/user/draft/$draftid" . $pathname . $filename, $forcedownload);
}

function make_fileinstitution_url($urlbase, $path, $forcedownload = false) {
    $params = array();
    if ($forcedownload) {
        $params['forcedownload'] = 1;
    }

    $url = new moodle_url($urlbase, $params);
    $url->set_slashargument($path);
    return $url;
}

// Added by Shiuli for pathway details page.
//Lists of courses under each pathway as an array.
function course_under_pathway($pathwayid) {
    global $DB, $USER;
    $pathways = $DB->get_records('course_extrasettings_general', array('coursestatus' => 1), '', 'id,specializations, courseid');

    $courses = array();
    foreach ($pathways as $pathway) {
        $ids = explode(',', $pathway->specializations);
        if (in_array($pathwayid, $ids)) {
            if (!isset($courses[$pathway->courseid])) {
                $courses[] = $pathway->courseid;
            }
        }
    }

    return $courses;
}

/*
  |-----------------------
  | Enrol user in courses
  | by given pathwayid
  |---------------------
 */

function self_enrol_in_pathways($pathwayid, $userid = null) {
    global $DB, $CFG, $USER;
    if (is_null($userid)) {
        $userid = $USER->id;
    }
    $pathways = $DB->get_records('course_extrasettings_general', array('coursestatus' => 1), '', 'id,specializations, courseid');

    $courses = array();
    foreach ($pathways as $pathway) {
        $ids = explode(',', $pathway->specializations);
        if (in_array($pathwayid, $ids)) {
            if (!isset($courses[$pathway->courseid])) {
                $courses[$pathway->courseid] = $pathway->courseid;
            }
        }
    }


    if (count($courses) > 0) {
        if (enrol_is_enabled('self')) {
            $enrolledcourses = array();
            $self = enrol_get_plugin('self');
            foreach ($courses as $course) {
                $enrolinstance = null;
                if ($instances = enrol_get_instances($course, false)) {
                    foreach ($instances as $instance) {
                        if ($instance->enrol === 'self') {
                            $enrolinstance = $instance;
                            break;
                        }
                    }
                }

                if ($enrolinstance != null) {
                    $self->enrol_user($enrolinstance, $userid, 5, time(), time() + 400000000000, ENROL_USER_ACTIVE);
                    $enrolledcourses[] = $course;
                }
            }
            // entry to custom table.
            $getRec = $DB->record_exists('custom_pathway_enrolment', array('pathwayid' => $pathwayid,
                'userid' => $userid));
            if ($getRec) {
                $result = $DB->get_record('custom_pathway_enrolment', array('pathwayid' => $pathwayid,
                    'userid' => $userid));
            }
            if (!$getRec) {
                $records = new stdClass();
                $records->pathwayid = $pathwayid;
                $records->userid = $userid;
                $records->enrolmethod = 'self';
                $records->timecreated = time();
                $records->timemodified = NULL;
                $DB->insert_record('custom_pathway_enrolment', $records);
            } else {
                $records1 = new stdClass();
                $records1->id = $result->id;
                $records1->pathwayid = $result->pathwayid;
                $records1->userid = $result->userid;
                $records1->enrolmethod = $result->enrolmethod;
                $records1->timecreated = $result->timecreated;
                $records1->timemodified = time();
                $DB->update_record('custom_pathway_enrolment', $records1);
            }
            return array('status' => true, 'courses' => $enrolledcourses);
        } else {
            throw new moodle_exception('self enrolment plugin not enabled.');
        }
    }
    return array('status' => false, 'msg' => 'No Courses present under this pathway');
}

// Added by Shiuli for delete an user from the site.
function custom_user_delete($user) {
    global $USER, $CFG;
    //  if (($USER->auth == 'email' || $USER->auth == 'manual')) {
    delete_user($USER);
    require_logout();

    //  }
    return true;
}
