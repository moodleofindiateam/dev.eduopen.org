<?php

/**
 * cron
 * @return boolean
 */

namespace block_alert\task;

defined('MOODLE_INTERNAL') || die();

class task_send_mail extends \core\task\scheduled_task {

    public function get_name() {
        // Shown in admin screens
        return get_string('pluginname', 'block_alert');
    }

    public function execute() {
        require_once(dirname(__FILE__) . "/../../inc.php");
        global $DB, $OUTPUT, $PAGE, $USER, $CFG;

        // Delete those records where courseid is not present in moodle database.
        $alerts = $DB->get_records('block_alert');
        foreach ($alerts as $alert) {
            $recExist = $DB->record_exists('course', array('id' => $alert->courseid));
            if (!$recExist) {
                $alertmailsents = $DB->get_records_sql("SELECT * FROM
                    {block_alert_mailssent} WHERE reminderid=$alert->id");
                foreach ($alertmailsents as $alertmailsent) {
                    $DB->execute("DELETE FROM
                        {block_alert_mailssent} WHERE id=$alertmailsent->id");
                }
                $DB->execute("DELETE FROM {block_alert} WHERE courseid=$alert->courseid");
            }
        }

        // Sending Reminder Portion.
        require_once($CFG->dirroot . "/completion/completion_completion.php");
        $entries = block_alert_get_pending_reminders();

        foreach ($entries as $entry) {
            $mailssent = 0;
            $cronruntime = time(); // Currenttime.
            $creator = $DB->get_record('user', array('id' => $entry->createdby));
            $course = $DB->get_record('course', array('id' => $entry->courseid));
            if (isset($course) && !empty($course)) {
                $coursecontext = \context_course::instance($course->id);
                $users = block_alert_filter_users($entry);
                $managers = array();
                // Go through users and send mails.
                foreach ($users as $user) {
                    $user->mailformat = FORMAT_HTML;
                    $lastaccess = $DB->get_field('user_lastaccess', 'timeaccess', array('userid' => $user->id, 'courseid' => $entry->courseid)); // Gives timestamp value.

                    $certificatedownload = $DB->get_records_sql("SELECT ce.id, ce.course, ci.certificateid, ci.userid, 
                ci.timecreated FROM {certificate} ce JOIN {certificate_issues} ci on ci.certificateid=ce.id 
                WHERE ce.course=$entry->courseid AND ci.userid=$user->id"); // If null then send mail


                    $params = array('userid' => $user->id, 'course' => $entry->courseid);
                    if ($params) {
                        $ccompletion = new \completion_completion($params);
                    }
                    $criterias = BLOCK_ALERT_CRITERIA_NOT_COMPLETED;
                    if ($ccompletion->is_complete()) {
                        $criterias = BLOCK_ALERT_CRITERIA_COMPLETION;
                    }
                    $criterias1 = BLOCK_ALERT_CRITERIA_NOTONLY_COMPLETED;
                    if ($ccompletion->is_complete()) {
                        $criterias1 = BLOCK_ALERT_CRITERIA_ONLY_COMPLETION;
                    }

                    $inact = $entry->inactive;
                    //var_dump($entry->text);
                    // Mail to Students.
                    $mailtext = block_alert_replace_placeholders($entry->text, $course->fullname, fullname($user), $user->email, '', '', $inact);
                    //var_dump($mailtext);
                    //var_dump($user);
                    // Send mail only if below conditions are satisfied.
                    $conditionA = ($entry->condition1 == 1); // MUST.
                    $conditionB = (($cronruntime - $lastaccess) >= ($entry->inactive * 86400)); // Inactive for.
                    $conditionC = (empty($certificatedownload) && ($certificatedownload == null)); // Certificate.
                    $conditionD = ($criterias == $entry->criteria); // Check Course completed or not.
                    $conditionD1 = ($criterias1 == $entry->criteria); // Check Course completed or not without Certificate download.
                    $conditionE = ($entry->sent == 0);


                    if (($conditionA && $conditionB && $conditionC && $conditionD && $conditionE) OR ( $conditionA && $conditionB && $conditionD1 && $conditionE)) {

                        email_to_user($user, $creator, $entry->subject, strip_tags($mailtext), $mailtext);
                        $mailssent++;
                        $DB->insert_record('block_alert_mailssent', array('userid' => $user->id,
                            'reminderid' => $entry->id, 'timetosend' => $cronruntime));

                        $event = \block_alert\event\send_mail::create(array(
                                    'objectid' => $creator->id,
                                    'context' => $coursecontext,
                                    'other' => 'student was notified',
                                    'relateduserid' => $user->id
                        ));
                        $event->trigger();
                        mtrace("A Reminder mail was sent to student $user->id for $entry->subject on $cronruntime");
                    }
                    // Mail sending time. We need the Last value only.
                    $mailsent = $DB->get_records_sql("SELECT DISTINCT timetosend FROM
                        {block_alert_mailssent} 
                        WHERE reminderid=$entry->id AND userid=$user->id");
                    if ($mailsent) {
                        foreach ($mailsent as $msent) {
                            $mailsenttime = $msent->timetosend; // Take last one.
                        }
                        $mailsenttime = $msent->timetosend;
                        //var_dump($mailsenttime);
                        $recurrentime = $entry->recurrent * 86400;

                        $sendingtimestamp = strtotime('+ ' . $recurrentime . 'sec', $mailsenttime); // Next mail sending time.
                        $conditionF = ($sendingtimestamp <= $cronruntime); // Next mail to send.
                        //exit();
                        if ($conditionA && $conditionB && $conditionC && $conditionD && $conditionF) {
                            email_to_user($user, $creator, $entry->subject, strip_tags($mailtext), $mailtext);
                            $mailssent++;
                            $DB->insert_record('block_alert_mailssent', array('userid' => $user->id,
                                'reminderid' => $entry->id, 'timetosend' => $cronruntime));
                            $event = \block_alert\event\send_mail::create(array(
                                        'objectid' => $creator->id,
                                        'context' => $coursecontext,
                                        'other' => 'student was notified',
                                        'relateduserid' => $user->id
                            ));
                            $event->trigger();
                            mtrace("A Reminder mail was sent to student $user->id for $entry->subject on $cronruntime");
                        }
                    }
                }

                // Mail to Teacher.
                $mailreceiver = $DB->get_records_sql("SELECT DISTINCT userid FROM
                    {block_alert_mailssent} WHERE reminderid=$entry->id");

                $mailtext = block_alert_get_mail_text($course->fullname, $users, $entry->text_teacher, $entry->inactive);
                if ($entry->to_reporttrainer && $mailssent > 0) {

                    // Get course teachers and send mails, and additional mails.
                    $teachers = block_alert_get_course_teachers($coursecontext);
                    foreach ($teachers as $teacher) {
                        email_to_user($teacher, $creator, $entry->subject, $mailtext); // Changed by G. Schwed (DUK).

                        $event = \block_alert\event\send_mail::create(array(
                                    'objectid' => $creator->id,
                                    'context' => $coursecontext,
                                    'other' => 'teacher was notified',
                                    'relateduserid' => $teacher->id
                        ));
                        $event->trigger();
                        mtrace("A Report mail was sent to teacher $teacher->id");
                    }
                }
                // Set sentmails.
                $entry->mailssent += $mailssent;
                // Set sent.
                $entry->sent = 1;

                $DB->update_record('block_alert', $entry);
            }
        }
        return true;
    }

}
