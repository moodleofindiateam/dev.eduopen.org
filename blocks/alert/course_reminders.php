<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * course_reminders.php
 *
 * @package    block_alert
 * @copyright  gtn gmbh <office@gtn-solutions.com>
 * @author       Florian Jungwirth <fjungwirth@gtn-solutions.com>
 * @ideaandconcept Gerhard Schwed <gerhard.schwed@donau-uni.ac.at>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(__FILE__)."/inc.php");
global $DB, $OUTPUT, $PAGE, $cg;
require_once($CFG->libdir . "/tablelib.php");

$courseid = required_param('courseid', PARAM_INT);
$sorting = optional_param('sorting', 'id', PARAM_TEXT);
$sorttype = optional_param('type', 'asc', PARAM_TEXT);

if (!$course = $DB->get_record('course', array('id' => $courseid))) {
    print_error('invalidcourse', 'block_simplehtml', $courseid);
}

require_login($course);

$context = context_course::instance($courseid);
require_capability('block/alert:use', $context);

/* DELETE */
if (($deleteid = optional_param('delete', 0, PARAM_INT)) > 0) {
    $deleterecord = $DB->get_record('block_alert', array('id' => $deleteid));
    if ($deleterecord->courseid == $courseid) {
        $DB->delete_records('block_alert', array('id' => $deleteid));
        $DB->delete_records('block_alert_mailssent', array('reminderid' => $deleteid));
    };
}

$pageidentifier = 'tab_course_reminders';

$PAGE->set_url('/blocks/alert/course_reminders.php', array('courseid' => $courseid));
$PAGE->set_heading(get_string('pluginname', 'block_alert'));
$PAGE->set_title(get_string($pageidentifier, 'block_alert'));
$PAGE->set_pagelayout('admin');


// Build breadcrumbs navigation.
$coursenode = $PAGE->navigation->find($courseid, navigation_node::TYPE_COURSE);
$blocknode = $coursenode->add(get_string('pluginname', 'block_alert'));
$pagenode = $blocknode->add(get_string($pageidentifier, 'block_alert'), $PAGE->url);
$pagenode->make_active();

// Build tab navigation & print header.
echo $OUTPUT->header();
echo $OUTPUT->tabtree(block_alert_build_navigation_tabs($courseid), $pageidentifier);

/* CONTENT REGION */
$status = array(0 => get_string('form_to_status_completed', 'block_alert'),
    1 => get_string('form_to_status_notcompleted', 'block_alert'));
$table = new html_table();

$table->head = array(html_writer::link($PAGE->url . "&sorting=title", get_string('form_title', 'block_alert')),
        html_writer::link($PAGE->url . "&sorting=subject", get_string('form_subject', 'block_alert')),
        html_writer::link($PAGE->url . "&sorting=inactive&type=desc", get_string('form_inactive', 'block_alert')),
        html_writer::link($PAGE->url . "&sorting=recurrent&type=desc", get_string('recurrent', 'block_alert')),
        html_writer::link($PAGE->url . "&sorting=criteria&type=desc", get_string('form_criteria', 'block_alert')),
        html_writer::link($PAGE->url . "&sorting=mailssent&type=desc", get_string('form_mailssent', 'block_alert')),
        '');


$data = $DB->get_records('block_alert',
    array('courseid' => $courseid),
    $sorting . ' ' . $sorttype,
    'id, title, subject, inactive, recurrent, criteria, mailssent');
foreach ($data as $record) {
    //$record->dateabsolute = ($record->dateabsolute > 0) ? date('d.m.Y', $record->dateabsolute) : '-';
    // HIDE $record->to_status = $status[$record->to_status];.
    //$record->inactive = ($record->inactive > 0) ? ($record->inactive / 86400) . ' days': '-';
    $record->inactive = ($record->inactive > 0) ? ($record->inactive) . ' days': '-';
    $record->recurrent = ($record->recurrent > 0) ? ($record->recurrent) . ' days': '-';
    $record->criteria = block_alert_get_criteria($record->criteria);
    $record->actions =
        html_writer::link(
            new moodle_url('/blocks/alert/new_reminder.php', array('courseid' => $COURSE->id, 'reminderid' => $record->id)),
            $OUTPUT->pix_icon("t/editstring", "edit"))
        .' '.html_writer::link(
            new moodle_url('/blocks/alert/course_reminders.php',
                array('courseid' => $COURSE->id, 'sorting' => $sorting, 'delete' => $record->id)),
            $OUTPUT->pix_icon("t/delete", "delete"),
            array("onclick" => "return confirm('".get_string('form_delete', 'block_alert')."')"));

    // Don't display id, it is only used for the delete link.
    unset($record->id);
}
$table->data = $data;
echo html_writer::table($table);

/* END CONTENT REGION */

echo $OUTPUT->footer();
