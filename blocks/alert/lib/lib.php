<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Collection of useful functions and constants
 *
 * @package    block_alert
 * @copyright  gtn gmbh <office@gtn-solutions.com>
 * @author       Florian Jungwirth <fjungwirth@gtn-solutions.com>
 * @ideaandconcept Gerhard Schwed <gerhard.schwed@donau-uni.ac.at>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */


define('BLOCK_ALERT_PLACEHOLDER_COURSENAME', '###coursename###');
define('BLOCK_ALERT_PLACEHOLDER_USERNAME', '###fullname###');
define('BLOCK_ALERT_PLACEHOLDER_USERMAIL', '###usermail###');
define('BLOCK_ALERT_PLACEHOLDER_USERCOUNT', '###usercount###');
define('BLOCK_ALERT_PLACEHOLDER_USERS', '###users_fullname###');
define('BLOCK_ALERT_PLACEHOLDER_INACTIVE', '###inactive###');

define('BLOCK_ALERT_CRITERIA_COMPLETION', 250000);
define('BLOCK_ALERT_CRITERIA_NOT_COMPLETED', 250002);
define('BLOCK_ALERT_CRITERIA_ONLY_COMPLETION', 250003);
define('BLOCK_ALERT_CRITERIA_NOTONLY_COMPLETED', 250004);

// SHOULD BE CHANGED.
define('BLOCK_ALERT_EMAIL_DUMMY', 2);

/**
 * Build navigation tabs
 * @param integer $courseid
 */
function block_alert_build_navigation_tabs($courseid) {

    $rows[] = new tabobject('tab_course_reminders',
        new moodle_url('/blocks/alert/course_reminders.php',
        array("courseid" => $courseid)),
        get_string('tab_course_reminders', 'block_alert'));
    $rows[] = new tabobject('tab_new_reminder',
        new moodle_url('/blocks/alert/new_reminder.php',
        array("courseid" => $courseid)),
        get_string('tab_new_reminder', 'block_alert'));
    return $rows;
}

/**
 * Init Js and CSS
 * @return nothing
 */
function block_alert_init_js_css() {

}
/**
 * This function gets all the pending reminder entries. An entry is pending
 * if dateabsolute is set and it is not sent yet (sent = 0)
 * OR
 * if daterelative is set
 *
 * @return array $entries
 */
function block_alert_get_pending_reminders() {
    global $DB;
    $entries = $DB->get_records('block_alert');
    return $entries;
}

/**
 * Replace placeholders
 * @param string $text
 * @param string $coursename
 * @param string $username
 * @param string $usermail
 * @param string $users
 * @param string $usercount
 * @return nothing
 */
function block_alert_replace_placeholders($text, $coursename = '', $fullname = '',
            $usermail = '', $users_fullname = '', $usercount = '', $inactive) {

    $textparams1 = new stdClass();
    $textparams1->inactive  = $inactive;

    $text = str_replace(BLOCK_ALERT_PLACEHOLDER_COURSENAME, $coursename, $text);
    $text = str_replace(BLOCK_ALERT_PLACEHOLDER_USERMAIL, $usermail, $text);
    $text = str_replace(BLOCK_ALERT_PLACEHOLDER_USERNAME, $fullname, $text);
    $text = str_replace(BLOCK_ALERT_PLACEHOLDER_USERCOUNT, $usercount, $text);
    $text = str_replace(BLOCK_ALERT_PLACEHOLDER_USERS, $users_fullname, $text);
    $text = str_replace(BLOCK_ALERT_PLACEHOLDER_INACTIVE, $inactive, $text);

     

    return $text;
}

/**
 * This function filters the users to recieve a reminder according to the
 * criterias recorded in the database.
 * The criterias are:
 *  - deadline: amount of sec after course enrolment
 *  - groups: user groups specified in the course
 *  - completion status: if users have already completed/not completed the course
 *
 * @param stdClass $entry database entry of block_alert table
 * @return array $users users to recieve a reminder
 */
function block_alert_filter_users($entry) {
    global $DB;
    // All potential users.
    $users = get_role_users(5, context_course::instance($entry->courseid));
    return $users;
}

/**
 * Get manager
 * @param object $user
 * @return boolean
 */
function block_alert_get_manager($user) {
    global $DB;
    // Bestimme Vorgesetzten (= Manager) zum User
    if (isset($user->address)) { // Vorgesetzte stehen in Moodle im Adressfeld des Users
        $manager = addslashes(substr($user->address, 0, 50)) . "%"; // addslashes wegen ' in manchen Usernamen
        // Suche userid des Vorgesetzten in mdl_user.
        $select = "idnumber LIKE '$manager'";
        $managerid = $DB->get_field_select('user', 'id', $select);

        // Hole Details des Vorgesetzten aus mdl_user.
        return $DB->get_record('user', array('id' => $managerid));
        /* $managers[$managerid]->username = $DB->get_field_select('user', 'username', $select);
         $managers[$managerid]->firstname = $DB->get_field_select('user', 'firstname', $select);
        $managers[$managerid]->lastname = $DB->get_field_select('user', 'lastname', $select);
        $managers[$managerid]->email = $DB->get_field_select('user', 'email', $select);
        */
    }
    return false;
}

/**
 * Replace placeholders
 * @param string $course
 * @param array $users
 * @param boolean $textteacher
 * @return nothing
 */
function block_alert_get_mail_text($course, $users, $textteacher = null, $inactive) {
    //global $DB;
    //$mailreceiver = $DB->get_record_sql("SELECT DISTINCT userid FROM {block_alert_mailssent}
    //WHERE reminderid=$entry->id");
    $userlisting = '';

    foreach ($users as $user) {
        //if ($mailreceiver->userid == $user->id) { // List those users who got the mail only.
            $userlisting .= ' ' . fullname($user). ",";// Seperate usernames by using delemeters.
        //}
    }

    // If text_teacher is not set, use lang string (for old reminders).
    if (!$textteacher) {
        $textparams = new stdClass();
        $textparams->amount = count($users);
        $textparams->course  = $course;
        $textparams->inactive  = $inactive; //Show it to teacher.

        $mailtext = get_string('email_teacher_notification', 'block_alert', $textparams);
        $mailtext .= $userlisting;
    } else {
        // If text_teacher is set, use it and replace placeholders.

        //if ($mailreceiver->userid == $user->id) {
            $mailtext = block_alert_replace_placeholders($textteacher, $course, '', '', $userlisting, count($users), $inactive);
            $mailtext = strip_tags($mailtext);
       /* } else {
            $mailtext = block_alert_replace_placeholders($textteacher, $course, '', '', $userlisting, count($entry->id));
            $mailtext = strip_tags($mailtext);
        }*/
    }

    return $mailtext;
}

/**
 * Get course teachers
 * @param string $coursecontext
 * @return array
 */
function block_alert_get_course_teachers($coursecontext) {
    return array_merge(get_role_users(4, $coursecontext),
        get_role_users(3, $coursecontext),
        get_role_users(2, $coursecontext),
        get_role_users(1, $coursecontext));
}

/**
 * Get criteria
 * @param string $entry
 * @return string
 */
function block_alert_get_criteria($entry) {
    global $DB;

    if ($entry == BLOCK_ALERT_CRITERIA_COMPLETION) {
        return get_string('form_condition3', 'block_alert');
    };
    if ($entry == BLOCK_ALERT_CRITERIA_NOT_COMPLETED) {
        return get_string('form_condition2', 'block_alert');
    };
    if ($entry == BLOCK_ALERT_CRITERIA_ONLY_COMPLETION) {
        return get_string('form_condition4', 'block_alert');
    };
    if ($entry == BLOCK_ALERT_CRITERIA_NOTONLY_COMPLETED) {
        return get_string('form_condition5', 'block_alert');
    }
}
