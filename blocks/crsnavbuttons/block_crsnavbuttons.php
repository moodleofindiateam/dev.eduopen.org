<?php

// This file is part of the Navigation buttons plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

class block_crsnavbuttons extends block_base {

    function init() {
        $this->title = get_string('crsnavbuttons', 'block_crsnavbuttons');
    }

    function has_config() {
        return true;
    }

    function get_content() {
        global $CFG;

        if (!has_capability('moodle/course:manageactivities', $this->context)) {
            return NULL;
        }

        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->footer = '';

        if ($CFG->version < 2012120300) {
            $courseid = get_courseid_from_context($this->context);
        } else {
            $coursecontext = $this->context->get_course_context(true);
            $courseid = $coursecontext->instanceid;
        }
        $editlink = new moodle_url('/blocks/crsnavbuttons/edit.php', array('course' => $courseid));
        $this->content->text = '<a href="' . $editlink . '">' . get_string('editsettings', 'block_crsnavbuttons') . '</a>';

        return $this->content;
    }

    public function applicable_formats() {
        return array(
            'course' => true,
            'site' => false,
            'course-category' => false,
        );
    }

}
