<?php

// This file is part of the Navigation buttons plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

require_once(dirname(__FILE__) . '/definitions.php');
require_once(dirname(__FILE__) . '/activityready.php');

function custom_section_name($courseid) {
    global $COURSE, $DB, $CFG, $PAGE;
    $courseid = $COURSE->id;
    $cmid = $PAGE->cm->id;
    $ModCIcon = '';
    // Get the particular section in which module is present.
    $sql1 = "SELECT DISTINCT cs.section, cs.name, cs.sequence, cm.instance, cm.module FROM {course_modules} cm
    JOIN {course_sections} cs ON cs.course=cm.course AND cs.id=cm.section
    WHERE cs.visible=1 AND cm.course=$courseid AND cm.id=$cmid ORDER BY cs.section";
    $modsection = $DB->get_record_sql($sql1);
    $seqArr = explode(',', $modsection->sequence);

    // count only visible activities.
    $ij = 0;
    for ($ij = 0; $ij < count($seqArr); $ij++) {
        $labelSQL = $DB->get_record('modules', array('name' => 'eduplayer'), 'id');
        $RecExists = $DB->get_record_sql("SELECT * FROM {course_modules} WHERE id=$seqArr[$ij] AND
                    visible=1 AND module=$labelSQL->id");
        if ($RecExists) {
            $visibleAct[] = $seqArr[$ij];
        }
    }

    if (!empty($visibleAct)) {
        $countsec = count($visibleAct);
        $key = array_search($cmid, $visibleAct);
    }

    //$countsec = count($seqArr);
    $modtab = $DB->get_record('modules', array('id' => $modsection->module));
    $modinfo = get_fast_modinfo($COURSE);
    foreach ($modinfo->cms as $mod) {
        if (isset($_REQUEST['id'])) {
            if ($_REQUEST['id'] == $mod->id) {
                $modNames[] = $mod->modname;
                $CurrentModIcon = '';
                if ($mod->modname != 'eduplayer') {
                    $CurrentModIcon = $mod->get_icon_url();
                    $ModCIcon = '<span class="currentIcon"><img class="img-responsive" src="' . $CurrentModIcon . '"></span>';
                }
            }
        }
    }
    $modtab1 = $DB->get_record($modtab->name, array('id' => $modsection->instance));
    echo '<p class="ActName">' . $ModCIcon . '<span class="secName">' . $modtab1->name . '</span>';
    if (isset($modNames)) {
        if ($modNames[0] == 'eduplayer') {
            echo '<p class="lesscount">Lecture ' . ($key + 1) . ' of ' . $countsec . '</p>';
        }
    }
    echo '<p class="secname">' . $modsection->name . '</p>';
}

function draw_crsprevbuttons($courseid) {
    global $COURSE, $DB, $CFG, $PAGE;
    $courseid = $COURSE->id;

    $settings = new stdClass();
    $settings->course = $courseid;
    $settings->enabled = 1;
    $settings->buttonstype = 'text';
    $settings->homebuttonshow = 1;
    $settings->homebuttontype = 2;
    $settings->prevbuttonshow = 1;
    $settings->nextbuttonshow = 1;
    $settings->lastbuttonshow = 1;
    $settings->lastbuttontype = 1;

    $output = '<!-- crsnavbuttons start -->';
    $outend = '<!-- crsnavbuttons end -->';

    if (isset($CFG->crsnavbuttons_self_test)) {
        $CFG->crsnavbuttons_self_test = 0; // All OK
        return $output . '<!-- Self test -->' . $outend;
    }

    if ($COURSE->id <= 1) {
        return $output . '<!-- Front page -->' . $outend;
    }
    if (!$settings) {
        return $output . '<!-- No settings -->' . $outend;
    }

    if (!$settings->enabled) {
        return $output . '<!-- Not enabled -->' . $outend;
    }
    if (!$PAGE->cm) {
        return $output . '<!-- No course module -->' . $outend;
    }
    if (!crsnavbuttons_activity_showbuttons($PAGE->cm)) {
        return $output . '<!-- Activity not ready for crsnavbuttons -->' . $outend;
    }

    $cmid = $PAGE->cm->id;

    $modinfo = get_fast_modinfo($COURSE);
    if ($CFG->version < 2011120100) {
        $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
    } else {
        $context = context_course::instance($COURSE->id);
    }
    $sections = $DB->get_records('course_sections', array('course' => $COURSE->id), 'section', 'section,visible,summary, name');

    $next = false;
    $prev = false;
    $firstcourse = false;
    $firstsection = false;
    $lastcourse = false;
    $lastsection = false;

    $sectionnum = -1;
    $thissection = null;
    $firstthissection = false;
    $flag = false;
    $sectionflag = false;
    $previousmod = false;
    foreach ($modinfo->cms as $mod) {
        // Get the particular section in which module is present.
        $cmidd = $_REQUEST['id'];
        $sql1 = "SELECT DISTINCT cs.section, cs.name, cs.sequence, cm.instance,
            cm.module FROM {course_modules} cm JOIN {course_sections} cs ON cs.course=cm.course
            AND cs.id=cm.section WHERE cs.visible=1 AND cm.course=$COURSE->id
            AND cm.id=$cmidd ORDER BY cs.section";
        $modsection = $DB->get_record_sql($sql1);

        // Show previous button only for Eduplayer Activity.
        // Show previous button only for Eduplayer Activity & only for one particular Section.
        if (($mod->modname == 'eduplayer') && ($mod->sectionnum == $modsection->section)) {
            $ModIcon = $mod->get_icon_url();

            if ($mod->modname == 'label') {
                continue;
            }

            if ($CFG->version >= 2012120300) { // Moodle 2.4
                $opts = course_get_format($COURSE)->get_format_options();
                if ($opts['numsections'] && $mod->sectionnum > $opts['numsections']) {
                    break;
                }
            } else {
                if ($mod->sectionnum > $COURSE->numsections) {
                    break;
                }
            }

            if (!$mod->uservisible) {
                continue;
            }

            if ($mod->sectionnum > 0 && $sectionnum != $mod->sectionnum) {
                $thissection = $sections[$mod->sectionnum];

                if ($thissection->visible || !$COURSE->hiddensections ||
                        has_capability('moodle/course:viewhiddensections', $context)) {
                    $sectionnum = $mod->sectionnum;
                    $firstthissection = false;
                    if ($sectionflag) {
                        if ($flag) { // flag means selected mod was the last in the section
                            $lastsection = 'none';
                        } else {
                            $lastsection = $previousmod;
                        }
                        $sectionflag = false;
                    }
                } else {
                    continue;
                }
            }

            $thismod = new stdClass;
            $thismod->link = new moodle_url('/mod/' . $mod->modname . '/view.php', array('id' => $mod->id));
            $thismod->name = strip_tags(format_string($mod->name, true));
            $thismod->iconsrc = $ModIcon;
            $thismod->modulename = $mod->modname;

            if ($flag) { // Current mod is the 'next' mod
                $next = $thismod;
                $flag = false;
            }

            if ($cmid == $mod->id) {
                $flag = true;
                $sectionflag = true;
                $prev = $previousmod;
            }

            $firstsection = $firstthissection;
            if (!$firstcourse) {
                $firstcourse = 'none'; // Prevent the 'firstcourse' link if this is the first item
            }

            if (!$firstthissection) {
                $firstthissection = $thismod;
            }
            if (!$firstcourse) {
                $firstcourse = $thismod;
            }

            $previousmod = $thismod;
        }
    }

    if (!$flag) { // flag means selected mod is the last in the course
        if (!$lastsection) {
            $lastsection = $previousmod;
        }
        $lastcourse = $previousmod;
    }
    if ($firstcourse == 'none') {
        $firstcourse = false;
    }
    if ($lastsection == 'none') {
        $lastsection = false;
    }
    $output .= '<div id="crsprevbuttons">';
    // Previous Button.

    if ($settings->prevbuttonshow && $prev) {
        $prevPath = $prev->link->get_path();
        $PrevPathRes = strchr($prevPath, 'resource');

        if (($COURSE->format == 'eduopen') || ($COURSE->format == 'topcoll')) {
            if (isset($_REQUEST['section'])) {
                $PrevUrl = $prev->link . '&section=' . $_REQUEST['section'];
            } else {
                $PrevUrl = $prev->link;
            }
        } else {
            $PrevUrl = $prev->link;
        }

        $modicon = html_writer::link(new moodle_url($PrevUrl), html_writer::empty_tag('img', array(
                            'src' => $prev->iconsrc,
                            'alt' => get_string('prev_lecture', 'block_crsnavbuttons'))) .
                        '<span class="Prv">' . get_string('prevactivity', 'block_crsnavbuttons')
                        . '</span>', array('title' => get_string('prev_lecture', 'block_crsnavbuttons')));
        list($icon, $bgcolour) = crsnavbutton_get_icon($settings->buttonstype, 'prev', $context, BLOCK_crsnavbuttons_PREVICON);
        $output .= make_crsprevbutton($icon, $bgcolour, $modicon, $prev->name, $PrevUrl);
    }
    $output .= '</div>';
    return $output;
}

// Next Button.
function draw_crsnextbuttons($courseid) {
    global $COURSE, $DB, $CFG, $PAGE;
    $courseid = $COURSE->id;
    $settings = new stdClass();
    $settings->course = $courseid;
    $settings->enabled = 1;
    $settings->buttonstype = 'text';
    $settings->homebuttonshow = 1;
    $settings->homebuttontype = 2;
    $settings->prevbuttonshow = 1;
    $settings->nextbuttonshow = 1;
    $settings->lastbuttonshow = 1;
    $settings->lastbuttontype = 1;

    $output = '<!-- crsnavbuttons start -->';
    $outend = '<!-- crsnavbuttons end -->';

    if (isset($CFG->crsnavbuttons_self_test)) {
        $CFG->crsnavbuttons_self_test = 0; // All OK
        return $output . '<!-- Self test -->' . $outend;
    }

    if ($COURSE->id <= 1) {
        return $output . '<!-- Front page -->' . $outend;
    }
    if (!$settings) {
        return $output . '<!-- No settings -->' . $outend;
    }

    if (!$settings->enabled) {
        return $output . '<!-- Not enabled -->' . $outend;
    }
    if (!$PAGE->cm) {
        return $output . '<!-- No course module -->' . $outend;
    }
    if (!crsnavbuttons_activity_showbuttons($PAGE->cm)) {
        return $output . '<!-- Activity not ready for crsnavbuttons -->' . $outend;
    }

    $cmid = $PAGE->cm->id;


    $modinfo = get_fast_modinfo($COURSE);
    if ($CFG->version < 2011120100) {
        $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
    } else {
        $context = context_course::instance($COURSE->id);
    }
    $sections = $DB->get_records('course_sections', array('course' => $COURSE->id), 'section', 'section,visible,summary,name');

    $next = false;
    $prev = false;
    $firstcourse = false;
    $firstsection = false;
    $lastcourse = false;
    $lastsection = false;

    $sectionnum = -1;
    $thissection = null;
    $firstthissection = false;
    $flag = false;
    $sectionflag = false;
    $previousmod = false;

    foreach ($modinfo->cms as $mod) {
        if ($mod->modname == 'label') {
            continue;
        }

        if ($CFG->version >= 2012120300) { // Moodle 2.4
            $opts = course_get_format($COURSE)->get_format_options();
            if ($opts['numsections'] && $mod->sectionnum > $opts['numsections']) {
                break;
            }
        } else {
            if ($mod->sectionnum > $COURSE->numsections) {
                break;
            }
        }

        if (!$mod->uservisible) {
            continue;
        }

        if ($mod->sectionnum > 0 && $sectionnum != $mod->sectionnum) {
            $thissection = $sections[$mod->sectionnum];
            if ($thissection->visible || !$COURSE->hiddensections ||
                    has_capability('moodle/course:viewhiddensections', $context)) {
                $sectionnum = $mod->sectionnum;
                $firstthissection = false;
                if ($sectionflag) {
                    if ($flag) { // flag means selected mod was the last in the section
                        $lastsection = 'none';
                    } else {
                        $lastsection = $previousmod;
                    }
                    $sectionflag = false;
                }
            } else {
                continue;
            }
        }

        $thismod = new stdClass;
        $thismod->link = new moodle_url('/mod/' . $mod->modname . '/view.php', array('id' => $mod->id));
        $thismod->name = strip_tags(format_string($mod->name, true));
        $thismod->modulename = $mod->modname;
        $thismod->sections = $mod->sectionnum;

        // Get the particular section in which module is present.
        $cmidd = $_REQUEST['id'];
        $sql1 = "SELECT DISTINCT cs.section, cs.name, cs.sequence, cm.instance,
            cm.module FROM {course_modules} cm JOIN {course_sections} cs ON cs.course=cm.course
            AND cs.id=cm.section WHERE cs.visible=1 AND cm.course=$COURSE->id
            AND cm.id=$cmidd ORDER BY cs.section";
        $modsection = $DB->get_record_sql($sql1);

        // Show next button only for eduplayer activity.
        // Show next button only for eduplayer activity upto One particular Section.
        if ($flag && ($thismod->modulename == 'eduplayer') && ($thismod->sections == $modsection->section)) { // Current mod is the 'next' mod
            //if ($flag) {
            $next = $thismod;
            $modnexticon = $mod->get_icon_url();
            $flag = false;
        }
        if ($cmid == $mod->id) {
            $flag = true;
            $sectionflag = true;
            $prev = $previousmod;
            $firstsection = $firstthissection;
            if (!$firstcourse) {
                $firstcourse = 'none'; // Prevent the 'firstcourse' link if this is the first item
            }
        }
        if (!$firstthissection) {
            $firstthissection = $thismod;
        }
        if (!$firstcourse) {
            $firstcourse = $thismod;
        }

        $previousmod = $thismod;
    }
    if (!$flag) { // flag means selected mod is the last in the course
        if (!$lastsection) {
            $lastsection = $previousmod;
        }
        $lastcourse = $previousmod;
    }
    if ($firstcourse == 'none') {
        $firstcourse = false;
    }
    if ($lastsection == 'none') {
        $lastsection = false;
    }

    $output .= '<div id="crsnextbuttons">';
    // Next Button.
    if ($settings->nextbuttonshow && $next) {
        $nextPath = $next->link->get_path();
        $NextPathRes = strchr($nextPath, 'resource');

        if (($COURSE->format == 'eduopen') || ($COURSE->format == 'topcoll')) {
            if (isset($_REQUEST['section'])) {
                $NextUrl = $next->link . '&section=' . $_REQUEST['section'];
            } else {
                $NextUrl = $next->link;
            }
        } else {
            $NextUrl = $next->link;
        }

        $modicon = html_writer::link(new moodle_url($NextUrl), html_writer::empty_tag('img', array(
                            'src' => $modnexticon,
                            'alt' => get_string('next_lecture', 'block_crsnavbuttons'))) .
                        '<span class="nex">' . get_string('nextactivity', 'block_crsnavbuttons') .
                        '</span>', array('title' => get_string('next_lecture', 'block_crsnavbuttons')));
        list($icon, $bgcolour) = crsnavbutton_get_icon($settings->buttonstype, 'next', $context, BLOCK_crsnavbuttons_NEXTICON);
        $output .= make_crsnextbutton($icon, $bgcolour, $modicon, $next->name, $NextUrl);
    }
    $output .= '</div>';
    $output .= '<br style="clear:both;
    " />';
    $output .= $outend;
    $output .= '</div>';

    return $output;
}

// Course Back Button.
function draw_crsbackbuttons($courseid) {
    global $COURSE, $DB, $CFG, $PAGE;
    $courseid = $COURSE->id;

    $settings = new stdClass();
    $settings->course = $courseid;
    $settings->enabled = 1;
    $settings->buttonstype = 'text';
    $settings->homebuttonshow = 1;
    $settings->homebuttontype = 2;
    $settings->prevbuttonshow = 1;
    $settings->nextbuttonshow = 1;
    $settings->lastbuttonshow = 1;
    $settings->lastbuttontype = 1;

    $output = '<!-- crsnavbuttons start -->';
    $outend = '<!-- crsnavbuttons end -->';

    if (isset($CFG->crsnavbuttons_self_test)) {
        $CFG->crsnavbuttons_self_test = 0; // All OK
        return $output . '<!-- Self test -->' . $outend;
    }

    if ($COURSE->id <= 1) {
        return $output . '<!-- Front page -->' . $outend;
    }
    if (!$settings) {
        return $output . '<!-- No settings -->' . $outend;
    }

    if (!$settings->enabled) {
        return $output . '<!-- Not enabled -->' . $outend;
    }
    if (!$PAGE->cm) {
        return $output . '<!-- No course module -->' . $outend;
    }
    if (!crsnavbuttons_activity_showbuttons($PAGE->cm)) {
        return $output . '<!-- Activity not ready for crsnavbuttons -->' . $outend;
    }

    $cmid = $PAGE->cm->id;


    $modinfo = get_fast_modinfo($COURSE);
    if ($CFG->version < 2011120100) {
        $context = get_context_instance(CONTEXT_COURSE, $COURSE->id);
    } else {
        $context = context_course::instance($COURSE->id);
    }
    $sections = $DB->get_records('course_sections', array('course' => $COURSE->id), 'section', 'section,visible,summary, name');

    $next = false;
    $prev = false;
    $firstcourse = false;
    $firstsection = false;
    $lastcourse = false;
    $lastsection = false;

    $sectionnum = -1;
    $thissection = null;
    $firstthissection = false;
    $flag = false;
    $sectionflag = false;
    $previousmod = false;

    foreach ($modinfo->cms as $mod) {
        if ($mod->modname == 'label') {
            continue;
        }

        if ($CFG->version >= 2012120300) { // Moodle 2.4
            $opts = course_get_format($COURSE)->get_format_options();
            if ($opts['numsections'] && $mod->sectionnum > $opts['numsections']) {
                break;
            }
        } else {
            if ($mod->sectionnum > $COURSE->numsections) {
                break;
            }
        }

        if (!$mod->uservisible) {
            continue;
        }

        if ($mod->sectionnum > 0 && $sectionnum != $mod->sectionnum) {
            $thissection = $sections[$mod->sectionnum];

            if ($thissection->visible || !$COURSE->hiddensections ||
                    has_capability('moodle/course:viewhiddensections', $context)) {
                $sectionnum = $mod->sectionnum;
                $firstthissection = false;
                if ($sectionflag) {
                    if ($flag) { // flag means selected mod was the last in the section
                        $lastsection = 'none';
                    } else {
                        $lastsection = $previousmod;
                    }
                    $sectionflag = false;
                }
            } else {
                continue;
            }
        }

        $thismod = new stdClass;
        $thismod->link = new moodle_url('/mod/' . $mod->modname . '/view.php', array('id' => $mod->id));
        $thismod->name = strip_tags(format_string($mod->name, true));

        if ($flag) { // Current mod is the 'next' mod
            $next = $thismod;
            $flag = false;
        }
        if ($cmid == $mod->id) {
            $flag = true;
            $sectionflag = true;
            $prev = $previousmod;
            $firstsection = $firstthissection;
            if (!$firstcourse) {
                $firstcourse = 'none'; // Prevent the 'firstcourse' link if this is the first item
            }
        }
        if (!$firstthissection) {
            $firstthissection = $thismod;
        }
        if (!$firstcourse) {
            $firstcourse = $thismod;
        }

        $previousmod = $thismod;
    }
    if (!$flag) { // flag means selected mod is the last in the course
        if (!$lastsection) {
            $lastsection = $previousmod;
        }
        $lastcourse = $previousmod;
    }
    if ($firstcourse == 'none') {
        $firstcourse = false;
    }
    if ($lastsection == 'none') {
        $lastsection = false;
    }

    $output .= '<div id="crscrsbackbuttons">';
    // Home Button.
    if ($settings->homebuttonshow) {
        $home = new stdClass;
        if ($settings->homebuttontype == BLOCK_crsnavbuttons_HOME_COURSE) {
            $home->link = new moodle_url('/course/view.php', array('id' => $COURSE->id));
            $home->name = get_string('coursepage', 'block_crsnavbuttons');
        } else {
            $home->link = $CFG->wwwroot;
            $home->name = get_string('frontpage', 'block_crsnavbuttons');
        }
        list($icon, $bgcolour) = crsnavbutton_get_icon($settings->buttonstype, 'home', $context, BLOCK_crsnavbuttons_HOMEICON);
        $output .= make_crscrsbackbutton($icon, $bgcolour, $home->name, $home->link);
    }
    $output .= '</div>';
    return $output;
}

function crsnavbutton_get_icon($buttonstype, $default, $context, $iconid) {
    global $CFG, $OUTPUT;

    $bgcolour = '';
    $customusebackground = '';

    if ($buttonstype == BLOCK_crsnavbuttons_TYPE_ICON) {
        $defaulturl = $OUTPUT->pix_url($default . 'icon', 'block_crsnavbuttons');

        $fs = get_file_storage();
        $files = $fs->get_area_files($context->id, 'block_crsnavbuttons', 'icons', $iconid, '', false);

        if (empty($files)) {
            return array($defaulturl, $bgcolour);
        }

        $file = reset($files);
        $iconfilename = $file->get_filename();
        $iconurl = file_encode_url($CFG->wwwroot . '/pluginfile.php', '/' . $context->id . '/block_crsnavbuttons/icons/' . $iconid . '/' . $iconfilename);

        return array($iconurl, $customusebackground ? $bgcolour : false);
    } else {
        return array(null, false); // Do not use an icon.
    }
}

function make_crsprevbutton($imgsrc, $bgcolour, $modicon, $title, $url, $newwindow = false) {
    global $COURSE;
    $url = preg_replace('/[\'"<>]/', '', $url);
    $bgcolour = preg_replace('/[^a-zA-Z0-9#]/', '', $bgcolour);
    $target = $newwindow ? ' target="_blank" ' : '';
    $resource = strchr($url, 'resource');
    if ($imgsrc !== null) {
        // Generate an icon button.
        $output = '<a href="' . $url . '" ' . $targetBlank . '><img alt="' . $title . '" title="' . $title . '" src="' . $imgsrc . '" style="';
        if ($bgcolour) {
            $output .= 'background-color: ' . $bgcolour . '; ';
        }
        $output .= 'margin-right: 5px;" width="50" height="50" /></a>';
    } else {
        // Generate a Previous Arrow button.
//        if ($resource) {
//            $output = '<a href="' . $url . '" target="_blank"><span id="PreArrow"><</span></a>';
//        } else {
//            $output = '<a href="' . $url . '" ><span id="PreArrow"><</span></a>';
//        }
        $output = $modicon;

        if ($resource) {
            $output .= html_writer::empty_tag('input', array('type' => 'submit',
                        'class' => 'cpre', 'id' => 'PreviousBtn1', 'name' => 'navbutton',
                        'value' => $title, 'formtarget' => "_blank"));
        } else {
            $output .= html_writer::empty_tag('input', array('type' => 'submit',
                        'class' => 'cpre', 'id' => 'PreviousBtn1', 'name' => 'navbutton',
                        'value' => $title));
        }

        $params = explode('?', $url, 2);
        if (count($params) > 1) {
            $params = str_replace('&amp;', '&', $params[1]);
            $params = explode('&', $params);
            foreach ($params as $param) {

                $parts = explode('=', $param, 2);
                if (!isset($parts[1])) {
                    $parts[1] = null;
                }
                if ($resource) {
                    $output .= html_writer::empty_tag('input', array('type' => 'hidden',
                                'class' => 'cpre1', 'name' => $parts[0],
                                'value' => $parts[1], 'formtarget' => "_blank"));
                } else {
                    $output .= html_writer::empty_tag('input', array('type' => 'hidden',
                                'class' => 'cpre1', 'name' => $parts[0],
                                'value' => $parts[1]));
                }
            }
        }
        if (($COURSE->format == 'eduopen') || ($COURSE->format == 'topcoll')) {
            if (isset($_REQUEST['section'])) {
                $Url = $url . '&section=' . $_REQUEST['section'];
            } else {
                $Url = $url;
            }
        } else {
            $Url = $url;
        }
        $output = html_writer::tag('form', $output, array('action' => $Url, 'method' => 'get', 'id' => 'PreviousBtn', 'class' => 'prevbuttontext'));
    }
    return $output;
}

function make_crsnextbutton($imgsrc, $bgcolour, $modicon, $title, $url, $newwindow = false) {
    global $COURSE;
    $url = preg_replace('/[\'"<>]/', '', $url);

    $bgcolour = preg_replace('/[^a-zA-Z0-9#]/', '', $bgcolour);
    $target = $newwindow ? ' target="_blank" ' : '';
    if ($imgsrc !== null) {
        // Generate an icon button.
        $output = '<a href="' . $url . '" ' . $target . '><img alt="' . $title . '" title="' . $title . '" src="' . $imgsrc . '" style="';
        if ($bgcolour) {
            $output .= 'background-color: ' . $bgcolour . '; ';
        }
        $output .= 'margin-right: 5px;" width="50" height="50" /></a>';
    } else {

        // Generate a text button.
        //$output = html_writer::empty_tag('input', array('type' => 'submit', 'name' => 'navbutton', 'value' => $title));
        $output = $modicon;
        $resource = strchr($url, 'resource');
        if ($resource) {
            $output .= '<input type="submit" name="navbutton" '
                    . 'value="' . $title . '" formtarget = "_blank">';
        } else {
            $output .= '<input type="submit" name="navbutton" '
                    . 'value="' . $title . '">';
        }

        $params = explode('?', $url, 2);

        if (count($params) > 1) {
            $params = str_replace('&amp;', '&', $params[1]);
            $params = explode('&', $params);
            foreach ($params as $param) {
                $parts = explode('=', $param, 2);
                if (!isset($parts[1])) {
                    $parts[1] = null;
                }
//                if ($resource) {
//                    $output .= html_writer::empty_tag('input', array('type' => 'hidden',
//                                'class' => 'cnext', 'name' => $parts[0], 'value' => $parts[1],
//                                'formtarget' => "_blank"));
//                    $output .= '<a href="' . $url . '" target="_blank"><span id="NextArrow">></span></a>';
//                } else {
//                    $output .= html_writer::empty_tag('input', array('type' => 'hidden',
//                                'class' => 'cnext', 'name' => $parts[0], 'value' => $parts[1]));
//                    $output .= '<a href="' . $url . '"><span id="NextArrow">></span></a>';
//                }
                $output .= html_writer::empty_tag('input', array('type' => 'hidden',
                            'class' => 'cnext', 'name' => $parts[0], 'value' => $parts[1]));
            }
        }

        if (($COURSE->format == 'eduopen') || ($COURSE->format == 'topcoll')) {
            if (isset($_REQUEST['section'])) {
                $Url = $url . '&section=' . $_REQUEST['section'];
            } else {
                $Url = $url;
            }
        } else {
            $Url = $url;
        }
        $output = html_writer::tag('form', $output, array('action' => $url, 'id' => 'NextBtn', 'method' => 'get', 'class' => 'nextbuttontext'));
    }
    return $output;
}

function make_crscrsbackbutton($imgsrc, $bgcolour, $title, $url, $newwindow = false) {
    global $COURSE;
    $url = preg_replace('/[\'"<>]/', '', $url);
    $bgcolour = preg_replace('/[^a-zA-Z0-9#]/', '', $bgcolour);
    $target = $newwindow ? ' target="_blank" ' : '';
    if ($imgsrc !== null) {
        // Generate an icon button.
        $output = '<a href="' . $url . '" ' . $target . '><img alt="' . $title . '" title="' . $title . '" src="' . $imgsrc . '" style="';
        if ($bgcolour) {
            $output .= 'background-color: ' . $bgcolour . '; ';
        }
        $output .= 'margin-right: 5px;" width="50" height="50" /></a>';
    } else {
        // Generate a text button.
        $output = html_writer::empty_tag('input', array('type' => 'submit', 'class' => 'cback', 'name' => 'navbutton', 'value' => $title));
        $params = explode('?', $url, 2);
        if (count($params) > 1) {
            $params = str_replace('&amp;', '&', $params[1]);
            $params = explode('&', $params);
            foreach ($params as $param) {
                $parts = explode('=', $param, 2);
                if (!isset($parts[1])) {
                    $parts[1] = null;
                }
                $output .= html_writer::empty_tag('input', array('type' => 'hidden', 'class' => 'cback', 'name' => $parts[0], 'value' => $parts[1]));
            }
        }
        if (($COURSE->format == 'eduopen' || ($COURSE->format == 'topcoll'))) {
            if (isset($_REQUEST['section'])) {
                $Url = $url . '#section-' . $_REQUEST['section'];
            } else {
                $Url = $url;
            }
        } else {
            $Url = $url;
        }
        $output = html_writer::tag('form', $output, array('action' => $Url, 'method' => 'get', 'class' => 'crsnextbuttontext'));
    }
    return $output;
}
