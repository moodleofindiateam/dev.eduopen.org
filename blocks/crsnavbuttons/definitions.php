<?php

// This file is part of the Navigation buttons plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

define("BLOCK_crsnavbuttons_HOME_FRONTPAGE", 1);
define("BLOCK_crsnavbuttons_HOME_COURSE", 2);

define("BLOCK_crsnavbuttons_FIRST_COURSE", 1);
define("BLOCK_crsnavbuttons_FIRST_IN_COURSE", 2);
define("BLOCK_crsnavbuttons_FIRST_IN_SECTION", 3);

define("BLOCK_crsnavbuttons_LAST_COURSE", 1);
define("BLOCK_crsnavbuttons_LAST_IN_COURSE", 2);
define("BLOCK_crsnavbuttons_LAST_IN_SECTION", 3);

define("BLOCK_crsnavbuttons_NEWWINDOW", 1);
define("BLOCK_crsnavbuttons_SAMEWINDOW", 0);

define("BLOCK_crsnavbuttons_HOMEICON", 0);
define("BLOCK_crsnavbuttons_FIRSTICON", 1);
define("BLOCK_crsnavbuttons_PREVICON", 2);
define("BLOCK_crsnavbuttons_NEXTICON", 3);
define("BLOCK_crsnavbuttons_LASTICON", 4);
define("BLOCK_crsnavbuttons_EXTRA1ICON", 5);
define("BLOCK_crsnavbuttons_EXTRA2ICON", 6);

define("BLOCK_crsnavbuttons_TYPE_ICON", 'icon');
define("BLOCK_crsnavbuttons_TYPE_TEXT", 'text');