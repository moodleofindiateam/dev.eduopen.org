<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Note class is build for Manage Notes (Create/Update/Delete)
 * @desc Note class have one parameterized constructor to receive global 
 *       resources.
 * 
 * @package    local_myanalytics
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
include_once('../../config.php');
include_once($CFG->dirroot . '/course/lib.php');
define(AJAX_SCRIPT, true);
$firstname = optional_param('firstname', '', PARAM_RAW);
$lastname = optional_param('lastname', '', PARAM_RAW);
$phone = optional_param('phone', '', PARAM_NUMBER);
$email = optional_param('email', '', PARAM_EMAIL);
$query = optional_param('query', '', PARAM_TEXT);
header('Content-Type:application/json');
$response = array();
$user = null;
global $USER;
try{
    if(isset($USER)) {
       $user = $USER;  
    } else {
       $user = get_admin();
    }
    $supportuser = core_user::get_support_user();
    $subject = 'Test mail';
    $content = '';
    $content .= 'Name '.$firstname.' '.$lastname;
    $content .= 'Phone '.$phone;
    $content .= 'Email '.$email;
    $content .= "Query : ".$query;
    $message = $content;
    $messagehtml = text_to_html($content, false, false, true);
    if(email_to_user($supportuser, $user, $subject, $message, $messagehtml)){
        $response = ['status' => true,'message' => 'Mail has been sent succesfully!!'];
    } else {
        $response = ['status' => false,'message' => 'Something went wrong please try again.'];
    }
} catch (Exception $ex) {
    $response = ['status' => false,'message' => $ex->getMessage()];
}
echo json_encode($response);