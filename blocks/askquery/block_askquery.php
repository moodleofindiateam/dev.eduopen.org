<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
    
/**
 * Note class is build for Manage Notes (Create/Update/Delete)
 * @desc Note class have one parameterized constructor to receive global 
 *       resources.
 * 
 * @package    block_askquery
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */

class block_askquery extends block_base {
    
    /**
     * Initialises the block instance.
     */
    function init() {
        $this->blockname = get_class($this);
        $this->title = get_string('pluginname', $this->blockname);
    }
    
    public function get_content() {
        global $PAGE,$CFG;
        $PAGE->requires->js('/blocks/askquery/script.js');
        if ($this->content !== null) {
            return $this->content;
        }
        $this->content = new stdClass;

        if (empty($this->instance)) {
            return $this->content;
        }
        $this->content->text = '';
        $this->content->text .= '<div id="response"></div>
                                 <div id="query-form">                                    
                                    <input type="hidden" id="path" value="'.$CFG->wwwroot.'/blocks/askquery/'.'"/>
                                    <input type="text" id="qfirstname" placeholder="First Name">
                                    <input type="text" id="qlastname"  placeholder="Last Name">
                                    <input type="text" id="qphone"  required="" placeholder="Mobile Number">
                                    <input type="text" id="qemail" required="" placeholder="Email">
                                    <textarea id="qquery" rows="3" cols="30" style="margin-bottom:5px" placeholder="Your Query?"></textarea>
                                    <button type="submit" onclick="send_feedback()" class="btn btn-primary buttonhiai submit_query">SUBMIT QUERY</button><div id="sending"></div>
                                </div>';
       
        return $this->content;
    }
 public function applicable_formats() {
         return array('admin-index' => true);
    }
    
}