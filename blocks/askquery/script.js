function send_feedback(){
    var path = document.getElementById("path").value;
    var firstname = document.getElementById("qfirstname").value;
    var lastname = document.getElementById("qlastname").value;
    var phone = document.getElementById("qphone").value;
    var email = document.getElementById("qemail").value;
    var query = document.getElementById("qquery").value;      
    var field = firstname == '' ? 'First name' : lastname === '' ? 'Last name' : phone === '' ? 'Mobile' : email === '' ? 'Email' : query === '' ? 'Query': '';    
    
    if (firstname === '' || lastname === '' || phone === '' || email ==='' || query === '' ) { 
        document.getElementById("response").innerHTML = '<div class="alert alert-warning">'+field+' cann\'t empty</div>';
        return;
    } else if (phone === ""){
        document.getElementById("response").innerHTML = '<div class="alert alert-warning">Please enter valid phone number.</div>';
        return;
    }else if(!validateEmail(email)){
        document.getElementById("response").innerHTML = '<div class="alert alert-warning">Please enter valid email address.</div>';
        return;
    }else{
            document.getElementById("sending").innerHTML = '<p style="margin-top:4px"><img src="'+path+'pix/ajaxLoader.GIF"/> Sending ...</p>';
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("qfirstname").value = '';
                document.getElementById("qlastname").value = '';
                document.getElementById("qphone").value = '';
                document.getElementById("qemail").value = '';
                document.getElementById("qquery").value = '';
                document.getElementById("sending").innerHTML = '';
                var json = JSON.parse(xmlhttp.responseText);
                if(json.status) {
                    document.getElementById("response").innerHTML = '<div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-right:18px;">&times;</a>'+json.message+'</div>';
                } else {
                    document.getElementById("response").innerHTML = '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert" aria-label="close" style="margin-right:18px;">&times;</a>'+json.message+'</div>';
                }
                 setTimeout(function() {
                    document.getElementById("response").innerHTML = '';
                },3000);
            }
        }
        xmlhttp.open("POST", path+"ajax_query_submit.php?firstname="+firstname+"&lastname="+lastname+"&phone="+phone+"&email="+email+"&query="+query, true);
        xmlhttp.send();
    }
    return false;
}

function validateEmail(email) {
    var re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
    return re.test(email);
}

function IsNumber(num) {
    var mob = /^[1-9]{1}[0-9]{9}$/;
    if (mob.test(num.value) == false) {
        return false;
    }
    return true;
}