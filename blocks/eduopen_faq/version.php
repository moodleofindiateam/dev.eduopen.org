<?php
/**
 * Version details
 *
 * @package    block_eduopen_faq
 * @copyright  2015 Moodle of India (www.moodleofindia.com)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014111000;// The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2014110400;// Requires this Moodle version
$plugin->component = 'block_eduopen_faq';// Full name of the plugin (used for diagnostics)
