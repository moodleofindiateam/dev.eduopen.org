<?php
/**
 * @author    Shiuli Jana <shiuli@elearn10.com>
 * @package    block_unimore_faq
 * @copyright  2015 onwards Moodle of India  {@link  http://www.moodleofindia.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die;

/**
 * @param stdClass $data
 * @param stdClass $mform
 */
function eduopen_faq_add($data) {
    global $DB;
    $record1 = new stdClass();
    $record1->type = $data->type;
    if ($data->type == '1') {
        $record1->elementid = NULL;
    } else if ($data->type == '3') {
        $record1->elementid = NULL;
    } else {
        $record1->elementid = $data->elementid;
    }
    $faqid = $DB->insert_record('eduopen_faq', $record1);
    if ($faqid) {
        $record2 = new stdClass();
        $record2->faq   = $faqid;
        $record2->question = $data->question;
        $record2->answer = $data->answer;
        $faqdetailsid = $DB->insert_record('eduopen_faq_details', $record2);
        return $faqdetailsid;
    }
}

function eduopen_faq_update($data) {
    global $DB;
    $record3 = new stdClass();
    $record3->id    = $data->updateid;
    $record3->type  = $data->type;
    if ($data->type == '1') {
        $record3->elementid = NULL;
    } else if ($data->type == '3') {
        $record3->elementid = NULL;
    } else {
        $record3->elementid = $data->elementid;
    }
    //$record1->elementid = $data->type1;
    $faqid1 = $DB->update_record('eduopen_faq', $record3);
    if ($faqid1) {
        $record4 = new stdClass();
        $record4->id    = $data->updateid;
        $record4->faq = $data->updateid;
        $record4->question = $data->question;
        $record4->answer = $data->answer;
        $faqdetailsid1 = $DB->update_record('eduopen_faq_details', $record4);
        return $faqdetailsid1;
    }
}
