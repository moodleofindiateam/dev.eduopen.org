<?php
/**
 * class block_eduopen_faq
 * @author    Shiuli Jana <shiuli@elearn10.com>
 * @package    block_eduopen_faq
 * @copyright 2015 onwards Moodle of India  {@link  http://www.moodleofindia.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once($CFG->dirroot.'/course/lib.php');

// This block should only visible to Admin
//if (is_siteadmin()) {
    class block_eduopen_faq extends block_base {

        /**
         * Use {@link block_eduopen_faq::get_timestart()} to access
         *
         * @var int stores the time since when we want to show recent activity
         */
        protected $timestart = null;

        /**
         * Initialises the block
         */
        function init() {
            $this->title = get_string('pluginname', 'block_eduopen_faq');
        }
        /**
         * Returns the content object
         *
         * @return stdObject
         */
        function get_content() {
            global $CFG, $DB;
            if ($this->content !== NULL) {
                return $this->content;
            }

            if (empty($this->instance)) {
                $this->content = '';
                return $this->content;
            }


            $this->content = new stdClass;
            //Only admin can show the FAQ Block
            $this->content->text = "<a href='".$CFG->wwwroot."/blocks/eduopen_faq/create_faq.php'>Create FAQ</a><br>";
            $this->content->text .= "<a href='".$CFG->wwwroot."/blocks/eduopen_faq/view_faq.php'>View FAQ</a><br>";
            //$this->content->footer = '';
            return $this->content;
        }
        public function applicable_formats() {
          return array(
                   'admin-index' => true,
                  'my-index' => false,
          );
        }
    }
//}
