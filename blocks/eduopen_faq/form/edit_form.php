<?php
/**
 * @author    Shiuli Jana <shiuli@elearn10.com>
 * @package    block_eduopen_faq
 * @copyright 2015 onwards Moodle of India  {@link  http://www.moodleofindia.com}
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
require_once(dirname(__FILE__) . '../../../../config.php');
//moodleform is defined in formslib.php
require_once($CFG->libdir."/formslib.php");
//require_once(dirname(__FILE__).'/lib.php');

class create_faq extends moodleform {
    //Add elements to form
    public function definition() {
        global $CFG, $DB;
        $editid = optional_param('editid', 0, PARAM_INT);
        if (isset($editid)) {
            $uid = $editid;
        } else {
            $uid = 0;
        }
        $mform = $this->_form; // Don't forget the underscore!
        //Heading
        if ($editid) {
            $mform->addElement('header', 'formhead', get_string('edithead', 'block_eduopen_faq'));
        } else {
            $mform->addElement('header', 'formhead', get_string('addhead', 'block_eduopen_faq'));
        }

        //select your FAQ type
        $options = array('1' => get_string('site_faq', 'block_eduopen_faq'),
         '2' => get_string('course_faq', 'block_eduopen_faq'),
         '3' => get_string('payment_faq', 'block_eduopen_faq'));
        $select = $mform->addElement('select', 'type', get_string('faqtype', 'block_eduopen_faq'), $options);
        // This will select SITE.
        //$select->setSelected('1');

        //select your course
        $coursename = $DB->get_records('course');
        foreach ($coursename as $course) {
            if ($course->id != 1) {
                $options2[$course->id] = $course->fullname;
            }
        }
        $select = $mform->addElement('select', 'elementid', get_string('select_course', 'block_eduopen_faq'), $options2);
        //$select->setSelected('0');
        //Type your question here
        $mform->addElement('text', 'question', get_string('type_question', 'block_eduopen_faq')); // Add elements to your form
        $mform->setType('question', PARAM_NOTAGS);                   //Set type of element
        $mform->setDefault('question', '');        //Default value
        //Type your Answer here
        $mform->addElement('textarea', 'answer', get_string('type_answer', 'block_eduopen_faq'), 'wrap="virtual" rows="20" cols="50"');

        //hidden field for update
        $mform->addElement('hidden', 'updateid', $uid);
        $mform->setType('updateid', PARAM_TEXT);

        //Submit Button
        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('savechanges'));
        $buttonarray[] = &$mform->createElement('reset', 'resetbutton', get_string('reset'));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
        //$this->add_action_buttons();
    }
    //Custom validation should be added here
    public function validation($data, $files) {
        return array();
    }
}
