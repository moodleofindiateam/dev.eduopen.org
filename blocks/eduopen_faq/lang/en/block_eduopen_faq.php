<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_eduopen_faq', language 'en', branch 'MOODLE_20_STABLE'
 *
 * @package   block_eduopen_faq
 * @copyright 1999 onwards Martin Dougiamas  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['addhead'] = 'Add FAQ';
$string['edithead'] = 'Edit FAQ';

$string['pluginname'] = 'FAQ';
$string['addfaq'] = 'Add FAQ';
$string['editfaq'] = 'Edit FAQ';
$string['eduopen_faq:addinstance'] = 'Add a new FAQ block';
$string['eduopen_faq:viewaddupdatemodule'] = 'View added and updated modules in FAQ block';
$string['eduopen_faq:viewdeletemodule'] = 'View deleted modules in FAQ block';
//code edited by Shiuli on 14th Jan, 2015
$string['faqtype'] = 'Select Your FAQ Type';
$string['select_course'] = 'Select your Course';
$string['type_question'] = 'Type your Question here';
$string['type_answer'] = 'Type your Answer here';
$string['insertfaq'] = 'FAQ is Inserted Successfully..';
$string['view'] = 'View FAQ';
$string['add'] = 'Add FAQ';
$string['updatefaq']='FAQ is Updated Successfully..';

$string['site_faq'] = 'SITE';
$string['course_faq'] = 'Course';
$string['payment_faq'] = 'Payment';
