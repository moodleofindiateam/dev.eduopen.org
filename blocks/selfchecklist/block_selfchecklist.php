<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package block_selfchecklist
 * @author     Jerome Mouneyrac <jerome@mouneyrac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 1999 onwards Martin Dougiamas  http://dougiamas.com
 *
 * The selfchecklist block
 */

//This block should only visible to Admin

class block_selfchecklist extends block_base  {
    function init() {
        $this->title = get_string('pluginname', 'block_selfchecklist');
    }
    function has_config() {
        return true;
    }

    function get_content() {
        global $USER, $CFG, $DB, $OUTPUT, $COURSE;
        $loggedinuser = $USER->id;
        $editingteacher = user_has_role_assignment($loggedinuser, 3, 0);
        
        if ($this->content !== NULL) {
            return $this->content;
        }

        $this->content = new stdClass;
        $this->content->text = '';
        $this->content->footer = '';

        if (empty($this->instance)) {
            return $this->content;
        }
        //display view button if user is admin.
        $viewall = get_string('viewall', 'block_selfchecklist');
        $viewsingle = get_string('viewsingle', 'block_selfchecklist');
        $blockcontextid = $this->context->id;
        $courseid = $COURSE->id;
        
        if (is_siteadmin() || $editingteacher) {
            $this->content->text = "<div class='blcontent'><a 
            href='".$CFG->wwwroot."/mod/selfchecklist/graphs.php?id=".$courseid."'>
            <div class='btn redbtn'>".$viewall."</div></a></div>";
        } else {
            $this->content->text = "<div class='blcontent'><a 
            href='".$CFG->wwwroot."/mod/selfchecklist/graph.php?courseid=".$courseid."'>
            <div class='btn redbtn'>".$viewsingle."</div></a></div>";
        }
        return $this->content;
    }
    public function applicable_formats() {
        return array('course-view' => true);
    }
}

