<?php

require_once (dirname(dirname(dirname(__FILE__))) . '/config.php');
require_login(0, false);
require_once('locallib.php');
include_once($CFG->dirroot . '/mod/diary/diary.class.php');
$courseid = optional_param('courseid', false, PARAM_INT);
$id = optional_param('id', false, PARAM_INT);
$userid = optional_param('userid', false, PARAM_INT);
if($id){
	$courseid = $DB->get_record('diary', array('id' => $id),'course')->course;
}
if (empty($userid)) {
    $user = $USER;
} else {
    $user = $DB->get_record('user', array('id' => $userid));
}
$config = array();
$config['author'] = 'Eduopen.org';
$config['title'] = 'Eduopen';
$config['subject'] = 'Dairy';
$config['keyword'] = '';
$pdf = initialize_pdf($config, $courseid, $user);

$pdf->AddPage('P', 'A4');
$pdf->SetFont('times', '', 12);

if ($id) {
    $html = get_diary_list($id, $courseid, $userid);
    $pdf->writeHTML($html, true, false, true, false, '');
	$courseid = '';
}

/* if ($userid && $courseid) {
    $html = get_diary_list(null, $courseid, $userid);
    $pdf->writeHTML($html, true, false, true, false, '');
} */
if (!empty($courseid)) {
    $html = get_diary_list(null, $courseid,$userid);
    $pdf->writeHTML($html, true, false, true, false, '');
}

$pdf->Output($user->firstname . '_' . $user->lastname . '.pdf', 'I');
