<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Newblock block caps.
 *
 * @package    block_diary
 * @copyright  Daniel Neis <danielneis@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

class block_diary extends block_base {

    function init() {
        $this->title = get_string('pluginname', 'block_diary');
    }

    function get_content() {
        global $CFG, $OUTPUT, $COURSE, $USER, $DB;
        include_once 'locallib.php';
        include_once($CFG->dirroot . '/mod/diary/locallib.php');
        if ($this->content !== null) {
            return $this->content;
        }

        if (empty($this->instance)) {
            $this->content = '';
            return $this->content;
        }

        $this->content = new stdClass();
        $this->content->footer = '';
        $context = context_course::instance($COURSE->id);
        $contents = '';
        $questionlist = $DB->get_records("diary", array("course" => $COURSE->id));

        if ($questionlist == null) {
            $this->content->text = html_writer::div(get_string('nodiary', 'block_diary'), 'alert alert-warning');
            return $this->content;
        }

        if (has_capability('moodle/course:update', context_course::instance($COURSE->id))) {
            $contents .= html_writer::link(new moodle_url($CFG->wwwroot . '/blocks/diary/view.php', array('courseid' => $COURSE->id)), get_string('viewalldownloads', 'block_diary'), array('class' => 'btn btn-success'));
        } else if (user_has_role_assignment($USER->id, 5, $context->id)) {

            $table = new html_table();
            $table->attributes = array('class' => 'table table-responsive table-striped', 'style' => 'font-size:9pt');
            $table->head = (array) get_strings(array('sl', 'name', 'status', 'download'), 'block_diary');
            $downloadflg = 'disabled';
            foreach ($questionlist as $id => $row) {
                static $i = 1;
                $status = get_string('notattempted', 'block_diary');
                $response = diary_get_user_responses($id, $USER->id, false);
                foreach ($response as $field) {
                    if ($field->complete == 'y') {
                        $status = get_string('submitted', 'block_diary');
                        $downloadflg = "";
                    } else if ($field->complete == 'n') {
                        $status = get_string('saved', 'block_diary');
                    }
                }
                $link = '-';
                if ($status == 'Submitted') {
                    $link = html_writer::link(
                                    new moodle_url($CFG->wwwroot . '/blocks/diary/download.php', array('id' => $id)), get_string('clickhere', 'block_diary'), array('download' => 'download', 'target' => '_blank')
                    );
                }

                $table->data[] = array(
                    $i++,
                    $row->name,
                    $status,
                    $link
                );
            }
            $contents .= html_writer::table($table);

            if ($downloadflg == 'disabled') {
                $contents .= html_writer::tag('button', get_string('downoadconsolidated', 'block_diary'), array('class' => 'btn btn-success ' . $downloadflg));
            } else {
                $contents .= html_writer::link(new moodle_url($CFG->wwwroot . '/blocks/diary/download.php', array('courseid' => $COURSE->id)), get_string('downoadconsolidated', 'block_diary'), array('class' => 'btn btn-success', 'download' => 'download', 'target' => '_blank'));
            }
        }

        $this->content->text = $contents;
        return $this->content;
    }

    public function applicable_formats() {
        return array('all' => false,
            'site' => true,
            'site-index' => true,
            'course-view' => true,
            'course-view-social' => false,
            'mod' => true,
            'mod-quiz' => false);
    }

    public function instance_allow_multiple() {
        return true;
    }

    function has_config() {
        return true;
    }

    public function cron() {
        mtrace("Hey, my cron script is running");
        return true;
    }

}
