<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'block_diary', language 'en'
 *
 * @package   block_diary
 * @copyright Daniel Neis <danielneis@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['blockstring'] = 'Block string';
$string['descconfig'] = 'Description of the config section';
$string['descfoo'] = 'Config description';
$string['headerconfig'] = 'Config section header';
$string['labelfoo'] = 'Config label';
$string['diary:addinstance'] = 'Add a Diary block';
$string['diary:myaddinstance'] = 'Add a Diary block to my moodle';
$string['pluginname'] = 'Diary';
$string['sl'] = 'S.L.';
$string['name'] = 'Name';
$string['status'] = 'Status';
$string['download'] = 'Download';
$string['downoadconsolidated'] = 'Download consolidated diary';
$string['clickhere'] = 'Click here';
$string['qlabel'] = '<strong style="float:left">{$a}. Question</strong>';
$string['alabel'] = '<strong>Response </strong><br/>';
$string['nodiary'] = 'No diary available';
$string['title'] = 'View your diary';
$string['heading'] = 'View your diary';
$string['headtext'] = 'Please click to download diary of each student.';
$string['notattempted'] = 'Not attempted';
$string['saved'] = 'Saved';
$string['submitted'] = 'Submitted';
$string['viewalldownloads'] = 'View diary for all students';
