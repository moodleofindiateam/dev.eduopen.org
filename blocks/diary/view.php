<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * Add Note page 
 * @desc this page will crate a web form for note.
 * 
 * @package    block_diary
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_login(0, false);
$id = required_param('courseid', PARAM_INT);
$context = context_course::instance($id);
$course = get_course($id);
require_once('locallib.php');
$PAGE->set_url(new moodle_url('/blocks/diary/view.php'));
$PAGE->set_context($context);
$PAGE->set_pagelayout('course');
$PAGE->set_title(get_string('title', 'block_diary'));
$PAGE->set_heading(get_string('heading', 'block_diary'));
$PAGE->navbar->add($course->fullname,new moodle_url($CFG->wwwroot . '/course/view.php', array('id' => $course->id)));
$PAGE->navbar->add('Diary');
$PAGE->requires->jquery();
echo $OUTPUT->header();
echo html_writer::tag('p', get_string('headtext', 'block_diary'), array('class'=>'lead'));
$users = get_enrolled_users($context);
$table = new html_table();
$table->attributes = array('class' => 'table table-responsive table-striped', 'style' => 'font-size:9pt');
$table->head = (array) get_strings(array('sl', 'name', 'download'), 'block_diary');

foreach ($users as $user) {
//if($response = $DB->get_records('diary_response', array('complete' => 'y', 'username' => $user->id))) {
if($response = $DB->get_records_sql("SELECT * FROM {diary_response} WHERE complete = 'y' AND username = $user->id 
												AND submitted = (SELECT MAX(submitted) FROM {diary_response}  
												WHERE complete = 'y' AND username = $user->id)")) {
    static $i = 1;
    $link = html_writer::link(
                    new moodle_url($CFG->wwwroot . '/blocks/diary/download.php', array('userid' => $user->id, 'courseid' => $id)), get_string('clickhere', 'block_diary'), array('download' => 'download', 'target' => '_blank')
    );
    $table->data[] = array(
        $i++,
        $user->firstname . ' ' . $user->lastname,
        $link
    );
}
}
echo html_writer::table($table);


echo $OUTPUT->footer();
