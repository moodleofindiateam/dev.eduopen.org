<?php

defined('MOODLE_INTERNAL') || die();
require_once (dirname(dirname(dirname(__FILE__))) . '/config.php');
global $PAGE;
require_once $CFG->dirroot . '/mod/diary/questiontypes/diarycls.class.php';
require_once("{$CFG->libdir}/tcpdf/tcpdf.php");
require_once("{$CFG->libdir}/tcpdf/fonts/times.php");
//$PAGE->set_context(context_system::instance());

class AUTHPDF extends TCPDF {

    protected $processId = 0;
    protected $header = '';
    protected $footer = '';
    static $errorMsg = '';

    /**
     * This method is used to override the parent class method.
     * */
    public function Header() {
        $this->writeHTMLCell($w = '', $h = '', $x = '', $y = '', $this->header, $border = 0, $ln = 0, $fill = 0, $reseth = true, $align = 'L', $autopadding = true);
        $this->SetLineStyle(array('width' => 0.30, 'color' => array(222, 222, 222)));

        $this->Line(5, 5, $this->getPageWidth() - 5, 5);

        $this->Line($this->getPageWidth() - 5, 5, $this->getPageWidth() - 5, $this->getPageHeight() - 5);
        $this->Line(5, $this->getPageHeight() - 5, $this->getPageWidth() - 5, $this->getPageHeight() - 5);
        $this->Line(5, 5, 5, $this->getPageHeight() - 5);
    }

    public function Footer() {
        $cur_y = $this->y;
        $this->SetTextColorArray($this->footer_text_color);
        //set style for cell border
        $line_width = (0.85 / $this->k);
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));
       // $htmlbody = "Copyright © 2016 eduopen.org All rights reserved.";
        $htmlbody = '';
        $this->Text(34, 283, $htmlbody, $fstroke = false, $fclip = false, $ffill = true, $border = 0, $ln = 0, $align = 'C', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M', $rtloff = false);

        $w_page = isset($this->l['w_page']) ? $this->l['w_page'] . ' ' : '';

        if (empty($this->pagegroups)) {
            $pagenumtxt = $w_page . $this->getAliasNumPage() . ' / ' . $this->getAliasNbPages();
        } else {
            $pagenumtxt = $w_page . $this->getPageNumGroupAlias() . ' / ' . $this->getPageGroupAlias();
        }
        $this->SetY($cur_y);
        //Print page number
        if ($this->getRTL()) {
            $this->SetX($this->original_rMargin);
            $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
        } else {
            $this->SetX($this->original_lMargin);
            $this->Cell(0, 0, $this->getAliasRightShift() . $pagenumtxt, 'T', 0, 'R');
        }
    }

}

function initialize_pdf(array $config, $courseid, $userid='') {
    global $DB, $USER, $CFG, $OUTPUT;
    // create new PDF document
    if (empty($userid)) {
        $user = $USER;
    } else {
        $user = $DB->get_record('user', array('id' => $userid->id));
    }

    $pdf = new AUTHPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    $pdf->SetAuthor($config['author']);
    $pdf->SetTitle($config['title']);
    $pdf->SetSubject($config['subject']);
    $pdf->SetKeywords($config['keyword']);

    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
    $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
    $pdf->setFooterData(array(200, 200, 200), array(255, 255, 255));
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
    $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
    $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
        require_once(dirname(__FILE__) . '/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    $pdf->setFontSubsetting(true);

    $pdf->SetFont('times', '', 14, '', true);
    $pdf->setFontSpacing(0);

    $pdf->AddPage();
    $img_file = 'pix/bg12.png';
    $pdf->Image($img_file, 0, 0, 160, 400, '', '', '', false, 300, '', false, false, 0);
    $pdf->Image($CFG->wwwroot . '/user/pix.php?file=/' . $user->id . '/f1.jpg', $x = '', $y = '', 16, 16);
    $pdf->Text(34, 29, $user->firstname . ' ' . $user->lastname);
    $pdf->Text(34, 36, $user->email);
    $pdf->SetFont('times', 'B', 23);
    $pdf->SetTextColor(0, 63, 127);
    $pdf->Text(34, 26, get_course($courseid)->fullname, $fstroke = false, $fclip = false, $ffill = true, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M', $rtloff = false);
    $pdf->SetFont('times', '', 12);
    $pdf->SetTextColor(0, 0, 0);
    $pdf->Text(0, 36, userdate(time(), get_string('strftimedate')), $fstroke = false, $fclip = false, $ffill = true, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M', $rtloff = false);
    $pdf->SetFont('times', '', 12);

    $id = optional_param('id', false, PARAM_INT);
    $courseid = optional_param('courseid', false, PARAM_INT);

    $html = '';
    if (!empty($id)) {
        $questionniare = $DB->get_record('diary', array('id' => $id));
        $pdf->Text(0, 52, $questionniare->name, $fstroke = false, $fclip = false, $ffill = true, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M', $rtloff = false);
    } else if (!empty($courseid)) {
        $questionniarelist = $DB->get_records('diary', array('course' => $courseid));
        $pos = 62;
        foreach ($questionniarelist as $list) {
            //$responses = $DB->get_records('diary_response', array('survey_id' => $list->id, 'username' => $user->id));
			$responses = $DB->get_records_sql("SELECT * FROM {diary_response} WHERE survey_id = $list->id AND username = '$user->id' 
												AND submitted = (SELECT MAX(submitted) FROM {diary_response}  
												WHERE survey_id = $list->id AND username = '$user->id')");
			foreach($responses as $response) {
            if ($response->complete == 'y') {
                $pdf->Text(0, $pos+=8, $list->name, $fstroke = false, $fclip = false, $ffill = true, $border = 0, $ln = 0, $align = 'R', $fill = false, $link = '', $stretch = 0, $ignore_min_height = false, $calign = 'T', $valign = 'M', $rtloff = false);
            }
			}
        }
    }

    $logourl = $OUTPUT->pix_url('logo', 'theme');
   // $pdf->Image($CFG->wwwroot.'/pluginfile.php/1/theme_eduopen/logo/1451916009/eduopen-small2.png', 15, 250, 0, 0, $type = '', $link = '', $align = 'L', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false, $altimgs = array());
    $pdf->Image($CFG->wwwroot.'/blocks/diary/logo.png', 15, 250, 0, 0, $type = '', $link = '', $align = 'L', $resize = false, $dpi = 300, $palign = '', $ismask = false, $imgmask = false, $border = 0, $fitbox = false, $hidden = false, $fitonpage = false, $alt = false, $altimgs = array());

    return $pdf;
}

function get_diary_list($id = null, $courseid = null, $userid) {
    global $DB, $USER;

    if (empty($userid)) {
        $user = $USER;
    } else {
        $user = $DB->get_record('user', array('id' => $userid));
    }

    $content = '';
    $responsetable = array();
    if (!($types = $DB->get_records('diary_question_type', array(), 'typeid', 'typeid, has_choices, response_table'))) {
        return '';
    }
    foreach ($types as $type) {
        $responsetable[$type->typeid] = $type->response_table;
    }
	
	if ($id) {
	$questionniarelist = $DB->get_record('diary', array('course' => $courseid, 'id' => $id));
		$questions = $DB->get_records('diary_question', array('survey_id' => $id));
			$responses = $DB->get_record_sql("SELECT * FROM {diary_response} WHERE survey_id = $questionniarelist->id AND
			username = '$user->id' AND submitted = (SELECT MAX(submitted) FROM {diary_response}  
											WHERE survey_id = $questionniarelist->id AND username = '$user->id')");
			if ($responses->complete == 'y') {
			$content .= '<h2>' . $questionniarelist->name . '</h2><hr/>';
			$qnum = 1;
			$content .='<dl>';
			//print_object($questions);
			foreach ($questions as $question) {
				if($question->content) {
					$content .= '<dt>' . get_string('qlabel', 'block_diary', $qnum++) . $question->content . '</dt>';
					$answer = $DB->get_record('diary' . '_' . $responsetable[$question->type_id], 
					array('question_id' => $question->id, 'response_id' => $responses->id));
					//print_object($answer->response);
					if($answer) {
						$content .= '<dt>' . get_string('alabel', 'block_diary') . $answer->response . '</dt>';
					}
				}
			}
			$content .= '</dl>';
		}
	}
		
	if (!empty($courseid) && !$id) {
        $questionniarelist = $DB->get_records('diary', array('course' => $courseid));
        foreach ($questionniarelist as $list) {
            $questions = $DB->get_records('diary_question', array('survey_id' => $list->id));
            //$responses = $DB->get_records('diary_response', array('survey_id' => $list->id, 'username' => $user->id)); //this is commented due 
																		//to if user has attaemtedmultiple times.
			
			$responses = $DB->get_records_sql("SELECT * FROM {diary_response} WHERE survey_id = $list->id AND username = '$user->id' 
												AND submitted = (SELECT MAX(submitted) FROM {diary_response}  
												WHERE survey_id = $list->id AND username = '$user->id')");
				
			foreach($responses as $response) {
            if ($response->complete == 'y') {
                $content .= '<h2>' . $list->name . '</h2><hr/>';
                $qnum = 1;
                $content .='<dl>';
                foreach ($questions as $question) {
					if($question->content) {
                    $content .= '<dt>' . get_string('qlabel', 'block_diary', $qnum++) . $question->content . '</dt>';
                    $answer = $DB->get_record('diary' . '_' . $responsetable[$question->type_id], array('question_id' => $question->id, 'response_id' => $response->id));
                    if($answer) {
					$content .= '<dt>' . get_string('alabel', 'block_diary') . $answer->response . '</dt>';
					}
					}
                }
                $content .= '</dl>';
            }
			}
        }
    }
	/* else {
        $questionniare = $DB->get_record('diary', array('id' => $id));
        $content .= '<h2>' . $questionniare->name . '</h2><hr>';
        $response = $DB->get_record('diary_response', array('survey_id' => $id, 'username' => $USER->id));
        $questions = $DB->get_records('diary_question', array('survey_id' => $id));
        $qnum = 1;
        $content .='<dl>';
        foreach ($questions as $question) {
            $content .= '<dt>' . get_string('qlabel', 'block_diary', $qnum++) . $question->content . '</dt>';
            $answer = $DB->get_record('diary' . '_' . $responsetable[$question->type_id], array('question_id' => $question->id, 'response_id' => $response->id));
            $content .= '<dt>' . get_string('alabel', 'block_diary') . $answer->response . '</dt>';
        }
        $content .= '</dl>';
    } */
    return $content;
}
