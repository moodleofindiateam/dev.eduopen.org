<?php
define('NO_DEBUG_DISPLAY', true);

require_once('../../config.php');
//require_once("lib.php");
require_once($CFG->libdir.'/eventslib.php');
require_once($CFG->libdir.'/enrollib.php');
require_once($CFG->libdir . '/filelib.php');

/// Keep out casual intruders
if (empty($_POST) or !empty($_GET)) {
    print_error("Sorry, you can not use the script that way.");
}

/// Read all the data from PayPal and get it ready for later;
/// we expect only valid UTF-8 encoding, it is the responsibility
/// of user to set it up properly in PayPal business account,
/// it is documented in docs wiki.

$req = 'cmd=_notify-validate';

$data = new stdClass();
foreach ($_POST as $key => $value) {
    $req .= "&$key=".urlencode($value);
    $data->$key = $value;
}

$custom = explode('|', $data->custom);
$data->userid           = (int)$custom[0];
$data->courseid         = (int)$custom[1];
$data->instanceid       = (int)$custom[2];
$data->promocode = $custom[3];
$data->payment_gross    = $data->mc_gross;
$data->payment_currency = $data->mc_currency;
$data->timeupdated      = time();

$rs = $DB->get_record('edudiscount', array('courseid' => $data->courseid,'enrolid' => $data->instanceid,'ppcode' => $data->promocode, 'ppflag' => 1), 'id, ppaplied');

if($rs) {
    $rs->ppaplied += 1;
    $DB->update_record('edudiscount',$rs);
} 

/// get the user and course records

if (! $user = $DB->get_record("user", array("id"=>$data->userid))) {
    message_paypal_error_to_admin("Not a valid user id", $data);
    die;
}

if (! $course = $DB->get_record("course", array("id"=>$data->courseid))) {
    message_paypal_error_to_admin("Not a valid course id", $data);
    die;
}

if (! $context = context_course::instance($course->id, IGNORE_MISSING)) {
    message_paypal_error_to_admin("Not a valid context id", $data);
    die;
}

if (! $plugin_instance = $DB->get_record("enrol", array("id"=>$data->instanceid, "status"=>0))) {
    message_paypal_error_to_admin("Not a valid instance id", $data);
    die;
}

$plugin = enrol_get_plugin('edupay');

/// Open a connection back to PayPal to validate the data
$paypaladdr = empty($CFG->usepaypalsandbox) ? 'www.paypal.com' : 'www.sandbox.paypal.com';
$c = new curl();
$options = array(
    'returntransfer' => true,
    'httpheader' => array('application/x-www-form-urlencoded', "Host: $paypaladdr"),
    'timeout' => 30,
    'CURLOPT_HTTP_VERSION' => CURL_HTTP_VERSION_1_1,
);
$location = "https://$paypaladdr/cgi-bin/webscr";
$result = $c->post($location, $req, $options);

if (!$result) {  /// Could not connect to PayPal - FAIL
    echo "<p>Error: could not access paypal.com</p>";
    message_paypal_error_to_admin("Could not access paypal.com to verify payment", $data);
    die;
}

/// Connection is OK, so now we post the data to validate it

/// Now read the response and check if everything is OK.

if (strlen($result) > 0) {
    if (strcmp($result, "VERIFIED") == 0) { // VALID PAYMENT!


        // check the payment_status and payment_reason

        // If status is not completed or pending then unenrol the student if already enrolled
        // and notify admin

        if ($data->payment_status != "Completed" and $data->payment_status != "Pending") {
            $plugin->unenrol_user($plugin_instance, $data->userid);
            message_paypal_error_to_admin("Status not completed or pending. User unenrolled from course", $data);
            die;
        }

        // If currency is incorrectly set then someone maybe trying to cheat the system

        if ($data->mc_currency != $plugin_instance->currency) {
            message_paypal_error_to_admin("Currency does not match course settings, received: ".$data->mc_currency, $data);
            die;
        }

        // If status is pending and reason is other than echeck then we are on hold until further notice
        // Email user to let them know. Email admin.

        if ($data->payment_status == "Pending" and $data->pending_reason != "echeck") {
            $eventdata = new stdClass();
            $eventdata->modulename        = 'moodle';
            $eventdata->component         = 'enrol_edupay';
            $eventdata->name              = 'edupay_enrolment';
            $eventdata->userfrom          = get_admin();
            $eventdata->userto            = $user;
            $eventdata->subject           = "Moodle: edupay PayPal payment";
            $eventdata->fullmessage       = "Your edupay PayPal payment is pending.";
            $eventdata->fullmessageformat = FORMAT_PLAIN;
            $eventdata->fullmessagehtml   = '';
            $eventdata->smallmessage      = '';
            message_send($eventdata);

            message_paypal_error_to_admin("Payment pending", $data);
            die;
        }

        // If our status is not completed or not pending on an echeck clearance then ignore and die
        // This check is redundant at present but may be useful if paypal extend the return codes in the future

        if (! ( $data->payment_status == "Completed" or
               ($data->payment_status == "Pending" and $data->pending_reason == "echeck") ) ) {
            die;
        }

        // At this point we only proceed with a status of completed or pending with a reason of echeck



        if ($existing = $DB->get_record("enrol_edupay", array("txn_id"=>$data->txn_id))) {   // Make sure this transaction doesn't exist already
            message_paypal_error_to_admin("Transaction $data->txn_id is being repeated!", $data);
            die;

        }

        if (core_text::strtolower($data->business) !== core_text::strtolower($plugin->get_config('edupaybusiness'))) {   // Check that the email is the one we want it to be
            message_paypal_error_to_admin("Business email is {$data->business} (not ".
                    $plugin->get_config('edupaybusiness').")", $data);
            die;

        }

        if (!$user = $DB->get_record('user', array('id'=>$data->userid))) {   // Check that user exists
            message_paypal_error_to_admin("User $data->userid doesn't exist", $data);
            die;
        }

        if (!$course = $DB->get_record('course', array('id'=>$data->courseid))) { // Check that course exists
            message_paypal_error_to_admin("Course $data->courseid doesn't exist", $data);
            die;
        }

        $coursecontext = context_course::instance($course->id, IGNORE_MISSING);

		// we are not checking the cost in the plugin so we will ignore this
        // Check that amount paid is the correct amount
       /*  if ( (float) $plugin_instance->cost <= 0 ) {
            $cost = (float) $plugin->get_config('cost');
        } else {
            $cost = (float) $plugin_instance->cost;
        }

        // Use the same rounding of floats as on the enrol form.
        $cost = format_float($cost, 2, false);

        if ($data->payment_gross < $cost) {
            message_paypal_error_to_admin("Amount paid is not enough ($data->payment_gross < $cost))", $data);
            die;

        } */

        // ALL CLEAR !
		
		global $CFG, $DB;
		$data->option_name2 = $data->payment_gross;
		$data->item_name = $data->item_number; // we are putting item number into the item_name field
		//message_paypal_error_to_admin("Check the data here!", $data);
        var_dump($data);
        $DB->insert_record("enrol_edupay", $data);
		

        if ($plugin_instance->enrolperiod) {
            $timestart = time();
            $timeend   = $timestart + $plugin_instance->enrolperiod;
        } else {
            $timestart = 0;
            $timeend   = 0;
        }

        // Enrol user commented by moodleofindia this is not needed anyway
       // $plugin->enrol_user($plugin_instance, $user->id, $plugin_instance->roleid, $timestart, $timeend);

        // Pass $view=true to filter hidden caps if the user cannot see them
        /*
		if ($users = get_users_by_capability($context, 'moodle/course:update', 'u.*', 'u.id ASC',
                                             '', '', '', '', false, true)) {
            $users = sort_by_roleassignment_authority($users, $context);
            $teacher = array_shift($users);
        } else {
            $teacher = false;
        }
		*/

        $mailstudents = $plugin->get_config('mailstudents');
        $mailteachers = $plugin->get_config('mailteachers');
        $mailadmins   = $plugin->get_config('mailadmins');
        $shortname = format_string($course->shortname, true, array('context' => $context));


        //if (!empty($mailstudents)) {
            $a = new stdClass();
			$site = get_site();
			$a->coursename = format_string($course->fullname, true, array('context' => $coursecontext));
            $a->profileurl = "$CFG->wwwroot/user/view.php?id=$user->id";
			$a->firstname = fullname($user);
			$a->user = fullname($user);
			$a->sitename = $site->fullname;
			//$a->amount = $data->payment_gross;
			$a->amount = $data->mc_gross;
			$a->currency = $data->mc_currency;
			 
            $eventdata = new stdClass();
            $eventdata->modulename        = 'moodle';
            $eventdata->component         = 'enrol_edupay';
            $eventdata->name              = 'edupay_enrolment';
            $eventdata->userfrom          = empty($teacher) ? get_admin() : $teacher;
            $eventdata->userto            = $user;
            
			
			if ($data->item_name =='firstattend') {
    			$eventdata->subject           = get_string("thankpaymentfirst", 'block_edupayments');
                $eventdata->fullmessage       = get_string('welcomefirst', 'block_edupayments', $a);
			} else if ($data->item_name =='attendance_of_completion') {
    			$eventdata->subject           = get_string("thankpaymentattendcert", 'block_edupayments');
                $eventdata->fullmessage       = get_string('welcomeattend', 'block_edupayments', $a);
			} else if ($data->item_name =='verifiedcerti') {
			//get_string('signuppaypalatdc', 'block_edupayments')
    			$eventdata->subject           = get_string("thankpaymentverified", 'block_edupayments');
                $eventdata->fullmessage       = get_string('welcomeverified', 'block_edupayments', $a);
			} else if ($data->item_name =='examination') {
    			$eventdata->subject           = get_string("thankpaymentexam", 'block_edupayments');
                $eventdata->fullmessage       = get_string('welcomeexam', 'block_edupayments', $a);
			}
			
            $eventdata->fullmessageformat = FORMAT_PLAIN;
            $eventdata->fullmessagehtml   = '';
            $eventdata->smallmessage      = '';
            message_send($eventdata);

        //}

        if (!empty($mailteachers) && !empty($teacher)) {
            $a->course = format_string($course->fullname, true, array('context' => $coursecontext));
            $a->user = fullname($user);

            $eventdata = new stdClass();
            $eventdata->modulename        = 'moodle';
            $eventdata->component         = 'enrol_edupay';
            $eventdata->name              = 'edupay_enrolment';
            $eventdata->userfrom          = $user;
            $eventdata->userto            = $teacher;
            if ($data->item_name =='firstattend') {
			$eventdata->subject           = get_string("thankpaymentfirst", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomefirst', 'block_edupayments', $a);
			} else if ($data->item_name =='attendance_of_completion') {
			$eventdata->subject           = get_string("thankpaymentattendcert", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomeattend', 'block_edupayments', $a);
			} else if ($data->item_name =='verifiedcerti') {
			//get_string('signuppaypalatdc', 'block_edupayments')
			$eventdata->subject           = get_string("thankpaymentverified", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomeverified', 'block_edupayments', $a);
			} else if ($data->item_name =='examination') {
			$eventdata->subject           = get_string("thankpaymentexam", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomeexam', 'block_edupayments', $a);
			}
            $eventdata->fullmessageformat = FORMAT_PLAIN;
            $eventdata->fullmessagehtml   = '';
            $eventdata->smallmessage      = '';
            message_send($eventdata);
        }

       // if (!empty($mailadmins)) {
            $a->course = format_string($course->fullname, true, array('context' => $coursecontext));
            $a->user = fullname($user);
            $admins = get_admins();
            foreach ($admins as $admin) {
                $eventdata = new stdClass();
                $eventdata->modulename        = 'moodle';
                $eventdata->component         = 'enrol_edupay';
                $eventdata->name              = 'edupay_enrolment';
                $eventdata->userfrom          = $user;
                $eventdata->userto            = $admin;
                if ($data->item_name =='firstattend') {
			$eventdata->subject           = get_string("thankpaymentfirst", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomefirst', 'block_edupayments', $a);
			} else if ($data->item_name =='attendance_of_completion') {
			$eventdata->subject           = get_string("thankpaymentattendcert", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomeattend', 'block_edupayments', $a);
			} else if ($data->item_name =='verifiedcerti') {
			//get_string('signuppaypalatdc', 'block_edupayments')
			$eventdata->subject           = get_string("thankpaymentverified", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomeverified', 'block_edupayments', $a);
			} else if ($data->item_name =='examination') {
			$eventdata->subject           = get_string("thankpaymentexam", 'block_edupayments');
            $eventdata->fullmessage       = get_string('welcomeexam', 'block_edupayments', $a);
			}
                $eventdata->fullmessageformat = FORMAT_PLAIN;
                $eventdata->fullmessagehtml   = '';
                $eventdata->smallmessage      = '';
                message_send($eventdata);
            }
        //}

    } else if (strcmp ($result, "INVALID") == 0) { // ERROR
        $DB->insert_record("enrol_edupay", $data, false);
        message_paypal_error_to_admin("Received an invalid payment notification!! (Fake payment?)", $data);
    }
}

exit;


//--- HELPER FUNCTIONS --------------------------------------------------------------------------------------


function message_paypal_error_to_admin($subject, $data) {
    echo $subject;
    $admin = get_admin();
    $site = get_site();

    $message = "$site->fullname:  Transaction failed.\n\n$subject\n\n";

    foreach ($data as $key => $value) {
        $message .= "$key => $value\n";
    }

    $eventdata = new stdClass();
    $eventdata->modulename        = 'moodle';
    $eventdata->component         = 'enrol_edupay';
    $eventdata->name              = 'edupay_enrolment';
    $eventdata->userfrom          = $admin;
    $eventdata->userto            = $admin;
    $eventdata->subject           = "PAYPAL ERROR: ".$subject;
    $eventdata->fullmessage       = $message;
    $eventdata->fullmessageformat = FORMAT_PLAIN;
    $eventdata->fullmessagehtml   = '';
    $eventdata->smallmessage      = '';
    message_send($eventdata);
}

/**
 * Silent exception handler.
 *
 * @param Exception $ex
 * @return void - does not return. Terminates execution!
 */
function enrol_edupay_ipn_exception_handler($ex) {
    $info = get_exception_info($ex);

    $logerrmsg = "enrol_edupay IPN exception handler: ".$info->message;
    if (debugging('', DEBUG_NORMAL)) {
        $logerrmsg .= ' Debug: '.$info->debuginfo."\n".format_backtrace($info->backtrace, true);
    }
    error_log($logerrmsg);

    exit(0);
}