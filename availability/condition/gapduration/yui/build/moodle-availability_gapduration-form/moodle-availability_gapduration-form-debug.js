YUI.add('moodle-availability_gapduration-form', function (Y, NAME) {

/**
 * JavaScript for form editing completion conditions.
 *
 * @module moodle-availability_gapduration-form
 */
M.availability_gapduration = M.availability_gapduration || {};

/**
 * @class M.availability_gapduration.form
 * @extends M.core_availability.plugin
 */
M.availability_gapduration.form = Y.Object(M.core_availability.plugin);

/**
 * Initialises this plugin.
 *
 * @method initInner
 * @param {Array} cms Array of objects containing cmid => name
 */
M.availability_gapduration.form.initInner = function(cms) {
    this.cms = cms;
};

M.availability_gapduration.form.getNode = function(json) {
    // Create HTML structure.
    var strings = M.str.availability_gapduration;
    var html = 'Activity' + ' <span class="availability-group"><label>' +
            '<span class="accesshide">' + strings.label_cm + ' </span>' +
            '<select name="cm" title="' + strings.label_cm + '">' +
            '<option value="0">' + M.str.moodle.choosedots + '</option>';
    for (var i = 0; i < this.cms.length; i++) {
        var cm = this.cms[i];
        // String has already been escaped using format_string.
        html += '<option value="' + cm.id + '">' + cm.name + '</option>';
    }

    html += '</select></label> <label>';
    html += ' must be completed & gap duration must be ';
    html += '<input type="text" name="gd" style="width: 10em">&nbsp;&nbsp;';
    html += '<span class="accesshide">' + strings.label_completion +
            ' </span><select name="e" title="' + strings.label_completion + '">' +
                '<option value="0">' + 'Hour' + '</option>' +
                '<option value="1">' + 'Day' + '</option>' +
            '</select></label></span>';
    var node = Y.Node.create('<span>' + html + '</span>');

    // Set initial values.
    if (json.cm !== undefined &&
            node.one('select[name=cm] > option[value=' + json.cm + ']')) {
        node.one('select[name=cm]').set('value', '' + json.cm);
    }
    if (json.e !== undefined) {
        node.one('select[name=e]').set('value', '' + json.e);
    }
//to set value for gap duration input box
    if (json.gd !== undefined) {
        node.one('input[name=gd]').set('value', '' + json.gd);
    }

    // Add event handlers (first time only).
    if (!M.availability_gapduration.form.addedEvents) {
        M.availability_gapduration.form.addedEvents = true;
        var root = Y.one('#fitem_id_availabilityconditionsjson');
        //added by nihat-to make the input box updatable
        root.delegate('change', function() {
            // Whichever dropdown changed, just update the form.
            M.core_availability.form.update(); //commented by nihar due to save the extra input box value.
        }, '.availability_gapduration input'); //here i have changed select to input.

        root.delegate('change', function() {
            // Whichever dropdown changed, just update the form.
            M.core_availability.form.update(); //commented by nihar due to save the extra input box value.
        }, '.availability_gapduration select'); //here i have changed select to input.
    }


    return node;
};

M.availability_gapduration.form.fillValue = function(value, node) {
    value.cm = parseInt(node.one('select[name=cm]').get('value'), 10);
    value.e = parseInt(node.one('select[name=e]').get('value'), 10);
    value.gd = parseInt(node.one('input[name=gd]').get('value'), 10);
};

M.availability_gapduration.form.fillErrors = function(errors, node) {
    var cmid = parseInt(node.one('select[name=cm]').get('value'), 10);
    if (cmid === 0) {
        errors.push('availability_gapduration:error_selectcmid');
    }
//added by nihar-to put the validation input box
    var gd = parseInt(node.one('input[name=gd]').get('value'), 10);

    if (gd % 1 != 0) {
        errors.push('availability_gapduration:error_gapdurationvalue');
    }
};

}, '@VERSION@', {"requires": ["base", "node", "event", "moodle-core_availability-form"]});
