<?php  // Moodle configuration file

unset($CFG);
global $CFG;
$CFG = new stdClass();

$CFG->dbtype    = 'pgsql';
$CFG->dblibrary = 'native';
$CFG->dbhost    = 'localhost';
$CFG->dbname    = 'dev_learn29';
$CFG->dbuser    = 'devuser';
$CFG->dbpass    = 'MuW5qjYM';
$CFG->prefix    = 'mdl_';
$CFG->dboptions = array (
  'dbpersist' => 0,
  'dbport' => '',
  'dbsocket' => '',
);

$CFG->wwwroot   = 'https://dev.eduopen.org';
$CFG->dataroot  = '/var/www/vhosts/eduopen.org/devpgdata';
//$CFG->dataroot  = '/var/www/vhosts/eduopen.org/devpgdata';
$CFG->admin     = 'admin';

/*$CFG->debug = 6143;
$CFG->debugdisplay = 1; */

//for paypal
$CFG->usepaypalsandbox=1; 

$CFG->directorypermissions = 0777;
$CFG->disableupdatenotifications = true;
require_once(dirname(__FILE__) . '/lib/setup.php');

// There is no php closing tag in this file,
// it is intentional because it prevents trailing whitespace problems!
