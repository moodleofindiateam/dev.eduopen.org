$(document).ready(function(){
   $(".editor_atto_wrap").attr('class','felement');
})
function sendlink(id) {
    var xhttp = new XMLHttpRequest();
    document.getElementById("ajax-response").innerHTML = '<div class="alert alert-warning">Please wait while we are sending link to  the users<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         document.getElementById("ajax-response").innerHTML = '';
         var json = JSON.parse(xhttp.responseText);
         if(json.status) {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-success">'+json.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } else {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-warning"><i class="icon icon-warning-sign"></i> '+json.message+' <button class="label mail-btn pull-right" onclick="resendlink('+id+')"><i class="icon icon-refresh"></i> Resend</button><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } 
      }
    }
    xhttp.open("POST", config.ajaxpath+'?action=sendlink&id='+id, true);
    xhttp.send(); 
}

function sendmail(id) {
   var xhttp = new XMLHttpRequest();
    document.getElementById("ajax-response").innerHTML = '<div class="alert alert-warning">Please wait while we are sending link to  the users <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         document.getElementById("ajax-response").innerHTML = '';
         var json = JSON.parse(xhttp.responseText);
         if(json.status) {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-success">'+json.message+'</div>';
         } else {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-warning"><i class="icon icon-warning-sign"></i> '+json.message+' <button class="label mail-btn pull-right" onclick="resendmail('+id+')"><i class="icon icon-refresh"></i> Resend</button><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } 
      }
    }
    xhttp.open("POST", config.ajaxpath+'?action=sendmail&id='+id, true);
    xhttp.send(); 
}
/**
 * 
 * @param {type} id
 * @returns {undefined}
 */
function resendlink(id) {
    var xhttp = new XMLHttpRequest();
    document.getElementById("ajax-response").innerHTML = '<div class="alert alert-warning">Please wait while we are sending link to  the users <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         document.getElementById("ajax-response").innerHTML = '';
         var json = JSON.parse(xhttp.responseText);
         if(json.status) {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-success">'+json.message+'</div>';
         } else {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-warning"><i class="icon icon-warning-sign"></i> '+json.message+' <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } 
      }
    }
    xhttp.open("POST", config.ajaxpath+'?action=resendlink&id='+id, true);
    xhttp.send();
}

/**
 * 
 * @param {type} id
 * @returns {undefined}
 */
function resendmail(id) {
   var xhttp = new XMLHttpRequest();
    document.getElementById("ajax-response").innerHTML = '<div class="alert alert-warning">Please wait while we are sending link to  the users<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         document.getElementById("ajax-response").innerHTML = '';
         var json = JSON.parse(xhttp.responseText);
         if(json.status) {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-success">'+json.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } else {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-warning"><i class="icon icon-warning-sign"></i> '+json.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } 
      }
    }
    xhttp.open("POST", config.ajaxpath+'?action=resendmail&id='+id, true);
    xhttp.send();
}

function sendtestmail(id){
    var xhttp = new XMLHttpRequest();
    document.getElementById("ajax-response").innerHTML = '<div class="alert alert-warning"><img src="pix/spinner.gif" width="30" height="30">Please wait while we are sending email to  the testmail user<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
    xhttp.onreadystatechange = function() {
      if (xhttp.readyState == 4 && xhttp.status == 200) {
         document.getElementById("ajax-response").innerHTML = '';
         var json = JSON.parse(xhttp.responseText);
         if(json.status) {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-success">'+json.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } else {
            document.getElementById('ajax-response').innerHTML = '<div class="alert alert-warning"><i class="icon icon-warning-sign"></i> '+json.message+'<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>';
         } 
      }
    }
    xhttp.open("POST", config.ajaxpath+'?action=sendtestmail&id='+id, true);
    xhttp.send();
}

