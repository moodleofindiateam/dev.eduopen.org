<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
require_once('locallib.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('deletenewletter', 'local_newsletter'));
$PAGE->set_heading(get_string('deletenewletter', 'local_newsletter'));
$PAGE->set_url(new moodle_url($CFG->wwwroot . '/local/newsletter/view_domain.php'));
$newsletter = get_string('deletenewletter', 'local_newsletter');
$PAGE->navbar->add($newsletter);
$id = optional_param('id', false, PARAM_INT);
if(!is_siteadmin()) {
    throw new moodle_exception('nopermissiontoshow');
}
$message =false;
$form = new delete_form();
$std = new stdClass();
$std->id = $id;
$form->set_data($std);
if ($form->is_cancelled()) {
    redirect(new moodle_url($CFG->wwwroot));
} else if ($data = $form->get_data()) {

    if ($DB->delete_records('newsletter', array('id' => $data->id))) {
        $DB->delete_records('newsletter_senditems', array('newsletterid' => $data->id));
        $message = html_writer::div(get_string('newsletterdeleted' ,'local_newsletter'), 'alert alert-success');
        $form = null;
    } else {
        $message = \html_writer::div(get_string('domaindeletedproblem' ,'local_newsletter'), 'alert alert-danger');
        $form = null;
    }
}
echo $OUTPUT->header();
echo html_writer::start_div('row');
echo html_writer::start_div('span12');
if($message){
    echo $message;
}
if ($form != null) {
    echo html_writer::tag('p', get_string('deleteconfirm', 'local_newsletter') , ['class' =>'lead']);
    $form->display();
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();