<?php
$string['pluginname'] = 'News letter';
$string['simplehtml'] = 'News letter';
$string['simplehtml:addinstance'] = 'Add a new Newsletter';
$string['simplehtml:myaddinstance'] = 'Add a new Newsletter block to the My Moodle page';
$string['add_domain'] ='Add Domain';
$string['add_news_letter'] ='Add News letter';
$string['view_news_letter'] ='View News letter';
$string['add_newsletter'] ='Add News letter';
$string['view_newsletter'] ='View News letter';
$string['add']='abc';
$string['name']='Name';
$string['domainselect'] ='Domain Select';
$string['courseempty'] ='Please select any course';
$string['newsletter'] = 'News Letter';
$string['news_letter'] = 'News Letter';
$string['techflash'] = 'Tech Flash';
$string['sectorbuzz'] = 'Sector Buzz';
$string['biznews'] = 'Biz News';
$string['sectornews'] = 'Sector News';
$string['brainstorm'] = 'Brainstorm';
$string['watchout'] = 'WATCH OUT';
$string['select_date'] = 'Select date';
$string['domain_name'] = 'Domain name';
$string['courseselect'] = 'Select course';
$string['description'] = 'Description';
$string['title'] = 'Title';
$string['news_letter_title'] = 'Title';
$string['domain_title'] = 'Add new Domain';
$string['domain_heading'] = 'Add new Domain';
$string['add_news_letter_heading'] = 'Add News Letter';
$string['add_news_letter_title'] = 'Add News Letter';
$string['data_inserted'] = 'Data inserted Successfully!!';
$string['domain'] = 'Domains';
$string['action'] = 'Actions';
$string['news_letter_heading'] = 'News Letter';
$string['no_record_found'] = 'No record available';
$string['no_news_current_month'] ='No News uploaded in current Month'; 
$string['no_news_earlier_month'] ='No News uploaded in Earlier Month'; 
$string['add_domain_desc'] ='Add your domain and description.';
$string['edit_domain_desc'] ='Edit your domain and description.';
$string['editnewsletter'] ='Edit news letter';
$string['add_news_letter_desc'] ='Add your news letter and description.';
$string['current_letter'] ='Current Edition';
$string['earlier_letter'] ='Earlier Editions';
$string['record_updated'] ='Record updated Successfully!!';
$string['view_domain'] ='View Domain';
$string['view_domains'] ='View Domain';
$string['date'] ='Date';
$string['status'] ='Status';
$string['deleteconfirm'] ='Are you sure you want to delete.';
$string['deletedomain'] ='Delete domain';
$string['deletenewletter'] ='Delete new letter';
$string['deletenewsletter'] ='Delete new letter';
$string['domaindeleted'] ='Domain has been deleted successfully !!';
$string['domaindeletedproblem'] ='Domain deleting problem!!';
$string['newsletterdeleted'] ='Newsletter has been deleted successfully !!';
$string['newsletteradded'] ='News letter has been added successfully !!';
$string['newletteredited'] ='News letter has been updated successfully !!';
$string['domainadded'] ='Domain has been added successfully !!';
$string['domainedited'] ='Domain has been updated successfully !!';
$string['newsletterdeletedproblem'] ='Newsletter deleting problem!!';
$string['domaincannotempty'] ='Domain name can\'t be empty';
$string['domaindesccannotempty'] ='Domain description can\'t be empty';
$string['titlecannotempty'] ='Title can\'t be empty';
$string['desccannotempty'] ='Description can\'t be empty';
$string['domaincannotempty'] ='Please select can\'t be empty';
$string['testmailidcannotempty'] ='Please Enter mail id.can\'t be empty';
$string['newslettermailsubjectcannotempty'] ='Please Enter subject.can\'t be empty';


$string['newsletter'] ='News letters';
$string['newsletterdomain'] ='News letter domains';
$string['collapseheading'] ='Please click on the arrow icon to view more.';
$string['mailaction'] ='Mail action';
$string['maillist'] ='Mail lists';
$string['mailtype'] ='Mail type';
$string['sl'] ='S.No';
$string['mailqeue'] ='Mail qeue';
$string['mailsent'] ='Mail sent';
$string['mailstatus'] ='Mail status';
$string['textformat'] ='Link mail';
$string['htmlformat'] ='Newsletter mail';
$string['mailsentque'] ='Mail not sent yet';
$string['emptysentitem'] ='Empty sent item';
$string['course'] ='Course';
$string['siteuser'] ='Site Users';
$string['newslettersection'] ='News Letter Section';
$string['whentosend'] ='whentosend';
$string['newsletteremailsection'] ='News Letter Email Section';
$string['testmailid'] ='Enter Test Mail Id';
$string['testmailid_help'] ='Enter Test Mail Id separated by comma.All the mail id should be existing id of our system.';
$string['newslettermailsubject'] ='Enter subject for newslettermail';
$string['optionalcontent'] = 'Optional Content';

$string['lebel1'] ='Label1 Name';
$string['lebel2'] ='Label2 Name';
$string['lebel3'] ='Label3 Name';
$string['lebel4'] ='Label4 Name';
$string['lebel5'] ='Label5 Name';
$string['lebel6'] ='Label6 Name';

$string['content1'] ='Content1';
$string['content2'] ='content2';
$string['content3'] ='content3';
$string['content4'] ='content4';
$string['content5'] ='content5';
$string['content6'] ='content6';
$string['senderemail'] ='Sender Mail';