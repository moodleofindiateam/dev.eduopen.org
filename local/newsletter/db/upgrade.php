<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the institution block
 *
 * Sometimes, changes between versions involve alterations to database structures
 * and other major things that may break installations.
 *
 * The upgrade function in this file will attempt to perform all the necessary
 * actions to upgrade your older installation to the current version.
 *
 * If there's something it cannot do itself, it will tell you what you need to do.
 *
 * The commands in here will all be database-neutral, using the methods of
 * database_manager class
 *
 * Please do not forget to use upgrade_set_timeout()
 * before any action that may take longer time to finish.
 *
 * @since Moodle 2.0
 * @package local_newsletter
 * @copyright 2010 Jerome Mouneyrac
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 *
 * @param int $oldversion
 * @param object $block
 */
function xmldb_local_newsletter_upgrade($oldversion) {
    global $CFG, $DB, $OUTPUT;

    $dbman = $DB->get_manager(); // Loads ddl manager and xmldb classes.

    // Add a new column newcol 
    if ($oldversion < 2012012438) {
        // Define field encompetence to be added to newsletter.
        $table = new xmldb_table('newsletter');
        $field1 = new xmldb_field('techflash_lebel', XMLDB_TYPE_TEXT, null, null, null, null, null, 'description');
        $field2 = new xmldb_field('sectorbuzz_lebel', XMLDB_TYPE_TEXT, null, null, null, null, null, 'techflash');
        $field3 = new xmldb_field('biznews_lebel', XMLDB_TYPE_TEXT, null, null, null, null, null, 'sectorbuzz');
        $field4 = new xmldb_field('sectornews_lebel', XMLDB_TYPE_TEXT, null, null, null, null, null, 'biznews');
        $field5 = new xmldb_field('brainstorm_lebel', XMLDB_TYPE_TEXT, null, null, null, null, null, 'sectornews');
        $field6 = new xmldb_field('watchout_lebel', XMLDB_TYPE_TEXT, null, null, null, null, null, 'brainstorm');
        $field7 = new xmldb_field('whentosend', XMLDB_TYPE_INTEGER, '11', null, null, null, null, 'watchout');
        $field8 = new xmldb_field('testmailid', XMLDB_TYPE_TEXT, null, null, null, null, null, 'whentosend');
        $field9 = new xmldb_field('newslettermailsubject', XMLDB_TYPE_TEXT, null, null, null, null, null, 'testmailid');
        $field10 = new xmldb_field('optionalcontent', XMLDB_TYPE_TEXT, null, null, null, null, null, 'newslettermailsubject');
        $field11 = new xmldb_field('watchout_lebel', XMLDB_TYPE_TEXT, null, null, null, null, null, 'brainstorm');
        $field12 = new xmldb_field('senderemail', XMLDB_TYPE_TEXT, null, null, null, null, null, 'watchout');

        // Conditionally launch add field encompetence.
        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }
        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }
        if (!$dbman->field_exists($table, $field3)) {
            $dbman->add_field($table, $field3);
        }
        if (!$dbman->field_exists($table, $field4)) {
            $dbman->add_field($table, $field4);
        }
        if (!$dbman->field_exists($table, $field5)) {
            $dbman->add_field($table, $field5);
        }
        if (!$dbman->field_exists($table, $field6)) {
            $dbman->add_field($table, $field6);
        }
        if (!$dbman->field_exists($table, $field7)) {
            $dbman->add_field($table, $field7);
        }
        if (!$dbman->field_exists($table, $field8)) {
            $dbman->add_field($table, $field8);
        }
        if (!$dbman->field_exists($table, $field9)) {
            $dbman->add_field($table, $field9);
        }
        if (!$dbman->field_exists($table, $field10)) {
            $dbman->add_field($table, $field10);
        }
        if (!$dbman->field_exists($table, $field11)) {
            $dbman->add_field($table, $field11);
        }
        if (!$dbman->field_exists($table, $field12)) {
            $dbman->add_field($table, $field12);
        }

        $table = new xmldb_table('newsletter');
        $field = new xmldb_field('courseid', XMLDB_TYPE_CHAR, '250', null, null, null, null, 'categoryid');

        // Launch change of type for field courseid.
        $dbman->change_field_type($table, $field);

        // newsletter savepoint reached.
        upgrade_plugin_savepoint(true, 2012012438, 'local', 'newsletter');
    }
    return true;
}