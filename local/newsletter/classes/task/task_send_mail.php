<?php
/**
 * cron
 * @return boolean
 */



namespace local_newsletter\task;
defined('MOODLE_INTERNAL') || die();

class task_send_mail extends \core\task\scheduled_task  {
    
     public function get_name() {
        // Shown in admin screens
        return get_string('pluginname', 'local_newsletter');
    }

    public function execute() {
        global $DB, $CFG, $OUTPUT, $SITE;
        try{
            mtrace('news letter mail send task started');
            include_once("{$CFG->libdir}/accesslib.php");
            include_once("{$CFG->dirroot}/local/newsletter/locallib.php");   
            // Code for sending only links
            $sentitems = $DB->get_records('newsletter_senditems', array('flag' => null));
            $mailflags = array();
            foreach ($sentitems as $item) {
                
                $newsletter = $DB->get_record('newsletter', array('id'=> $item->newsletterid));
      
                if(($newsletter->whentosend <= time()) && ($newsletter->status == 1)) { 
                $courseidexplode = explode(',', $newsletter->courseid);
                foreach($courseidexplode as $newlettercrsid) {
                $context = \context_course::instance($newlettercrsid);
               
                if($newlettercrsid == '1') {
                    $allusers = new \stdClass();
                    $allusers = get_users(true, '', false, null, 'firstname ASC', '', '', '', '');

                    
                } else {
                    $users = get_enrolled_users($context);
                }

                if($item->mailtype === 'textformat') {
                    foreach ($users as $user) {
                        $body = 'Newsletter Link : '.$CFG->wwwroot.'/local/newsletter/?id='.$item->newsletterid;
                        if(email_to_user($user, \core_user::get_support_user(), 'Test mail', $body)) {
                            $mailflags[$item->id] = 1;
                        }
                    }
                } else if($item->mailtype === 'htmlformat') {
                    $htmlmail = newsletter_html($item->newsletterid);
                }
                
            }
           $totaluser_pernewsitem = array_unique((array)(object)array_merge((array)$allusers,(array)$users),SORT_REGULAR);
            mtrace('sending newsletter mails to users');
            $senderuser = $DB->get_record_sql("SELECT * FROM {user} WHERE email LIKE '%$newsletter->senderemail%'");
            foreach ($totaluser_pernewsitem as $user) {
                    if (email_to_user($user, $senderuser, $newsletter->newslettermailsubject, '', $htmlmail)) {
                        $mailflags[$item->id] = 1;
                   }
                }
            }
        }
            if($mailflags) {
                foreach ($mailflags as $id => $flag) {
                    $std =  new \stdClass;
                    $std->id =  $id;
                    $std->flag = $flag != 0 ? true : false;
                    $std->timemodified = time();
                    $DB->update_record('newsletter_senditems', $std);
                }                    
            }  
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }       
        return true;
    }
}
