<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
require_once('locallib.php');
$context = context_system::instance();
$catid = optional_param('catid', 1, PARAM_INT);
$PAGE->set_context($context);
$PAGE->set_pagelayout('eduopen_institution');
$PAGE->requires->jquery();
$PAGE->requires->js('/local/newsletter/scripts.js');
$PAGE->set_title(get_string("add_news_letter_title", "local_newsletter"));
$PAGE->set_heading(get_string("add_news_letter_heading", "local_newsletter"));
$PAGE->set_url($CFG->wwwroot . '/local/newsletter/add_news_letter.php');
$PAGE->set_title(get_string('add_news_letter_title', 'local_newsletter'));
$message = false;
if(!is_siteadmin()) {
    throw new moodle_exception('nopermissiontoshow');
}
$mform = new add_newsletter_form($CFG->wwwroot . '/local/newsletter/add_news_letter.php', array('catid'=>$catid));
if ($mform->is_cancelled()) {
    redirect(new moodle_url($CFG->wwwroot.'/admin/'));
}
else if ($data = $mform->get_data()) {
    global $USER;
    $record = new stdClass();
    $record->categoryid = $data->domain;
    $courseidimplode = implode(',', $data->courseid);
    $trimedcourseid = rtrim($courseidimplode, ',');
    $record->courseid = $trimedcourseid;
    $record->title = $data->title;
    $record->description = $data->description;
    $record->techflash_lebel = $data->techflash_lebel;
    $record->techflash = $data->techflash;
    $record->sectorbuzz_lebel = $data->sectorbuzz_lebel;
    $record->sectorbuzz = $data->sectorbuzz;
    $record->biznews_lebel = $data->biznews_lebel;
    $record->biznews = $data->biznews;
    $record->sectornews_lebel = $data->sectornews_lebel;
    $record->sectornews = $data->sectornews;
    $record->brainstorm_lebel = $data->brainstorm_lebel;
    $record->brainstorm = $data->brainstorm;
    $record->watchout_lebel = $data->watchout_lebel;
    $record->watchout = $data->watchout;
    $record->whentosend = $data->whentosend;
    $record->testmailid = $data->testmailid;
    $record->newslettermailsubject = $data->newslettermailsubject;
    $record->optionalcontent = $data->optionalcontent;
    $record->senderemail = $data->senderemail;
    $record->createdtime = time();
    $record->lastmodiferdid = $USER->id;
    $record->status = $data->status;

    if ($DB->insert_record('newsletter', $record)) {
        $message = html_writer::div(get_string("newsletteradded", "local_newsletter"), 'alert alert-success');
        $mform = null;
    }
}

if($message) {
    //echo $message;
    redirect(new moodle_url($CFG->wwwroot.'/local/newsletter/view_domain.php'),get_string("newsletteradded", "local_newsletter"));
}

echo $OUTPUT->header();
echo html_writer::start_div('row-fluid');
echo html_writer::start_div('col-md-12 newsletterform');

if ($mform != null) {
    echo html_writer::tag('p', get_string('add_news_letter_desc', 'local_newsletter'), array('class' => 'lead bottomline'));
    $mform->display();
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();