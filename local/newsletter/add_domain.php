<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
require_once('locallib.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('eduopen_institution');
$PAGE->set_title(get_string('domain_title', 'local_newsletter'));
$PAGE->set_heading(get_string('domain_heading', 'local_newsletter'));
$PAGE->set_url(new moodle_url($CFG->wwwroot . '/local/newsletter/add_domain.php'));
$message = false;
if (!is_siteadmin()) {
    throw new moodle_exception('nopermissiontoshow');
}
$mform = new add_domain_form();
if ($mform->is_cancelled()) {
    redirect(new moodle_url('/admin/'));
} else if ($data = $mform->get_data()) {
    $record = new stdClass();
    $record->name = $data->name;
    $record->description = $data->description;
    if ($DB->insert_record('newsletter_category', $record)) {
        $message = html_writer::div(get_string("domainadded", "local_newsletter"), 'alert alert-success');

        $mform = null;
    }
}
echo $OUTPUT->header();
echo html_writer::start_div('row-fluid');

echo html_writer::start_div('col-md-12');
echo html_writer::tag('p', get_string('add_domain_desc', 'local_newsletter'), array('class' => 'lead bottomline'));
if ($message) {
    echo $message;
    echo html_writer::link(new moodle_url($CFG->wwwroot.'/local/newsletter/add_domain.php'), html_writer::tag('button','Click to add more domain'),array('class' => 'pull-right'));
}
if ($mform != null) {
    $mform->display();
}
$record = $DB->get_records('newsletter_category', null, 'id desc');
if ($record != null) {
    $table = new html_table();
    $table->head = (array) get_strings(['name', 'description', 'action'], 'local_newsletter');
    foreach ($record as $key => $row) {
        $table->data[] = array(
            html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/news_letters.php', array('viewid' => $row->id)), $row->name, array('class' => 'viewdomain')),
            substr($row->description, 0, 60) . '...',
            html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/edit_domain.php', ['id' => $row->id]), 'Edit', ['class' => 'label label-error editbutton']) . ' ' .
            html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/delete_domain.php', ['id' => $row->id]), 'Delete', ['class' => 'label label-warning'])
        );
    }
    echo html_writer::start_tag('div', array('class' => 'no-overflow'));
    echo html_writer::table($table);
    echo html_writer::end_tag('div');
} else {
    echo html_writer::div(get_string("no_record_found", "local_newsletter"), 'alert alert-success');
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();