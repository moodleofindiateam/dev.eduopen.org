<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * My notes 
 * @desc this page will view on author of notes
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
/* Authenticate Admin user to access this local plugin */
if (is_siteadmin()) {
    /*
    * Create custom menu in side administration
    * @plugin News Letter
    * admin category has three parameter
    * @param string $name The internal name for this category. Must be unique amongst ALL part_of_admin_tree objects
    * @param string $visiblename The displayed named for this category. Usually obtained through get_string()
    * @param bool $hidden hide category in admin tree block, defaults to false
    */
    $ADMIN->add('root', new admin_category('newsletter', get_string('newsletter', 'local_newsletter'), false));
    $ADMIN->add("newsletter", new admin_externalpage('add_domain', get_string('add_domain', 'local_newsletter'),
        "$CFG->wwwroot/local/newsletter/add_domain.php"));
    $ADMIN->add("newsletter", new admin_externalpage('add_newsletter', get_string('add_newsletter', 'local_newsletter'),
       "$CFG->wwwroot/local/newsletter/add_news_letter.php"));
    $ADMIN->add("newsletter", new admin_externalpage('view_newsletter', get_string('view_domains', 'local_newsletter'),
        "$CFG->wwwroot/local/newsletter/view_domain.php"));
    $ADMIN->add("newsletter", new admin_externalpage('maillist', get_string('maillist', 'local_newsletter'),
        "$CFG->wwwroot/local/newsletter/maillist.php"));
}