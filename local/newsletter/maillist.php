<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
require_once('locallib.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('maillist', 'local_newsletter'));
$PAGE->set_heading(get_string('maillist', 'local_newsletter'));
$PAGE->set_url(new moodle_url($CFG->wwwroot . '/local/newsletter/maillist.php'));
$id = optional_param('id', false, PARAM_INT);

if(!is_siteadmin()) {
    throw new moodle_exception('nopermissiontoshow');
}
$sentitems = $DB->get_records('newsletter_senditems', null, 'id desc');
$maillist = array();
foreach ($sentitems as $item) {
    if(!isset($maillist[$item->newsletterid])) {
        $maillist[$item->newsletterid] = $item->newsletterid;
    }    
}
echo $OUTPUT->header();
echo html_writer::start_div('row');
echo html_writer::start_div('span12');
echo html_writer::tag('p', get_string('maillist', 'local_newsletter') , ['class' =>'lead bottomline']);
if($maillist) {
    list($in, $params) = $DB->get_in_or_equal($maillist);
    $rs = $DB->get_recordset_select('newsletter', "id $in", $params, '', 'id,title');
    $titles = array();
    while($rs->valid()){
        $row = $rs->current();
        $titles[$row->id] = $row->title;
        $rs->next();
    }
    $table = new html_table();
    $table->head = (array) get_strings(['sl', 'title', 'mailstatus', 'mailtype', 'mailqeue', 'mailsent'], 'local_newsletter');
    $count = 1;
    foreach ($sentitems as $item) {
        $mailtype = $item->mailtype == 'htmlformat' ? get_string('htmlformat', 'local_newsletter') : get_string('textformat', 'local_newsletter');
        $flag = html_writer::tag('span',get_string('mailsentque', 'local_newsletter'), array('class'=>'label label-warning'));
        if($item->flag) {
            $flag = html_writer::tag('span',get_string('mailsent', 'local_newsletter'), array('class'=>'label label-success'));
        }
        $link = html_writer::link(new moodle_url($CFG->wwwroot.'/local/newsletter/news_letters.php', array('viewid' =>$item->newsletterid)),  $titles[$item->newsletterid]);
        $timemodified = '-';
        if($item->timemodified) {
             $timemodified = userdate($item->timemodified, get_string('strftimedatetimeshort', 'langconfig'));
        }
        $table->data[] = array($count++, $link, $flag, $mailtype, userdate($item->timecreated, get_string('strftimedatetimeshort', 'langconfig')), $timemodified);
    }
    echo html_writer::table($table);
} else {
   echo html_writer::div(get_string('emptysentitem', 'local_newsletter'), 'alert alert-warning');
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();