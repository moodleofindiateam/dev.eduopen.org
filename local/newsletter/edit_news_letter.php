<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once('locallib.php');
$newsletterid = optional_param('id', false, PARAM_INT);
$general = $DB->get_record('newsletter', array('id' => $newsletterid));
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('eduopen_institution');
$PAGE->set_title(get_string("editnewsletter", "local_newsletter"));
$PAGE->set_heading(get_string("editnewsletter", "local_newsletter"));
$PAGE->set_url(new moodle_url($CFG->wwwroot.'/local/newsletter/view_domain.php'));
$PAGE->navbar->add(get_string("editnewsletter", "local_newsletter"));
if(!is_siteadmin()) {
    throw new moodle_exception('nopermissiontoshow');
}
$message = false;
$mform = new add_newsletter_form();
$general->domain = $general->categoryid;
$mform->set_data($general);
if ($mform->is_cancelled()) {
    redirect(new moodle_url($CFG->wwwroot.'/local/newsletter/news_letters.php?viewid='.$general->categoryid));
} else if ($data = $mform->get_data(false)) {
    global $USER;
    $record = new stdClass();
    $record->id = $newsletterid;
    $record->categoryid = $data->domain;
    $courseidimplode = implode(',', $data->courseid);
    $trimedcourseid = rtrim($courseidimplode, ',');
    $record->courseid = $trimedcourseid;
    $record->title = $data->title;
    $record->description = $data->description;
    $record->techflash_lebel = $data->techflash_lebel;
    $record->techflash = $data->techflash;
    $record->sectorbuzz_lebel = $data->sectorbuzz_lebel;
    $record->sectorbuzz = $data->sectorbuzz;
    $record->biznews_lebel = $data->biznews_lebel;
    $record->biznews = $data->biznews;
    $record->sectornews_lebel = $data->sectornews_lebel;
    $record->sectornews = $data->sectornews;
    $record->brainstorm_lebel = $data->brainstorm_lebel;
    $record->brainstorm = $data->brainstorm;
    $record->watchout_lebel = $data->watchout_lebel;
    $record->watchout = $data->watchout;
    $record->whentosend = $data->whentosend;
    $record->testmailid = $data->testmailid;
    $record->newslettermailsubject = $data->newslettermailsubject;
    $record->optionalcontent = $data->optionalcontent;
    $record->senderemail = $data->senderemail;
    $record->modifiedtime = time();
    $record->lastmodiferdid = $USER->id;
    $record->status = $data->status;
    if ($DB->update_record('newsletter', $record)) {
        $message = html_writer::div(get_string("newletteredited", "local_newsletter"), 'alert alert-success');
        $mform = null;
    }
}
if($message) {
    //echo $message;
    redirect(new moodle_url($CFG->wwwroot.'/local/newsletter/view_domain.php'),get_string("newletteredited", "local_newsletter"));
}
echo $OUTPUT->header();
echo html_writer::start_div('row-fluid');
echo html_writer::start_div('col-md-12 newsletterform');

if ($mform != null) {
    echo html_writer::tag('p', get_string('editnewsletter', 'local_newsletter'), array('class' => 'lead bottomline'));
    $mform->display();
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();