<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
require_once('locallib.php');
if (!is_siteadmin())
    exit('Unauthoriesd access');
define(AJAX_SCRIPT, true);
header('Content-Type:application\json');
$action = required_param('action', PARAM_RAW);
$params = array();
unset($_GET['action']);
foreach ($_GET as $key => $value) {
    $params[$key] = required_param($key, PARAM_RAW);
}
try{    
    echo call_user_func_array($action, $params);    
} catch (Exception $ex) {
    print_object($ex);
}