<?php
// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
$id = optional_param('views', null, PARAM_INT);
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('news_letter_title', 'local_newsletter'));
$PAGE->set_heading(get_string('news_letter_heading', 'local_newsletter'));
$PAGE->set_url($CFG->wwwroot . '/local/newsletter/view_domain.php');
echo $OUTPUT->header();
echo html_writer::start_div('row');
echo html_writer::start_div('span12');
$row = $DB->get_record('newsletter', array('id' => $id));
?>
<div class="span12 pull-right">
    <!-- Blog Post Content Column -->
    <div class="col-lg-12">
        <!-- Blog Post -->
        <!-- Title -->
        <h1><?php echo $row->title; ?></h1>

        <!-- Author -->
        <p class="lead">
            <?php 
                $user = $DB->get_record('user', array('id' => $row->lastmodiferdid), 'firstname,lastname');
                echo 'By '.html_writer::link(new moodle_url($CFG->wwwroot.'/user/profile.php',array('id' =>$row->lastmodiferdid)), $user->firstname.' '.$user->lastname);
            ?>
        </p>
        <?php echo $row->description; ?>
        <hr>
        <!-- Date/Time -->
        <p><span class="icon icon-time"></span> Posted on <?php echo userdate($row->createdtime, '%d-%m-%Y'); ?></p>
        <hr/>
        <!-- Comment -->
        <div class="media">
            <a class="pull-left" href="#">
            </a>
            <div class="media-body">
                <h4 class="media-heading">Tech Flash
                </h4>
                <?php echo $row->techflash; ?>
            </div>
        </div>
        <div class="media">
            <a class="pull-left" href="#">
            </a>
            <div class="media-body">
                <h4 class="media-heading">Sector Buzz</h4>
                <?php echo $row->sectorbuzz; ?>                
            </div>
        </div>
        <div class="media">
            <a class="pull-left" href="#">
            </a>
            <div class="media-body">
                <h4 class="media-heading">Biz News</h4>
                <?php echo $row->biznews; ?>                
            </div>
        </div>
        <div class="media">
            <a class="pull-left" href="#">
            </a>
            <div class="media-body">
                <h4 class="media-heading">Sector News</h4>
                <?php echo $row->sectornews; ?>                 
            </div>
        </div>
        <div class="media">
            <a class="pull-left" href="#">
            </a>
            <div class="media-body">
                <h4 class="media-heading">Brainstorm</h4>
                <?php echo $row->brainstorm; ?>                  
            </div>
        </div>
        <div class="media">
            <a class="pull-left" href="#">
            </a>
            <div class="media-body">
                <h4 class="media-heading">Watchout</h4>
                <?php echo $row->watchout; ?>                 
            </div>
        </div>
    </div>
</div>
<?php
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();