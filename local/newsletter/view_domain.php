<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.


/**
 * 
 * @package    local_newsletter
 * @copyright  2015 MoodleOfIndia
 * @author     shambhu kumar
 * @license    MoodleOfIndia {@web http://www.moodleofindia.com}
 */
require_once('./../../config.php');
$PAGE->set_context(context_system::instance());
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('view_domain', 'local_newsletter'));
$PAGE->set_heading(get_string('view_domain', 'local_newsletter'));
$PAGE->set_url($CFG->wwwroot . '/local/newsletter/view_domain.php');
echo $OUTPUT->header();
echo html_writer::start_div('row');
echo html_writer::start_div('span12');
if(!is_siteadmin()) {
    throw new moodle_exception('nopermissiontoshow');
}
echo html_writer::tag('p', get_string('view_domain', 'local_newsletter'), array('class' => 'lead bottomline'));
$record = $DB->get_records('newsletter_category');
if ($record != null) {
    $table = new html_table();
    $table->head = array('Name', 'Description', 'Action');
    foreach ($record as $key => $row) {
        $table->data[] = array(
            html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/news_letters.php', ['viewid' => $row->id]), $row->name, array('class' => 'viewdomain')),
            substr($row->description, 0, 45) . '..',
            html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/edit_domain.php', ['id' => $row->id]), 'Edit', array('class' => 'label label-error editbutton')).' '.
            html_writer::link(new moodle_url($CFG->wwwroot . '/local/newsletter/delete_domain.php', ['id' => $row->id]), 'Delete', array('class' => 'label label-warning'))
        );
    }
    echo html_writer::start_tag('div', array('class' => 'no-overflow'));
    echo html_writer::table($table);
    echo html_writer::end_tag('div');
}
else {
    echo html_writer::div(get_string("no_record_found", "local_newsletter"), 'alert alert-warning');
}
echo html_writer::end_div();
echo html_writer::end_div();
echo $OUTPUT->footer();