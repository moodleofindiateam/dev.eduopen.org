<?php
/**
 * cron
 * @return boolean
 */

namespace local_timespent\task;
defined('MOODLE_INTERNAL') || die();

class task_time_spent extends \core\task\scheduled_task {
     public function get_name() {
        // Shown in admin screens
        return get_string('pluginname', 'local_timespent');
    }

    public function execute() {
        global $DB, $OUTPUT, $PAGE, $USER, $CFG;
        $lastcronruntime = parent::get_last_run_time();
        $sql = "SELECT DISTINCT bt.log_id, bt.timespent, l.module, l.action, l.course, l.cmid,
        l.userid, l.time FROM {log} l
        JOIN {block_timestat} bt ON l.id=bt.log_id WHERE l.time>$lastcronruntime
        ORDER BY bt.log_id DESC";
        $logdata = $DB->get_records_sql($sql);
        foreach ($logdata as $value) {
            $hasRecord = $DB->record_exists('custom_log', array('logid' => $value->log_id));
            if (!empty($logdata) && $hasRecord) {
                $updatesql ="UPDATE {custom_log} SET (logid, userid, module, action, course, cmid, time, timespent) =
                 ($value->log_id, $value->userid, '$value->module', '$value->action', $value->course,
                  $value->cmid, $value->time, $value->timespent) WHERE logid=$value->log_id";
                $timespentupdate = $DB->execute($updatesql);
                //echo "Records has successfully been updated";
            } else {
                $insrecord = new \stdClass();
                $insrecord->logid   = $value->log_id;
                $insrecord->userid   = $value->userid;
                $insrecord->module   = $value->module;
                $insrecord->action   = $value->action;
                $insrecord->course   = $value->course;
                $insrecord->cmid   = $value->cmid;
                $insrecord->time   = $value->time;
                $insrecord->timespent   = $value->timespent;
                $timespentinsert = $DB->insert_record('custom_log', $insrecord);
                //echo "Records has successfully been inserted";
            }
        }
        echo "Timespent plugin is working properly";
    }
}
