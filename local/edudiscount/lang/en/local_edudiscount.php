<?php

// This file is part of MoodleofIndia - http://www.moodleofindia.com/
/**
 * Manage License block caps.
 *
 * @package    block_godspeed_promo
 * @copyright  moodleofindia  {@link  http://www.moodleofindia.com}
 * @license     http://www.moodleofindia.comcopyleft/gpl.html GNU GPL v3 or later
 */

$string['configtimetosee'] = 'Number of minutes determining the period of inactivity after which a user is no longer considered to be online.';
$string['edudiscount:addinstance'] = 'Add a new Manage License block';
$string['edudiscount:myaddinstance'] = 'Add a new Manage License block to the My Moodle page';
$string['edudiscount:viewlist'] = 'View License';
$string['periodnminutes'] = 'last {$a} minutes';
$string['pluginname'] = 'Edudiscount';
$string['timetosee'] = 'Remove after inactivity (minutes)';
$string['coursediscountreports'] = 'Course discount reports';
$string['requiredcourse'] = 'Please select any course';
$string['requiredenroll'] = 'Please select any enroll';
$string['search'] = 'Search';
$string['sl'] = 'S.L';
$string['promotionscode'] = 'Promotions Code';
$string['promotionsstartdate'] = 'Promotions Start Date';
$string['promotionsenddate'] = 'Promotions End Date';
$string['discount'] = 'Discount';
$string['active'] = 'Active';
$string['inactive'] = 'In-active';
$string['course'] = 'Course';
$string['enrol'] = 'Enrol';
$string['action'] = 'Action';
$string['percentdiscount'] = 'PERCENTAGE DISCOUNT (Only numbers are allowed)';
$string['multisingle'] = 'Multiple use/Single use';
$string['promostart'] = 'Promo Start Date:';
$string['promoend'] = 'Promo End Date:';
$string['usetimes'] = 'Can be used times (e.g 1, 5, 10, 50)';
$string['creatediscount'] = 'Create discount';
$string['editdiscount'] = 'Edit discount';
$string['deletediscount'] = 'Delete discount';
$string['viewdiscount'] = 'View discount';
$string['discount'] = 'Discount';
$string['discountcreated'] = 'Discount code has been created succssfully!!';
$string['discountupdated'] = 'Discount code has been updated succssfully!!';
$string['viewdiscounts'] = 'View discounts';
$string['promocreated'] = 'Discount code has been created succssfully!!';
$string['submit'] = 'Submit';
$string['discountnotfound'] = 'Discount not found!!';
$string['percent'] = ' %';
$string['deleteconfirm'] = 'Are you sure you want to delete !!';
$string['discountdeleted'] = 'Discount code has been deleted succssfully!!';
$string['selectcategory'] = 'Select any category please';
$string['nodiscountfound'] = 'No discount found';
$string['use'] = 'Multiple/Single use';
$string['ause'] = 'Use';

$string['requiredppcode'] = 'Promotions Code can\'t empty';
