<?php

require_once("$CFG->libdir/formslib.php");

class categorycolor_form extends moodleform {

    public function definition() {
        global $CFG, $DB;
        $mform = $this->_form;
        $mform->addElement('header', 'formheader', get_string('categorycolorsetting', 'local_categorycolor'));

        $allcategories = $DB->get_records_sql("SELECT id, name from {course_categories} where visible =1");
        foreach ($allcategories as $allcategory) {
            $mform->addElement('text', 'category['.$allcategory->id.']', $allcategory->name,array('placeholder'=>'Enter color code(Hexcode)'));
            $mform->addRule('category['.$allcategory->id.']', null, 'required');
            $mform->setType('category['.$allcategory->id.']', PARAM_RAW);
        }

        $this->add_action_buttons();
    }

    function validation($data, $files) {
        return array();
    }

}

?>