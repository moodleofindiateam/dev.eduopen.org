<?php

require_once(dirname(__FILE__).'/../config.php');

class eduopenappws_course extends EduopenApp_external_api {

    /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function get_all_institutions_parameters() {
        return new external_function_parameters(
                array('options' => new external_single_structure(
                            array('ids' => new external_multiple_structure(
                                        new external_value(PARAM_INT, 'Course id')
                                        , 'List of institution id. If empty return all institution',
                                        VALUE_OPTIONAL)
                            ), 'options - operator OR is used', VALUE_DEFAULT, array())
                )
        );
    }
    
    /**
     * Get all institution
     *
     * @param array $options It contains an array (list of ids)
     * @return array
     * @since Moodle 2.2
     */
    public static function get_all_institutions($options = array()) {
        global $CFG, $DB;
        //require_once($CFG->dirroot . "/course/lib.php");
        require_once($CFG->dirroot . "/eduopen/lib.php");
        //validate parameter
        $params = self::validate_parameters(self::get_all_courses_parameters(),
                        array('options' => $options));

        //retrieve courses
        if (!array_key_exists('ids', $params['options'])
                or empty($params['options']['ids'])) {
            $institutions = $DB->get_records('block_eduopen_master_inst');
        } else {
            $institutions = $DB->get_records_list('course', 'id', $params['options']['ids']);
        }

        //create return value
        $institutionsinfo = array();
        foreach ($institutions as $institution) {

            $institutioninfo = array();
            $institutioninfo['id'] = $institution->id;
            $institutioninfo['name_in_EN'] = $institution->name;
            $institutioninfo['name_in_IT'] = $institution->itname;
            $institutioninfo['description_in_EN'] = $institution->description;
            $institutioninfo['description_in_IT'] = $institution->itdescription;
            $instimgurl = cinstitution_images($institution);
			$instlogourl = cinstitution_logo($institution);
            $tt= ''.$instimgurl.'';
            $tt1 = ''.$instlogourl.'';
			$institutioninfo['logo'] = $tt1;
            $institutioninfo['sigil'] = $tt;
            $institutioninfo['country'] = $institution->country;
            $institutioninfo['address'] = $institution->address;
            $institutioninfo['website'] = $institution->web;
            $institutioninfo['facebook'] = $institution->facebook;
            $institutioninfo['twitter'] = $institution->twitter;
            $institutioninfo['youtube'] = $institution->youtube;
            $institutioninfo['linkedin'] = $institution->linkedin;
            $institutionsinfo[] = $institutioninfo;
        }

        return $institutionsinfo;
    }
    
    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function get_all_institutions_returns() {
        return new external_multiple_structure(
                new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'institution id'),
                            'name_in_EN' => new external_value(PARAM_TEXT, 'institution name in EN'),
							'name_in_IT' => new external_value(PARAM_TEXT, 'institution name in IT'),
                            'description_in_EN' => new external_value(PARAM_RAW, 'institution description in EN'),
							'description_in_IT' => new external_value(PARAM_RAW, 'institution description in IT'),
							'logo' => new external_value(PARAM_RAW, 'logo', VALUE_OPTIONAL),
                            'sigil' => new external_value(PARAM_RAW, 'sigil', VALUE_OPTIONAL),
                            'country' => new external_value(PARAM_RAW, 'country', VALUE_OPTIONAL),
                            'address' => new external_value(PARAM_RAW, 'address'),
                            'website' => new external_value(PARAM_RAW,'website'),
                            'facebook' => new external_value(PARAM_RAW, 'facebook'),
                            'twitter' => new external_value(PARAM_RAW, 'twitter'),
                            'youtube' => new external_value(PARAM_RAW, 'youtube'),
                            'linkedin' => new external_value(PARAM_RAW, 'linkedin'),
                        ), 'institution'
                )
        );
    }
    
/**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function get_all_courses_parameters() {
        return new external_function_parameters(
                array('options' => new external_single_structure(
                            array('ids' => new external_multiple_structure(
                                        new external_value(PARAM_INT, 'Course id')
                                        , 'List of course id. If empty return all courses
                                            except front page course.',
                                        VALUE_OPTIONAL)
                            ), 'options - operator OR is used', VALUE_DEFAULT, array())
                )
        );
    }

    /**
     * Get courses
     *
     * @param array $options It contains an array (list of ids)
     * @return array
     * @since Moodle 2.2
     */
    public static function get_all_courses($options = array()) {
        global $CFG, $DB;
		require_once($CFG->dirroot . "/eduopen/lib.php");
        require_once($CFG->dirroot . "/course/lib.php");

        //validate parameter
        $params = self::validate_parameters(self::get_all_courses_parameters(),
                        array('options' => $options));

        //retrieve courses
        if (!array_key_exists('ids', $params['options'])
                or empty($params['options']['ids'])) {
            //$courses = $DB->get_records('course');
            $courser = "SELECT c.*, ceg.institution, ceg.whatsinside, ceg.syllabus, ceg.language as eduopenlang, ceg.engtitle, ceg.audience,ceg.length, ceg.estimated,ceg.courselevel, ceg.costforattendance, ceg.badgeimage,ceg.enbadgecriteria, ceg.itbadgecriteria, ceg.encompetence, ceg.itcompetence, ceg.enbadgetags,ceg.itbadgetags,ceg.badgestatus FROM {course} c LEFT JOIN {course_extrasettings_general} ceg ON c.id = ceg.courseid";
            
            $courses = $DB->get_records_sql($courser);
            
        } else {
            $courses = $DB->get_records_list('course', 'id', $params['options']['ids']);
        }

        //create return value
        $coursesinfo = array();
        foreach ($courses as $course) {

            // now security checks
            $context = context_course::instance($course->id, IGNORE_MISSING);
            $courseformatoptions = course_get_format($course)->get_format_options();
            try {
                self::validate_context($context);
            } catch (Exception $e) {
                $exceptionparam = new stdClass();
                $exceptionparam->message = $e->getMessage();
                $exceptionparam->courseid = $course->id;
                throw new moodle_exception('errorcoursecontextnotvalid', 'webservice', '', $exceptionparam);
            }
            require_capability('moodle/course:view', $context);

            $courseinfo = array();
            $courseinfo['id'] = $course->id;
             /* if($course->language == 'en') {
                   $courseinfo['fullname'] = $course->engtitle; 
                } else {
                    $courseinfo['fullname'] = $course->fullname;
                } */
            $courseinfo['title_ENG'] = $course->engtitle; 
            $courseinfo['title_IT'] = $course->fullname; 
            $courseinfo['shortname'] = $course->shortname;
            $courseinfo['categoryid'] = $course->category;
            list($courseinfo['summaryformat']) =
                external_format_text($course->summaryformat, $context->id, 'course', 'summary', 0);
            $courseinfo['format'] = $course->format;
            $courseinfo['startdate'] = $course->startdate;
            if (array_key_exists('numsections', $courseformatoptions)) {
                // For backward-compartibility
                $courseinfo['numsections'] = $courseformatoptions['numsections'];
            }

            //some field should be returned only if the user has update permission
            $courseadmin = has_capability('moodle/course:update', $context);
            if ($courseadmin) {
                $courseinfo['categorysortorder'] = $course->sortorder;
                $courseinfo['idnumber'] = $course->idnumber;
                $courseinfo['showgrades'] = $course->showgrades;
                $courseinfo['showreports'] = $course->showreports;
                $courseinfo['newsitems'] = $course->newsitems;
                $courseinfo['visible'] = $course->visible;
                $courseinfo['maxbytes'] = $course->maxbytes;
                if (array_key_exists('hiddensections', $courseformatoptions)) {
                    // For backward-compartibility
                    $courseinfo['hiddensections'] = $courseformatoptions['hiddensections'];
                }
                $courseinfo['groupmode'] = $course->groupmode;
                $courseinfo['groupmodeforce'] = $course->groupmodeforce;
                $courseinfo['defaultgroupingid'] = $course->defaultgroupingid;
                $courseinfo['lang'] = $course->lang;
                $courseinfo['timecreated'] = $course->timecreated;
                $courseinfo['timemodified'] = $course->timemodified;
                $courseinfo['forcetheme'] = $course->theme;
                $courseinfo['enablecompletion'] = $course->enablecompletion;
                $courseinfo['completionnotify'] = $course->completionnotify;
                $courseinfo['courseformatoptions'] = array();
                foreach ($courseformatoptions as $key => $value) {
                    $courseinfo['courseformatoptions'][] = array(
                        'name' => $key,
                        'value' => $value
                    );
                }
				if($course->language == 'en') {
                    $courseinfo['description_ENG'] = strip_tags($course->syllabus);
                }else {
					$courseinfo['description_ENG'] = strip_tags($course->syllabus);
				}
                if($course->language == 'it') {
				  $courseinfo['description_LOCAL'] = strip_tags($course->whatsinside);
                } else {
					$courseinfo['description_LOCAL'] = strip_tags($course->whatsinside);
				}
				
                $courseinfo['language'] = $course->eduopenlang;
                $instname = $DB->get_record('block_eduopen_master_inst', array('id' => $course->institution));
                $courseinfo['issuer'] = $instname->name;
                $courseinfo['id_issuer'] = $instname->id;
                //to get all the instructor of the course
                $instructors = get_role_users(3, context_course::instance($course->id));
                foreach($instructors as $instructor){
                    $instructorname[] = $instructor->firstname.' '.$instructor->lastname;
                }
                $instructorstring = implode(',', $instructorname);
                $courseinfo['instructors'] = $instructorstring;
            }
            $courseinfo['learningoutcomes'] = strip_tags($course->audience);
			$csectionq = "SELECT name from {course_sections} where sequence !='' and course=$course->id and section !=0 ORDER BY section";
            $recsec = $DB->get_records_sql($csectionq);
			foreach($recsec as $recseci2) {
				$secname .= $recseci2->name.',';
			}
            $courseinfo['topics'] = rtrim($secname, ',');
            $courseinfo['releaseyear'] = userdate($course->startdate, '%Y');
            $courseinfo['startdate'] = userdate($course->startdate, '%Y-%m-%d');
            $courseinfo['duration'] = $course->length;
            $courseinfo['effort'] = $course->estimated;
            $courseinfo['courselevel'] = $course->courselevel;
            if($course->costforattendance == 0) {
                $attendancefee = 'FREE';
            } else {
                $attendancefee = $course->costforattendance;
            }
            $courseinfo['attendancefee'] = $attendancefee;
			$badgeimageurl = course_extrasetting_badgeimage($course);
			$badgeurl= ''.$badgeimageurl.'';
			$courseinfo['badgeimage'] = $badgeurl;
			$courseinfo['issuingcriteria_ENG'] = $course->enbadgecriteria;
			$courseinfo['issuingcriteria_IT'] = $course->itbadgecriteria;
			$courseinfo['competences_ENG'] = $course->encompetence;
			$courseinfo['competences_IT'] = $course->itcompetence;
			$courseinfo['tags_ENG'] = $course->enbadgetags;
			$courseinfo['tags_IT'] = $course->itbadgetags;
			$courseinfo['badgestatus'] = $course->badgestatus;
            if ($courseadmin or $course->visible
                    or has_capability('moodle/course:viewhiddencourses', $context)) {
                $coursesinfo[] = $courseinfo;
            }
            unset($instructorname);
            unset($secname);
			
        }
        
        return $coursesinfo;
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.2
     */
    public static function get_all_courses_returns() {
        return new external_multiple_structure(
                new external_single_structure(
                        array(
                            'id' => new external_value(PARAM_INT, 'course id'),
                            'shortname' => new external_value(PARAM_TEXT, 'course short name'),
                            'categoryid' => new external_value(PARAM_INT, 'category id'),
                            'title_ENG' => new external_value(PARAM_RAW, 'title_ENG'),
                            'title_IT' => new external_value(PARAM_RAW, 'title_IT'),
							
                            'idnumber' => new external_value(PARAM_RAW, 'id number', VALUE_OPTIONAL),
                            /*'summary' => new external_value(PARAM_RAW, 'summary'),*/
                            'summaryformat' => new external_format_value('summary'),
                            //'whatsinside' => new external_value(PARAM_TEXT, 'whatsinside'),
                            'description_ENG' => new external_value(PARAM_RAW, 'description_ENG'),
                            'description_LOCAL' => new external_value(PARAM_RAW, 'description_LOCAL'),
                            'language' => new external_value(PARAM_RAW, 'language'),
                            'issuer' => new external_value(PARAM_RAW, 'issuer'),
                            'id_issuer' => new external_value(PARAM_INT, 'id_issuer'),
                            'instructors' => new external_value(PARAM_RAW, 'instructors'),
                            'learningoutcomes' => new external_value(PARAM_RAW, 'learningoutcomes'),
                            'topics' => new external_value(PARAM_TEXT, 'topics'),
                            'releaseyear' => new external_value(PARAM_RAW, 'releaseyear'),
                            'startdate' => new external_value(PARAM_RAW, 'startdate'),
                            'duration' => new external_value(PARAM_RAW, 'duration'),
                            'effort' => new external_value(PARAM_RAW, 'effort'),
                            'courselevel' => new external_value(PARAM_RAW, 'courselevel'),
                            'attendancefee' => new external_value(PARAM_RAW, 'attendancefee'),
                            'badgeimage' => new external_value(PARAM_RAW, 'badgeimage'),
                            'issuingcriteria_ENG' => new external_value(PARAM_RAW, 'issuingcriteria_ENG'),
                            'issuingcriteria_IT' => new external_value(PARAM_RAW, 'issuingcriteria_ENG'),
                            'competences_ENG' => new external_value(PARAM_RAW, 'competences_ENG'),
                            'competences_IT' => new external_value(PARAM_RAW, 'competences_IT'),
                            'tags_ENG' => new external_value(PARAM_RAW, 'tags_ENG'),
                            'tags_IT' => new external_value(PARAM_RAW, 'tags_IT'),
                            'badgestatus' => new external_value(PARAM_RAW, 'badgestatus'),
                        ), 'course'
                )
        );
    }
    
     /**
     * Returns description of method parameters
     *
     * @return external_function_parameters
     * @since Moodle 2.3
     */
    public static function get_categories_parameters() {
        return new external_function_parameters(
            array(
                'criteria' => new external_multiple_structure(
                    new external_single_structure(
                        array(
                            'key' => new external_value(PARAM_ALPHA,
                                         'The category column to search, expected keys (value format) are:'.
                                         '"id" (int) the category id,'.
                                         '"name" (string) the category name,'.
                                         '"parent" (int) the parent category id,'.
                                         '"idnumber" (string) category idnumber'.
                                         ' - user must have \'moodle/category:manage\' to search on idnumber,'.
                                         '"visible" (int) whether the returned categories must be visible or hidden. If the key is not passed,
                                             then the function return all categories that the user can see.'.
                                         ' - user must have \'moodle/category:manage\' or \'moodle/category:viewhiddencategories\' to search on visible,'.
                                         '"theme" (string) only return the categories having this theme'.
                                         ' - user must have \'moodle/category:manage\' to search on theme'),
                            'value' => new external_value(PARAM_RAW, 'the value to match')
                        )
                    ), 'criteria', VALUE_DEFAULT, array()
                ),
                'addsubcategories' => new external_value(PARAM_BOOL, 'return the sub categories infos
                                          (1 - default) otherwise only the category info (0)', VALUE_DEFAULT, 1)
            )
        );
    }

    /**
     * Get categories
     *
     * @param array $criteria Criteria to match the results
     * @param booln $addsubcategories obtain only the category (false) or its subcategories (true - default)
     * @return array list of categories
     * @since Moodle 2.3
     */
    public static function get_categories($criteria = array(), $addsubcategories = true) {
        global $CFG, $DB;
        require_once($CFG->dirroot . "/course/lib.php");

        // Validate parameters.
        $params = self::validate_parameters(self::get_categories_parameters(),
                array('criteria' => $criteria, 'addsubcategories' => $addsubcategories));

        // Retrieve the categories.
        $categories = array();
        if (!empty($params['criteria'])) {

            $conditions = array();
            $wheres = array();
            foreach ($params['criteria'] as $crit) {
                $key = trim($crit['key']);

                // Trying to avoid duplicate keys.
                if (!isset($conditions[$key])) {

                    $context = context_system::instance();
                    $value = null;
                    switch ($key) {
                        case 'id':
                            $value = clean_param($crit['value'], PARAM_INT);
                            break;

                        case 'idnumber':
                            if (has_capability('moodle/category:manage', $context)) {
                                $value = clean_param($crit['value'], PARAM_RAW);
                            } else {
                                // We must throw an exception.
                                // Otherwise the dev client would think no idnumber exists.
                                throw new moodle_exception('criteriaerror',
                                        'webservice', '', null,
                                        'You don\'t have the permissions to search on the "idnumber" field.');
                            }
                            break;

                        case 'name':
                            $value = clean_param($crit['value'], PARAM_TEXT);
                            break;

                        case 'parent':
                            $value = clean_param($crit['value'], PARAM_INT);
                            break;

                        case 'visible':
                            if (has_capability('moodle/category:manage', $context)
                                or has_capability('moodle/category:viewhiddencategories',
                                        context_system::instance())) {
                                $value = clean_param($crit['value'], PARAM_INT);
                            } else {
                                throw new moodle_exception('criteriaerror',
                                        'webservice', '', null,
                                        'You don\'t have the permissions to search on the "visible" field.');
                            }
                            break;

                        case 'theme':
                            if (has_capability('moodle/category:manage', $context)) {
                                $value = clean_param($crit['value'], PARAM_THEME);
                            } else {
                                throw new moodle_exception('criteriaerror',
                                        'webservice', '', null,
                                        'You don\'t have the permissions to search on the "theme" field.');
                            }
                            break;

                        default:
                            throw new moodle_exception('criteriaerror',
                                    'webservice', '', null,
                                    'You can not search on this criteria: ' . $key);
                    }

                    if (isset($value)) {
                        $conditions[$key] = $crit['value'];
                        $wheres[] = $key . " = :" . $key;
                    }
                }
            }

            if (!empty($wheres)) {
                $wheres = implode(" AND ", $wheres);

                $categories = $DB->get_records_select('course_categories', $wheres, $conditions);

                // Retrieve its sub subcategories (all levels).
                if ($categories and !empty($params['addsubcategories'])) {
                    $newcategories = array();

                    // Check if we required visible/theme checks.
                    $additionalselect = '';
                    $additionalparams = array();
                    if (isset($conditions['visible'])) {
                        $additionalselect .= ' AND visible = :visible';
                        $additionalparams['visible'] = $conditions['visible'];
                    }
                    if (isset($conditions['theme'])) {
                        $additionalselect .= ' AND theme= :theme';
                        $additionalparams['theme'] = $conditions['theme'];
                    }

                    foreach ($categories as $category) {
                        $sqlselect = $DB->sql_like('path', ':path') . $additionalselect;
                        $sqlparams = array('path' => $category->path.'/%') + $additionalparams; // It will NOT include the specified category.
                        $subcategories = $DB->get_records_select('course_categories', $sqlselect, $sqlparams);
                        $newcategories = $newcategories + $subcategories;   // Both arrays have integer as keys.
                    }
                    $categories = $categories + $newcategories;
                }
            }

        } else {
            // Retrieve all categories in the database.
            $categories = $DB->get_records('course_categories');
        }

        // The not returned categories. key => category id, value => reason of exclusion.
        $excludedcats = array();

        // The returned categories.
        $categoriesinfo = array();

        // We need to sort the categories by path.
        // The parent cats need to be checked by the algo first.
        usort($categories, "core_course_external::compare_categories_by_path");

        foreach ($categories as $category) {

            // Check if the category is a child of an excluded category, if yes exclude it too (excluded => do not return).
            $parents = explode('/', $category->path);
            unset($parents[0]); // First key is always empty because path start with / => /1/2/4.
            foreach ($parents as $parentid) {
                // Note: when the parent exclusion was due to the context,
                // the sub category could still be returned.
                if (isset($excludedcats[$parentid]) and $excludedcats[$parentid] != 'context') {
                    $excludedcats[$category->id] = 'parent';
                }
            }

            // Check category depth is <= maxdepth (do not check for user who can manage categories).
            if ((!empty($CFG->maxcategorydepth) && count($parents) > $CFG->maxcategorydepth)
                    and !has_capability('moodle/category:manage', $context)) {
                $excludedcats[$category->id] = 'depth';
            }

            // Check the user can use the category context.
            $context = context_coursecat::instance($category->id);
            try {
                self::validate_context($context);
            } catch (Exception $e) {
                $excludedcats[$category->id] = 'context';

                // If it was the requested category then throw an exception.
                if (isset($params['categoryid']) && $category->id == $params['categoryid']) {
                    $exceptionparam = new stdClass();
                    $exceptionparam->message = $e->getMessage();
                    $exceptionparam->catid = $category->id;
                    throw new moodle_exception('errorcatcontextnotvalid', 'webservice', '', $exceptionparam);
                }
            }

            // Return the category information.
            if (!isset($excludedcats[$category->id])) {

                // Final check to see if the category is visible to the user.
                if ($category->visible
                        or has_capability('moodle/category:viewhiddencategories', context_system::instance())
                        or has_capability('moodle/category:manage', $context)) {

                    $categoryinfo = array();
                    $categoryinfo['id'] = $category->id;
                    $categoryinfo['name'] = $category->name;
                    list($categoryinfo['description'], $categoryinfo['descriptionformat']) =
                        external_format_text($category->description, $category->descriptionformat,
                                $context->id, 'coursecat', 'description', null);
                    $categoryinfo['parent'] = $category->parent;
                    $categoryinfo['sortorder'] = $category->sortorder;
                    $categoryinfo['coursecount'] = $category->coursecount;
                    $categoryinfo['depth'] = $category->depth;
                    $categoryinfo['path'] = $category->path;

                    // Some fields only returned for admin.
                    if (has_capability('moodle/category:manage', $context)) {
                        $categoryinfo['idnumber'] = $category->idnumber;
                        $categoryinfo['visible'] = $category->visible;
                        $categoryinfo['visibleold'] = $category->visibleold;
                        $categoryinfo['timemodified'] = $category->timemodified;
                        $categoryinfo['theme'] = $category->theme;
                    }

                    $categoriesinfo[] = $categoryinfo;
                } else {
                    $excludedcats[$category->id] = 'visibility';
                }
            }
        }

        // Sorting the resulting array so it looks a bit better for the client developer.
        usort($categoriesinfo, "core_course_external::compare_categories_by_sortorder");

        return $categoriesinfo;
    }

    /**
     * Sort categories array by path
     * private function: only used by get_categories
     *
     * @param array $category1
     * @param array $category2
     * @return int result of strcmp
     * @since Moodle 2.3
     */
    private static function compare_categories_by_path($category1, $category2) {
        return strcmp($category1->path, $category2->path);
    }

    /**
     * Sort categories array by sortorder
     * private function: only used by get_categories
     *
     * @param array $category1
     * @param array $category2
     * @return int result of strcmp
     * @since Moodle 2.3
     */
    private static function compare_categories_by_sortorder($category1, $category2) {
        return strcmp($category1['sortorder'], $category2['sortorder']);
    }

    /**
     * Returns description of method result value
     *
     * @return external_description
     * @since Moodle 2.3
     */
    public static function get_categories_returns() {
        return new external_multiple_structure(
            new external_single_structure(
                array(
                    'id' => new external_value(PARAM_INT, 'category id'),
                    'name' => new external_value(PARAM_TEXT, 'category name'),
                    'idnumber' => new external_value(PARAM_RAW, 'category id number', VALUE_OPTIONAL),
                    'description' => new external_value(PARAM_RAW, 'category description'),
                    'descriptionformat' => new external_format_value('description'),
                    'parent' => new external_value(PARAM_INT, 'parent category id'),
                    'sortorder' => new external_value(PARAM_INT, 'category sorting order'),
                    'coursecount' => new external_value(PARAM_INT, 'number of courses in this category'),
                    'visible' => new external_value(PARAM_INT, '1: available, 0:not available', VALUE_OPTIONAL),
                    'visibleold' => new external_value(PARAM_INT, '1: available, 0:not available', VALUE_OPTIONAL),
                    'timemodified' => new external_value(PARAM_INT, 'timestamp', VALUE_OPTIONAL),
                    'depth' => new external_value(PARAM_INT, 'category depth'),
                    'path' => new external_value(PARAM_TEXT, 'category path'),
                    'theme' => new external_value(PARAM_THEME, 'category theme', VALUE_OPTIONAL),
                ), 'List of categories'
            )
        );
    }
}

?>
