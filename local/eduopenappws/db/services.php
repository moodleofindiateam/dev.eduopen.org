<?php

// Web service functions to install.
$functions = array(
    // course
    //Get all the courses present in moodle.
    'eduopenappws_get_all_courses' => array(
        'classname' => 'eduopenappws_course',
        'methodname' => 'get_all_courses',
        'classpath' => 'local/eduopenappws/course/externallib.php',
        'description' => 'Returns all the courses',
        'type' => 'read',
        'capabilities' => '',
    ),
    
    //Get all the institutions present in eduopen system.
    'eduopenappws_get_all_institutions' => array(
        'classname' => 'eduopenappws_course',
        'methodname' => 'get_all_institutions',
        'classpath' => 'local/eduopenappws/course/externallib.php',
        'description' => 'Returns all institutions',
        'type' => 'read',
        'capabilities' => '',
    ),
    //Get all the courses categoriespresent in moodle.
    'eduopenappws_course_get_categories' => array(
        'classname' => 'eduopenappws_course',
        'methodname' => 'get_categories',
        'classpath' => 'local/eduopenappws/course/externallib.php',
        'description' => 'Returns all the course categories',
        'type' => 'read',
        'capabilities' => '',
    ),
);


$functionlist = array();
foreach ($functions as $key => $value) {
    $functionlist[] = $key;
}

$services = array(
    'EduopenApp web services' => array(
        'functions' => $functionlist,
        'shortname' => 'EduopenApp',
        'enabled' => 0,
        'restrictedusers' => 0,
    ),
);
?>
