<?php

// This file is part of MoodleofIndia - http://moodleofindia.com/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Private sendinvitation functions
 * @author     Siddharth Behera <siddharth@elearn10.com>
 * @package    local_sendinvitation
 * @copyright  2016 lms of india
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
/*
 * Site level 
 */
$string['pluginname'] = 'bestrbadge';
$string['plugin'] = 'bestrbadge';
$string['indexpage'] = 'bestrbadge';
$string['pagetitle'] = 'bestrbadge';

$string['pageheader'] = 'bestrbadge';
$string['bestrbadgesetting'] = 'bestrbadge Setting';
/*====================================================*/
$string['token'] = 'Token';
$string['tokendescription'] = 'Enter bestr account csrf token ';

$string['authurl'] = 'Auth url';
$string['username'] = 'username';
$string['siteusername'] = 'Site Username';
$string['usernamedescription'] = 'Enter bestr account username ';
$string['siteusernamedescription'] = 'Enter bestr site username ';


$string['password'] = 'password';
$string['sitepassword'] = 'Site Password';
$string['authurldescription'] = 'Enter Authentication url';
$string['passworddescription'] = 'Enter bestr account password ';
$string['sitepassworddescription'] = 'Enter bestr site password ';
$string['bestrbadge'] = 'BestrBadge';
$string['bestrbadgelist'] = 'Badge List';

