<?php
require_once(__DIR__ . '/../../config.php');
require_once(__DIR__ . '/class/bestrclient.php');
require_once(__DIR__ . '/class/utils.php');
require_once($CFG->libdir . '/adminlib.php');

$badgeid = optional_param('id', '', PARAM_ALPHANUM);
$action = optional_param('action', 'list', PARAM_ALPHANUM);
$courseid = optional_param('courseid', null, PARAM_INT);

$context = empty($courseid) ? context_system::instance() : context_course::instance($courseid);

$url = new moodle_url('/local/bestrbadge/badge.php', array('action' => $action));
$badge = empty($badgeid) ? null : bestrbadge_badge::get_instance($badgeid);

if (!empty($badgeid)) {
    $url->param('id', $badgeid);
}

// Site context.
if (empty($courseid)) {
    require_login();
} else { // Course context.
    $url->param('courseid', $courseid);
    require_login($courseid);
}
$PAGE->set_context($context);
$PAGE->set_url($url);
$PAGE->set_pagelayout(empty($courseid) ? 'admin' : 'course');
$PAGE->set_title(get_string('bestrbadge', 'local_bestrbadge'));
$PAGE->add_body_class('local-bestrbadge');

$content = '';
//$hasissuecapability = has_capability('local/bestrbadge:issuebadge', $context);
$bestrclientobj = new bestrclient();
switch ($action) {

    // Show the list of badges.
  case 'list':
        //require_capability('local/obf:viewallbadges', $context);

        try {
             $badges = $bestrclientobj->get_badges_bymailid();
            if($context instanceof context_system) {
                $content .= html_writer::start_tag('div', array('class' =>'col-md-12'));
				if($badges) {
                foreach($badges as $badge) {
                    
                   $content .=html_writer::tag('div', html_writer::link('',html_writer::img($badge->earnedBadge->imageUrl, $badge->earnedBadge->obiname, array('width' =>'150px'))), array('class' => 'col-md-4')); 
                }
				}
                $content .= html_writer::end_tag('div');
            }
        } catch (Exception $e) {
            $content .= $OUTPUT->notification($e->getMessage(), 'notifyproblem');
        }

        break;

    // Display badge info.
    case 'show':
       // require_capability('local/bestrbadge:viewdetails', $context);

//        $client = bestrbadge_client::get_instance();
//        $page = optional_param('page', 0, PARAM_INT);
//        $show = optional_param('show', 'details', PARAM_ALPHANUM);
//        $baseurl = new moodle_url('/local/bestrbadge/badge.php',
//                array('action' => 'show', 'id' => $badgeid));

        if ($context instanceof context_system) {
            navigation_node::override_active_url(new moodle_url('/local/bestrbadge/badge.php',
                    array('action' => 'list')));
            $PAGE->navbar->add($badge->get_name(), $baseurl);
        } else {
            navigation_node::override_active_url(new moodle_url('/local/bestrbadge/badge.php',
                    array('action' => 'list', 'courseid' => $courseid)));
            $coursebadgeurl = clone $baseurl;
            $coursebadgeurl->param('courseid', $courseid);
            $PAGE->navbar->add($badge->get_name(), $coursebadgeurl);
        }

        $renderer = $PAGE->get_renderer('local_bestrbadge', 'badge');
        $content .= $PAGE->get_renderer('local_bestrbadge')->render_badge_heading($badge,
                $context);

        break;
}

echo $OUTPUT->header();
echo $content;
echo $OUTPUT->footer();