<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class bestrclient {
    
    public $token_url;
    public $request_auth;
    public $request_awarded_badge_url;
    public $request_site_username;
    public $request_sitepassword;
    public $username;
    public $password;
    
    public function authenticate() {
        global $CFG;
        $newtoken = self::generate_newtoken($this->request_site_username, $this->request_sitepassword, $this->token_url);
		//$requestedcurl = 'curl -k -u bestr:bestr! -H "Content-Type: application/json" -X POST -d '.'{"_csrf": "'.$newtoken.'","email": "test-app@bestr.it", "password": "test-app@bestr.it"} -b coo '. utils::REQUEST_AUTH;
        $requestedcurl = 'curl -k -u '. $this->request_site_username.':'.$this->request_sitepassword.' -H "Content-Type: application/json" -X POST -d '."'".'{"_csrf": "'.$newtoken.'","email": "'.$this->username.'", "password": "'.$this->password.'"}'."' ". ' -b coo '.$this->request_auth;
       // echo $requestedcurl;
        $userauth = exec($requestedcurl);
       //var_dump($userauth);

    } 
    public function generate_newtoken() {
        global $CFG;
        $requestedcurl = 'curl -k -u '.$this->request_site_username.':'.$this->request_sitepassword.'  -c coo '.$this->token_url;
        $token = exec($requestedcurl);
        $tokenstring = json_decode($token);
       return $tokenstring->_csrf;
    }
    /**
     * Gets and returns the badges from bestr.
     *
     * @param bestr_client $client The client instance.
     * @return bestr_badge[] The badges.
     */
public function get_badges_bymailid() {
        global $CFG,$USER;
		$this->token_url = get_config('local_bestrbadge', 'bestrbadgeauthurl').'/csrfToken';
		$this->request_auth = get_config('local_bestrbadge', 'bestrbadgeauthurl').'/auth/process';
		$this->request_awarded_badge_url = get_config('local_bestrbadge', 'bestrbadgeauthurl').'/api/badge/awardsbymail/';
		$this->request_site_username = get_config('local_bestrbadge', 'bestrbadgesiteusername');
		$this->request_sitepassword = get_config('local_bestrbadge', 'bestrbadgesitepassword');
		$this->username = get_config('local_bestrbadge', 'bestrbadgeusername');
		$this->password = get_config('local_bestrbadge', 'bestrbadgepassword');
        self::generate_newtoken();
        self::authenticate($this->request_site_username, $this->request_sitepassword,$this->username,$this->password,$this->request_auth);
        $requestedcurl = 'curl -k -u '.$this->request_site_username.':'.$this->request_sitepassword.' -v -H '.'"'.'Content-Type: application/json'.'"'.' -b coo '.$this->request_awarded_badge_url.$USER->email;

        $allbadge = exec($requestedcurl);
        $allbadgesarray = json_decode($allbadge);

        return $allbadgesarray;
}
 
}