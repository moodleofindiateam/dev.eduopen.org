<?php

if (is_siteadmin()) {
    $bestr = new admin_category('bestr', get_string('bestrbadge', 'local_bestrbadge'));

// Add pages to navigation.

    $ADMIN->add('root', $bestr, 'location');

// Badge list -page.
    $badgelist = new admin_externalpage('bestrbadgelist', get_string('bestrbadgelist', 'local_bestrbadge'), new moodle_url('/local/bestrbadge/badge.php', array('action' => 'list')));

    $ADMIN->add('bestr', $badgelist);

    $settings = new admin_settingpage('bestrbadgeheaders', get_string('pageheader', 'local_bestrbadge'));
    $settings->add(new admin_setting_configtext('local_bestrbadge/bestrbadgeauthurl', get_string('authurl', 'local_bestrbadge'), get_string('authurldescription', 'local_bestrbadge'), 'Auth url', PARAM_RAW));

    $settings->add(new admin_setting_configtext('local_bestrbadge/bestrbadgesiteusername', get_string('siteusername', 'local_bestrbadge'), get_string('siteusernamedescription', 'local_bestrbadge'), 'Enter Site User Name', PARAM_RAW));
    $settings->add(new admin_setting_configpasswordunmask('local_bestrbadge/bestrbadgesitepassword', get_string('sitepassword', 'local_bestrbadge'), get_string('sitepassworddescription', 'local_bestrbadge'), '', PARAM_RAW));


    $settings->add(new admin_setting_configtext('local_bestrbadge/bestrbadgeusername', get_string('username', 'local_bestrbadge'), get_string('usernamedescription', 'local_bestrbadge'), 'Enter User name', PARAM_RAW));

    $settings->add(new admin_setting_configpasswordunmask('local_bestrbadge/bestrbadgepassword', get_string('password', 'local_bestrbadge'), get_string('passworddescription', 'local_bestrbadge'), '', PARAM_RAW));

    $ADMIN->add('localplugins', $settings);
}
