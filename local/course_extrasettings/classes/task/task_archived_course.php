<?php

/**
 * cron
 * @return boolean
 */

namespace local_course_extrasettings\task;

defined('MOODLE_INTERNAL') || die();

class task_archived_course extends \core\task\scheduled_task {

    public function get_name() {
        // Shown in admin screens
        return get_string('archived_crs', 'local_course_extrasettings');
    }

    public function execute() {
        global $DB, $CFG;
        // Make the Course as archived if the course end time is less than current time.
        $extraSettings = $DB->get_records_sql("SELECT * FROM
            {course_extrasettings_general} WHERE endtutoredses IS NOT NULL
            OR endselfpacedses IS NOT NULL");
        foreach ($extraSettings as $extraSetting) {
            if ($extraSetting->selfpaced == 1) {
                $courseendTime = $extraSetting->endselfpacedses;
            } else {
                $courseendTime = $extraSetting->endtutoredses;
            }
            if ($courseendTime <= time()) {
                $updateq = $DB->execute("UPDATE {course_extrasettings_general} SET coursestatus = 0
                    WHERE id=$extraSetting->id AND coursestatus=1");
            }
        }
        echo 'Event for Archived Course is Working fine';
    }

}
