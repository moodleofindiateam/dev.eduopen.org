<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function xmldb_local_course_extrasettings_upgrade($oldversion) {
    global $DB;
    $dbman = $DB->get_manager();

    /// Add a new column newcol 
    if ($oldversion < 2012012422) {
        // Define field encompetence to be added to course_extrasettings_general.
        $table = new xmldb_table('course_extrasettings_general');
        $field1 = new xmldb_field('encompetence', XMLDB_TYPE_TEXT, null, null, null, null, null, 'courseicon');
        $field2 = new xmldb_field('enbadgecriteria', XMLDB_TYPE_TEXT, null, null, null, null, null, 'encompetence');
        $field3 = new xmldb_field('enbadgetags', XMLDB_TYPE_CHAR, '1000', null, null, null, null, 'enbadgecriteria');
        $field4 = new xmldb_field('itcompetence', XMLDB_TYPE_TEXT, null, null, null, null, null, 'enbadgetags');
        $field5 = new xmldb_field('itbadgecriteria', XMLDB_TYPE_TEXT, null, null, null, null, null, 'itcompetence');
        $field6 = new xmldb_field('itbadgetags', XMLDB_TYPE_CHAR, '1000', null, null, null, null, 'itbadgecriteria');

        // Conditionally launch add field encompetence.
        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }
        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }
        if (!$dbman->field_exists($table, $field3)) {
            $dbman->add_field($table, $field3);
        }
        if (!$dbman->field_exists($table, $field4)) {
            $dbman->add_field($table, $field4);
        }
        if (!$dbman->field_exists($table, $field5)) {
            $dbman->add_field($table, $field5);
        }
        if (!$dbman->field_exists($table, $field6)) {
            $dbman->add_field($table, $field6);
        }

        // Course_extrasettings savepoint reached.
        upgrade_plugin_savepoint(true, 2012012422, 'local', 'course_extrasettings');
    }
    if ($oldversion < 2012012424) {
        // Define field encompetence to be added to course_extrasettings_general.
        $table = new xmldb_table('course_extrasettings_general');
        $field = new xmldb_field('badgeimage', XMLDB_TYPE_CHAR, '30', null, null, null, null, 'courseicon');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // Course_extrasettings savepoint reached.
        upgrade_plugin_savepoint(true, 2012012424, 'local', 'course_extrasettings');
    }
    if ($oldversion < 2012012425) {
        // Changing not null ability of field institution on table course_extrasettings_general to null.
        $table = new xmldb_table('course_extrasettings_general');
        $field = new xmldb_field('institution', XMLDB_TYPE_CHAR, '255', null, null, null, null, 'specializations');

        // Launch change of nullability for field institution.
        $dbman->change_field_notnull($table, $field);

        // Course_extrasettings savepoint reached.
        upgrade_plugin_savepoint(true, 2012012425, 'local', 'course_extrasettings');
    }

    if ($oldversion < 2012012427) {
        // Define field encompetence to be added to course_extrasettings_general.
        $table = new xmldb_table('course_extrasettings_general');
        $field = new xmldb_field('badgestatus', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0', 'badgeimage');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // Course_extrasettings savepoint reached.
        upgrade_plugin_savepoint(true, 2012012427, 'local', 'course_extrasettings');
    }

    if ($oldversion < 2012012428) {
        // Define field encompetence to be added to course_extrasettings_general.
        $table = new xmldb_table('course_extrasettings_general');
        $field = new xmldb_field('selfpaced', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '0', 'courselevel');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        // Course_extrasettings savepoint reached.
        upgrade_plugin_savepoint(true, 2012012428, 'local', 'course_extrasettings');
    }
    if ($oldversion < 2012012430) {
        // Define field encompetence to be added to course_extrasettings_general.
        $table = new xmldb_table('course_extrasettings_general');
        $field1 = new xmldb_field('endtutoredses', XMLDB_TYPE_INTEGER, '20', null, null, null, null, 'courselevel');
        $field2 = new xmldb_field('endselfpacedses', XMLDB_TYPE_INTEGER, '20', null, null, null, null, 'endtutoredses');
        $field3 = new xmldb_field('coursestatus', XMLDB_TYPE_INTEGER, '20', null, XMLDB_NOTNULL, null, '1', 'endselfpacedses');
        if (!$dbman->field_exists($table, $field1)) {
            $dbman->add_field($table, $field1);
        }
        if (!$dbman->field_exists($table, $field2)) {
            $dbman->add_field($table, $field2);
        }
        if (!$dbman->field_exists($table, $field3)) {
            $dbman->add_field($table, $field3);
        }
        // Course_extrasettings savepoint reached.
        upgrade_plugin_savepoint(true, 2012012430, 'local', 'course_extrasettings');
    }
    return true;
}
