<?php
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once ('lib.php');
require_once ('form/editform_general.php');

$courseid = required_param('courseid', PARAM_INT);
if ($course = $DB->get_record('course', array('id' => $courseid))) {
    $general = $DB->get_record('course_extrasettings_general', array('courseid' => $courseid));
    if ($general->courseid) {
        $dltbtn = $general->courseid;
    }
}
/// Security and access check

require_course_login($course);
$context = context_course::instance($course->id);
require_capability('moodle/role:assign', $context);
/// Start making page
$PAGE->set_pagelayout('course');
$editpromo = 'Edit Course Extrasettings';
$PAGE->set_title($editpromo);
$PAGE->set_url('/local/course_extrasettings/edit_general.php');
$PAGE->requires->jquery();

$mform = new course_extrasettings_form($CFG->wwwroot . '/local/course_extrasettings/edit_general.php?courseid=' . $courseid);

/*
  $mform->set_data(array('courseimage' => $general->courseimage,
  'itemid' => $general->courseimage,
  'context' =>"63",
  'userid' =>"11",
  )); */
//
//Added by shiuli on 25th sep
$maxbytes = 5000000;
//added by nihar
// Course Icon.
if (isset($general->courseicon) && !empty($general->courseicon)) {
    $draftitemid0 = file_get_submitted_draft_itemid('courseicon');
    file_prepare_draft_area($draftitemid0, $context->id, 'local_course_extrasettings', 'content', $general->courseicon, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
    $general->courseicon = $draftitemid0; //Comment during editing, else comment out.
}
// Courfse Image.
$draftitemid1 = file_get_submitted_draft_itemid('courseimage');
file_prepare_draft_area($draftitemid1, $context->id, 'local_course_extrasettings', 'content', $general->courseimage, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
$general->courseimage = $draftitemid1; //Comment during editing, else comment out.
//end of Courfse Image.
// Attendance certificate
$draftitemid2 = file_get_submitted_draft_itemid('certificatedownload1');
file_prepare_draft_area($draftitemid2, $context->id, 'local_course_extrasettings', 'content', $general->certificatedownload1, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
$general->certificatedownload1 = $draftitemid2;
//end of ttendance certificate
// Verified Certificate
$draftitemid3 = file_get_submitted_draft_itemid('certificatedownload2');
file_prepare_draft_area($draftitemid3, $context->id, 'local_course_extrasettings', 'content', $general->certificatedownload2, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
$general->certificatedownload2 = $draftitemid3;
//end of Verified Certificate
// Exam Certificate
$draftitemid4 = file_get_submitted_draft_itemid('certificatedownload');
file_prepare_draft_area($draftitemid4, $context->id, 'local_course_extrasettings', 'content', $general->certificatedownload, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
$general->certificatedownload = $draftitemid4;
//end of Exam Certificate
// Badge Image.
$draftitemid5 = file_get_submitted_draft_itemid('badgeimage');
file_prepare_draft_area($draftitemid5, $context->id, 'local_course_extrasettings', 'content', $general->badgeimage, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
$general->badgeimage = $draftitemid5; //Comment during editing, else comment out.
// Tags.
$enbadgetagarray = explode(',', $general->enbadgetags);
$general->enbadgetags = $enbadgetagarray;
$itbadgetagarray = explode(',', $general->itbadgetags);
$general->itbadgetags = $itbadgetagarray;

$mform->set_data($general);

if ($mform->is_cancelled()) {
    redirect(new moodle_url('/course/view.php?id=' . $courseid));
} else if ($data = $mform->get_data(false)) {
    //var_dump($data);
    global $USER, $CFG;
    //modified by nihar
    $coursecontext = context_course::instance($course->id);

    //$personalcontext = context_user::instance($USER->id);
    $generalupd = new stdClass();
    $generalupd->id = $general->id;
    // General
    $generalupd->engtitle = $data->engtitle;
    $generalupd->syllabus = $data->syllabus;
    $generalupd->audience = $data->audience; // learning Outcome
    $generalupd->courseicon = $data->courseicon;
    $generalupd->courseimage = $data->courseimage;
    $arraysp = $data->specializations;
    $specialdata = implode(",", $arraysp);
    $generalupd->specializations = $specialdata;
    $generalupd->institution = $data->institution;
    $generalupd->coursetype = $data->coursetype;
    $generalupd->courselevel = $data->courselevel;
    $generalupd->selfpaced = $data->selfpaced;
    $generalupd->currency = $data->currency;
    $generalupd->cost = $data->cost;
    $generalupd->language = $data->language;
    $generalupd->featurecourse = $data->featurecourse;
    $generalupd->courseid = $data->courseid;


    //$generalupd->lifetime = $data->lifetime;
    // Attendance Certificate
    $generalupd->certificate1 = $data->certificate1;
    if ($data->certificate1 == 1) {
        $generalupd->certificatedownload1 = $data->certificatedownload1;
        ?>
        <div id="fitem_id_certificatedownload1" class="fitem fitem_ffilepicker" style="display:block;"></div>
        <?php
    } else {
        $generalupd->certificatedownload1 = 0;
        ?>
        <div id="fitem_id_certificatedownload1" class="fitem fitem_ffilepicker" style="display:none;"></div>
        <?php
    }
    $generalupd->examrule1 = $data->examrule1;
    $generalupd->attendancecompletion = $data->attendancecompletion;
    if ($data->attendancecompletion == 1) {
        $generalupd->costforattendance = $data->costforattendance;
        ?>
        <div id="fitem_id_costforattendance" style="display:block;"></div>
        <?php
    } else {
        $generalupd->costforattendance = 0;
        ?>
        <div id="fitem_id_costforattendance" style="display:none;"></div>
        <?php
    }
    // Verified Certificate
    $generalupd->certificate2 = $data->certificate2;
    if ($data->certificate2 == 1) {
        $generalupd->certificatedownload2 = $data->certificatedownload2;
        ?>
        <div id="fitem_id_certificatedownload2" style="display:block;"></div>
        <?php
    } else {
        $generalupd->certificatedownload2 = 0;
        ?>
        <div id="fitem_id_certificatedownload2" style="display:none;"></div>
        <?php
    }
    $generalupd->examrule2 = $data->examrule2;
    $generalupd->vattendancecompletion = $data->vattendancecompletion;
    if ($data->vattendancecompletion == 1) {
        $generalupd->vcostforattendance = $data->vcostforattendance;
        ?>
        <div id="fitem_id_vcostforattendance" style="display:block;"></div>
        <?php
    } else {
        $generalupd->vcostforattendance = 0;
        ?>
        <div id="fitem_id_vcostforattendance" style="display:none;"></div>
        <?php
    }
    // Exam Certificate
    $generalupd->certificate = $data->certificate;
    if ($data->certificate == 1) {
        $generalupd->certificatedownload = $data->certificatedownload;
        ?>
        <div id="fitem_id_certificatedownload" style="display:block;"></div>
        <?php
    } else {
        $generalupd->certificatedownload = 0;
        ?>
        <div id="fitem_id_certificatedownload" style="display:none;"></div>
        <?php
    }
    $generalupd->examrule = $data->examrule;
    $generalupd->formalcredit = $data->formalcredit;
    if ($data->formalcredit == 1) {
        $generalupd->costforformalcredit = $data->costforformalcredit;
        ?>
        <div id="fitem_id_costforformalcredit" style="display:block;"></div>
        <?php
    } else {
        $generalupd->costforformalcredit = 0;
        ?>
        <div id="fitem_id_costforformalcredit" style="display:none;"></div>
        <?php
    }
    $generalupd->credits = $data->credits;
    // Others
    $generalupd->length = $data->length;
    $generalupd->estimated = $data->estimated;
    $generalupd->videourl = $data->videourl;
    $generalupd->textbook = $data->textbook;
    $generalupd->whatsinside = $data->whatsinside;
    $generalupd->recommendedbackground = $data->recommendedbackground;
    $generalupd->crecruitments = $data->crecruitments; //as learning outcomes
    $generalupd->contextid = $coursecontext->id;

    // Course date and archival.
    $generalupd->endtutoredses = $data->endtutoredses;
    $generalupd->endselfpacedses = $data->endselfpacedses;
    $generalupd->coursestatus = $data->coursestatus;

    // Badge Status.
    $generalupd->badgestatus = $data->badgestatus;
    //get course category.
    $coursecat = $DB->get_record('course', array('id' => $general->courseid));
    $categ = $DB->get_record('course_categories', array('id' => $coursecat->category));

    // Badge section in EN.
    $generalupd->badgeimage = $data->badgeimage;
    $generalupd->enbadgecriteria = $data->enbadgecriteria;
    $generalupd->encompetence = $data->encompetence;
    $enarraytags = $data->enbadgetags;
    if (in_array($categ->name, $general->enbadgetags)) {
        $entagdata = implode(",", $enarraytags);
        $generalupd->enbadgetags = $entagdata;
    } else {
        array_push($enarraytags, $categ->name);
        $entagdata1 = implode(",", $enarraytags);
        $generalupd->enbadgetags = $entagdata1;
    }

    // Badge section in IT.
    $generalupd->itbadgecriteria = $data->itbadgecriteria;
    $generalupd->itcompetence = $data->itcompetence;
    $itarraytags = $data->itbadgetags;
    if (in_array($categ->name, $general->itbadgetags)) {
        $ittagdata = implode(",", $itarraytags);
        $generalupd->itbadgetags = $ittagdata;
    } else {
        array_push($itarraytags, $categ->name);
        $ittagdata1 = implode(",", $itarraytags);
        $generalupd->itbadgetags = $ittagdata1;
    }
    $generalupdate = $DB->update_record('course_extrasettings_general', $generalupd);
    //added by nihar
    //Course Icon.
    file_save_draft_area_files($data->courseicon, $context->id, 'local_course_extrasettings', 'content', $data->courseicon, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
    //Course Image.
    file_save_draft_area_files($data->courseimage, $context->id, 'local_course_extrasettings', 'content', $data->courseimage, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
    // Attendance certificate
    file_save_draft_area_files($data->certificatedownload1, $context->id, 'local_course_extrasettings', 'content', $data->certificatedownload1, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
    //end of Attendance certificate
    // Verified Certificate
    file_save_draft_area_files($data->certificatedownload2, $context->id, 'local_course_extrasettings', 'content', $data->certificatedownload2, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
    //end of Verified Certificate
    // Exam Certificate
    file_save_draft_area_files($data->certificatedownload, $context->id, 'local_course_extrasettings', 'content', $data->certificatedownload, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
    //end of Exam Certificate
    // Badge Image.
    file_save_draft_area_files($data->badgeimage, $context->id, 'local_course_extrasettings', 'content', $data->badgeimage, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
}
//$coursesetting1 = $DB->get_records_sql("SELECT * FROM {eduopen_special_course_sequence}");
if (isset($generalupdate)) {
    if ($generalupdate) {
        redirect(new moodle_url($CFG->wwwroot . '/course/view.php?id=' . $course->id));
    }
}
echo $OUTPUT->header();
$strinscriptions = get_string('course_extrasettings', 'local_course_extrasettings');
echo $OUTPUT->heading_with_help($strinscriptions, 'course_extrasettings', 'local_course_extrasettings', 'icon', get_string('course_extrasettings', 'local_course_extrasettings'));
//echo $OUTPUT->box (get_string('course_extrasettings_info', 'local_course_extrasettings'), 'center');
if ($dltbtn) {
    echo '<div class = "delete_course_extras"><button title = "Click this button to delete Course Extra Setting Details"
    id = "dlt_extra" onclick = "delete_extra(' . $dltbtn . ');">Delete Extra Settings Details</button></div>';
}
$mform->display();
echo $OUTPUT->footer();
?>

<script>
// Attendance Certificate
    $("#id_certificate1").change(function () {
        if (document.getElementById("id_certificate1").value == 1) {
            $("#fitem_id_certificatedownload1").show();
        } else {
            $("#fitem_id_certificatedownload1").hide();
        }
    });
    $("#id_attendancecompletion").change(function () {
        if (document.getElementById("id_attendancecompletion").value == 1) {
            $("#fitem_id_costforattendance").show();
        } else {
            $("#fitem_id_costforattendance").hide();
        }
    });

// Verified Certificate
    $("#id_certificate2").change(function () {
        if (document.getElementById("id_certificate2").value == 1) {
            $("#fitem_id_certificatedownload2").show();
        } else {
            $("#fitem_id_certificatedownload2").hide();
        }
    });
    $("#id_vattendancecompletion").change(function () {
        if (document.getElementById("id_vattendancecompletion").value == 1) {
            $("#fitem_id_vcostforattendance").show();
        } else {
            $("#fitem_id_vcostforattendance").hide();
        }
    });

// Exam Certificate
    $("#id_certificate").change(function () {
        if (document.getElementById("id_certificate").value == 1) {
            $("#fitem_id_certificatedownload").show();
        } else {
            $("#fitem_id_certificatedownload").hide();
        }
    });
    $("#id_formalcredit").change(function () {
        if (document.getElementById("id_formalcredit").value == 1) {
            $("#fitem_id_costforformalcredit").show();
        } else {
            $("#fitem_id_costforformalcredit").hide();
        }
    });

    // Self Paced option.
    $(document).ready(function () {
        if (document.getElementById("id_selfpaced").value == 1) {
            $("#fitem_id_endselfpacedses").show();
        } else {
            $("#fitem_id_endselfpacedses").hide();
        }
    });
    $("#id_selfpaced").change(function () {
        if (document.getElementById("id_selfpaced").value == 1) {
            $("#fitem_id_endselfpacedses").show();
        } else {
            $("#fitem_id_endselfpacedses").hide();
        }
    });

</script>
<script>
    /* $("#id_institution").on("keyup", function(event){                                           
     var query = $(this).val();
     if(query.length > 1){
     $.post( "autocomplete.php", { mode: "institution", data: query}, function( data ) { 
     searchInstitution = data;
     
     //First search
     if(drewInstitution == false){                                                                                       
     //Create list for results
     $("#id_institution ").after('<ul id="selectorSearch" class="searchul"></ul>');
     //Prevent redrawing/binding of list
     drewInstitution = true;
     //Bind click event to list elements in results
     $("#selectorSearch").on("click", "li", function(){
     $("#id_institution").val($(this).text());                    
     $("#selectorSearch").empty();
     });
     }else{//Clear old results  
     $("#selectorSearch").empty();   
     }
     for(term in searchInstitution){  
     $("#selectorSearch").append("<li class='searchli'>" + searchInstitution[term] + "</li>"); 
     }                                                                                              
     },"json");
     }else if(drewInstitution){//Handle backspace/delete so results don't remain with less than 3 characters
     $("#selectorSearchResult").empty();
     }
     });
     });*/
</script>

<script src="<?php echo $CFG->wwwroot . '/local/course_extrasettings/js/delete.js' ?>"></script>
<script>
    var page = {url: "<?php echo $CFG->wwwroot ?>"};
</script>