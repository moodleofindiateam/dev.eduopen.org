<?php
$string['pluginname'] = 'Customized Course Settings';

$string['course_extrasettings'] = 'Customized Course Settings';
$string['course_extrasettings_info'] = <<<EOS
<b>Please fill up the details </b>
EOS;
$string['delete_extrasettings'] = 'Delete Extra Settings Details';

$string['course_extrasettings_help'] = <<<EOS
<b>This course extra setting where you can add payment,course length, license etc for your course. </b>
</p>
EOS;
$string['savechanges'] = 'Save Changes';
$string['savechanges1'] = 'Save and Return to Course';
$string['courseimage'] = 'Course Image';
$string['featurecourse'] = 'Featured';
$string['classroom'] = 'Classroom';
$string['online'] = 'Online';
$string['blended'] = 'Blended';
$string['coursetype'] = 'Course Type';
$string['courselevel'] = 'Course Level';
$string['generals'] = 'General';
$string['payments'] = 'Payment and Promo';
$string['lifetime'] = 'Is It Life Time Access';

$string['cost'] = 'Cost for Course Attendance';
$string['attendance_cost'] = 'Attendance fee';

$string['yes'] = 'Yes';
$string['no'] = 'No';
$string['pcode'] = 'Is having Promocode';
$string['promocode'] = 'Your Promocode';
$string['discount'] = 'Discount';
$string['promoenddate'] = 'Promo End Date';
$string['active'] = 'Active';
$string['promocodetaken'] = 'Promo code alredy exists in this course';
$string['coursepromo'] = 'Course Extra Settings';
$string['addpromo'] = 'Add Course Extra Settings';
$string['viewpromo'] = 'View Course Extra Settings';
$string['license'] = 'License';
$string['whatsinside'] = 'About this course';
$string['recommendedbackground'] = 'Recommended Background';

/*$string['crecruitments'] = 'Course Pre-requisite'; */
/* $string['crecruitments'] = 'Get Certificate'; */
$string['crecruitments'] = 'Course Format';
$string['videourl'] = 'Video Url';
$string['estimated'] = 'Estimated Effort';
$string['length'] = 'Course Length';
$string['schooluniv'] = 'School / University';
$string['question1'] = 'Can I download the videos & learn?';
$string['question2'] = 'If I have any doubt while learning, How can I clear my doubts?';
$string['question3'] = 'Is it safe to use my debit/credit card for making the payment?';
$string['question4'] = 'How will I receive the certificate?';
$string['faq'] = 'Course FAQ';
$string['length_help_help'] = 'Enter the length of the course. For example "10 weeks"';
$string['length_help'] = 'Course Length';
$string['videourl_help_help'] = 'Enter the url of the video For example "https://www.youtube.com/watch?v=6ih0d-pEI_s" ';
$string['videourl_help'] = 'Video Url';
$string['estimated_help'] = 'Estimated Effort';
$string['estimated_help_help'] = 'Enter the Estimated effort for the course For Example "20 hours / week"';
/* $string['levelfree'] = 'Level Free';

$string['Examination'] = 'Examination'; */
$string['clevel1'] = 'Beginner';
$string['clevel2'] = 'Intermediate';
$string['clevel3'] = 'Advanced';

$string['specializations'] = 'Pathways';
$string['institution'] = 'Institution';
$string['ctfedownload'] = 'Certificate ';


$string['costforformalcreditexam'] = 'Cost for Formal Credit Exam ';
$string['course_pic'] = 'Image size of Course Picture';
$string['course_pic_help'] = 'Upload Images of maximum size 200×200';
$string['languagetype'] = 'Language';
$string['english'] = 'English';
$string['italian'] = 'Italian';
$string['spanish'] = 'Spanish';
$string['french'] = 'French';
$string['german'] = 'German';
$string['exam'] = 'Currency';

$string['cert'] = 'Certificates';

$string['attendcmpl'] = 'Certificate';
$string['others'] = 'Other Details';

$string['australiandollar'] = 'Australian Dollar';
$string['brazilianreal'] = 'Brazilian Real';
$string['canadiandollar'] = 'Canadian Dollar';
$string['czechkoruna'] = 'Czech Koruna';
$string['danishkrone'] = 'Danish Krone';
$string['euro'] = 'Euro';
$string['hongkongdollar'] = 'Hong Kong Dollar';
$string['hungarianforint'] = 'Hungarian Forint';

$string['japaneseyen'] = 'Japanese Yen';
$string['malaysianringgit'] = 'Malaysian Ringgit';

$string['mexicanpeso'] = 'Mexican Peso';
$string['norwegiankrone'] = 'Norwegian Krone';
$string['newzdollar'] = 'New Zealand Dollar';
$string['israeliheqel'] = 'Israeli New Sheqel';
$string['philippinepeso'] = 'Philippine Peso';
$string['polishzloty'] = 'Polish Zloty';
$string['poundsterling'] = 'Pound Sterling';
$string['russianruble'] = 'Russian Ruble';
$string['singaporedollar'] = 'Singapore Dollar';
$string['swedishkrona'] = 'Swedish Krona';
$string['swissfranc'] = 'Swiss Franc';
$string['taiwandollar'] = 'Taiwan New Dollar';
$string['thaibaht'] = 'Thai Baht';
$string['turkishlira'] = 'Turkish Lira';
$string['usdollar'] = 'U.S. Dollar';
$string['indianrupee'] = 'Indian Rupee';


$string['syllabus'] = 'Summary in English';
$string['audience'] = 'Learning outcomes'; //Previous Course format
$string['textbook'] = 'Textbooks and Suggested Readings';
$string['att_cert_rule'] = 'Attendance Certificate Rules';


$string['certificate_section'] = 'Certificates and Exam rules';
$string['certificate_image'] = 'Certificate Images';

// Attendance Certificate
$string['att_cert'] = 'Attendance Certificate';
$string['att_certificate'] = 'Will You Provide Attendance Certificate for this Course?';
$string['att_cert_rule'] = 'Attendance Certificate Rules';
$string['attendancecompletion'] = 'Will you charge any cost for this Certificate?';
$string['costforattendanceofcomplettion'] = 'Cost for Attendance Certificate';

// Verified Certificate
$string['verify_cert'] = 'Verified Certificate';
$string['verify_cert_course'] = 'Will You Provide Verified Certificate for this Course?';
$string['sverify_cert'] = 'Sample Verified Certificate';
$string['verify_cert_rule'] = 'Verified Certificate Rules';
$string['vattendancecompletion'] = 'Will you charge any cost for this Certificate?';
$string['vcostforattendanceofcomplettion'] = 'Cost for Verified Certificate';

// Exam Certificate as Exam
$string['exam_cert'] = 'Exam';
$string['exam_certificate'] = 'Certificate';
$string['exam_cert_course'] = 'Will You Provide Certificate for this Course?';
$string['exam_cert_rule'] = 'Exam Rules';
$string['formalcredit'] = 'Will you charge any cost of Formal Credit for Examination?';
$string['costforformalcreditexam'] = 'Cost for Formal Credit Exam ';
$string['noofcredit'] = 'No of Credits';

// General Section
$string['engtitle'] = 'Course Title in English';
$string['courseicon'] = 'Course Icon';

// Badge Status
$string['badgesec'] = 'Badge Section';
$string['badgestatus'] = 'Badge Status';
$string['activebadge'] = 'Active';
$string['inactivebadge'] = 'Inactive';

// Badge Section in English.
$string['badgeimage'] = 'Badge Image';
$string['enbadgehead'] = 'Badge Section in English';
$string['competence'] = 'Competences';
$string['badgeccriteria'] = 'Issuing Criteria';
$string['btags'] = 'Tags';
// Badge Section in Italian.
$string['itbadgehead'] = 'Badge Section in Italian';

$string['enbadgetagshelp'] = 'Help for badge tags';
$string['itbadgetagshelp'] = 'Help for badge tags';
$string['enbadgetagshelp_help'] = 'After clicking on Save button it will automatically store the course category';
$string['itbadgetagshelp_help'] = 'After clicking on Save button it will automatically store the course category';

$string['selfpaced'] = 'Self Paced';

$string['dateandarchival'] = 'Course End Date and Archival';
$string['endtutoredses'] = 'End of Tutored Session';
$string['endtutoredses_help_help'] = 'Enter the End of Tutored session.';
$string['endtutoredses_help'] = 'End of Tutored Session';
$string['endselfpacedses'] = 'End of Self Paced';
$string['endselfpacedses_help_help'] = 'Enter the End of Self Paced.';
$string['endselfpacedses_help'] = 'End of Self Paced';
$string['coursestatus'] = 'Course Status';
$string['cactive'] = 'Active';
$string['carchive'] = 'Archive';

$string['archived_crs'] = 'Make Course as Archived';
$string['eventarchivedcourse'] = 'Event for Archived Course';

