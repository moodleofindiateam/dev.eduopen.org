<?php
// $Id: inscriptions_massives.php 356 2010-02-27 13:15:34Z ppollet $
/**
 * A bulk enrolment plugin that allow teachers to massively enrol existing accounts to their courses,
 * with an option of adding every user to a group
 * Version for Moodle 1.9.x courtesy of Patrick POLLET & Valery FREMAUX  France, February 2010
 * Version for Moodle 2.x by pp@patrickpollet.net March 2012
 */
require(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once ('lib.php');
global $PAGE;

$id = required_param('courseid', PARAM_INT);

$course = $DB->get_record('course', array('id' => $id));
/// Security and access check
require_course_login($course);
$context = context_course::instance($course->id);
require_capability('moodle/role:assign', $context);

/// Start making page
$PAGE->set_pagelayout('course');
$PAGE->set_url('/local/course_extrasettings/course_extrasettings.php');

$addnewpromo = 'Add Course Extrasettings';
$PAGE->navbar->add($addnewpromo);
$PAGE->set_title($addnewpromo);
$PAGE->set_heading("$course->fullname" . ' Add course_extrasettings');

$PAGE->requires->jquery();


if ($gen = $DB->record_exists('course_extrasettings_general', array('courseid' => $id))) {
    /* if(isset($gen)) {
      if($gen){ */
    $url = new moodle_url($CFG->wwwroot . '/local/course_extrasettings/edit_general.php?courseid=' . $id);
    //redirect($CFG->wwwroot . "/local/course_extrasettings/view_promo.php?id=$id");
    redirect($url);
    $insertgen = false;
} else {
    require_once('form/course_extrasettings_form.php');
    $insertgen = true;
}

$mform = new course_extrasettings_form($CFG->wwwroot . '/local/course_extrasettings/course_extrasettings.php', array('course' => $course));

if ($generalfirst = $DB->record_exists('course_extrasettings_general', array('courseid' => $course->id))) {
    $generalalredy = new stdClass();
    $generalalredy = $DB->get_record('course_extrasettings_general', array('courseid' => $course->id));
    $mform->set_data($generalalredy);
}
if (!isset($generalalredy)) {
    $generalalredy = new stdClass();
    $generalalredy->courseid = -1;
}
if ($mform->is_cancelled()) {
    redirect(new moodle_url('/course/view.php?id=' . $id));
} else if ($data = $mform->get_data(false)) {
    global $USER;
    //modified by nihar
    $coursecontext = context_course::instance($course->id);
    //$personalcontext = context_user::instance($USER->id);
    if ($data->courseid == $generalalredy->courseid) {
        if ($insertgen == true) {
            $generalupd = new stdClass();
            $generalupd->id = $generalalredy->id;
            // General
            $general->engtitle = $data->engtitle;
            $general->syllabus = $data->syllabus;
            $general->audience = $data->audience; //course format
            $general->courseicon = $data->courseicon;
            $general->courseimage = $data->courseimage;
            $arraysp = $data->specializations;
            $specialdata = implode(",", $arraysp);
            $general->specializations = $specialdata;
            $general->institution = $data->institution;
            $general->coursetype = $data->coursetype;
            $general->courselevel = $data->courselevel;
            $general->selfpaced = $data->selfpaced;
            $general->currency = $data->currency;
            $general->cost = $data->cost;
            $general->language = $data->language;
            $general->featurecourse = $data->featurecourse;
            $general->courseid = $data->courseid;
            //$general->lifetime    = $data->lifetime;
            // Attendance Certificate
            $general->certificate1 = $data->certificate1;
            $general->certificatedownload1 = $data->certificatedownload1;
            $general->examrule1 = $data->examrule1;
            $general->attendancecompletion = $data->attendancecompletion;
            $general->costforattendance = $data->costforattendance;
            // Verified Certificate
            $general->certificate2 = $data->certificate2;
            $general->certificatedownload2 = $data->certificatedownload2;
            $general->examrule2 = $data->examrule2;
            $general->vattendancecompletion = $data->vattendancecompletion;
            $general->vcostforattendance = $data->vcostforattendance;

            // Exam Certificate
            $general->certificate = $data->certificate;
            $general->certificatedownload = $data->certificatedownload;
            $general->examrule = $data->examrule;
            $general->formalcredit = $data->formalcredit;
            $general->costforformalcredit = $data->costforformalcredit;
            $general->credits = $data->credits;

            // Others
            $general->length = $data->length;
            $general->estimated = $data->estimated;
            $general->videourl = $data->videourl;
            $general->textbook = $data->textbook;
            $general->whatsinside = $data->whatsinside;
            $general->recommendedbackground = $data->recommendedbackground;
            $general->crecruitments = $data->crecruitments;  // as learning outcomes
            $general->contextid = $coursecontext->id;

            // Course date and archival.
            $general->endtutoredses = $data->endtutoredses;
            $general->endselfpacedses = $data->endselfpacedses;
            $general->coursestatus = $data->coursestatus;

            // Badge Status.
            $general->badgestatus = $data->badgestatus;
            //get course category.
            $coursecat = $DB->get_record('course', array('id' => $general->courseid));
            $categ = $DB->get_record('course_categories', array('id' => $coursecat->category));

            // Badge section in EN.
            $general->badgeimage = $data->badgeimage;
            $general->enbadgecriteria = $data->enbadgecriteria;
            $general->encompetence = $data->encompetence;
            $enarraytags = $data->enbadgetags;
            array_push($enarraytags, $categ->name);
            $entagdata = implode(",", $enarraytags);
            $general->enbadgetags = $entagdata;

            // Badge section in IT.
            $general->itbadgecriteria = $data->itbadgecriteria;
            $general->itcompetence = $data->itcompetence;
            $itarraytags = $data->itbadgetags;
            array_push($itarraytags, $categ->name);
            $ittagdata = implode(",", $itarraytags);
            $general->itbadgetags = $ittagdata;

            $DB->update_record('course_extrasettings_general', $generalupd);
        }
    } else {
        $general = new stdClass();
        // General
        $general->engtitle = $data->engtitle;
        $general->syllabus = $data->syllabus;
        $general->audience = $data->audience; //learning outcomes
        $general->courseicon = $data->courseicon;
        $general->courseimage = $data->courseimage;
        $arraysp = $data->specializations;
        $specialdata = implode(",", $arraysp);
        $general->specializations = $specialdata;
        $general->institution = $data->institution;
        $general->coursetype = $data->coursetype;
        $general->courselevel = $data->courselevel;
        $general->selfpaced = $data->selfpaced;
        $general->currency = $data->currency;
        $general->cost = $data->cost;
        $general->language = $data->language;
        $general->featurecourse = $data->featurecourse;
        $general->courseid = $data->courseid;
        //$general->lifetime   = $data->lifetime;
        // Attendance Certificate
        $general->certificate1 = $data->certificate1;
        $general->certificatedownload1 = $data->certificatedownload1;
        $general->examrule1 = $data->examrule1;
        $general->attendancecompletion = $data->attendancecompletion;
        $general->costforattendance = $data->costforattendance;
        // Verified Certificate
        $general->certificate2 = $data->certificate2;
        $general->certificatedownload2 = $data->certificatedownload2;
        $general->examrule2 = $data->examrule2;
        $general->vattendancecompletion = $data->vattendancecompletion;
        $general->vcostforattendance = $data->vcostforattendance;
        // Exam Certificate
        $general->certificate = $data->certificate;
        $general->certificatedownload = $data->certificatedownload;
        $general->examrule = $data->examrule;
        $general->formalcredit = $data->formalcredit;
        $general->costforformalcredit = $data->costforformalcredit;
        $general->credits = $data->credits;

        // Others
        $general->length = $data->length;
        $general->estimated = $data->estimated;
        $general->videourl = $data->videourl;
        $general->textbook = $data->textbook;
        $general->whatsinside = $data->whatsinside;
        $general->recommendedbackground = $data->recommendedbackground;
        $general->crecruitments = $data->crecruitments; //course format
        $general->contextid = $coursecontext->id;

        // Course date and archival.
        $general->endtutoredses = $data->endtutoredses;
        $general->endselfpacedses = $data->endselfpacedses;
        $general->coursestatus = $data->coursestatus;

        // Badge Status.
        $general->badgestatus = $data->badgestatus;
        //get course category.
        $coursecat = $DB->get_record('course', array('id' => $general->courseid));
        $categ = $DB->get_record('course_categories', array('id' => $coursecat->category));

        // Badge section in EN.
        $general->badgeimage = $data->badgeimage;
        $general->enbadgecriteria = $data->enbadgecriteria;
        $general->encompetence = $data->encompetence;
        $enarraytags = $data->enbadgetags;
        //array_push($enarraytags, $categ->name);
        $entagdata = implode(",", $enarraytags);
        $general->enbadgetags = $entagdata;

        // Badge section in IT.
        $general->itbadgecriteria = $data->itbadgecriteria;
        $general->itcompetence = $data->itcompetence;
        $itarraytags = $data->itbadgetags;
        // array_push($itarraytags, $categ->name);
        $ittagdata = implode(",", $itarraytags);
        $general->itbadgetags = $ittagdata;

        $generalinsertid = $DB->insert_record('course_extrasettings_general', $general);
        //added by nihar
        $maxbytes = 5000000;
        file_save_draft_area_files($data->courseicon, $context->id, 'local_course_extrasettings', 'content', $data->courseicon, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));

        file_save_draft_area_files($data->courseimage, $context->id, 'local_course_extrasettings', 'content', $data->courseimage, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
        // Attendance certificate
        file_save_draft_area_files($data->certificatedownload1, $context->id, 'local_course_extrasettings', 'content', $data->certificatedownload1, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
        //end
        // Verified Certificate
        file_save_draft_area_files($data->certificatedownload2, $context->id, 'local_course_extrasettings', 'content', $data->certificatedownload2, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
        //end
        // Exam Certificate
        file_save_draft_area_files($data->certificatedownload, $context->id, 'local_course_extrasettings', 'content', $data->certificatedownload, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
        // Badge Image.
        file_save_draft_area_files($data->badgeimage, $context->id, 'local_course_extrasettings', 'content', $data->badgeimage, array('subdirs' => 0, 'maxbytes' => $maxbytes, 'maxfiles' => 1));
        //end
    }
    if (isset($generalinsertid)) {
        if ($generalinsertid) { //after inserting record the page should redirect to course view page
            redirect(new moodle_url($CFG->wwwroot . '/course/view.php?id=' . $general->courseid));
        }
    }
}
echo $OUTPUT->header();


$strinscriptions = get_string('course_extrasettings', 'local_course_extrasettings');
echo $OUTPUT->heading_with_help($strinscriptions, 'course_extrasettings', 'local_course_extrasettings', 'icon', get_string('course_extrasettings', 'local_course_extrasettings'));
echo $OUTPUT->box(get_string('course_extrasettings_info', 'local_course_extrasettings'), 'center');
$mform->display();
echo $OUTPUT->footer();
?>
<script>

    $("#fitem_id_certificatedownload").hide();
    $("#fitem_id_certificatedownload1").hide();
    $("#fitem_id_certificatedownload2").hide();
    $("#fitem_id_costforattendance").hide();
    $("#fitem_id_vcostforattendance").hide();
    $("#fitem_id_costforformalcredit").hide();

// Attendance Certificate
    $("#id_certificate1").change(function () {
        if (document.getElementById("id_certificate1").value == 1) {
            $("#fitem_id_certificatedownload1").show();
        } else {
            $("#fitem_id_certificatedownload1").hide();
        }
    });
    $("#id_attendancecompletion").change(function () {
        if (document.getElementById("id_attendancecompletion").value == 1) {
            $("#fitem_id_costforattendance").show();
        } else {
            $("#fitem_id_costforattendance").hide();
        }
    });

// Verified Certificate
    $("#id_certificate2").change(function () {
        if (document.getElementById("id_certificate2").value == 1) {
            $("#fitem_id_certificatedownload2").show();
        } else {
            $("#fitem_id_certificatedownload2").hide();
        }
    });
    $("#id_vattendancecompletion").change(function () {
        if (document.getElementById("id_vattendancecompletion").value == 1) {
            $("#fitem_id_vcostforattendance").show();
        } else {
            $("#fitem_id_vcostforattendance").hide();
        }
    });

// Exam Certificate
    $("#id_certificate").change(function () {
        if (document.getElementById("id_certificate").value == 1) {
            $("#fitem_id_certificatedownload").show();
        } else {
            $("#fitem_id_certificatedownload").hide();
        }
    });
    $("#id_formalcredit").change(function () {
        if (document.getElementById("id_formalcredit").value == 1) {
            $("#fitem_id_costforformalcredit").show();
        } else {
            $("#fitem_id_costforformalcredit").hide();
        }
    });

    // Self Paced option.
    $(document).ready(function () {
        if (document.getElementById("id_selfpaced").value == 1) {
            $("#fitem_id_endselfpacedses").show();
        } else {
            $("#fitem_id_endselfpacedses").hide();
        }
    });
    $("#id_selfpaced").change(function () {
        if (document.getElementById("id_selfpaced").value == 1) {
            $("#fitem_id_endselfpacedses").show();
        } else {
            $("#fitem_id_endselfpacedses").hide();
        }
    });

</script>

<script>
    /* $("#id_institution").on("keyup", function(event){                                           
     var query = $(this).val();
     if(query.length > 1){
     $.post( "autocomplete.php", { mode: "institution", data: query}, function( data ) { 
     searchInstitution = data;
     
     //First search
     if(drewInstitution == false){                                                                                       
     //Create list for results
     $("#id_institution ").after('<ul id="selectorSearch" class="searchul"></ul>');
     //Prevent redrawing/binding of list
     drewInstitution = true;
     //Bind click event to list elements in results
     $("#selectorSearch").on("click", "li", function(){
     $("#id_institution").val($(this).text());                    
     $("#selectorSearch").empty();
     });
     }else{//Clear old results  
     $("#selectorSearch").empty();   
     }
     for(term in searchInstitution){  
     $("#selectorSearch").append("<li class='searchli'>" + searchInstitution[term] + "</li>");
     }                                                                                              
     },"json");
     }else if(drewInstitution){//Handle backspace/delete so results don't remain with less than 3 characters
     $("#selectorSearchResult").empty();
     }
     }); 
     });*/
</script>