<?php
require(dirname(dirname(dirname(__FILE__))).'/config.php');

$courseid = optional_param('general', 0, PARAM_INT);
/* $paymentid = optional_param('payment', 0, PARAM_INT);*/
if ($courseid) {
    $course = $DB->get_record('course', array('id' => $courseid));
    $generaldel = $DB->delete_records('course_extrasettings_general', array('courseid' => $courseid));
    // Added by Shiuli Delete all entries from Course_sequence table where courseid=$courseid.
    $deletesequence = $DB->delete_records('eduopen_special_course_seq', array('courseid' => $courseid));
}
if (isset($generaldel)) {
    redirect(new moodle_url($CFG->wwwroot . '/course/view.php?id='.$courseid));
}

echo $OUTPUT->footer();
